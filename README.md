# Tony vs the World


## ![logo-32.png](https://bitbucket.org/repo/BgoKX69/images/3485908136-logo-32.png) ** [Codebase online documentation](http://daanruiter.net/games/tonyvstheworld/docs/)** ##


### Beschrijving ###

* Dit is het project voor de proeve van bekwaamheid voor de klassen game-development & game-artist van 2017 aan het Mediacollege Amsterdam.
* De game draait in HTML5 en vereist geen plugin-ins of downloads om gespeeld te worden.
* We gebruiken Phaser en een game met Typescript te maken wat naar Javascript wordt vertaald.

### Links ###


* **Game:**[Tony vs the world](tony-vs-the-world.com)
* **Phaser:** [Phaser Homepage](https://phaser.io)
* **Google drive folder:** [Google Drive, Proeve van bekwaamheid](https://drive.google.com/drive/folders/0B22VQ6sxaPlCeDZCWFBEeEdDaXc)
* **Technisch ontwerp:** [Google Doc, Proeve van bekwaamheid Technisch design](https://docs.google.com/document/d/1y90GJ692HV_q60eQqUv40U_fktfuYkuF4VV8BrCsI8Q)
* **IDE document:** [Google Doc, IDE document](https://docs.google.com/document/d/1YijW9Lww6SbGN6eORqfqsYxYZSzXpsuFpCPaVZnmoC8/edit)

### Software & IDE ###

* **Game Engine:** Phaser
* **Code Editor:** Visual Studio
* **Versioning:** GIT, Bitbucket, SourceTree
* **2D Art:** Photoshop, Flash
* **2D Animatie:** Spine, Flash

### Team ###
#### Art ####
* Cerys Hancock, **Lead**
* Bart Willemsen
* Gerben van de Bosch
* Lotte Hageman
* Luuc Veenker

#### Programming ####
* Menno Jongejan, **Lead**
* Koen van der Velden
* Daan Ruiter