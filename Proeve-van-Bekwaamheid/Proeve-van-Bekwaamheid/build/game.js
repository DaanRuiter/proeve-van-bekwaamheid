var ProeveVanBekwaamheid;
(function (ProeveVanBekwaamheid) {
    var Ads;
    (function (Ads) {
        class adManager {
            constructor(game) {
                this._game = game;
                this.checkDate();
                if (ProeveVanBekwaamheid.Object.SharedObject.GetLocalStorageItem('AdDate') == null) {
                    ProeveVanBekwaamheid.Object.SharedObject.SetLocalStorageItem('AdDate', this._date);
                }
                this._date2 = new Date(ProeveVanBekwaamheid.Object.SharedObject.GetLocalStorageItem('AdDate'));
                this.counter = 0;
                this._adsArray = new Array();
                this._adsArray.push('ad1', 'ad2', 'ad3', 'ad4', 'ad5', 'ad6', 'ad7');
            }
            checkIfAdsAllowed() {
                if (this._date2.getFullYear() >= this._currentYear) {
                    if (this._date2.getDay() >= this._currentMonth) {
                        if (this._date2.getDate() == this._currentDay) {
                            return true;
                        }
                    }
                }
                return false;
            }
            checkDate() {
                this._date = new Date();
                this._currentYear = this._date.getFullYear();
                this._currentMonth = this._date.getDay();
                this._currentDay = this._date.getDate();
            }
            renewAddDate() {
                console.log('renew add date');
            }
            giveReward() {
                this.renewAddDate();
                this.renewAddDate();
                ProeveVanBekwaamheid.statics.Inventory.CoinsOwned += 50;
                var menuState = this._game.CurrentState;
                menuState.shopMenu();
                menuState.updateCurrencyDisplay();
                this.openAdd.destroy();
            }
            startAdd() {
                this.createAdd(this._adsArray[Math.floor(Math.random() * this._adsArray.length)]);
                this.counter = 0;
                this._game.time.events.loop(Phaser.Timer.SECOND, this.countDown, this);
            }
            countDown() {
                this.counter++;
                console.log(this.counter);
                if (this.counter >= 5) {
                    this._game.time.events.stop();
                    this.giveReward();
                }
            }
            createAdd(adName) {
                this.openAdd = this._game.CurrentState.add.sprite(0, 0, adName);
                this.openAdd.anchor.x = 0;
                this.openAdd.anchor.y = 0;
                this.openAdd.width = this._game.width;
                this.openAdd.scale.y = this.openAdd.scale.x;
            }
        }
        Ads.adManager = adManager;
    })(Ads = ProeveVanBekwaamheid.Ads || (ProeveVanBekwaamheid.Ads = {}));
})(ProeveVanBekwaamheid || (ProeveVanBekwaamheid = {}));
var ProeveVanBekwaamheid;
(function (ProeveVanBekwaamheid) {
    var Actors;
    (function (Actors) {
        class Background extends ProeveVanBekwaamheid.Object.Entity {
            constructor(game, x, y, key, frame) {
                super(game, x, y, key, frame);
                this._textureKeys = [];
                this._isLooping = false;
                this._randomiseImage = false;
            }
            static get BACKGROUND_HEIGHT() {
                return 1920;
            }
            static get BACKGROUND_WIDTH() {
                return 1080;
            }
            set IsLooping(value) {
                this._isLooping = value;
            }
            get IsLooping() {
                return this._isLooping;
            }
            set RandomizeImage(value) {
                this._randomiseImage = value;
            }
            get RandomizeImage() {
                return this._randomiseImage;
            }
            AddTextureKey(key) {
                this._textureKeys.push(key);
            }
            RemoveTextureKey(key) {
                for (var i = 0; i > this._textureKeys.length; i++) {
                    if (this._textureKeys[i] == key) {
                        this._textureKeys.splice(i, 1);
                        break;
                    }
                }
            }
            ChangeTexture() {
                if (this._randomiseImage && this._textureKeys.length > 0) {
                    var texture = this._textureKeys[this.game.rnd.integerInRange(0, this._textureKeys.length - 1)];
                    this.loadTexture(texture);
                }
            }
            OnStart() { }
            IndexDependencies(graphicsRequests) { }
            OnUpdate() {
                if (this.position.y >= this.game.height) {
                    this.OnBottomReached(this.position.y - this.height);
                }
            }
            OnBottomReached(yOffset) {
                if (this._isLooping) {
                    this.position.y = -1920 + yOffset;
                }
                this.ChangeTexture();
            }
        }
        Actors.Background = Background;
    })(Actors = ProeveVanBekwaamheid.Actors || (ProeveVanBekwaamheid.Actors = {}));
})(ProeveVanBekwaamheid || (ProeveVanBekwaamheid = {}));
var ProeveVanBekwaamheid;
(function (ProeveVanBekwaamheid) {
    var Actors;
    (function (Actors) {
        class EmptyObject extends ProeveVanBekwaamheid.Object.Entity {
            constructor(game, x, y, textureKey) {
                super(game, x, y, textureKey);
            }
            OnStart() {
            }
            IndexDependencies(graphicsRequests) {
            }
            Destroy() {
                super.Destroy();
            }
            OnUpdate() {
            }
        }
        Actors.EmptyObject = EmptyObject;
    })(Actors = ProeveVanBekwaamheid.Actors || (ProeveVanBekwaamheid.Actors = {}));
})(ProeveVanBekwaamheid || (ProeveVanBekwaamheid = {}));
var ProeveVanBekwaamheid;
(function (ProeveVanBekwaamheid) {
    var Actors;
    (function (Actors) {
        class Enemy extends ProeveVanBekwaamheid.Object.PhysicsEntity {
            constructor(game, x, y, spineKey) {
                super(game, x, y);
                this._isInitialized = false;
                this.Initialize(game, x, y, spineKey);
            }
            set isHovering(hover) {
                if (this._isMoving) {
                    this._moveTween.stop();
                }
                this._isMoving = false;
                this._isHovering = true;
            }
            set isMoving(moving) {
                if (this._isHovering) {
                    this._moveTween.stop();
                }
                this._isHovering = false;
                this._isMoving = true;
            }
            set scale(scale) {
                super.scale = scale;
                if (this._spineObject) {
                    this._spineObject.scale = scale;
                }
            }
            get position() {
                if (this._spineObject) {
                    return this._spineObject.position;
                }
                return super.position;
            }
            set position(point) {
                if (this._spineObject) {
                    this._spineObject.position.x = point.x;
                    this._spineObject.position.y = point.y;
                    return;
                }
                super.position = new Phaser.Point(point.x, point.y);
            }
            OnStart() {
            }
            IndexDependencies(graphicsRequests) {
            }
            Destroy() {
                if (this._isDeath)
                    return;
                this._moveTween.stop(false);
                this.DisableCircleCollision(ProeveVanBekwaamheid.Physics.CollisionGroups.ENEMY);
                this._spineObject.destroy();
                this._isDeath = true;
                this._deathSprite = this.game.add.sprite(0, 0, 'enemy-death');
                this._deathSprite.scale.set(0.75, 0.75);
                this.addChild(this._deathSprite);
                this._deathMoveTween = this.game.add.tween(this._deathSprite);
                var yPos = this._deathSprite.y - 400;
                var xPos = this._deathSprite.x + 100;
                this._deathMoveTween.to({ y: yPos, x: xPos, alpha: 0 }, 2000, Phaser.Easing.Linear.None);
                this._deathMoveTween.start();
                this._deathMoveTween.onComplete.add(this.DestroySuper, this);
            }
            OnUpdate() {
                super.OnUpdate();
                if (!this._isInitialized && !this._isDeath)
                    return;
                if (this.y > 0 && !this._startedMoving) {
                    if (this._isHovering) {
                        this.Hover();
                    }
                    else if (this._isMoving) {
                        this.Move();
                    }
                }
                else if (this.y > 0) {
                    this.x += this._movePosition.x;
                    this.y += this._movePosition.y;
                }
                if (this._spineObject) {
                    this._spineObject.x = this.x;
                    this._spineObject.y = this.y;
                }
            }
            OnCollision(other, group) {
            }
            Initialize(game, x, y, spineKey) {
                this.game = game;
                this.name = "Enemy";
                this.SetSpineObject(game, x, y, spineKey);
                this.SetPhysics();
                this._maxHoverY = 5;
                this._moveDelay = 3;
                this._currentMoveDelay = 0;
                this._yDirection = 1;
                this._startedMoving = false;
                this._isDeath = false;
                var x = this.game.rnd.integerInRange(-100, 100);
                var y = this.game.rnd.integerInRange(-100, 100);
                this._moveTarget = new Phaser.Point(x, y);
                this._movePosition = new Phaser.Point(0, 0);
                this._moveTween = this.game.add.tween(this._movePosition);
                this._isInitialized = true;
            }
            DestroySuper() {
                super.Destroy();
            }
            Hover() {
                if (!this._isInitialized)
                    return;
                this._startedMoving = true;
                var duration = 2000;
                var yMove = this._maxHoverY * this._yDirection * (duration / 1000 * this.game.physics.p2.frameRate);
                this._moveTween.to({ y: yMove }, duration, 'Back.easeInOut', true, 0, -1, true);
            }
            Move() {
                if (!this._isInitialized || this.game == null)
                    return;
                this._movePosition.x = 0;
                this._movePosition.y = 0;
                this._startedMoving = true;
                var duration = 2000;
                var maxDistance = 50 * (duration / 1000 * this.game.physics.p2.frameRate);
                var x = this.game.rnd.integerInRange(-maxDistance, maxDistance);
                if (this.x + x > this.game.width) {
                    x -= 100;
                }
                else if (this.x + x < 0) {
                    x += 100;
                }
                console.log(this.x + x);
                this._moveTarget = new Phaser.Point(x, 0);
                this._moveTween.to({ x: this._moveTarget.x }, duration, 'Back.easeInOut', true);
                this._moveTween.onComplete.addOnce(this.Move, this);
            }
            MoveHover() {
                if (!this._isInitialized)
                    return;
                this._movePosition.x = 0;
                this._movePosition.y = 0;
                var duration = 2000;
                var yMove = this.y + this._maxHoverY * this._yDirection * (duration / 1000 * this.game.physics.p2.frameRate);
                this._moveTween.to({ y: yMove }, duration, 'Back.easeInOut', true);
                this._moveTween.onComplete.addOnce(this.Move, this);
            }
            SetSpineObject(game, x, y, spineKey) {
                this._spineObject = ProeveVanBekwaamheid.Spine.SpineFactory.createSpineObject(game, x, y, spineKey);
                this._spineObject.setAnimationByName(0, "animation", true);
                this._spineObject.scale = this.scale;
            }
            SetPhysics() {
                this.EnableCircleCollision(60, ProeveVanBekwaamheid.Physics.CollisionGroups.ENEMY);
            }
        }
        Actors.Enemy = Enemy;
    })(Actors = ProeveVanBekwaamheid.Actors || (ProeveVanBekwaamheid.Actors = {}));
})(ProeveVanBekwaamheid || (ProeveVanBekwaamheid = {}));
var ProeveVanBekwaamheid;
(function (ProeveVanBekwaamheid) {
    var Actors;
    (function (Actors) {
        class Hand extends ProeveVanBekwaamheid.Object.Entity {
            constructor(game, x, y, textureKey) {
                super(game, x, y, textureKey);
                this._moveSpeed = 0.5;
                this._direction = 1;
                this._maxX = 20;
                this.scale.x = 0.5;
                this.scale.y = 0.5;
                this.rotation = 90;
                this._startX = x;
            }
            OnStart() { }
            IndexDependencies(graphicsRequests) { }
            OnUpdate() {
                this.position.x += this._moveSpeed * this._direction;
                if (this.position.x >= this._startX + this._maxX && this._direction == 1
                    || this.position.x <= this._startX - this._maxX && this._direction == -1) {
                    this._direction *= -1;
                }
            }
        }
        Actors.Hand = Hand;
    })(Actors = ProeveVanBekwaamheid.Actors || (ProeveVanBekwaamheid.Actors = {}));
})(ProeveVanBekwaamheid || (ProeveVanBekwaamheid = {}));
var ProeveVanBekwaamheid;
(function (ProeveVanBekwaamheid) {
    var Actors;
    (function (Actors) {
        class Keypoint extends ProeveVanBekwaamheid.Object.PhysicsEntity {
            constructor(game, x, y, key, frame) {
                super(game, x, y, key, frame);
                this.name = "Keypoint";
                this.SetOnClickHandlers();
                this.SetEventHandlers();
                this.CalculateRadius();
                this.pivot.x = this.width / 2;
                this.pivot.y = this.height / 2;
            }
            get OnClickedEvent() {
                return this._onClickedEventHandler;
            }
            OnStart() { }
            OnDestroy() { }
            OnCollision(other, group) { }
            IndexDependencies(requests) { }
            SetOnClickHandlers() {
                this.inputEnabled = true;
                this.events.onInputDown.add(this.OnFingerDown, this);
                this.events.onInputUp.add(this.OnFingerUp, this);
            }
            CalculateRadius() {
                this._collisionRadius = this.width / 2;
            }
            SetEventHandlers() {
                this._onClickedEventHandler = new ProeveVanBekwaamheid.Events.EventHandlerHolder();
            }
            OnFingerDown(sprite, position) {
                var eventHandlers = this._onClickedEventHandler.EventHandlers;
                for (var i = 0; i < eventHandlers.length; i++) {
                    eventHandlers[i].OnKeypointDown(this, position);
                }
            }
            OnFingerUp(sprite, position) {
                var eventHandlers = this._onClickedEventHandler.EventHandlers;
                for (var i = 0; i < eventHandlers.length; i++) {
                    eventHandlers[i].OnKeypointUp(this, position);
                }
            }
        }
        Actors.Keypoint = Keypoint;
    })(Actors = ProeveVanBekwaamheid.Actors || (ProeveVanBekwaamheid.Actors = {}));
})(ProeveVanBekwaamheid || (ProeveVanBekwaamheid = {}));
var ProeveVanBekwaamheid;
(function (ProeveVanBekwaamheid) {
    var Actors;
    (function (Actors) {
        class LaserBeam extends ProeveVanBekwaamheid.Object.PhysicsEntity {
            constructor(game, x, y, texture) {
                super(game, x, y, texture);
                this._activeTimer = game.time.create(false);
                this._disabledTimer = game.time.create(false);
                this._startPosition = new Phaser.Point(0, 0);
                this._endPosition = new Phaser.Point(0, 0);
                this._firstTimeInScreen = false;
                this._addedBeamScaleX = 0;
                this._beamDirectionX = 1;
                this._maxBeamScaleX = 1.5;
                this._minBeamScaleX = -1.5;
                this.alpha = 0.15;
                this.SetPhysics();
                this.name = "LaserBeam";
            }
            set StartPosition(value) {
                this._startPosition = value;
            }
            get StartPosition() {
                return this._startPosition;
            }
            set EndPosition(value) {
                this._endPosition = value;
            }
            get EndPosition() {
                return this._endPosition;
            }
            static get LASERBEAM_ACTIVE_TIME() {
                return 3000;
            }
            static get LASERBEAM_DISABLED_TIME() {
                return 5000;
            }
            Destroy() {
                if (this._alphaTween)
                    this._alphaTween.stop(false);
                if (this._disabledTimer) {
                    this._disabledTimer.stop();
                    this._disabledTimer.destroy();
                }
                if (this._activeTimer) {
                    this._activeTimer.stop();
                    this._activeTimer.destroy();
                }
                this._coils.forEach(function (coil) { coil.Destroy(); });
                super.Destroy();
            }
            OnStart() { }
            IndexDependencies(graphicsRequests) { }
            OnCollision(other, group) { }
            CalculateScaleAndAngle() {
                var angle = Phaser.Math.radToDeg(Phaser.Math.angleBetweenPoints(this._startPosition, this._endPosition)) + 90;
                this._coils = [new Actors.EmptyObject(this.game, this._startPosition.x, this._startPosition.y, 'laser-coil'),
                    new Actors.EmptyObject(this.game, this._endPosition.x, this._endPosition.y, 'laser-coil')];
                for (let i = 0; i < this._coils.length; i++) {
                    this.game.CurrentState.ObjectScroller.ScrollingObjects.push(this._coils[i]);
                    this.game.CurrentState.EntityGroup.add(this._coils[i]);
                    this.game.add.existing(this._coils[i]);
                }
                this.bringToTop();
                let leftCoil = this._coils[0];
                leftCoil.anchor.set(0.5);
                leftCoil.angle = angle;
                leftCoil.x += leftCoil.width / 2;
                this._startPosition.x += leftCoil.width;
                let rightCoil = this._coils[1];
                rightCoil.anchor.set(0.5);
                rightCoil.angle = angle + 180;
                rightCoil.x -= rightCoil.width / 2;
                this._endPosition.x -= rightCoil.width;
                var beamX = (this._endPosition.x + this._startPosition.x) / 2;
                var beamY = (this._endPosition.y + this._startPosition.y) / 2;
                var distance = Phaser.Point.distance(this._startPosition, this._endPosition);
                this.body.angle = angle;
                this.body.x = beamX;
                this.body.y = beamY;
                this.scale.set(3, distance / 20);
                this.CalculateRectangle();
            }
            OnUpdate() {
                if (!this._firstTimeInScreen && this.y > 0) {
                    this.ActivateBeam();
                    this._firstTimeInScreen = true;
                }
                else {
                    this.DrawBeam();
                }
            }
            DrawBeam() {
                this._addedBeamScaleX += 0.025 * this._beamDirectionX;
                if (this._addedBeamScaleX >= this._maxBeamScaleX && this._beamDirectionX == 1 ||
                    this._addedBeamScaleX <= this._minBeamScaleX && this._beamDirectionX == -1) {
                    this._beamDirectionX *= -1;
                }
                this.scale.set(3 + this._addedBeamScaleX, this.scale.y);
            }
            DisableBeam() {
                this.DisableCollision();
                this._disabledTimer.stop();
                this._disabledTimer.removeAll();
                this._disabledTimer.add(LaserBeam.LASERBEAM_DISABLED_TIME, this.ActivateBeam, this);
                this._disabledTimer.start();
            }
            ActivateBeam() {
                this._alphaTween = this.game.add.tween(this);
                this._alphaTween.to({ alpha: 1 }, 2500, Phaser.Easing.Linear.None, true);
                this._alphaTween.onComplete.addOnce(this.BeamActivated, this);
            }
            DeactivateBeam() {
                this._alphaTween = this.game.add.tween(this);
                this._alphaTween.to({ alpha: 0.15 }, 1000, Phaser.Easing.Linear.None, true);
                this._alphaTween.onComplete.addOnce(this.DisableBeam, this);
            }
            BeamActivated() {
                this.EnableCollision();
                this._activeTimer.stop();
                this._activeTimer.removeAll();
                this._activeTimer.add(LaserBeam.LASERBEAM_ACTIVE_TIME, this.DeactivateBeam, this);
                this._activeTimer.start();
            }
            SetPhysics() {
                this.EnableBody(ProeveVanBekwaamheid.Object.PhysicsMode.Kinematic);
            }
            CalculateRectangle() {
                this.body.setRectangle(this.width, this.height, 0, 0, this.rotation);
                this.body.setCollisionGroup(ProeveVanBekwaamheid.Physics.Collision.GetCollisionGroup(ProeveVanBekwaamheid.Physics.CollisionGroups.ENEMY));
                this.body.onBeginContact.addOnce(this.OnBodyCollisonHandler, this);
            }
            EnableCollision() {
                this.body.collides(ProeveVanBekwaamheid.Physics.Collision.GetCollisionGroup(ProeveVanBekwaamheid.Physics.CollisionGroups.PLAYER));
            }
            DisableCollision() {
                this.body.removeCollisionGroup(ProeveVanBekwaamheid.Physics.Collision.GetCollisionGroup(ProeveVanBekwaamheid.Physics.CollisionGroups.PLAYER), true);
            }
            OnBodyCollisonHandler(body1, body2, shape01, shape02, equation) {
                this.game.CurrentState.OnDeath();
            }
        }
        Actors.LaserBeam = LaserBeam;
    })(Actors = ProeveVanBekwaamheid.Actors || (ProeveVanBekwaamheid.Actors = {}));
})(ProeveVanBekwaamheid || (ProeveVanBekwaamheid = {}));
var ProeveVanBekwaamheid;
(function (ProeveVanBekwaamheid) {
    var PickUp;
    (function (PickUp) {
        class Coin extends PickUp.PickUpObject {
            constructor(game, x, y, key, frame) {
                super(game, x, y, key, frame);
                this._pullSpeed = 200;
                this.name = "coin";
            }
            static get CIRCLE_RADIUS() {
                return 50;
            }
            GetDuration() {
                return -1;
            }
            CreateObject() {
                this.scale.setTo(0.5);
            }
            customFunction() {
                if (!this.game) {
                    return;
                }
                this._pickedUp = true;
                var gameState = this.game.CurrentState;
                this._player = gameState.Player;
            }
            OnPowerupEnded() { }
            SetCircleRadius(radius) {
                this._circleRadius = radius;
            }
            OnUpdate() {
                super.OnUpdate();
                if (this._pickedUp && this._player) {
                    var distanceX = (this._player.x - this.x);
                    var distanceY = (this._player.y - this.y);
                    this.x += distanceX / this.game.width * this._pullSpeed;
                    this.y += distanceY / this.game.height * this._pullSpeed;
                    if (Math.abs(distanceX) < 50 && Math.abs(distanceY) < 50) {
                        ProeveVanBekwaamheid.statics.Inventory.CoinsOwned++;
                        var coins = ProeveVanBekwaamheid.statics.Inventory.CoinsOwned;
                        var gameState = this.game.CurrentState;
                        gameState.UpdateNumericDisplay('display_currency', coins);
                        gameState.collectCoinAudio();
                        this.Destroy();
                    }
                }
            }
        }
        PickUp.Coin = Coin;
    })(PickUp = ProeveVanBekwaamheid.PickUp || (ProeveVanBekwaamheid.PickUp = {}));
})(ProeveVanBekwaamheid || (ProeveVanBekwaamheid = {}));
var ProeveVanBekwaamheid;
(function (ProeveVanBekwaamheid) {
    var PickUp;
    (function (PickUp) {
        class GunPowerUp extends PickUp.PickUpObject {
            static get GUN_DURATION() {
                return 22;
            }
            static get GUN_SHOOT_COOLDOWN() {
                return Phaser.Timer.SECOND * 1.35;
            }
            static get GUN_AIM_DURATION() {
                return Phaser.Timer.SECOND * 0.65;
            }
            static get GUN_RANGE() {
                return 450;
            }
            GetDuration() {
                var powerup = ProeveVanBekwaamheid.statics.Inventory.FindUpgrade(ProeveVanBekwaamheid.statics.PowerupUpgrade.POWERUP_GUN_KEY);
                var modifier = powerup.CurrentModifier;
                var gunDuration = GunPowerUp.GUN_DURATION;
                if (modifier != 0) {
                    gunDuration *= modifier;
                }
                return gunDuration;
            }
            OnStart() {
                super.OnStart();
                this.scale = new Phaser.Point(0.2, 0.2);
                this._gunPickUpAudio = this.game.add.audio('gunPickUp');
                this._gunShootAudio = this.game.add.audio('GunShoot');
            }
            OnUpdate() {
                super.OnUpdate();
                if (this._currentTarget) {
                    this.AimAtTarget();
                }
                else {
                    if (this._isGunOnCooldown || !this._pickedUp) {
                        return;
                    }
                    var enemies = this._gameState.Enemies;
                    for (let i = 0; i < enemies.length; i++) {
                        let distance = this._gameState.Player.position.distance(enemies[i].position);
                        if (distance <= GunPowerUp.GUN_RANGE) {
                            this._currentTarget = enemies[i];
                            this._aimTimer = this.game.time.events.add(GunPowerUp.GUN_AIM_DURATION, function () {
                                this.ShootAtTarget();
                            }, this);
                            this._aimTimer.timer.start();
                            return;
                        }
                    }
                }
            }
            customFunction() {
                this._gameState = this.game.CurrentState;
                this._gunPickUpAudio.play();
                if (this._gameState) {
                    this.visible = false;
                    this._gameState.Player.ToggleGuns(true);
                }
            }
            ShootAtTarget() {
                this._gunShootAudio.play();
                this._isGunOnCooldown = true;
                if (this._currentTarget) {
                    this._gameState.Enemies.splice(this._gameState.Enemies.indexOf(this._currentTarget), 1);
                    this._currentTarget.Destroy();
                    this._currentTarget = null;
                    for (let i = 0; i < this._gameState.Player.Guns.length; i++) {
                        this._gameState.Player.Guns[i].MuzzleFlare.visible = true;
                    }
                    let muzzleFlareTimer = this.game.time.create();
                    muzzleFlareTimer.add(85, this.DisableMuzzleFlares, this);
                    muzzleFlareTimer.autoDestroy = true;
                    muzzleFlareTimer.start();
                }
                this.game.time.events.add(GunPowerUp.GUN_SHOOT_COOLDOWN, function () {
                    this._isGunOnCooldown = false;
                }, this);
            }
            AimAtTarget() {
                var guns = this._gameState.Player.Guns;
                let distance = this._gameState.Player.position.distance(this._currentTarget.position);
                if (distance > GunPowerUp.GUN_RANGE) {
                    if (this._aimTimer) {
                        this._aimTimer.timer.stop();
                        this._aimTimer.timer.destroy();
                    }
                    this._currentTarget = null;
                    return;
                }
                for (let i = 0; i < guns.length; i++) {
                    var angle = Phaser.Math.radToDeg(Phaser.Math.angleBetweenPoints(guns[i].Sprite.position, this._currentTarget.position)) + 90;
                    guns[i].Sprite.angle = angle;
                }
            }
            DisableMuzzleFlares(timer) {
                for (let i = 0; i < this._gameState.Player.Guns.length; i++) {
                    this._gameState.Player.Guns[i].MuzzleFlare.visible = false;
                }
            }
            OnPowerupEnded() {
                if (this._gameState) {
                    this._gameState.Player.ToggleGuns(false);
                }
                if (this._aimTimer)
                    this._aimTimer.timer.stop();
                this.Destroy();
            }
        }
        PickUp.GunPowerUp = GunPowerUp;
    })(PickUp = ProeveVanBekwaamheid.PickUp || (ProeveVanBekwaamheid.PickUp = {}));
})(ProeveVanBekwaamheid || (ProeveVanBekwaamheid = {}));
var ProeveVanBekwaamheid;
(function (ProeveVanBekwaamheid) {
    var PickUp;
    (function (PickUp) {
        class JetpackPowerUp extends PickUp.PickUpObject {
            constructor(...args) {
                super(...args);
                this._jetpackAudio = this.game.add.sound('jetpackSound', 1, true);
            }
            static get BOOST_STRENGTH() {
                return 550;
            }
            static get BOOST_DURATION() {
                return 7;
            }
            static get COLLISION_ENABLE_DELAY() {
                return Phaser.Timer.SECOND * 1.5;
            }
            static get EMIT_Y_OFFSET() {
                return -5;
            }
            GetDuration() {
                var powerup = ProeveVanBekwaamheid.statics.Inventory.FindUpgrade(ProeveVanBekwaamheid.statics.PowerupUpgrade.POWERUP_JETPACK_KEY);
                var modifier = powerup.CurrentModifier;
                var jetpackDuration = JetpackPowerUp.BOOST_DURATION;
                if (modifier != 0) {
                    jetpackDuration *= modifier;
                }
                return jetpackDuration;
            }
            customFunction() {
                this._gameState = this.game.CurrentState;
                this._jetpackAudio.play();
                if (this._gameState) {
                    this.visible = false;
                    this._jetpackParticles = this.game.add.emitter(0, 0, 150);
                    this._jetpackParticles.makeParticles('particles-jetack');
                    this._jetpackParticles.minParticleScale = 2;
                    this._jetpackParticles.maxParticleScale = 6;
                    this._jetpackParticles.gravity = this.game.physics.p2.gravity.y;
                    this._gameState.Player.bringToTop();
                    this._gameState.Player.Invulnerable = true;
                    this._gameState.Player.ToggleRope(false);
                    this._gameState.Player.inputEnabled = true;
                    this._gameState.Player.input.enableDrag(true);
                    this._gameState.Player.events.onInputDown.add(this.OnMouseDown, this);
                    this._gameState.Player.events.onInputUp.add(this.OnMouseUp, this);
                }
                else {
                    this.Destroy();
                }
            }
            OnUpdate() {
                super.OnUpdate();
                if (!this._pickedUp || !this.alive) {
                    return;
                }
                this._gameState.Player.body.thrust(JetpackPowerUp.BOOST_STRENGTH);
                var px = this._gameState.Player.body.velocity.x * -1;
                var py = this._gameState.Player.body.velocity.y * -1;
                this._jetpackParticles.minParticleSpeed.set(px, py);
                this._jetpackParticles.maxParticleSpeed.set(px, py);
                this._jetpackParticles.emitX = this._gameState.Player.Torso.x;
                this._jetpackParticles.emitY = this._gameState.Player.Torso.y + JetpackPowerUp.EMIT_Y_OFFSET;
                this._jetpackParticles.alpha = 0.5;
                this._jetpackParticles.flow(350, 750, 2, JetpackPowerUp.BOOST_DURATION / 750 * 2);
                if (!this._isMouseDown) {
                    return;
                }
                this._gameState.Player.body.moveLeft(this._gameState.Player.body.x - this.game.input.activePointer.x);
            }
            OnPowerupEnded() {
                this._jetpackAudio.fadeOut(500);
                this._jetpackParticles.emitX = 99999;
                this._jetpackParticles.destroy();
                this.events.onInputDown.remove(this.OnMouseDown, this);
                this.events.onInputUp.remove(this.OnMouseUp, this);
                this._gameState.Player.ToggleRope(true);
                this.kill();
                this.game.time.events.add(JetpackPowerUp.COLLISION_ENABLE_DELAY, function () {
                    this._gameState.Player.Invulnerable = false;
                    this.Destroy();
                }, this);
            }
            OnMouseDown() {
                this._isMouseDown = true;
            }
            OnMouseUp() {
                this._isMouseDown = false;
            }
        }
        PickUp.JetpackPowerUp = JetpackPowerUp;
    })(PickUp = ProeveVanBekwaamheid.PickUp || (ProeveVanBekwaamheid.PickUp = {}));
})(ProeveVanBekwaamheid || (ProeveVanBekwaamheid = {}));
var ProeveVanBekwaamheid;
(function (ProeveVanBekwaamheid) {
    var PickUp;
    (function (PickUp) {
        class MagnetPowerUp extends PickUp.PickUpObject {
            static get MAGNET_DURATION() {
                return 15;
            }
            static get MAGNET_PULL_STRENGTH() {
                return 350;
            }
            GetDuration() {
                var powerup = ProeveVanBekwaamheid.statics.Inventory.FindUpgrade(ProeveVanBekwaamheid.statics.PowerupUpgrade.POWERUP_MAGNET_KEY);
                var modifier = powerup.CurrentModifier;
                var magnetDuration = MagnetPowerUp.MAGNET_DURATION;
                if (modifier != 0) {
                    magnetDuration *= modifier;
                }
                return magnetDuration;
            }
            OnStart() {
                super.OnStart();
            }
            OnUpdate() {
                if (!this._pickedUp) {
                    return;
                }
                super.OnUpdate();
                this._magnetParticles.emitX = this._gameState.Player.x;
                this._magnetParticles.emitY = this._gameState.Player.y;
                for (var i = 0; i < this._magnetParticles.children.length; i++) {
                    this._magnetParticles.children[i].x = this._gameState.Player.x;
                    this._magnetParticles.children[i].y = this._gameState.Player.y;
                }
            }
            customFunction() {
                this.visible = false;
                this._pickedUp = true;
                this._gameState = this.game.CurrentState;
                if (this._gameState) {
                    this._gameState.Player.MagnetEnabled = true;
                    for (var i = 0; i < this._gameState.Coins.length; i++) {
                        this._gameState.Coins[i].SetCircleRadius(MagnetPowerUp.MAGNET_PULL_STRENGTH);
                    }
                    this._magnetParticles = this.game.add.emitter(0, 0, 10);
                    this._magnetParticles.makeParticles('particles-magnet');
                    this._magnetParticles.minParticleScale = 0;
                    this._magnetParticles.maxParticleScale = 4;
                    this._magnetParticles.gravity = 0;
                    this._magnetParticles.minParticleSpeed.set(0, 0);
                    this._magnetParticles.maxParticleSpeed.set(0, 0);
                    this._magnetParticles.setAlpha(1, 0, 2500);
                    this._magnetParticles.setScale(3, 0, 3, 0, 3000);
                    this._magnetParticles.flow(3000, 500, 1, this.GetDuration() / 500);
                }
            }
            OnPowerupEnded() {
                this._gameState.Player.MagnetEnabled = false;
                for (var i = 0; i < this._gameState.Coins.length; i++) {
                    this._gameState.Coins[i].SetCircleRadius(PickUp.Coin.CIRCLE_RADIUS);
                }
                this.Destroy();
            }
        }
        PickUp.MagnetPowerUp = MagnetPowerUp;
    })(PickUp = ProeveVanBekwaamheid.PickUp || (ProeveVanBekwaamheid.PickUp = {}));
})(ProeveVanBekwaamheid || (ProeveVanBekwaamheid = {}));
var ProeveVanBekwaamheid;
(function (ProeveVanBekwaamheid) {
    var PickUp;
    (function (PickUp) {
        class PickUpObject extends ProeveVanBekwaamheid.Object.PhysicsEntity {
            constructor(game, x, y, key, frame) {
                super(game, x, y, key, frame);
                this._destroyTimestamp = -1;
                this.AddTag('pick-up');
                this.EnableCircleCollision(50, ProeveVanBekwaamheid.Physics.CollisionGroups.PICKUP);
                this.CreateObject();
            }
            IndexDependencies(graphicsRequests) { }
            OnStart() { }
            OnUpdate() {
                super.OnUpdate();
                if (this._destroyTimestamp > 0 && this.game.time.totalElapsedSeconds() >= this._destroyTimestamp) {
                    this.OnPowerupEnded();
                    this._destroyTimestamp = -1;
                }
            }
            OnCollision(other) {
                this.DisableCircleCollision(ProeveVanBekwaamheid.Physics.CollisionGroups.PICKUP);
                if (!this._pickedUp) {
                    var duration = this.GetDuration();
                    if (duration > 0)
                        this._destroyTimestamp = this.game.time.totalElapsedSeconds() + this.GetDuration();
                    this._pickedUp = true;
                    this.visible = true;
                    this.customFunction();
                }
            }
            customFunction() {
                this.kill();
                if (this._pickedUp) {
                    this.Destroy();
                }
            }
            CreateObject() { }
        }
        PickUp.PickUpObject = PickUpObject;
    })(PickUp = ProeveVanBekwaamheid.PickUp || (ProeveVanBekwaamheid.PickUp = {}));
})(ProeveVanBekwaamheid || (ProeveVanBekwaamheid = {}));
var ProeveVanBekwaamheid;
(function (ProeveVanBekwaamheid) {
    var Actors;
    (function (Actors) {
        class Platform extends ProeveVanBekwaamheid.Object.PhysicsEntity {
            constructor(game, x, y, texture) {
                super(game, x, y, texture);
                this.scale.setTo(2, 2);
                this.EnableBody(ProeveVanBekwaamheid.Object.PhysicsMode.Static);
                this.body.setRectangle(this.width, this.height - 20);
                this.body.setCollisionGroup(ProeveVanBekwaamheid.Physics.Collision.GetCollisionGroup(ProeveVanBekwaamheid.Physics.CollisionGroups.PLATFORM));
                this.body.collides([ProeveVanBekwaamheid.Physics.Collision.GetCollisionGroup(ProeveVanBekwaamheid.Physics.CollisionGroups.PLAYER)]);
            }
            OnDestroy() { }
            OnStart() { }
            IndexDependencies(graphicsRequests) { }
            Enable() {
                this.visible = true;
            }
            Disable() {
                this.visible = false;
            }
            OnCollision(other, group) {
            }
        }
        Actors.Platform = Platform;
    })(Actors = ProeveVanBekwaamheid.Actors || (ProeveVanBekwaamheid.Actors = {}));
})(ProeveVanBekwaamheid || (ProeveVanBekwaamheid = {}));
var ProeveVanBekwaamheid;
(function (ProeveVanBekwaamheid) {
    var PickUp;
    (function (PickUp) {
        class PlatformPowerUp extends PickUp.PickUpObject {
            static get PLATFORM_DURATION() {
                return 5;
            }
            GetDuration() {
                var powerup = ProeveVanBekwaamheid.statics.Inventory.FindUpgrade(ProeveVanBekwaamheid.statics.PowerupUpgrade.POWERUP_PLATFORM_KEY);
                var modifier = powerup.CurrentModifier;
                var platformDuration = PlatformPowerUp.PLATFORM_DURATION;
                if (modifier != 0) {
                    platformDuration *= modifier;
                }
                return platformDuration;
            }
            OnStart() {
                super.OnStart();
            }
            customFunction() {
                this._gameState = this.game.CurrentState;
                this.visible = false;
                this._pickedUp = true;
                this.EnablePlatform();
            }
            OnPowerupEnded() {
                this.Destroy();
                this._platform.Destroy();
            }
            EnablePlatform() {
                this._platform = this._gameState.InstantiateEntity(ProeveVanBekwaamheid.Actors.Platform, this.game.width / 2, this.game.height - 20, "Platform");
            }
        }
        PickUp.PlatformPowerUp = PlatformPowerUp;
    })(PickUp = ProeveVanBekwaamheid.PickUp || (ProeveVanBekwaamheid.PickUp = {}));
})(ProeveVanBekwaamheid || (ProeveVanBekwaamheid = {}));
var ProeveVanBekwaamheid;
(function (ProeveVanBekwaamheid) {
    var PickUp;
    (function (PickUp) {
        class ShieldPowerUp extends PickUp.PickUpObject {
            static get SHIELD_DURATION() {
                return 4.5;
            }
            static get SHIELD_USES() {
                return 5;
            }
            GetDuration() {
                var powerup = ProeveVanBekwaamheid.statics.Inventory.FindUpgrade(ProeveVanBekwaamheid.statics.PowerupUpgrade.POWERUP_SHIELD_KEY);
                var modifier = powerup.CurrentModifier;
                var shieldDuration = ShieldPowerUp.SHIELD_DURATION;
                if (modifier != 0) {
                    shieldDuration *= modifier;
                }
                return shieldDuration;
            }
            OnStart() {
                super.OnStart();
                this._shieldPickUp = this.game.sound.add('shieldPickUp');
            }
            OnUpdate() {
                super.OnUpdate();
            }
            customFunction() {
                this._shieldPickUp.play();
                this._gameState = this.game.CurrentState;
                if (this._gameState) {
                    this._pickedUp = true;
                    this.visible = false;
                    this._gameState.Player.EnableShield(ShieldPowerUp.SHIELD_USES);
                    var powerup = ProeveVanBekwaamheid.statics.Inventory.FindUpgrade(ProeveVanBekwaamheid.statics.PowerupUpgrade.POWERUP_SHIELD_KEY);
                    var modifier = powerup.CurrentModifier;
                }
            }
            OnPowerupEnded() {
                this._gameState.Player.DisableShield();
                this.Destroy();
            }
        }
        PickUp.ShieldPowerUp = ShieldPowerUp;
    })(PickUp = ProeveVanBekwaamheid.PickUp || (ProeveVanBekwaamheid.PickUp = {}));
})(ProeveVanBekwaamheid || (ProeveVanBekwaamheid = {}));
var ProeveVanBekwaamheid;
(function (ProeveVanBekwaamheid) {
    var PickUp;
    (function (PickUp) {
        class PlatformObject extends ProeveVanBekwaamheid.Object.PhysicsEntity {
            constructor(game, x, y, key, frame) {
                super(game, x, y, key, frame);
                this.CreateObject();
            }
            CreateObject() {
                this.scale.setTo(0.5);
            }
            IndexDependencies(graphicsRequests) {
            }
            OnStart() {
            }
            OnUpdate() {
                super.OnUpdate();
            }
            OnCollision(other) {
            }
            customFunction() {
                console.log('PowerupUsed');
                this.kill();
            }
        }
        PickUp.PlatformObject = PlatformObject;
    })(PickUp = ProeveVanBekwaamheid.PickUp || (ProeveVanBekwaamheid.PickUp = {}));
})(ProeveVanBekwaamheid || (ProeveVanBekwaamheid = {}));
var ProeveVanBekwaamheid;
(function (ProeveVanBekwaamheid) {
    var Actors;
    (function (Actors) {
        class PlayerShield extends ProeveVanBekwaamheid.Object.PhysicsEntity {
            constructor(game, x, y, texture) {
                super(game, x, y, texture);
                this._uses = 0;
                this.visible = false;
            }
            static get VERTICAL_OFFSET() {
                return -100;
            }
            static get HORIZONTAL_OFFSET() {
                return -100;
            }
            OnDestroy() { }
            OnStart() { }
            IndexDependencies(graphicsRequests) { }
            Enable(uses) {
                this._uses = uses;
                this.visible = true;
                this.EnableCircleCollision(50, ProeveVanBekwaamheid.Physics.CollisionGroups.PLAYER);
            }
            Disable() {
                this.visible = false;
                this.DisableCircleCollision(ProeveVanBekwaamheid.Physics.CollisionGroups.PLAYER);
            }
            OnCollision(other, group) {
                if (group == ProeveVanBekwaamheid.Physics.CollisionGroups.ENEMY) {
                    var index = this.game.CurrentState.Enemies.indexOf(other);
                    this.game.CurrentState.Enemies.splice(index, 1);
                    index = this.game.CurrentState.ObjectScroller.ScrollingObjects.indexOf(other);
                    this.game.CurrentState.ObjectScroller.ScrollingObjects.splice(index, 1);
                    other.Destroy();
                    this._uses--;
                    if (this._uses <= 0) {
                        this.Disable();
                    }
                }
            }
        }
        Actors.PlayerShield = PlayerShield;
        class PlayerGun {
            constructor(parent, sprite, muzzleFlare, xOffset = 0) {
                this.Parent = parent;
                this.Sprite = sprite;
                this.MuzzleFlare = muzzleFlare;
                this.XOffset = xOffset;
            }
        }
        Actors.PlayerGun = PlayerGun;
        class Player extends ProeveVanBekwaamheid.Object.PhysicsEntity {
            constructor(game, x, y, key, frame) {
                super(game, x, y, key, frame);
                this.Invulnerable = false;
                this._scale = 1.5;
                this.name = "Player";
                this.AddTag("Player");
            }
            static get HITBOX_RADIUS() {
                return 45;
            }
            static get ON_ENEMY_HIT_ROPE_COOLDOWN() {
                return Phaser.Timer.SECOND * 1.25;
            }
            get OnDeathEvent() {
                return this._onDeathEventHandler;
            }
            get VerticalClampOffset() {
                return this._verticalClampOffset;
            }
            get IsOnBorder() {
                return this._isOnBorder;
            }
            get MagnetEnabled() {
                return this._magnetEnabled;
            }
            set MagnetEnabled(value) {
                this._magnetEnabled = value;
            }
            get RagdollBodies() {
                return this._ragdollBodies;
            }
            get Guns() {
                return this._guns;
            }
            get Torso() {
                return this._torso;
            }
            get CurrentKeyPoint() {
                if (this._currentKeypoint) {
                    return this._currentKeypoint;
                }
                return null;
            }
            get RenderGroup() {
                return this._renderGroup;
            }
            OnDestroy() {
                this.DisableCircleCollision(ProeveVanBekwaamheid.Physics.CollisionGroups.ENEMY);
            }
            initialize(position) {
                this._onDeathEventHandler = new ProeveVanBekwaamheid.Events.EventHandlerHolder();
                this.position = position;
                this.scale = new Phaser.Point(1, 1);
                this._renderGroup = this.game.add.group(this);
                this.MaxVelocity = 70;
                this._ropeStrength = 4.25;
                this._ropeLength = 1;
                this._verticalClampOffset = this.game.height * 0.5;
                this._isRopeEnabled = true;
                this.SetPhysics();
                this._beam = this.game.add.sprite(-10000, -10000, 'beam');
                this._beam.visible = false;
                this._beamDirectionX = 1;
                this._addedBeamScaleX = 0;
                this._maxBeamScaleX = 0.5;
                this._minBeamScaleX = -0.5;
                this.CreateRagdoll();
                var fakeSprite = this.game.add.sprite(0, 0);
                this.game.physics.p2.enableBody(fakeSprite, false);
                this._currentRopeBody = fakeSprite.body;
                this._currentRopeBody.kinematic = true;
                this.EnableCircleCollision(Player.HITBOX_RADIUS, ProeveVanBekwaamheid.Physics.CollisionGroups.PLAYER);
                this._line = this.game.add.sprite(0, 0, this._ropeBitmapData);
                this._shield = this.game.CurrentState.InstantiateEntity(PlayerShield, this.x + PlayerShield.HORIZONTAL_OFFSET, this.y + PlayerShield.VERTICAL_OFFSET, "Shield");
                this._stunAnimation = this.game.add.spine(this.x, this.y, 'Player-Stuf-Effect');
                this._stunAnimation.setAnimationByName(0, "Stun", true);
                this._stunAnimation.scale.set(1.25);
                this._stunAnimation.visible = false;
                this._initialized = true;
            }
            OnKeypointDown(keypoint, position) {
                this.game.CurrentState.ArmLaunchAudio();
                if (!this._isRopeEnabled) {
                    return;
                }
                if (!this.body.dynamic) {
                    this.body.dynamic = true;
                }
                if (this.game.CurrentState.IsTutorial) {
                    var texts = this.game.CurrentState.TutorialTexts;
                    for (var i = texts.length - 1; i >= 0; i--) {
                        texts[i].Text.text = "Release to fling!";
                    }
                    var hands = this.game.CurrentState.Hands;
                    for (var i = hands.length - 1; i >= 0; i--) {
                        hands[i].Destroy();
                        hands.splice(i, 1);
                    }
                }
                if (!this._ropeAnchored) {
                    this.CreateRope(keypoint, position);
                    return;
                }
                this.game.physics.p2.removeSpring(this._ropeSpring);
                this._ropeSpring = this.game.physics.p2.createSpring(this._currentRopeBody, this, this._ropeLength, this._ropeStrength, 10, [-position.x, -position.y]);
                this._ropeAnchorX = position.x - this._currentRopeBody.x;
                this._ropeAnchorY = position.y - this._currentRopeBody.y;
                this._ropeAnchored = true;
            }
            OnKeypointUp(keypoint, position) {
                if (this.game.CurrentState.IsTutorial) {
                    this.game.CurrentState.IsTutorial = false;
                }
                this._beam.visible = false;
                this.DestroyRope();
            }
            RegisterKeypoint(keypoint) {
                keypoint.OnClickedEvent.AddEventListener(this);
            }
            RemoveKeypoint(keypoint) {
                keypoint.OnClickedEvent.RemoveEventListener(this);
            }
            OnStart() { }
            IndexDependencies(graphicsRequests) {
                let selectedSkinFolder = ProeveVanBekwaamheid.statics.Inventory.SelectedSkin.SpriteFolderName;
                if (Player._currentLoadedSkinKey != 'none' && Player._currentLoadedSkinKey != ProeveVanBekwaamheid.statics.Inventory.SelectedSkin.SkinKey) {
                    this.game.cache.removeImage('tony-torso');
                    this.game.cache.removeImage('tony-belly-1');
                    this.game.cache.removeImage('tony-belly-2');
                    this.game.cache.removeImage('tony-belly-3');
                    this.game.cache.removeImage('tony-foot');
                    this.game.cache.removeImage('tony-left-leg-piece');
                    this.game.cache.removeImage('tony-right-leg-piece');
                    this.game.cache.removeImage('tony-left-arm-piece');
                    this.game.cache.removeImage('tony-right-arm-piece');
                    this.game.cache.removeImage('tony-left-hand');
                    this.game.cache.removeImage('tony-right-hand');
                }
                graphicsRequests.AddSpriteRequest('tony-gun', './assets/sprites/player/guns.png');
                graphicsRequests.AddSpriteRequest('tony-torso', './assets/sprites/player/' + selectedSkinFolder + '/Body-1.png', true);
                graphicsRequests.AddSpriteRequest('tony-belly-1', './assets/sprites/player/' + selectedSkinFolder + '/Body-2.png', true);
                graphicsRequests.AddSpriteRequest('tony-belly-2', './assets/sprites/player/' + selectedSkinFolder + '/Body-3.png', true);
                graphicsRequests.AddSpriteRequest('tony-belly-3', './assets/sprites/player/' + selectedSkinFolder + '/Body-4.png', true);
                graphicsRequests.AddSpriteRequest('tony-foot', './assets/sprites/player/' + selectedSkinFolder + '/Foot.png', true);
                graphicsRequests.AddSpriteRequest('beam', './assets/sprites/player/Beam.png');
                graphicsRequests.AddSpriteRequest('tony-left-leg-piece', './assets/sprites/player/' + selectedSkinFolder + '/Leg-joint-left.png', true);
                graphicsRequests.AddSpriteRequest('tony-right-leg-piece', './assets/sprites/player/' + selectedSkinFolder + '/Leg-joint-right.png', true);
                graphicsRequests.AddSpriteRequest('tony-left-arm-piece', './assets/sprites/player/' + selectedSkinFolder + '/Arm-joint-left.png', true);
                graphicsRequests.AddSpriteRequest('tony-right-arm-piece', './assets/sprites/player/' + selectedSkinFolder + '/Arm-joint-right.png', true);
                graphicsRequests.AddSpriteRequest('tony-left-hand', './assets/sprites/player/' + selectedSkinFolder + '/Hand-Left.png', true);
                graphicsRequests.AddSpriteRequest('tony-right-hand', './assets/sprites/player/' + selectedSkinFolder + '/Hand-Right.png', true);
                Player._currentLoadedSkinKey = ProeveVanBekwaamheid.statics.Inventory.SelectedSkin.SkinKey;
            }
            OnUpdate() {
                super.OnUpdate();
                if (!this._initialized || !this.body)
                    return;
                this.DrawBeam(this._rightHandBody, this._currentRopeBody);
                this.ClampPlayer();
                this.UpdateGunTransforms();
                this.UpdateShieldTransform();
                this._stunAnimation.position.set(this._torso.x, this._torso.y + this._stunAnimation.height / 2);
            }
            EnableShield(uses) {
                this._shield.Enable(uses);
            }
            DisableShield() {
                this._shield.Disable();
            }
            OnCollision(other, group) {
                if (this.Invulnerable) {
                    return;
                }
                if (group == ProeveVanBekwaamheid.Physics.CollisionGroups.ENEMY) {
                    this.game.CurrentState.playerHitEnemyAudio();
                    other.Destroy();
                    this.game.CurrentState.Enemies.splice(this.game.CurrentState.Enemies.indexOf(other), 1);
                    this.ToggleRope(false);
                    this._stunAnimation.visible = true;
                    this.game.time.events.add(Player.ON_ENEMY_HIT_ROPE_COOLDOWN, function () {
                        this.ToggleRope(true);
                        this._stunAnimation.visible = false;
                    }, this);
                    return;
                }
                if (this.y >= other.y - other.height * 0.25) {
                    this.body.velocity.x *= -0.5;
                    this.body.velocity.y *= -0.5;
                }
                else {
                    this.body.velocity.y = -(this.body.velocity.y / 2);
                    this.body.velocity.x *= -0.5;
                    if (this.body.velocity.y > -100) {
                        this.body.velocity.y = -100;
                    }
                }
            }
            OnBodyCollisonHandler(body1, body2) {
                this.game.CurrentState.OnDeath();
            }
            ToggleRope(newState) {
                if (newState) {
                    this._isRopeEnabled = true;
                }
                else {
                    this._beam.visible = false;
                    this.DestroyRope();
                    this._isRopeEnabled = false;
                }
            }
            ToggleGuns(newState) {
                for (let i = 0; i < this._guns.length; i++) {
                    this._guns[i].Sprite.visible = newState;
                    this._guns[i].MuzzleFlare.visible = false;
                }
            }
            CreateRope(block, position) {
                this._currentKeypoint = block;
                this._currentRopeBody.x = position.x;
                this._currentRopeBody.y = position.y;
                this._ropeAnchorX = (position.x - this._currentKeypoint.x);
                this._ropeAnchorY = (position.y - this._currentKeypoint.y);
                if (this._ropeSpring) {
                    this.game.physics.p2.removeSpring(this._ropeSpring);
                }
                this._ropeSpring = this.game.physics.p2.createSpring(this._currentRopeBody, this._rightHandBody, this._ropeLength, this._ropeStrength, 3, [-(position.x), -(position.y)]);
                this._ropeAnchored = true;
                this.DrawBeam(this._rightHandBody, this._currentRopeBody);
                this._beam.visible = true;
            }
            DrawBeam(bodyA, bodyB) {
                if (!this._ropeAnchored)
                    return;
                this._currentRopeBody.x = this._currentKeypoint.x + this._ropeAnchorX;
                this._currentRopeBody.y = this._currentKeypoint.y + this._ropeAnchorY;
                this._addedBeamScaleX += 0.025 * this._beamDirectionX;
                if (this._addedBeamScaleX >= this._maxBeamScaleX && this._beamDirectionX == 1 ||
                    this._addedBeamScaleX <= this._minBeamScaleX && this._beamDirectionX == -1) {
                    this._beamDirectionX *= -1;
                }
                var beamX = bodyB.x;
                var beamY = bodyB.y;
                var distance = Phaser.Point.distance(bodyA, bodyB);
                var angle = Phaser.Math.radToDeg(Phaser.Math.angleBetweenPoints(new Phaser.Point(bodyA.x, bodyA.y), new Phaser.Point(bodyB.x, bodyB.y))) + 90;
                this._beam.x = beamX;
                this._beam.y = beamY;
                this._beam.scale.set(1 + this._addedBeamScaleX, distance / 20);
                this._beam.angle = angle;
            }
            ClampPlayer() {
                if (this.body.y < this._verticalClampOffset) {
                    this.body.y = this._verticalClampOffset;
                    this._isOnBorder = true;
                }
                else if (this.body.y > this.game.height - this.height) {
                    this.body.y = this.game.height - this.height;
                    this._isOnBorder = true;
                }
                else {
                    this._isOnBorder = false;
                }
                if (this.body.x < 0 || this.body.x > this.game.width) {
                    this.body.x += -(this.body.velocity.x / 60);
                    this.body.velocity.x = -(this.body.velocity.x * 2);
                }
            }
            UpdateGunTransforms() {
                for (let i = 0; i < this._guns.length; i++) {
                    var gun = this._guns[i];
                    gun.Sprite.rotation = gun.Parent.rotation;
                    gun.Sprite.x = gun.Parent.sprite.x + gun.XOffset;
                    gun.Sprite.y = gun.Parent.sprite.y;
                }
            }
            UpdateShieldTransform() {
                this._shield.x = this.x + PlayerShield.HORIZONTAL_OFFSET;
                this._shield.y = this.y + PlayerShield.VERTICAL_OFFSET;
            }
            SetPhysics() {
                this.EnableBody(ProeveVanBekwaamheid.Object.PhysicsMode.Static);
                this.body.mass = 100;
                this.body.clearShapes();
                this.body.setRectangleFromSprite(this);
                this.body.setCollisionGroup(ProeveVanBekwaamheid.Physics.Collision.GetCollisionGroup(ProeveVanBekwaamheid.Physics.CollisionGroups.PLAYER));
            }
            DestroyRope() {
                this.game.physics.p2.removeSpring(this._ropeSpring);
                this._ropeAnchored = false;
                this._currentKeypoint = null;
            }
            KillRagdoll() {
                var allConstraints = this.game.physics.p2.world.constraints.splice(0, this.game.physics.p2.world.constraints.length);
                for (let i = 0; i <= allConstraints.length; i++) {
                    this.game.physics.p2.removeConstraint(allConstraints[i]);
                }
                this._ragdollBodies.forEach(function (body) {
                    body.thrust(450);
                });
            }
            CreateRagdoll() {
                var leftGun = this.game.add.sprite(this.x, this.y, 'tony-gun');
                leftGun.pivot = new Phaser.Point(leftGun.width / 2, leftGun.height / 2 + 25 * this._scale);
                leftGun.scale = new Phaser.Point(0.2 * this._scale, 0.2 * this._scale);
                var leftFlare = this.game.add.sprite(this.x, this.y, 'muzzle-flare');
                leftGun.addChild(leftFlare);
                leftFlare.pivot.set(leftFlare.width / 2, 0);
                leftFlare.position.set(leftFlare.width / 3, -leftFlare.height);
                var rightGun = this.game.add.sprite(this.x, this.y + -25, 'tony-gun');
                rightGun.pivot = new Phaser.Point(rightGun.width / 2, rightGun.height / 2 + 25 * this._scale);
                rightGun.scale = new Phaser.Point(0.2 * this._scale, 0.2 * this._scale);
                var rightFlare = this.game.add.sprite(this.x, this.y, 'muzzle-flare');
                rightGun.addChild(rightFlare);
                rightFlare.pivot.set(0, 0);
                rightFlare.position.set(0, -rightFlare.height);
                rightFlare.tint = 0xff0000;
                var legOffset = 15;
                var armXOffset = 7;
                var tonyLeftHand = this.CreateBodyPiece('tony-left-hand', -80 - armXOffset, -3);
                var tonyLeftArmPiece1 = this.CreateBodyPiece('tony-left-arm-piece', -35 - armXOffset, -3);
                var tonyLeftArmPiece2 = this.CreateBodyPiece('tony-left-arm-piece', -50 - armXOffset, -3);
                var tonyLeftArmPiece3 = this.CreateBodyPiece('tony-left-arm-piece', -65 - armXOffset, -3);
                var tonyRightHand = this.CreateBodyPiece('tony-right-hand', 80 + armXOffset, -3);
                var tonyRightArmPiece1 = this.CreateBodyPiece('tony-right-arm-piece', 35 + armXOffset, -3);
                var tonyRightArmPiece2 = this.CreateBodyPiece('tony-right-arm-piece', 50 + armXOffset, -3);
                var tonyRightArmPiece3 = this.CreateBodyPiece('tony-right-arm-piece', 65 + armXOffset, -3);
                var tonyLeftFoot = this.CreateBodyPiece('tony-foot', -22, 64 + legOffset);
                var tonyLeftLegPiece2 = this.CreateBodyPiece('tony-left-leg-piece', -18, 54 + legOffset);
                var tonyLeftLegPiece1 = this.CreateBodyPiece('tony-left-leg-piece', -13, 46 + legOffset);
                var tonyRightFoot = this.CreateBodyPiece('tony-foot', 22, 64 + legOffset);
                var tonyRightLegPiece2 = this.CreateBodyPiece('tony-right-leg-piece', 18, 54 + legOffset);
                var tonyRightLegPiece1 = this.CreateBodyPiece('tony-right-leg-piece', 13, 46 + legOffset);
                var tonyBelly2 = this.CreateBodyPiece('tony-belly-2', 0, 27 + legOffset);
                var tonyBelly1 = this.CreateBodyPiece('tony-belly-1', 0, 18 + legOffset);
                var tonyBelly3 = this.CreateBodyPiece('tony-belly-3', 0, 40 + legOffset);
                var tonyTorso = this.CreateBodyPiece('tony-torso', 0, 0);
                this.CreateLockConstraint(tonyTorso, this.body);
                this.CreateRevoluteConstraint(tonyBelly1, tonyTorso, 0.025);
                this.CreateRevoluteConstraint(tonyBelly2, tonyBelly1, 0.025);
                this.CreateRevoluteConstraint(tonyBelly3, tonyBelly2, 0.025);
                this.CreateLockConstraint(tonyLeftArmPiece1, tonyTorso);
                this.CreateLockConstraint(tonyLeftArmPiece2, tonyLeftArmPiece1);
                this.CreateLockConstraint(tonyLeftArmPiece3, tonyLeftArmPiece2);
                this.CreateLockConstraint(tonyLeftHand, tonyLeftArmPiece3, 0.1);
                this.CreateLockConstraint(tonyRightArmPiece1, tonyTorso);
                this.CreateLockConstraint(tonyRightArmPiece2, tonyRightArmPiece1);
                this.CreateLockConstraint(tonyRightArmPiece3, tonyRightArmPiece2);
                this.CreateLockConstraint(tonyRightHand, tonyRightArmPiece3, 0.1);
                this.CreateLockConstraint(tonyLeftLegPiece1, tonyBelly3, 0.035);
                this.CreateLockConstraint(tonyLeftLegPiece2, tonyLeftLegPiece1, 0.035);
                this.CreateLockConstraint(tonyLeftFoot, tonyLeftLegPiece2, 0.035);
                this.CreateLockConstraint(tonyRightLegPiece1, tonyBelly3, 0.035);
                this.CreateLockConstraint(tonyRightLegPiece2, tonyRightLegPiece1, 0.035);
                this.CreateLockConstraint(tonyRightFoot, tonyRightLegPiece2, 0.035);
                this._rightHandBody = tonyRightHand;
                this._ragdollBodies =
                    [
                        tonyLeftArmPiece1, tonyLeftArmPiece2, tonyLeftArmPiece3, tonyLeftHand,
                        tonyRightArmPiece1, tonyRightArmPiece2, tonyRightArmPiece3, tonyRightHand,
                        tonyBelly2, tonyBelly3, tonyBelly1, tonyTorso, tonyLeftLegPiece1, tonyLeftLegPiece2, tonyLeftFoot, tonyRightLegPiece1, tonyRightLegPiece2, tonyRightHand
                    ];
                this._torso = tonyTorso;
                this._torso.mass = 0.25;
                this._guns =
                    [
                        new PlayerGun(tonyTorso, leftGun, leftFlare, -leftGun.width / 3),
                        new PlayerGun(tonyTorso, rightGun, rightFlare, rightGun.width / 3)
                    ];
                this.ToggleGuns(false);
            }
            CreateBodyPiece(spriteKey, relativeX, relativeY) {
                var position = new Phaser.Point(this.position.x, this.position.y);
                position.x += relativeX * this._scale;
                position.y += relativeY * this._scale;
                var bodyPiece = this.game.add.sprite(position.x, position.y, spriteKey);
                this.game.physics.p2.enable(bodyPiece);
                this.AttachChild(bodyPiece);
                bodyPiece.body.setCollisionGroup(ProeveVanBekwaamheid.Physics.Collision.GetCollisionGroup(ProeveVanBekwaamheid.Physics.CollisionGroups.PLAYER));
                bodyPiece.body.collides([ProeveVanBekwaamheid.Physics.Collision.GetCollisionGroup(ProeveVanBekwaamheid.Physics.CollisionGroups.PLATFORM), ProeveVanBekwaamheid.Physics.Collision.GetCollisionGroup(ProeveVanBekwaamheid.Physics.CollisionGroups.ENEMY)]);
                bodyPiece.scale.x = this._scale;
                bodyPiece.scale.y = this._scale;
                return bodyPiece.body;
            }
            CreateRevoluteConstraint(bodyPiece, parentBody, rotationLimit, mass = 0.15) {
                bodyPiece.mass = mass;
                var constraint = this.game.physics.p2.createRevoluteConstraint(parentBody, [bodyPiece.sprite.x, bodyPiece.sprite.height / 2], bodyPiece, [parentBody.sprite.x, -parentBody.sprite.y], 1000000, [this.body.sprite.position.x, this.body.sprite.position.y]);
                constraint.setLimits(-rotationLimit, rotationLimit);
                constraint.setStiffness(1000000);
                constraint.setRelaxation(0.5);
                constraint.collideConnected = false;
            }
            CreateLockConstraint(bodyPiece, parentBody, mass = 0.02) {
                bodyPiece.mass = mass;
                var constraint = this.game.physics.p2.createLockConstraint(bodyPiece, parentBody, [bodyPiece.sprite.x - parentBody.sprite.x, bodyPiece.sprite.y - parentBody.sprite.y]);
                constraint.setStiffness(100);
                constraint.setRelaxation(0.85);
            }
        }
        Player._currentLoadedSkinKey = 'none';
        Actors.Player = Player;
    })(Actors = ProeveVanBekwaamheid.Actors || (ProeveVanBekwaamheid.Actors = {}));
})(ProeveVanBekwaamheid || (ProeveVanBekwaamheid = {}));
var ProeveVanBekwaamheid;
(function (ProeveVanBekwaamheid) {
    var Actors;
    (function (Actors) {
        class WorldText extends ProeveVanBekwaamheid.Object.Entity {
            constructor(game, x, y, text, fill, fontSize, stroke, strokeThickness) {
                super(game, x, y);
                this._moveSpeed = 0.2;
                this._direction = 1;
                this._maxY = 3;
                this._isHovering = false;
                this._startY = y;
                this._text = game.add.text(x, y, text, {
                    fill: fill,
                    fontSize: fontSize,
                    stroke: stroke,
                    strokeThickness: strokeThickness
                });
            }
            get Text() {
                return this._text;
            }
            SetProperties(text, fill, fontSize, strokeThickness) {
                this._text.text = text;
                this._text.fill = fill;
                this._text.fontSize = fontSize;
                this._text.strokeThickness = strokeThickness;
            }
            StartHover() {
                this._isHovering = true;
            }
            OnStart() {
            }
            IndexDependencies(graphicsRequests) {
            }
            Destroy() {
                this._text.destroy();
                super.Destroy();
            }
            OnUpdate() {
                this._text.x = this.x;
                this._text.y = this.y;
                if (this._isHovering) {
                    this.position.y += this._moveSpeed * this._direction;
                    if (this.position.y >= this._startY + this._maxY && this._direction == 1
                        || this.position.y <= this._startY - this._maxY && this._direction == -1) {
                        this._direction *= -1;
                    }
                }
            }
        }
        Actors.WorldText = WorldText;
    })(Actors = ProeveVanBekwaamheid.Actors || (ProeveVanBekwaamheid.Actors = {}));
})(ProeveVanBekwaamheid || (ProeveVanBekwaamheid = {}));
var ProeveVanBekwaamheid;
(function (ProeveVanBekwaamheid) {
    var Object;
    (function (Object) {
        class Entity extends Object.SharedObject {
            constructor(game, x, y, key, frame) {
                super(game, x, y, key, frame);
                this.game = game;
                this._children = new Array();
            }
            set scale(scale) {
                super.scale = scale;
                if (this._children != null) {
                    this._children.forEach(function (child) {
                        child.scale = scale;
                    });
                }
            }
            get children() {
                return this._children;
            }
            AttachChild(child) {
                let index = this._children.indexOf(child);
                if (index == -1) {
                    this._children.push(child);
                }
                child.scale = this.scale;
                return child;
            }
            DetachChild(child) {
                let index = this._children.indexOf(child);
                if (index >= 0) {
                    this._children.splice(index);
                }
                return child;
            }
        }
        Object.Entity = Entity;
    })(Object = ProeveVanBekwaamheid.Object || (ProeveVanBekwaamheid.Object = {}));
})(ProeveVanBekwaamheid || (ProeveVanBekwaamheid = {}));
var ProeveVanBekwaamheid;
(function (ProeveVanBekwaamheid) {
    var Events;
    (function (Events) {
        class EventHandlerHolder {
            constructor() {
                this._eventHandlers = new Array();
            }
            get EventHandlers() {
                return this._eventHandlers;
            }
            AddEventListener(eventHandler) {
                this._eventHandlers.push(eventHandler);
            }
            RemoveEventListener(eventHandler) {
                var index = this._eventHandlers.indexOf(eventHandler);
                if (index > -1) {
                    this._eventHandlers.splice(index);
                }
            }
        }
        Events.EventHandlerHolder = EventHandlerHolder;
    })(Events = ProeveVanBekwaamheid.Events || (ProeveVanBekwaamheid.Events = {}));
})(ProeveVanBekwaamheid || (ProeveVanBekwaamheid = {}));
var ProeveVanBekwaamheid;
(function (ProeveVanBekwaamheid) {
    var Object;
    (function (Object) {
        (function (PhysicsMode) {
            PhysicsMode[PhysicsMode["Static"] = 0] = "Static";
            PhysicsMode[PhysicsMode["Dynamic"] = 1] = "Dynamic";
            PhysicsMode[PhysicsMode["Kinematic"] = 2] = "Kinematic";
        })(Object.PhysicsMode || (Object.PhysicsMode = {}));
        var PhysicsMode = Object.PhysicsMode;
        class PhysicsEntity extends Object.Entity {
            constructor(game, x, y, key, frame) {
                super(game, x, y, key, frame);
                this.MaxVelocity = -1;
                this._childConstraints = [];
            }
            get Radius() {
                return this._circleRadius;
            }
            static get PHYSICS_DEBUG_ENABLED() {
                return false;
            }
            OnUpdate() {
                this.ConstrainVelocity();
            }
            EnableCircleCollision(radius, collisionGroup) {
                ProeveVanBekwaamheid.Physics.Collision.AddRadiusCollisionObject(this, collisionGroup);
                this._circleRadius = radius;
            }
            DisableCircleCollision(collisionGroup) {
                ProeveVanBekwaamheid.Physics.Collision.RemoveRadiusCollisionObject(this, collisionGroup);
            }
            EnableBody(mode) {
                this.game.physics.p2.enableBody(this, PhysicsEntity.PHYSICS_DEBUG_ENABLED);
                this.SetPhysicsMode(this.body, mode);
            }
            DisableBody() {
                if (this.body != null) {
                    this.game.physics.p2.removeBody(this.body);
                    if (this._childConstraints.length > 0) {
                        for (var i = this._childConstraints.length - 1; i >= 0; i--) {
                            this.game.physics.p2.removeConstraint(this._childConstraints[i]);
                            this._childConstraints.pop();
                        }
                    }
                }
            }
            EnableChildren(mode, lockConstraint) {
                for (var i = 0; i < this._children.length; i++) {
                    var child = this._children[i];
                    this.game.physics.p2.enable(child, PhysicsEntity.PHYSICS_DEBUG_ENABLED);
                    this.SetPhysicsMode(child.body, mode);
                    if (this.body && lockConstraint) {
                        this._childConstraints.push(this.game.physics.p2.createLockConstraint(this.body, child.body));
                    }
                }
            }
            ConstrainVelocity() {
                if (this.MaxVelocity == -1) {
                    return;
                }
                var angle, currVelocitySqr, vx, vy;
                vx = this.body.data.velocity[0];
                vy = this.body.data.velocity[1];
                currVelocitySqr = vx * vx + vy * vy;
                if (currVelocitySqr > this.MaxVelocity * this.MaxVelocity) {
                    angle = Math.atan2(vy, vx);
                    vx = Math.cos(angle) * this.MaxVelocity;
                    vy = Math.sin(angle) * this.MaxVelocity;
                    this.body.data.velocity[0] = vx;
                    this.body.data.velocity[1] = vy;
                }
            }
            ;
            SetPhysicsMode(body, mode) {
                switch (mode) {
                    case PhysicsMode.Static:
                        body.static = true;
                        break;
                    case PhysicsMode.Dynamic:
                        body.dynamic = true;
                        break;
                    case PhysicsMode.Kinematic:
                        body.kinematic = true;
                        break;
                }
            }
        }
        Object.PhysicsEntity = PhysicsEntity;
    })(Object = ProeveVanBekwaamheid.Object || (ProeveVanBekwaamheid.Object = {}));
})(ProeveVanBekwaamheid || (ProeveVanBekwaamheid = {}));
var ProeveVanBekwaamheid;
(function (ProeveVanBekwaamheid) {
    var Factories;
    (function (Factories) {
        class EnemyFactory {
            static get STATIC_ENEMIES() {
                return ["Enemy-Static-1"];
            }
            static get MOVING_ENEMIES() {
                return ["Enemy-Moving-1"];
            }
            static CreateStaticEnemy(game, x, y) {
                var enemy;
                var randomEnemy;
                randomEnemy = EnemyFactory.STATIC_ENEMIES[game.rnd.integerInRange(0, EnemyFactory.STATIC_ENEMIES.length - 1)];
                enemy = game.CurrentState.InstantiateEntity(ProeveVanBekwaamheid.Actors.Enemy, x, y, randomEnemy);
                enemy.position = new Phaser.Point(x, y);
                enemy.isHovering = true;
                enemy.scale.set(1.5, 1.5);
                game.CurrentState.Enemies.push(enemy);
                game.CurrentState.ObjectScroller.ScrollingObjects.push(enemy);
                return enemy;
            }
            static CreateMoveEnemy(game, x, y) {
                var enemy;
                var randomEnemy;
                randomEnemy = EnemyFactory.MOVING_ENEMIES[game.rnd.integerInRange(0, EnemyFactory.MOVING_ENEMIES.length - 1)];
                enemy = game.CurrentState.InstantiateEntity(ProeveVanBekwaamheid.Actors.Enemy, x, y, randomEnemy);
                enemy.position = new Phaser.Point(x, y);
                enemy.isMoving = true;
                game.CurrentState.Enemies.push(enemy);
                game.CurrentState.ObjectScroller.ScrollingObjects.push(enemy);
                return enemy;
            }
            static CreateLaserBeam(game, x, y) {
                var laserBeam;
                laserBeam = game.CurrentState.InstantiateEntity(ProeveVanBekwaamheid.Actors.LaserBeam, x, y, 'laser-beam');
                laserBeam.position = new Phaser.Point(0, y);
                laserBeam.StartPosition.set(0, y + game.rnd.integerInRange(-100, 100));
                laserBeam.EndPosition.set(game.width, y + game.rnd.integerInRange(-100, 100));
                laserBeam.CalculateScaleAndAngle();
                game.CurrentState.LaserBeams.push(laserBeam);
                game.CurrentState.ObjectScroller.ScrollingObjects.push(laserBeam);
                return laserBeam;
            }
        }
        Factories.EnemyFactory = EnemyFactory;
    })(Factories = ProeveVanBekwaamheid.Factories || (ProeveVanBekwaamheid.Factories = {}));
})(ProeveVanBekwaamheid || (ProeveVanBekwaamheid = {}));
var ProeveVanBekwaamheid;
(function (ProeveVanBekwaamheid) {
    var Factories;
    (function (Factories) {
        class KeypointFactory {
            static get KEYPOINT_ART() {
                return ["Keypoint"];
            }
            static get MAX_SCALE() {
                return 0.75;
            }
            static get MIN_SCALE() {
                return 0.75;
            }
            static CreateStaticKeypoint(game, player, x, y) {
                var keypoint;
                var randomArt;
                randomArt = KeypointFactory.KEYPOINT_ART[game.rnd.integerInRange(0, KeypointFactory.KEYPOINT_ART.length - 1)];
                keypoint = game.CurrentState.InstantiateEntity(ProeveVanBekwaamheid.Actors.Keypoint, x, y, randomArt);
                keypoint.position = new Phaser.Point(x, y);
                var random = game.rnd.realInRange(KeypointFactory.MIN_SCALE, KeypointFactory.MAX_SCALE);
                keypoint.scale.set(random);
                player.RegisterKeypoint(keypoint);
                game.CurrentState.KeyPoints.push(keypoint);
                game.CurrentState.ObjectScroller.ScrollingObjects.push(keypoint);
                return keypoint;
            }
        }
        Factories.KeypointFactory = KeypointFactory;
    })(Factories = ProeveVanBekwaamheid.Factories || (ProeveVanBekwaamheid.Factories = {}));
})(ProeveVanBekwaamheid || (ProeveVanBekwaamheid = {}));
var ProeveVanBekwaamheid;
(function (ProeveVanBekwaamheid) {
    var Factories;
    (function (Factories) {
        class PickupFactory {
            static get GUN_POWERUP() {
                return "Gun-Powerup";
            }
            static get JETPACK_POWERUP() {
                return "Jetpack-Powerup";
            }
            static get PLATFORM_POWERUP() {
                return "Platform-Powerup";
            }
            static get SHIELD_POWERUP() {
                return "Shield-Powerup";
            }
            static get MAGNET_POWERUP() {
                return "Magnet-Powerup";
            }
            static get PICKUPS() {
                return [PickupFactory.GUN_POWERUP, PickupFactory.JETPACK_POWERUP, PickupFactory.PLATFORM_POWERUP, PickupFactory.SHIELD_POWERUP, PickupFactory.MAGNET_POWERUP];
            }
            static CreateRandomPickup(game, x, y) {
                var pickup;
                var randomPickup;
                randomPickup = PickupFactory.PICKUPS[game.rnd.integerInRange(0, PickupFactory.PICKUPS.length - 1)];
                switch (randomPickup) {
                    case PickupFactory.GUN_POWERUP:
                        pickup = game.CurrentState.InstantiateEntity(ProeveVanBekwaamheid.PickUp.GunPowerUp, x, y, randomPickup);
                        break;
                    case PickupFactory.JETPACK_POWERUP:
                        pickup = game.CurrentState.InstantiateEntity(ProeveVanBekwaamheid.PickUp.JetpackPowerUp, x, y, randomPickup);
                        break;
                    case PickupFactory.SHIELD_POWERUP:
                        pickup = game.CurrentState.InstantiateEntity(ProeveVanBekwaamheid.PickUp.ShieldPowerUp, x, y, randomPickup);
                        break;
                    case PickupFactory.PLATFORM_POWERUP:
                        pickup = game.CurrentState.InstantiateEntity(ProeveVanBekwaamheid.PickUp.PlatformPowerUp, x, y, randomPickup);
                        break;
                    case PickupFactory.MAGNET_POWERUP:
                        pickup = game.CurrentState.InstantiateEntity(ProeveVanBekwaamheid.PickUp.MagnetPowerUp, x, y, randomPickup);
                        break;
                    default:
                        pickup = game.CurrentState.InstantiateEntity(ProeveVanBekwaamheid.PickUp.GunPowerUp, x, y, randomPickup);
                }
                pickup.position = new Phaser.Point(x, y);
                pickup.scale.x = 0.75;
                pickup.scale.y = 0.75;
                pickup.anchor.set(0.5, 0.5);
                game.CurrentState.Pickups.push(pickup);
                game.CurrentState.ObjectScroller.ScrollingObjects.push(pickup);
                return pickup;
            }
            static CreateCoin(game, x, y) {
                var coin = game.CurrentState.InstantiateEntity(ProeveVanBekwaamheid.PickUp.Coin, x, y, "mainMenu", "CoinTurn-1");
                if (game.CurrentState.Player.MagnetEnabled) {
                    coin.SetCircleRadius(ProeveVanBekwaamheid.PickUp.MagnetPowerUp.MAGNET_PULL_STRENGTH);
                }
                game.CurrentState.Coins.push(coin);
                game.CurrentState.ObjectScroller.ScrollingObjects.push(coin);
                return coin;
            }
        }
        Factories.PickupFactory = PickupFactory;
    })(Factories = ProeveVanBekwaamheid.Factories || (ProeveVanBekwaamheid.Factories = {}));
})(ProeveVanBekwaamheid || (ProeveVanBekwaamheid = {}));
var ProeveVanBekwaamheid;
(function (ProeveVanBekwaamheid) {
    var Client;
    (function (Client) {
        class GameEngine extends Phaser.Game {
            constructor() {
                super(GameEngine.WINDOW_WIDTH, GameEngine.WINDIW_HEIGHT, Phaser.AUTO, 'content', null);
                GameEngine._instance = this;
                this.state.add('menuState', ProeveVanBekwaamheid.State.MenuState, false);
                this.state.add('Novel', ProeveVanBekwaamheid.State.NovelState, false);
                this.state.add('Preload', ProeveVanBekwaamheid.State.Preloader, false);
                this.state.add("Game", ProeveVanBekwaamheid.State.GameState, false);
                this.state.start('menuState');
            }
            static get Instance() {
                return GameEngine._instance;
            }
            static get SharedObjectCache() {
                if (GameEngine._sharedObjectCache == null) {
                    GameEngine._sharedObjectCache = new ProeveVanBekwaamheid.Object.SharedObjectCache();
                }
                return GameEngine._sharedObjectCache;
            }
            get CurrentState() {
                return this.state.getCurrentState();
            }
            static get WINDOW_WIDTH() { return 720; }
            static get WINDIW_HEIGHT() { return 1080; }
            SetWindowSettings() {
                this.stage.setBackgroundColor(0xDDDDDD);
                this.input.maxPointers = 1;
                this.stage.disableVisibilityChange = true;
                if (this.device.desktop) {
                    this.scale.pageAlignHorizontally = true;
                }
                else {
                    this.scale.setGameSize(GameEngine.WINDOW_WIDTH, GameEngine.WINDIW_HEIGHT);
                    this.scale.minHeight = GameEngine.WINDIW_HEIGHT;
                    this.scale.minWidth = GameEngine.WINDOW_WIDTH;
                    this.scale.maxHeight = GameEngine.WINDIW_HEIGHT;
                    this.scale.maxWidth = GameEngine.WINDOW_WIDTH;
                    this.scale.forcePortrait = true;
                    this.scale.pageAlignVertically = true;
                    this.scale.forceOrientation(true, false);
                    this.scale.refresh();
                }
            }
            update(time) {
                super.update(time);
                this.UpdateSharedObjects();
            }
            UpdateSharedObjects() {
                GameEngine.SharedObjectCache.CachedObjects.forEach(function (sharedObject) {
                    sharedObject.OnUpdate();
                });
            }
        }
        Client.GameEngine = GameEngine;
    })(Client = ProeveVanBekwaamheid.Client || (ProeveVanBekwaamheid.Client = {}));
})(ProeveVanBekwaamheid || (ProeveVanBekwaamheid = {}));
window.onload = () => {
    new ProeveVanBekwaamheid.Client.GameEngine();
};
var ProeveVanBekwaamheid;
(function (ProeveVanBekwaamheid) {
    var Generators;
    (function (Generators) {
        class BackgroundGenerator {
            constructor(game, createBackgrounds) {
                this._game = game;
                if (createBackgrounds)
                    this.CreateBackgrounds();
            }
            CreateBackgrounds() {
                var currentState = this._game.CurrentState;
                currentState.Backgrounds.push(currentState.InstantiateEntity(ProeveVanBekwaamheid.Actors.Background, 0, 0));
                currentState.Backgrounds.push(currentState.InstantiateEntity(ProeveVanBekwaamheid.Actors.Background, 0, -ProeveVanBekwaamheid.Actors.Background.BACKGROUND_HEIGHT));
                currentState.Backgrounds.push(currentState.InstantiateEntity(ProeveVanBekwaamheid.Actors.Background, 0, -ProeveVanBekwaamheid.Actors.Background.BACKGROUND_HEIGHT * 2));
                for (let i = 0; i < currentState.Backgrounds.length; i++) {
                    currentState.Backgrounds[i].AddTextureKey("SpacePart-01");
                    currentState.Backgrounds[i].AddTextureKey("SpacePart-02");
                    currentState.Backgrounds[i].AddTextureKey("SpacePart-03");
                    currentState.Backgrounds[i].RandomizeImage = true;
                    currentState.Backgrounds[i].IsLooping = true;
                    currentState.Backgrounds[i].ChangeTexture();
                    currentState.BackgroundScroller.ScrollingObjects.push(currentState.Backgrounds[i]);
                }
            }
        }
        Generators.BackgroundGenerator = BackgroundGenerator;
    })(Generators = ProeveVanBekwaamheid.Generators || (ProeveVanBekwaamheid.Generators = {}));
})(ProeveVanBekwaamheid || (ProeveVanBekwaamheid = {}));
var ProeveVanBekwaamheid;
(function (ProeveVanBekwaamheid) {
    var Generators;
    (function (Generators) {
        class EnemiesGenerator {
            constructor(game) {
                this._game = game;
                this._minCooldown = 5;
                this._maxCooldown = 30;
                this._maxHorizontalPos = this._game.width - 100;
                this._minHorizontalPos = 0 + 100;
                this._verticalMinOffset = 200;
                this._verticalMaxOffset = 600;
                this._verticalPos = -600;
                this._maxEnemies = 10;
                this._currentCooldown = 0;
                this._movingChance = 5;
                this._maxMovingChance = 75;
            }
            set MinCooldown(number) {
                console.log("[EnemiesGenerator] MinCooldown has been set by another instance.");
                this._minCooldown = number;
            }
            get MinCooldown() {
                return this._minCooldown;
            }
            set MaxCooldown(number) {
                console.log("[EnemiesGenerator] MaxCooldown has been set by another instance.");
                this._maxCooldown = number;
            }
            get MaxCooldown() {
                return this._maxCooldown;
            }
            get IsStarted() {
                return this._isStarted;
            }
            Start() {
                this._isStarted = true;
            }
            Update() {
                if (!this._isStarted)
                    return;
                if (this._currentCooldown < this._game.time.now && this._game.CurrentState.Enemies.length < this._maxEnemies) {
                    this.CreateLaserBeam();
                    this._currentCooldown = this._game.time.now + (this._game.rnd.integerInRange(this._minCooldown, this._maxCooldown) * 1000);
                }
            }
            Progress() {
                if (this._movingChance < this._maxMovingChance) {
                    this._movingChance += 10;
                }
                else if (this._minCooldown > 5) {
                    this._maxCooldown -= 5;
                    this._minCooldown -= 1;
                }
            }
            CreateEnemy() {
                var random = 100;
                if (random < this._movingChance) {
                    ProeveVanBekwaamheid.Factories.EnemyFactory.CreateMoveEnemy(this._game, this._game.rnd.integerInRange(this._minHorizontalPos, this._maxHorizontalPos), this._verticalPos);
                }
                else {
                    ProeveVanBekwaamheid.Factories.EnemyFactory.CreateStaticEnemy(this._game, this._game.rnd.integerInRange(this._minHorizontalPos, this._maxHorizontalPos), this._verticalPos);
                }
                this._verticalPos = -(this._game.rnd.integerInRange(this._verticalMinOffset, this._verticalMaxOffset));
            }
            CreateLaserBeam() {
                ProeveVanBekwaamheid.Factories.EnemyFactory.CreateLaserBeam(this._game, this._game.rnd.integerInRange(this._minHorizontalPos, this._maxHorizontalPos), this._verticalPos);
                this._verticalPos = -(this._game.rnd.integerInRange(this._verticalMinOffset, this._verticalMaxOffset));
            }
        }
        Generators.EnemiesGenerator = EnemiesGenerator;
    })(Generators = ProeveVanBekwaamheid.Generators || (ProeveVanBekwaamheid.Generators = {}));
})(ProeveVanBekwaamheid || (ProeveVanBekwaamheid = {}));
var ProeveVanBekwaamheid;
(function (ProeveVanBekwaamheid) {
    var Generators;
    (function (Generators) {
        class JsonObjectGenerator {
            constructor(game, player) {
                this._game = game;
                this._player = player;
                this._map = this._game.add.tilemap('presets');
                this._currentHeight = this._game.height / 2;
                this._pickupChance = 1;
                this.CreateRandomPreset();
            }
            static get TILE_WIDTH() {
                return 60;
            }
            static get TILE_HEIGHT() {
                return 60;
            }
            static get PRESETS() {
                return ["Preset 1", "Preset 2", "Preset 3"];
            }
            Update(playerHeight) {
                if (playerHeight - this._currentHeight >= 1920) {
                    this._currentHeight = playerHeight;
                    this.CreateRandomPreset();
                }
            }
            CreateRandomPreset() {
                var random = this._game.rnd.integerInRange(0, JsonObjectGenerator.PRESETS.length - 1);
                var randomPreset = JsonObjectGenerator.PRESETS[random];
                this.CreatePresetFromList(this._map.objects[randomPreset], true);
            }
            CreatePresetFromList(objects, inverseY) {
                var generatedObjects = new Array();
                var game = this._game;
                var player = this._player;
                var pickupChance = this._pickupChance;
                objects.forEach(function (value) {
                    var x = value.x;
                    var y = value.y;
                    if (inverseY)
                        y = -value.y;
                    if (value.type === "Coin") {
                        var random = game.rnd.integerInRange(0, 10000);
                        random *= 0.01;
                        if (random <= pickupChance) {
                            var pickup = ProeveVanBekwaamheid.Factories.PickupFactory.CreateRandomPickup(game, x, y);
                            generatedObjects.push(pickup);
                        }
                        else {
                            var coin = ProeveVanBekwaamheid.Factories.PickupFactory.CreateCoin(game, x, y);
                            generatedObjects.push(coin);
                        }
                    }
                    else if (value.type === "HoverEnemy") {
                        var staticEnemy = ProeveVanBekwaamheid.Factories.EnemyFactory.CreateStaticEnemy(game, x, y);
                        generatedObjects.push(staticEnemy);
                    }
                    else if (value.type === "Keypoint") {
                        var keypoint = ProeveVanBekwaamheid.Factories.KeypointFactory.CreateStaticKeypoint(game, player, x, y);
                    }
                    else if (value.type === "KeypointGlow") {
                        var glow = game.CurrentState.InstantiateEntity(ProeveVanBekwaamheid.Actors.EmptyObject, x, y, "Glow");
                        var keypoint = ProeveVanBekwaamheid.Factories.KeypointFactory.CreateStaticKeypoint(game, player, x, y);
                        glow.pivot.set(glow.width / 2, glow.height / 2);
                        glow.x = x;
                        glow.y = y;
                        glow.AddTag('fx');
                        game.CurrentState.ObjectScroller.ScrollingObjects.push(glow);
                        generatedObjects.push(keypoint);
                    }
                    else if (value.type === "Text") {
                        var textField = game.CurrentState.InstantiateEntity(ProeveVanBekwaamheid.Actors.WorldText, x - 150, y, "");
                        game.CurrentState.ObjectScroller.ScrollingObjects.push(textField);
                        var text = "";
                        if (value.Input) {
                            text = value.Input;
                        }
                        textField.SetProperties(text, '#fff', 32, 3);
                        game.CurrentState.WorldTexts.push(textField);
                        generatedObjects.push(textField);
                    }
                    else if (value.type === "Hand") {
                        var hand = game.CurrentState.InstantiateEntity(ProeveVanBekwaamheid.Actors.Hand, x, y, "Hand");
                        game.CurrentState.ObjectScroller.ScrollingObjects.push(hand);
                        game.CurrentState.Hands.push(hand);
                        generatedObjects.push(hand);
                    }
                });
                game.CurrentState.KeyPoints.sort(function (a, b) { return (a.y < b.y) ? 1 : ((b.y < a.y) ? -1 : 0); });
                return generatedObjects;
            }
            FindObjectsByType(type, map, layer) {
                var result = new Array();
                map.objects[layer].forEach(function (element) {
                    if (element.type === type) {
                        result.push(element);
                    }
                });
                return result;
            }
        }
        Generators.JsonObjectGenerator = JsonObjectGenerator;
    })(Generators = ProeveVanBekwaamheid.Generators || (ProeveVanBekwaamheid.Generators = {}));
})(ProeveVanBekwaamheid || (ProeveVanBekwaamheid = {}));
var ProeveVanBekwaamheid;
(function (ProeveVanBekwaamheid) {
    class PhaserSpriteLoadRequest {
        constructor(key, path) {
            this.Key = key;
            this.Path = path;
        }
    }
    ProeveVanBekwaamheid.PhaserSpriteLoadRequest = PhaserSpriteLoadRequest;
    class PhaserSpriteSheetLoadRequest extends PhaserSpriteLoadRequest {
        constructor(key, path, frameWidth, frameHeight, frameCount) {
            super(key, path);
            this.FrameWidth = frameWidth;
            this.FrameHeight = frameHeight;
            this.FrameCount = frameCount;
        }
    }
    ProeveVanBekwaamheid.PhaserSpriteSheetLoadRequest = PhaserSpriteSheetLoadRequest;
    class EntityGraphicsLoadRequestCache {
        constructor() {
            this._activeSpriteRequests = new Array();
            this._activeSpriteSheetRequests = new Array();
            this._archivedSpriteRequests = new Array();
            this._archivedSpriteSheetRequests = new Array();
        }
        AddSpriteRequest(key, path, ignoreArchived = false) {
            if (ignoreArchived || !this.HasKeyInArchive(key)) {
                this._activeSpriteRequests.push(new PhaserSpriteLoadRequest(key, path));
            }
        }
        AddSpriteSheetRequest(key, path, frameWidth, frameHeight, frameCount) {
            if (!this.HasKeyInArchive(key)) {
                this._activeSpriteSheetRequests.push(new PhaserSpriteSheetLoadRequest(key, path, frameWidth, frameHeight, frameCount));
            }
        }
        GetSpriteRequests(archiveActive = true) {
            if (!archiveActive) {
                return this._activeSpriteRequests;
            }
            var returnList = new Array();
            for (var i = this._activeSpriteRequests.length - 1; i >= 0; i--) {
                returnList.push(this._activeSpriteRequests[i]);
                this._archivedSpriteRequests.push(this._activeSpriteRequests[i]);
            }
            this._activeSpriteRequests.splice(0, this._activeSpriteRequests.length);
            return returnList;
        }
        GetSpritesheetRequests(archiveActive = true) {
            if (!archiveActive) {
                return this._activeSpriteSheetRequests;
            }
            var returnList = new Array();
            for (var i = this._activeSpriteSheetRequests.length - 1; i >= 0; i--) {
                returnList.push(this._activeSpriteSheetRequests[i]);
                this._archivedSpriteSheetRequests.push(this._activeSpriteSheetRequests[i]);
                this._activeSpriteSheetRequests.splice(0, this._activeSpriteSheetRequests.length);
            }
            return returnList;
        }
        HasKeyInArchive(key) {
            for (var i = 0; i < this._archivedSpriteRequests.length; i++) {
                if (this._archivedSpriteRequests[i].Key == key) {
                    return true;
                }
            }
            for (var i = 0; i < this._archivedSpriteSheetRequests.length; i++) {
                if (this._archivedSpriteSheetRequests[i].Key == key) {
                    return true;
                }
            }
            return false;
        }
    }
    ProeveVanBekwaamheid.EntityGraphicsLoadRequestCache = EntityGraphicsLoadRequestCache;
})(ProeveVanBekwaamheid || (ProeveVanBekwaamheid = {}));
var ProeveVanBekwaamheid;
(function (ProeveVanBekwaamheid) {
    var Handlers;
    (function (Handlers) {
        class ScrollRect {
            constructor(game, objects, startScrollSpeed, scrollSpeedAcceleration, maxScrollSpeed, scrollAccelerationTime) {
                this._scrollingObjects = [];
                this._game = game;
                if (objects)
                    this._scrollingObjects = objects;
                this._startScrollSpeed = startScrollSpeed;
                this._scrollSpeedAcceleration = scrollSpeedAcceleration;
                this._maxScrollSpeed = maxScrollSpeed;
                this._scrollAccelerationTime = scrollAccelerationTime;
                this._scrollSpeed = this._startScrollSpeed;
            }
            get ScrollingObjects() {
                return this._scrollingObjects;
            }
            Reset(startScrollAcceleration) {
                this._scrollSpeed = this._startScrollSpeed;
                for (let i = this._scrollingObjects.length - 1; i >= 0; i--) {
                    this._scrollingObjects[i].Destroy();
                    this._scrollingObjects.pop();
                }
                this._game.time.events.remove(this._timerEvent);
                if (startScrollAcceleration) {
                    this.StartScrollAcceleration();
                }
            }
            StartScrollAcceleration() {
                var timesToScroll = this._maxScrollSpeed / this._scrollAccelerationTime - this._scrollAccelerationTime / this._scrollSpeed;
                this._timerEvent = this._game.time.events.repeat(Phaser.Timer.SECOND * this._scrollAccelerationTime, timesToScroll, this.AccelerateScroll, this);
            }
            ScrollObjects(otherVelocity) {
                let velocity = this._scrollSpeed * this._game.physics.p2.frameRate;
                if ((otherVelocity)) {
                    velocity = otherVelocity * this._game.physics.p2.frameRate;
                }
                for (var i = 0; i < this._scrollingObjects.length; i++) {
                    if (this._scrollingObjects[i].body) {
                        this._scrollingObjects[i].body.y -= velocity;
                    }
                    else {
                        this._scrollingObjects[i].position.y -= velocity;
                    }
                }
            }
            Update() {
                this.ScrollObjects();
            }
            AccelerateScroll() {
                this._scrollSpeed += this._scrollSpeedAcceleration;
            }
        }
        Handlers.ScrollRect = ScrollRect;
    })(Handlers = ProeveVanBekwaamheid.Handlers || (ProeveVanBekwaamheid.Handlers = {}));
})(ProeveVanBekwaamheid || (ProeveVanBekwaamheid = {}));
var ProeveVanBekwaamheid;
(function (ProeveVanBekwaamheid) {
    var Physics;
    (function (Physics) {
        class CollisionGroups {
        }
        CollisionGroups.KEY_POINTS = "KeyPoints";
        CollisionGroups.ROPE = "Rope";
        CollisionGroups.PLAYER = "Player";
        CollisionGroups.ENEMY = "Enemy";
        CollisionGroups.PICKUP = "Pickup";
        CollisionGroups.PLATFORM = "Platform";
        Physics.CollisionGroups = CollisionGroups;
        class Collision {
            static Update() {
                Collision.radiusCollisionGroups.forEach(Collision.CheckCollision);
            }
            static CheckCollision(value, key, map) {
                if (Collision.radiusCollisionObjects.has(key)) {
                    for (var i = value.length - 1; i >= 0; i--) {
                        var groupKey = value[i];
                        for (var object of Collision.radiusCollisionObjects.get(key)) {
                            if (Collision.radiusCollisionObjects.has(groupKey)) {
                                for (var objectToCollideWith of Collision.radiusCollisionObjects.get(groupKey)) {
                                    var dx = object.x - objectToCollideWith.x;
                                    var dy = object.y - objectToCollideWith.y;
                                    var distance = Math.sqrt(dx * dx + dy * dy);
                                    if (distance < object.Radius + objectToCollideWith.Radius) {
                                        object.OnCollision(objectToCollideWith, groupKey);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            static Reset() {
                Collision.radiusCollisionGroups.clear();
                Collision.radiusCollisionObjects.clear();
            }
            static AddRadiusCollisionOnGroups(group01, group02) {
                if (!Collision.radiusCollisionGroups.has(group01)) {
                    Collision.radiusCollisionGroups.set(group01, []);
                }
                Collision.radiusCollisionGroups.get(group01).push(group02);
            }
            static RemoveRadiusCollisionOnGroups(group01, group02) {
                if (Collision.radiusCollisionGroups.has(group01)) {
                    var index = Collision.radiusCollisionGroups.get(group01).indexOf(group02);
                    Collision.radiusCollisionGroups.get(group01).splice(index, 1);
                    if (Collision.radiusCollisionGroups.get(group01).length == 0) {
                        Collision.radiusCollisionGroups.delete(group01);
                    }
                }
            }
            static AddRadiusCollisionObject(object, key) {
                if (!Collision.radiusCollisionObjects.has(key)) {
                    Collision.radiusCollisionObjects.set(key, []);
                }
                Collision.radiusCollisionObjects.get(key).push(object);
            }
            static RemoveRadiusCollisionObject(object, key) {
                if (Collision.radiusCollisionObjects.has(key)) {
                    var index = Collision.radiusCollisionObjects.get(key).indexOf(object);
                    Collision.radiusCollisionObjects.get(key).splice(index, 1);
                    if (Collision.radiusCollisionObjects.get(key).length == 0) {
                        Collision.radiusCollisionObjects.delete(key);
                    }
                    return;
                }
            }
            static CreateCollisionGroup(key) {
                if (!Collision.initialized) {
                    console.log("Physics class not initialized can not create new collision group!");
                    return;
                }
                var newCollisionGroup;
                newCollisionGroup = Collision.game.physics.p2.createCollisionGroup();
                Collision.collisionGroups.set(key, newCollisionGroup);
            }
            static GetCollisionGroup(key) {
                if (Collision.collisionGroups.has(key)) {
                    return Collision.collisionGroups.get(key);
                }
                console.log("Collision group not found, make sure that the key is correct!");
                return null;
            }
            static Initialize(game) {
                Collision.game = game;
                Collision.collisionGroups = new Map();
                Collision.radiusCollisionObjects = new Map();
                Collision.radiusCollisionGroups = new Map();
                Collision.initialized = true;
                Collision.CreateCollisionGroup(CollisionGroups.PLAYER);
                Collision.CreateCollisionGroup(CollisionGroups.PLATFORM);
                Collision.CreateCollisionGroup(CollisionGroups.ENEMY);
            }
        }
        Physics.Collision = Collision;
    })(Physics = ProeveVanBekwaamheid.Physics || (ProeveVanBekwaamheid.Physics = {}));
})(ProeveVanBekwaamheid || (ProeveVanBekwaamheid = {}));
var ProeveVanBekwaamheid;
(function (ProeveVanBekwaamheid) {
    var Object;
    (function (Object) {
        class SharedObject extends Phaser.Sprite {
            constructor(game, x, y, key, frame) {
                super(game, x, y, key, frame);
                this._id = SharedObject.GeneratedId();
                this._tags = new Array();
                this._onDestroyedEventHandler = new ProeveVanBekwaamheid.Events.EventHandlerHolder();
                ProeveVanBekwaamheid.Client.GameEngine.SharedObjectCache.CacheObject(this);
            }
            static get IsLocalStorageSupported() {
                return typeof (Storage) !== "undefined";
            }
            get Id() {
                return this._id;
            }
            get OnDestroyedEvent() {
                return this._onDestroyedEventHandler;
            }
            get objectCache() {
                return ProeveVanBekwaamheid.Client.GameEngine.SharedObjectCache;
            }
            get Tags() {
                return this._tags;
            }
            static SetLocalStorageItem(key, value) {
                if (!this.IsLocalStorageSupported) {
                    return;
                }
                localStorage.setItem(key, value);
            }
            static AddLocalStorageListedItem(key, value) {
                if (!this.IsLocalStorageSupported) {
                    return;
                }
                if (!this.HasLocalStorageItem(key)) {
                    this.AddLocalStorageListedItemEntry(key);
                }
                var list = this.GetLocalStorageListedItem(key);
                list.push(value);
                localStorage.setItem(key, JSON.stringify(list));
            }
            static AddLocalStorageListedItemEntry(key) {
                if (!this.IsLocalStorageSupported) {
                    return;
                }
                if (this.HasLocalStorageItem(key)) {
                    return;
                }
                localStorage.setItem(key, JSON.stringify(new Array()));
            }
            static GetLocalStorageItem(key) {
                if (!this.IsLocalStorageSupported) {
                    return null;
                }
                return localStorage.getItem(key);
            }
            static GetLocalStorageListedItem(key) {
                var result = [];
                if (!this.IsLocalStorageSupported) {
                    return result;
                }
                if (!this.HasLocalStorageItem(key)) {
                    return result;
                }
                result = JSON.parse(localStorage.getItem(key));
                return result;
            }
            static RemoveLocalStorageItem(key) {
                if (!this.IsLocalStorageSupported) {
                    return;
                }
                localStorage.removeItem(key);
            }
            static RemoveLocalStorageListedItem(key, value, stopAtFirstEntry = false) {
                var list = this.GetLocalStorageListedItem(key);
                for (let i = list.length - 1; i >= 0; i--) {
                    if (list[i] == value) {
                        list.splice(i, 1);
                        if (stopAtFirstEntry) {
                            break;
                        }
                    }
                }
                localStorage.setItem(key, JSON.stringify(list));
            }
            static HasLocalStorageItem(key) {
                if (!this.IsLocalStorageSupported) {
                    return false;
                }
                return localStorage.getItem(key) != null;
            }
            Equals(other) {
                return this._id == other.Id;
            }
            OnUpdate() { }
            Destroy() {
                var eventHandlers = this._onDestroyedEventHandler.EventHandlers;
                for (var i = 0; i < eventHandlers.length; i++) {
                    eventHandlers[i].OnDestroyed();
                }
                ProeveVanBekwaamheid.Client.GameEngine.SharedObjectCache.UnCacheObject(this);
                super.destroy();
            }
            AddTag(tag) {
                if (!this.HasTag(tag)) {
                    this._tags.push(tag);
                }
            }
            RemoveTag(tag) {
                let index = this._tags.indexOf(tag);
                if (index >= 0) {
                    this._tags.splice(index);
                }
            }
            HasTag(tag) {
                return this._tags.indexOf(tag) >= 0;
            }
            static GeneratedId() {
                return ++this._lastUsedId;
            }
        }
        SharedObject._lastUsedId = -1;
        Object.SharedObject = SharedObject;
    })(Object = ProeveVanBekwaamheid.Object || (ProeveVanBekwaamheid.Object = {}));
})(ProeveVanBekwaamheid || (ProeveVanBekwaamheid = {}));
var ProeveVanBekwaamheid;
(function (ProeveVanBekwaamheid) {
    var Object;
    (function (Object) {
        class SharedObjectCache {
            constructor() {
                this._cache = new Array();
            }
            get CachedObjects() {
                return this._cache;
            }
            CacheObject(object) {
                if (this._cache.indexOf(object) == -1) {
                    this._cache.push(object);
                }
            }
            UnCacheObject(object) {
                var index = this._cache.indexOf(object);
                if (index > -1) {
                    this._cache.splice(index, 1);
                }
            }
            UnCacheObjectWithId(id) {
                var foundObject = this.FindObjectWithId(id);
                if (foundObject != null) {
                    this.UnCacheObject(foundObject);
                }
            }
            FindObjectWithId(id) {
                for (var i = 0; i < this._cache.length; i++) {
                    if (this._cache[i].Id == id) {
                        return this._cache[i];
                    }
                }
                return null;
            }
            FindObjectWithTag(tag) {
                for (var i = 0; i < this._cache.length; i++) {
                    if (this._cache[i].HasTag(tag)) {
                        return this._cache[i];
                    }
                }
                return null;
            }
            FindObjectsWithTag(tag) {
                let result = new Array();
                for (var i = 0; i < this._cache.length; i++) {
                    if (this._cache[i].HasTag(tag)) {
                        result.push(this._cache[i]);
                    }
                }
                return result;
            }
        }
        Object.SharedObjectCache = SharedObjectCache;
    })(Object = ProeveVanBekwaamheid.Object || (ProeveVanBekwaamheid.Object = {}));
})(ProeveVanBekwaamheid || (ProeveVanBekwaamheid = {}));
var ProeveVanBekwaamheid;
(function (ProeveVanBekwaamheid) {
    var Spine;
    (function (Spine) {
        class SpineFactory {
            static createSpineObject(game, x, y, key) {
                var spineObject = game.add.spine(x, y, key);
                return spineObject;
            }
            static loadSpineAsset(game, key, jsonPath) {
                game.load.spine(key, jsonPath);
            }
        }
        Spine.SpineFactory = SpineFactory;
    })(Spine = ProeveVanBekwaamheid.Spine || (ProeveVanBekwaamheid.Spine = {}));
})(ProeveVanBekwaamheid || (ProeveVanBekwaamheid = {}));
var ProeveVanBekwaamheid;
(function (ProeveVanBekwaamheid) {
    var State;
    (function (State) {
        class GameState extends ProeveVanBekwaamheid.PhaserStateBase {
            constructor() {
                super();
                this._isGameOver = false;
                this._isTutorial = true;
                this._backgrounds = [];
                this._keyPoints = [];
                this._enemies = [];
                this._coins = [];
                this._worldTexts = [];
                this._hands = [];
                this._tutorialTexts = [];
                this._pickups = [];
                this._laserBeams = [];
            }
            static get NUMERIC_CHAR_AMOUNT() {
                return 6;
            }
            static get POINTS_PER_PIXEL() {
                return 0.5;
            }
            static get PLAYER_HEIGHT_LOSE_MARGIN() {
                return 5;
            }
            get IsTutorial() {
                return this._isTutorial;
            }
            set IsTutorial(value) {
                this._isTutorial = value;
            }
            get ObjectScroller() {
                return this._objectScroller;
            }
            get BackgroundScroller() {
                return this._backgroundScroller;
            }
            get KeyPoints() {
                return this._keyPoints;
            }
            get Hands() {
                return this._hands;
            }
            get Backgrounds() {
                return this._backgrounds;
            }
            get Coins() {
                return this._coins;
            }
            get WorldTexts() {
                return this._worldTexts;
            }
            get TutorialTexts() {
                return this._tutorialTexts;
            }
            get Enemies() {
                return this._enemies;
            }
            get Pickups() {
                return this._pickups;
            }
            get LaserBeams() {
                return this._laserBeams;
            }
            get Player() {
                return this._player;
            }
            get Score() {
                return Math.round(this._playerMaxHeightReached * GameState.POINTS_PER_PIXEL);
            }
            preload() {
                this._player = this.InstantiateEntity(ProeveVanBekwaamheid.Actors.Player, this.game.width / 2, this.game.height / 2, "", 1);
                var bar = this.game.add.graphics(0, 100);
                bar.beginFill(0x000000, 0.2);
                bar.drawRect(0, 100, 800, 100);
                var style = { font: "bold 32px Arial", fill: "#fff", boundsAlignH: "center", boundsAlignV: "middle" };
                this._loaderText = this.game.add.text(0, 0, "Loading...", style);
                this._loaderText.setShadow(3, 3, 'rgba(0,0,0,0.5)', 2);
                this._loaderText.setTextBounds(0, this.game.height / 2 - 100, this.game.width, 100);
                this.game.load.audio('themeMusic', './assets/sounds/ThemeMusic.mp3');
                this.game.load.audio('gameOverSpace', './assets/sounds/inGame/GameOver.mp3');
                this.game.load.audio('armLaunch', './assets/sounds/tony/ArmLaunch.wav');
                this.game.load.audio('shieldPickUp', './assets/sounds/powerUps/Shield/shieldPickUp.wav');
                this.game.load.audio('gunPickUp', './assets/sounds/powerUps/GunPowerUp/GunPickUp.wav');
                this.game.load.audio('GunShoot', './assets/sounds/powerUps/GunPowerUp/GunSound.wav');
                this.game.load.audio('jetpackSound', './assets/sounds/powerUps/JetPack/Jetpack.wav');
                this.game.load.audio('coinPickupSound', './assets/sounds/inGame/CoinSound.mp3');
                this.game.load.audio('TonyHitsEnemy', './assets/sounds/enemy/MonsterDeathSound.wav');
            }
            create() {
                super.create();
                this._loaderText.destroy();
                this._themeMusic = this.add.audio('themeMusic', this.game.sound.volume / 2, true);
                this._themeMusic.play();
                ProeveVanBekwaamheid.Physics.Collision.AddRadiusCollisionOnGroups(ProeveVanBekwaamheid.Physics.CollisionGroups.PLAYER, ProeveVanBekwaamheid.Physics.CollisionGroups.ENEMY);
                ProeveVanBekwaamheid.Physics.Collision.AddRadiusCollisionOnGroups(ProeveVanBekwaamheid.Physics.CollisionGroups.PICKUP, ProeveVanBekwaamheid.Physics.CollisionGroups.PLAYER);
                this.fadeIn();
                this._objectScroller = new ProeveVanBekwaamheid.Handlers.ScrollRect(this.game, null, 50, 50, 500, 60);
                this._backgroundScroller = new ProeveVanBekwaamheid.Handlers.ScrollRect(this.game, null, 10, 10, 100, 60);
                this._backgroundGenerator = new ProeveVanBekwaamheid.Generators.BackgroundGenerator(this.game, true);
                this._presetGenerator = new ProeveVanBekwaamheid.Generators.JsonObjectGenerator(this.game, this._player);
                this._laserBeamGenerator = new ProeveVanBekwaamheid.Generators.EnemiesGenerator(this.game);
                this._tutorial = new ProeveVanBekwaamheid.Tutorial(this.game, this._presetGenerator);
                this._inGameUI = new ProeveVanBekwaamheid.UI.InGameUICanvas(this.game, 0, 0);
                this.Start();
                this.game.load.bitmapFont('heroes legend', './assets/fonts/heroes-legend.png', './assets/fonts/heroes-legend.xml');
                this.UpdateNumericDisplay('display_currency', ProeveVanBekwaamheid.statics.Inventory.CoinsOwned);
                this._gameOverAudio = this.add.audio('gameOverSpace', 10);
                this._launchArm = this.add.audio('armLaunch');
                this._coinPickupAudio = this.add.audio('coinPickupSound');
                this._hitEnemyAudio = this.add.audio('TonyHitsEnemy', 10);
                ProeveVanBekwaamheid.Object.SharedObject.SetLocalStorageItem('hasPlayedOnce', 'true');
            }
            fadeIn() {
                this.game.camera.flash(0x000000, 500, false);
            }
            update() {
                super.update();
                if (this._isGameOver) {
                    return;
                }
                ProeveVanBekwaamheid.Physics.Collision.Update();
                if ((this._player.body) && this._player.body.dynamic) {
                    this._presetGenerator.Update(this._playerMaxHeightReached);
                    this._laserBeamGenerator.Update();
                    if (this._player.IsOnBorder) {
                        this._objectScroller.ScrollObjects(this._player.body.velocity.y);
                        this._backgroundScroller.ScrollObjects(this._player.body.velocity.y / 5);
                        if (this._player.body.velocity.y < 0) {
                            this._playerNewHeight -= this._player.body.velocity.y * this.game.physics.p2.frameRate;
                        }
                    }
                    this.CheckClean();
                    this.CheckLoseCondition();
                    this.CheckPlayerHeightForScore();
                }
            }
            Lose() {
                this._gameOverAudio.play();
                this._isGameOver = true;
                this._player.KillRagdoll();
                this._player.ToggleRope(false);
                this._inGameUI.SetGameOverPopupContent(this.Score);
                this._inGameUI.ToggleGameOverPopup(true);
                this._inGameUI.SetPlayButtonCallback(function () {
                    this.game.state.start('Game');
                    this.ResetGame();
                }, this);
                var highscore = Number(ProeveVanBekwaamheid.Object.SharedObject.GetLocalStorageItem('highscore'));
                if (this.Score > highscore) {
                    ProeveVanBekwaamheid.Object.SharedObject.SetLocalStorageItem('highscore', this.Score);
                }
            }
            OnDeath() {
                this.Lose();
            }
            ResetGame() {
                this._themeMusic.destroy();
                this._isTutorial = true;
                this._player.OnDeathEvent.RemoveEventListener(this);
                this._player.Destroy();
                for (let i = this._backgrounds.length - 1; i >= 0; i--) {
                    this._backgrounds[i].Destroy();
                }
                for (let i = this._keyPoints.length - 1; i >= 0; i--) {
                    this._keyPoints[i].Destroy();
                }
                for (let i = this._enemies.length - 1; i >= 0; i--) {
                    this._enemies[i].Destroy();
                }
                for (let i = this._coins.length - 1; i >= 0; i--) {
                    this._coins[i].Destroy();
                }
                for (let i = this._laserBeams.length - 1; i >= 0; i--) {
                    this._laserBeams[i].Destroy();
                }
                for (let i = this._worldTexts.length - 1; i >= 0; i--) {
                    this._worldTexts[i].Destroy();
                }
                var pickUps = ProeveVanBekwaamheid.Client.GameEngine.SharedObjectCache.FindObjectsWithTag('pick-up');
                for (let i = 0; i < pickUps.length; i++) {
                    pickUps[i].Destroy();
                }
                this._keyPoints = [];
                this._backgrounds = [];
                this._enemies = [];
                this._coins = [];
                this._laserBeams = [];
                this._tutorialTexts = [];
                this._worldTexts = [];
                this._objectScroller.Reset();
                this._backgroundScroller.Reset();
                ProeveVanBekwaamheid.Physics.Collision.Reset();
            }
            dissableMusic(fadeTime = 0) {
                this._themeMusic.fadeOut(fadeTime);
            }
            ArmLaunchAudio() {
                this._launchArm.play();
            }
            collectCoinAudio() {
                this._coinPickupAudio.play();
            }
            playerHitEnemyAudio() {
                this._hitEnemyAudio.play();
            }
            Start() {
                this._tutorial.CreateTutorial();
                this._player.initialize(new Phaser.Point(this.game.width / 2, this.game.height - 200));
                this._playerMaxHeightReached = this._player.y;
                this._playerNewHeight = this._playerMaxHeightReached;
                this._player.OnDeathEvent.AddEventListener(this);
                this._laserBeamGenerator.Start();
                this._isGameOver = false;
            }
            CheckLoseCondition() {
                if (this._player.body.y >= this.game.height - this._player.height - GameState.PLAYER_HEIGHT_LOSE_MARGIN && this._keyPoints[0].y < -100) {
                    this.Lose();
                }
            }
            CheckPlayerHeightForScore() {
                if (this._playerNewHeight > this._playerMaxHeightReached) {
                    this.UpdateNumericDisplay('display_score', Math.round(this._playerMaxHeightReached * GameState.POINTS_PER_PIXEL));
                    this._playerMaxHeightReached = this._playerNewHeight;
                }
            }
            CheckClean() {
                var fxObjects = ProeveVanBekwaamheid.Client.GameEngine.SharedObjectCache.FindObjectsWithTag('fx');
                this.CheckCleanList(fxObjects, true);
                this.CheckCleanList(this._keyPoints, true);
                this.CheckCleanList(this._enemies, true);
                this.CheckCleanList(this._coins, true);
                this.CheckCleanList(this._worldTexts, true);
                this.CheckCleanList(this._laserBeams, true);
            }
            UpdateNumericDisplay(tag, score) {
                var display = this._inGameUI.FindElement(tag);
                if (display != null) {
                    var scoreAsString = score.toString();
                    let missingChars = GameState.NUMERIC_CHAR_AMOUNT - scoreAsString.length;
                    for (var i = 0; i < missingChars; i++) {
                        scoreAsString = "0" + scoreAsString;
                    }
                    display.Text = scoreAsString;
                }
            }
            CheckCleanList(array, inObjectScroller) {
                for (let i = array.length - 1; i >= 0; i--) {
                    if ((array[i]) && array[i].y > this.game.height + 200) {
                        if (inObjectScroller) {
                            var scrollObjectIndex = this._objectScroller.ScrollingObjects.indexOf(array[i]);
                            this._objectScroller.ScrollingObjects.splice(scrollObjectIndex, 1);
                        }
                        array[i].Destroy();
                        array.splice(i, 1);
                    }
                }
            }
        }
        State.GameState = GameState;
    })(State = ProeveVanBekwaamheid.State || (ProeveVanBekwaamheid.State = {}));
})(ProeveVanBekwaamheid || (ProeveVanBekwaamheid = {}));
var ProeveVanBekwaamheid;
(function (ProeveVanBekwaamheid) {
    var State;
    (function (State) {
        class MenuState extends ProeveVanBekwaamheid.PhaserStateBase {
            constructor(...args) {
                super(...args);
                this._inOptionScreen = false;
            }
            preload() {
                this.game.load.image('ad1', './assets/advertenties/advertentie1.png');
                this.game.load.image('ad2', './assets/advertenties/advertentie2.png');
                this.game.load.image('ad3', './assets/advertenties/advertentie3.png');
                this.game.load.image('ad4', './assets/advertenties/advertentie4.png');
                this.game.load.image('ad5', './assets/advertenties/advertentie5.png');
                this.game.load.image('ad6', './assets/advertenties/advertentie6.png');
                this.game.load.image('ad7', './assets/advertenties/advertentie7.png');
                this.game.load.atlasJSONHash('mainMenu', './assets/ui/MenuSpriteSheetJsonHash.png', './assets/ui/MenuSpriteSheetJsonHash.json');
                this.game.load.atlasJSONHash('shopMenu', './assets/ui/ShopSpriteSheet.png', './assets/ui/ShopSpriteSheet.json');
                this.game.load.atlasJSONHash('gameOver', './assets/ui/SpriteSheetGameOver.png', './assets/ui/SpriteSheetGameOver.json');
                this.game.load.audio('buttonClick', './assets/sounds/Ui/SelectSound.wav');
                this.game.load.image('Gun-Icon', './assets/pickups/Gun.png');
                this.game.load.image('Shield-Icon', './assets/pickups/Shield.png');
                this.game.load.image('Jetpack-Icon', './assets/pickups/SpeedBoost.png');
                this.game.load.image('Platform-Icon', './assets/pickups/Platform.png');
                this.game.load.image('Magnet-Icon', './assets/pickups/magnet.png');
                this.game.load.audio('mainMenuMusic', './assets/sounds/MenuMusic.mp3');
                this.game.load.audio('returnSound', './assets/sounds/Ui/ReturnSound.wav');
                this.game.load.image('highscores-logo', './assets/ui/HighScores.png');
                this.game.load.image('back-button', './assets/ui/BackButton.png');
                this.game.load.image('highscores-button', './assets/ui/HighScoreButton.png');
                this.game.load.image('highscores-button-hover', './assets/ui/HighScoreButtonHover.png');
                this.game.load.image('adsButton', './assets/ui/AdButton.png');
                for (let i = 0; i < ProeveVanBekwaamheid.statics.Inventory.SKINS.length; i++) {
                    this.game.load.image(ProeveVanBekwaamheid.statics.Inventory.SKINS[i].SkinKey + '-icon', './assets/sprites/player/' + ProeveVanBekwaamheid.statics.Inventory.SKINS[i].SpriteFolderName + '/Icon.png');
                }
            }
            create() {
                this.game.stage.backgroundColor = '#17222b';
                this.CreateUi = new ProeveVanBekwaamheid.UI.UiCreater(this.game, this);
                this._mainMenu = new State.MainMenu(this.game, this);
                this._optionsMenu = new State.OptionsMenu(this.game, this, this.MainMenuButtonOptions);
                this._shopMenu = new State.ShopMenu(this.game, this);
                this._ads = new ProeveVanBekwaamheid.Ads.adManager(this.game);
                if (this._ads.checkIfAdsAllowed()) {
                    this._shopMenu.showAdsButton();
                }
                MenuState.ButtonAudio = this.game.add.audio('buttonClick');
                MenuState.BackAudio = this.game.add.audio('returnSound');
                this._mainMenuMusic = this.game.add.audio('mainMenuMusic', this.game.sound.volume, true);
                this._mainMenuMusic.play();
                this.fadeIn();
            }
            fadeIn() {
                this.game.camera.flash(0x000000, 500, false);
            }
            mainMenu() {
                this._mainMenu.enableMenu();
                this._shopMenu.dissableShopMenu();
            }
            startAdd() {
                console.log('test');
                this._ads.startAdd();
            }
            updateCurrencyDisplay() {
                this._mainMenu.updateCurrencyDisplay();
            }
            optionsMenu() {
                if (this._inOptionScreen) {
                    this._optionsMenu.dissableOptionsMenu();
                    this._inOptionScreen = false;
                    MenuState.BackAudio.play();
                }
                else {
                    this._optionsMenu.enableOptionsMenu();
                    this._inOptionScreen = true;
                    MenuState.ButtonAudio.play();
                }
            }
            shopMenu() {
                this._mainMenu.disableMenu();
                this._optionsMenu.dissableOptionsMenu();
                this._shopMenu.enableShopMenu();
                this._inOptionScreen = false;
                MenuState.ButtonAudio.play();
            }
            OnGameStarted() {
                this._shopMenu.Destroy();
            }
            dissableMusic(fadeTime = 0) {
                this._mainMenuMusic.fadeOut(fadeTime);
            }
        }
        State.MenuState = MenuState;
    })(State = ProeveVanBekwaamheid.State || (ProeveVanBekwaamheid.State = {}));
})(ProeveVanBekwaamheid || (ProeveVanBekwaamheid = {}));
var ProeveVanBekwaamheid;
(function (ProeveVanBekwaamheid) {
    var State;
    (function (State) {
        class NovelFrame {
            constructor(frame, texts, speechBubbles) {
                this._progress = -1;
                this._allImages = [frame];
                let allImages = this._allImages;
                texts.forEach(function (image) { allImages.push(image); });
                speechBubbles.forEach(function (image) { allImages.push(image); });
            }
            get Frame() {
                return this._allImages[0];
            }
            get Images() {
                return this._allImages;
            }
            get IsCompleted() {
                return this._progress + 1 >= this._allImages.length;
            }
            GetNextImage() {
                if (this.IsCompleted) {
                    return this._allImages[this._allImages.length - 1];
                }
                return this._allImages[++this._progress];
            }
        }
        class NovelState extends ProeveVanBekwaamheid.PhaserStateBase {
            preload() {
                this.load.image('novel-frame-0', './assets/novel/novel_frame_0.png');
                this.load.image('novel-frame-1', './assets/novel/novel_frame_1.png');
                this.load.image('novel-frame-2', './assets/novel/novel_frame_2.png');
                this.load.image('novel-text-0', './assets/novel/novel_text_0.png');
                this.load.image('novel-text-1', './assets/novel/novel_text_1.png');
                this.load.image('novel-speech-0', './assets/novel/novel_speech_0.png');
                this.load.image('novel-speech-1', './assets/novel/novel_speech_1.png');
                this.load.image('novel-speech-2', './assets/novel/novel_speech_2.png');
                this.load.image('novel-speech-3', './assets/novel/novel_speech_3.png');
            }
            create() {
                this.game.stage.backgroundColor = '#fff';
                var scale = 0.55;
                var xOffset = this.game.width / 2;
                var yOffset = this.game.height / 100 * 1.5;
                this._frames =
                    [
                        new NovelFrame(this.game.add.image(0, yOffset, 'novel-frame-0'), [this.game.add.image(0, yOffset, 'novel-text-0')], [this.game.add.image(0, yOffset, 'novel-speech-0'), this.game.add.image(0, yOffset, 'novel-speech-1')]),
                        new NovelFrame(this.game.add.image(0, yOffset, 'novel-frame-1'), [this.game.add.image(0, yOffset, 'novel-text-1')], []),
                        new NovelFrame(this.game.add.image(0, yOffset, 'novel-frame-2'), [], [this.game.add.image(0, yOffset, 'novel-speech-2'), this.game.add.image(0, yOffset, 'novel-speech-3')])
                    ];
                let x = this.game.width / 2;
                let combinedPreviousHeight = 0;
                this._frames.forEach(function (frame) {
                    frame.Images.forEach(function (image) {
                        image.pivot.set(image.width / 2, 0);
                        image.scale.setTo(scale, scale);
                        image.alpha = 0;
                    });
                    frame.Frame.x = x;
                    frame.Frame.y = yOffset + combinedPreviousHeight;
                    combinedPreviousHeight += frame.Frame.height + yOffset;
                });
                let frameWidth = this._frames[0].Frame.width;
                let frameOffset = (this.game.width - frameWidth) / 2;
                let celebrated = this._frames[0].Images[1];
                celebrated.pivot.set(0, 0);
                celebrated.x = frameOffset;
                let happy = this._frames[0].Images[2];
                happy.pivot.set(happy.width / 2, 0);
                happy.position.set(this._frames[0].Frame.x + frameWidth / 2 - happy.width, this._frames[0].Frame.y);
                happy.y += 20;
                let thank = this._frames[0].Images[3];
                thank.pivot.set(0, thank.height);
                thank.x = thank.width;
                thank.y = yOffset + this._frames[0].Frame.height / 2;
                let darkness = this._frames[1].Images[1];
                darkness.pivot.set(0, 0);
                darkness.x = frameWidth + frameOffset - darkness.width;
                darkness.y = this._frames[1].Frame.y;
                let save = this._frames[2].Images[1];
                save.pivot.set(0, save.height / 2);
                save.x = save.width;
                save.y = this._frames[2].Frame.y + this._frames[2].Frame.height * 0.325;
                let kid = this._frames[2].Images[2];
                kid.pivot.set(0, kid.height / 2);
                kid.x = this.game.width / 2 + kid.width / 2;
                kid.y = this._frames[2].Frame.y + this._frames[2].Frame.height - kid.height;
                let instructionText;
                if (this.game.device.desktop) {
                    instructionText = 'Click the screen to continue..';
                }
                else {
                    instructionText = 'Tap the screen to continue..';
                }
                this._tapInstructions = this.game.add.text(0, 0, instructionText, { fill: '#fff', stroke: '#000' });
                this._tapInstructions.alpha = 0;
                this._tapInstructions.x = this.game.width / 2 - this._tapInstructions.width / 2;
                this._tapInstructions.y = this.game.height - this._tapInstructions.height;
                this._frameIndex = 0;
                this._fadeDuration = Phaser.Timer.SECOND * 1.35;
                this._initialDelay = Phaser.Timer.SECOND * 1.25;
                this.FadeNextImage(this._initialDelay, this._initialDelay * 3);
                this.game.input.onDown.add(this.OnClick, this);
                this.game.camera.onFadeComplete.add(this.StartGameState, this);
            }
            OnClick() {
                if (!this._fadingOut) {
                    if (this._fadetween && this._fadetween.isRunning) {
                        this._fadetween.target.alpha = 1;
                        this._fadetween.stop();
                    }
                    this.FadeNextImage(0, this._initialDelay * 4);
                    var textTween = this.game.add.tween(this._tapInstructions);
                    textTween.to({ alpha: 0 }, 150);
                    textTween.start();
                }
            }
            FadeNextImage(delay = 0, instructionsDelay = 0) {
                if (this._frames[this._frameIndex].IsCompleted) {
                    if (this._frameIndex + 1 >= this._frames.length) {
                        this._fadingOut = true;
                        this.game.camera.fade(0x000000, 1000);
                        return;
                    }
                    this._frameIndex++;
                }
                var textTween = this.game.add.tween(this._tapInstructions);
                textTween.to({ alpha: 1 }, 650, Phaser.Easing.Linear.None, true, instructionsDelay);
                textTween.start();
                var image = this._frames[this._frameIndex].GetNextImage();
                this._fadetween = this.game.add.tween(image);
                this._fadetween.to({ alpha: 1 }, this._fadeDuration, Phaser.Easing.Linear.None);
                this._fadetween.delay(delay);
                this._fadetween.start();
            }
            StartGameState() {
                this.game.stage.backgroundColor = '#000';
                this.game.state.start("Preload");
            }
        }
        State.NovelState = NovelState;
    })(State = ProeveVanBekwaamheid.State || (ProeveVanBekwaamheid.State = {}));
})(ProeveVanBekwaamheid || (ProeveVanBekwaamheid = {}));
var ProeveVanBekwaamheid;
(function (ProeveVanBekwaamheid) {
    class PhaserStateBase extends Phaser.State {
        constructor() {
            super();
            this._loadQueue = new Array();
        }
        get EntityGroup() {
            return this.entityGroup;
        }
        InstantiateEntity(entityType, x, y, key, frame) {
            if (PhaserStateBase._entityGraphicLoadCache == null) {
                PhaserStateBase._entityGraphicLoadCache = new ProeveVanBekwaamheid.EntityGraphicsLoadRequestCache();
            }
            if (!x)
                x = 0;
            if (!y)
                y = 0;
            var instance = new entityType(this.game, x, y, key, frame);
            instance.IndexDependencies(PhaserStateBase._entityGraphicLoadCache);
            this.LoadSprites(PhaserStateBase._entityGraphicLoadCache.GetSpriteRequests());
            this.LoadSpriteSheets(PhaserStateBase._entityGraphicLoadCache.GetSpritesheetRequests());
            if (this.entityGroup == null) {
                this.entityGroup = this.game.add.group();
            }
            this.entityGroup.add(instance);
            this.game.add.existing(instance);
            if (!this._started) {
                this._loadQueue.push(instance);
            }
            else {
                instance.OnStart();
            }
            return instance;
        }
        create() {
            this.StartLoadedEntities();
        }
        LoadSprites(requests) {
            for (var i = 0; i < requests.length; i++) {
                let request = requests[i];
                this.game.load.image(request.Key, request.Path);
            }
        }
        LoadSpriteSheets(requests) {
            for (var i = 0; i < requests.length; i++) {
                let request = requests[i];
                this.game.load.spritesheet(request.Key, request.Path, request.FrameWidth, request.FrameHeight, request.FrameCount);
            }
        }
        StartLoadedEntities() {
            for (var i = this._loadQueue.length - 1; i >= 0; i--) {
                this._loadQueue[i].OnStart();
            }
            this._loadQueue.splice(0, this._loadQueue.length);
            this._started = true;
        }
    }
    ProeveVanBekwaamheid.PhaserStateBase = PhaserStateBase;
})(ProeveVanBekwaamheid || (ProeveVanBekwaamheid = {}));
var ProeveVanBekwaamheid;
(function (ProeveVanBekwaamheid) {
    var State;
    (function (State) {
        class Preloader extends ProeveVanBekwaamheid.PhaserStateBase {
            preload() {
                ProeveVanBekwaamheid.statics.Inventory.InitializeUpgrades();
                this.game.add.plugin(new PhaserSpine.SpinePlugin(this.game, this.game.plugins));
                this.LoadImages();
                this.load.tilemap('presets', './assets/json/Presets.json', null, Phaser.Tilemap.TILED_JSON);
                this.load.tilemap('tutorial', './assets/json/Tutorial.json', null, Phaser.Tilemap.TILED_JSON);
                this.game.physics.startSystem(Phaser.Physics.P2JS);
                this.game.physics.p2.gravity.y = 90;
                this.game.physics.p2.applyGravity = true;
                this.game.physics.p2.restitution = 0.8;
                this.game.physics.p2.frameRate = 1 / 30;
                this.game.physics.p2.solveConstraints = false;
                ProeveVanBekwaamheid.Physics.Collision.Initialize(this.game);
            }
            create() {
                this.game.state.start("Game");
            }
            LoadImages() {
                this.game.load.image('SpacePart-01', './assets/backgrounds/Space01.png');
                this.game.load.image('SpacePart-02', './assets/backgrounds/Space02.png');
                this.game.load.image('SpacePart-03', './assets/backgrounds/Space03.png');
                this.game.load.image('Keypoint', './assets/props/Keypoint01.png');
                this.game.load.image('Hand', './assets/ui/hand.png');
                this.game.load.image('Glow', './assets/ui/GlowKeypoint.png');
                this.game.load.image('Gun-Powerup', './assets/pickups/GunPickUp.png');
                this.game.load.image('Shield-Powerup', './assets/pickups/ShieldPickUp.png');
                this.game.load.image('Jetpack-Powerup', './assets/pickups/SpeedBoostPickUp.png');
                this.game.load.image('Platform-Powerup', './assets/pickups/PlatformPickUp.png');
                this.game.load.image('Magnet-Powerup', './assets/pickups/MagnetPickUp.png');
                this.game.load.image('Shield', './assets/pickups/ShieldPowerup.png');
                this.game.load.image('Platform', './assets/pickups/PlatformPowerup.png');
                this.game.load.image('particles-magnet', './assets/pickups/Pulse.png');
                this.game.load.image('laser-beam', './assets/enemy/laserbeam.png');
                this.game.load.image('laser-coil', './assets/enemy/lasercoil.png');
                this.game.load.image('enemy-death', './assets/enemy/Death ghost.png');
                this.game.load.spritesheet('particles-jetack', './assets/particles/jetpack_particles_placeholder.png', 32, 32);
                this.game.load.image('muzzle-flare', './assets/sprites/player/Muzzle-flare.png');
                ProeveVanBekwaamheid.Spine.SpineFactory.loadSpineAsset(this.game, "Player-Stuf-Effect", "./assets/sprites/player/Stun.json");
                ProeveVanBekwaamheid.Spine.SpineFactory.loadSpineAsset(this.game, "Enemy-Moving-1", "./assets/enemy/Character_Moving_Space.json");
                ProeveVanBekwaamheid.Spine.SpineFactory.loadSpineAsset(this.game, "Enemy-Static-1", "./assets/enemy/Character_Static_Space.json");
            }
        }
        State.Preloader = Preloader;
    })(State = ProeveVanBekwaamheid.State || (ProeveVanBekwaamheid.State = {}));
})(ProeveVanBekwaamheid || (ProeveVanBekwaamheid = {}));
var ProeveVanBekwaamheid;
(function (ProeveVanBekwaamheid) {
    var statics;
    (function (statics) {
        class PlayerSkin {
            constructor(skinKey, price, spriteFolderName) {
                this.SkinKey = skinKey;
                this.Price = price;
                this.SpriteFolderName = spriteFolderName;
            }
            static get OWNED_PLAYER_SKINS_LOCAL_STORAGE_KEY() {
                return 'owned-player-skins';
            }
            static get SELECTED_PLAYER_SKIN_LOCAL_STORAGE_KEY() {
                return 'selected-player-skin';
            }
            static get PLAYER_SKIN_DEFAULT_KEY() {
                return 'Tony';
            }
        }
        statics.PlayerSkin = PlayerSkin;
        class PowerupUpgrade {
            constructor(powerupKey, price, modifier, maxModifiers, icon) {
                this.PowerupKey = powerupKey;
                this.Price = price;
                this.Modifier = modifier;
                this.MaxAmountModifiers = maxModifiers;
                this.Icon = icon;
            }
            static get OWNED_PLAYER_SKINS_LOCAL_STORAGE_KEY() {
                return 'owned-powerups';
            }
            static get POWERUP_MAGNET_KEY() {
                return 'Magnet';
            }
            static get POWERUP_JETPACK_KEY() {
                return 'Jetpack';
            }
            static get POWERUP_PLATFORM_KEY() {
                return 'Platform';
            }
            static get POWERUP_SHIELD_KEY() {
                return 'Shield';
            }
            static get POWERUP_GUN_KEY() {
                return 'Gun';
            }
            get CurrentModifier() {
                if (ProeveVanBekwaamheid.Object.SharedObject.GetLocalStorageItem(this.PowerupKey) != 0)
                    return 1 + this.Modifier * ProeveVanBekwaamheid.Object.SharedObject.GetLocalStorageItem(this.PowerupKey);
                return 1;
            }
            get CurrentPrice() {
                if (ProeveVanBekwaamheid.Object.SharedObject.GetLocalStorageItem(this.PowerupKey) != 0)
                    return this.Price * (Number(ProeveVanBekwaamheid.Object.SharedObject.GetLocalStorageItem(this.PowerupKey)) + 1);
                return this.Price;
            }
        }
        statics.PowerupUpgrade = PowerupUpgrade;
        class Inventory {
            static get SKINS() {
                return [
                    new PlayerSkin(PlayerSkin.PLAYER_SKIN_DEFAULT_KEY, 0, 'default'),
                    new PlayerSkin('President', 5000, 'president'),
                    new PlayerSkin('Skeleton', 4000, 'skeleton'),
                    new PlayerSkin('Bomb King', 4000, 'bomb-king'),
                    new PlayerSkin('Catbug', 2000, 'catbug'),
                    new PlayerSkin('Pumpkin', 3000, 'pumpkin'),
                    new PlayerSkin('Punk', 3000, 'punk'),
                    new PlayerSkin('Caveman', 3000, 'caveman'),
                    new PlayerSkin('Chewbacca', 3000, 'chewbacca'),
                    new PlayerSkin('Barrel', 3000, 'barrel'),
                    new PlayerSkin('Stony', 1000, 'stony')
                ];
            }
            static get POWERUPS() {
                return [
                    new PowerupUpgrade(PowerupUpgrade.POWERUP_JETPACK_KEY, 500, 0.25, 3, 'Jetpack-Icon'),
                    new PowerupUpgrade(PowerupUpgrade.POWERUP_GUN_KEY, 500, 0.25, 3, 'Gun-Icon'),
                    new PowerupUpgrade(PowerupUpgrade.POWERUP_MAGNET_KEY, 500, 0.25, 3, 'Magnet-Icon'),
                    new PowerupUpgrade(PowerupUpgrade.POWERUP_PLATFORM_KEY, 500, 0.25, 3, 'Platform-Icon'),
                    new PowerupUpgrade(PowerupUpgrade.POWERUP_SHIELD_KEY, 500, 0.25, 3, 'Shield-Icon')
                ];
            }
            static get COINS_STORAGE_KEY() {
                return 'coins';
            }
            static get CoinsOwned() {
                if (!Inventory._coinsLoadedThisSessions) {
                    if (!ProeveVanBekwaamheid.Object.SharedObject.HasLocalStorageItem(Inventory.COINS_STORAGE_KEY)) {
                        ProeveVanBekwaamheid.Object.SharedObject.SetLocalStorageItem(Inventory.COINS_STORAGE_KEY, '0');
                        Inventory._coinsOwned = 0;
                    }
                    else {
                        Inventory._coinsOwned = Number(ProeveVanBekwaamheid.Object.SharedObject.GetLocalStorageItem(Inventory.COINS_STORAGE_KEY));
                    }
                }
                return Inventory._coinsOwned;
            }
            static set CoinsOwned(value) {
                Inventory._coinsOwned = value;
                ProeveVanBekwaamheid.Object.SharedObject.SetLocalStorageItem(Inventory.COINS_STORAGE_KEY, Inventory._coinsOwned);
            }
            static get SelectedSkin() {
                if (!ProeveVanBekwaamheid.Object.SharedObject.HasLocalStorageItem(PlayerSkin.SELECTED_PLAYER_SKIN_LOCAL_STORAGE_KEY)) {
                    ProeveVanBekwaamheid.Object.SharedObject.AddLocalStorageListedItem(PlayerSkin.OWNED_PLAYER_SKINS_LOCAL_STORAGE_KEY, PlayerSkin.PLAYER_SKIN_DEFAULT_KEY);
                    ProeveVanBekwaamheid.Object.SharedObject.SetLocalStorageItem(PlayerSkin.SELECTED_PLAYER_SKIN_LOCAL_STORAGE_KEY, PlayerSkin.PLAYER_SKIN_DEFAULT_KEY);
                }
                var selectedSkinKey = ProeveVanBekwaamheid.Object.SharedObject.GetLocalStorageItem(PlayerSkin.SELECTED_PLAYER_SKIN_LOCAL_STORAGE_KEY);
                return Inventory.FindSkin(selectedSkinKey);
            }
            static InitializeUpgrades() {
                for (var i = 0; i < Inventory.POWERUPS.length; i++) {
                    if (!ProeveVanBekwaamheid.Object.SharedObject.HasLocalStorageItem(Inventory.POWERUPS[i].PowerupKey)) {
                        ProeveVanBekwaamheid.Object.SharedObject.SetLocalStorageItem(Inventory.POWERUPS[i].PowerupKey, 0);
                    }
                }
            }
            static SelectSkin(skin) {
                var skinKey = "";
                if (typeof skin === 'string') {
                    skinKey = skin;
                }
                else if (typeof skin === 'number') {
                    skinKey = Inventory.SKINS[skin].SkinKey;
                }
                ProeveVanBekwaamheid.Object.SharedObject.SetLocalStorageItem(PlayerSkin.SELECTED_PLAYER_SKIN_LOCAL_STORAGE_KEY, skinKey);
            }
            static HasSkin(skinKey) {
                var ownedSkins = ProeveVanBekwaamheid.Object.SharedObject.GetLocalStorageListedItem(PlayerSkin.OWNED_PLAYER_SKINS_LOCAL_STORAGE_KEY);
                return ownedSkins.indexOf(skinKey) >= 0;
            }
            static HasMaxUpgrade(upgradeKey) {
                var amountUpgrades = ProeveVanBekwaamheid.Object.SharedObject.GetLocalStorageItem(upgradeKey);
                var upgrade = Inventory.FindUpgrade(upgradeKey);
                return (amountUpgrades == upgrade.MaxAmountModifiers);
            }
            static AttemptBuySkin(skinKey) {
                if (Inventory.HasSkin(skinKey)) {
                    return true;
                }
                var skin = Inventory.FindSkin(skinKey);
                if (skin && skin.Price <= Inventory.CoinsOwned) {
                    ProeveVanBekwaamheid.Object.SharedObject.AddLocalStorageListedItem(PlayerSkin.OWNED_PLAYER_SKINS_LOCAL_STORAGE_KEY, skinKey);
                    Inventory.CoinsOwned -= skin.Price;
                    return true;
                }
                return false;
            }
            static AttemptBuyUpgrade(upgradeKey) {
                if (Inventory.HasMaxUpgrade(upgradeKey)) {
                    return true;
                }
                var upgrade = Inventory.FindUpgrade(upgradeKey);
                if (upgrade && upgrade.Price <= Inventory.CoinsOwned) {
                    Inventory.CoinsOwned -= upgrade.CurrentPrice;
                    var amountUpgrades = ProeveVanBekwaamheid.Object.SharedObject.GetLocalStorageItem(upgradeKey);
                    amountUpgrades++;
                    ProeveVanBekwaamheid.Object.SharedObject.SetLocalStorageItem(upgradeKey, amountUpgrades);
                    return true;
                }
                return false;
            }
            static FindSkin(skinKey) {
                for (let i = 0; i < Inventory.SKINS.length; i++) {
                    if (Inventory.SKINS[i].SkinKey == skinKey) {
                        return Inventory.SKINS[i];
                    }
                }
                return Inventory.SKINS[0];
            }
            static FindUpgrade(upgradeKey) {
                for (let i = 0; i < Inventory.POWERUPS.length; i++) {
                    if (Inventory.POWERUPS[i].PowerupKey == upgradeKey) {
                        return Inventory.POWERUPS[i];
                    }
                }
                return null;
            }
        }
        Inventory._coinsOwned = 0;
        Inventory._coinsLoadedThisSessions = false;
        statics.Inventory = Inventory;
    })(statics = ProeveVanBekwaamheid.statics || (ProeveVanBekwaamheid.statics = {}));
})(ProeveVanBekwaamheid || (ProeveVanBekwaamheid = {}));
var ProeveVanBekwaamheid;
(function (ProeveVanBekwaamheid) {
    class Tutorial {
        constructor(game, presetGenerator) {
            this._game = game;
            this._presetGenerator = presetGenerator;
            this._tutorial = this._game.add.tilemap('tutorial');
        }
        CreateTutorial() {
            var objects = this._presetGenerator.CreatePresetFromList(this._tutorial.objects['Tutorial'], false);
            for (var i = 0; i < objects.length; i++) {
                if (objects[i].Text) {
                    objects[i].Text.text = "Hold to drag!";
                    this._game.CurrentState.TutorialTexts.push(objects[i]);
                    objects[i].StartHover();
                }
            }
            var text = this._game.CurrentState.InstantiateEntity(ProeveVanBekwaamheid.Actors.WorldText, this._game.width / 2 - 180, this._game.height / 2, "");
            text.SetProperties("The bigger the distance " + '\n' + "the harder you will pull!", '#fff', 36, 3);
            text.StartHover();
            this._game.CurrentState.WorldTexts.push(text);
            this._game.CurrentState.ObjectScroller.ScrollingObjects.push(text);
        }
    }
    ProeveVanBekwaamheid.Tutorial = Tutorial;
})(ProeveVanBekwaamheid || (ProeveVanBekwaamheid = {}));
var ProeveVanBekwaamheid;
(function (ProeveVanBekwaamheid) {
    var UI;
    (function (UI) {
        class InGameUICanvas extends UI.UICanvas {
            constructor(game, x, y) {
                super(game, x, y);
                let currenyDisplay = this.AddGroup(new UI.UIGroup(this.game, 0, 0));
                currenyDisplay.AddElement(new UI.UIText(this.game, 60, 10, '000000', 24)).AddTag('display_currency');
                let currencyImage = currenyDisplay.AddElement(new UI.UIImage(this.game, 8, 4, 'mainMenu', 'CoinTurn-1'));
                currencyImage.scale.setTo(0.5);
                currencyImage.animations.add('coinrot', Phaser.Animation.generateFrameNames('CoinTurn-', 1, 12), 10, true, false);
                currencyImage.play('coinrot');
                let scoreDisplay = this.AddGroup(new UI.UIGroup(this.game, this.game.width - 150, 0));
                scoreDisplay.AddElement(new UI.UIText(this.game, this.game.width - 100, 10, '000000', 24)).AddTag('display_score');
                this._gameOverPopUp = this.AddGroup(new UI.UIGroup(this.game, this.game.width / 2, this.game.height / 2));
                let bg = this._gameOverPopUp.AddElement(new UI.UIImage(this.game, 0, 300, 'shopMenu', 'ChoiceBorder'));
                bg.pivot = new Phaser.Point(bg.width / 2, bg.height / 2);
                bg.scale.x = 0.35;
                bg.scale.y = 0.45;
                bg.x = this.game.width / 2;
                bg.y = this.game.height / 2 + bg.height * 0.15;
                let gameOverTitle = this._gameOverPopUp.AddElement(new UI.UIImage(this.game, bg.x, bg.y, 'gameOver', 'GameOver'));
                gameOverTitle.scale.set(0.25);
                gameOverTitle.anchor.set(0.5);
                gameOverTitle.MoveTo(this.game.width / 2, bg.y - bg.height / 2 + 74);
                let currentScoreText = this._gameOverPopUp.AddElement(new UI.UIText(this.game, bg.x, bg.y, 'SCORE: 000000', 24, '#fff', '#000', 3, 'middle'));
                currentScoreText.TextObject.pivot = new Phaser.Point(currentScoreText.TextObject.width / 2, 12);
                currentScoreText.MoveTo(this.game.width / 2, bg.y - bg.height / 2 + 214);
                currentScoreText.AddTag('current_score');
                var highscore = Number(ProeveVanBekwaamheid.Object.SharedObject.GetLocalStorageItem('highscore'));
                let highscoreText = this._gameOverPopUp.AddElement(new UI.UIText(this.game, bg.x, bg.y, 'Highscore: ' + highscore.toString(), 24, '#fff', '#000', 3, 'middle'));
                highscoreText.TextObject.pivot = new Phaser.Point(highscoreText.TextObject.width / 2, 12);
                highscoreText.MoveTo(this.game.width / 2, bg.y - bg.height / 2 + 162);
                highscoreText.AddTag('high_score');
                let mainMenuButton = this._gameOverPopUp.AddElement(new UI.UIButton(this.game, bg.x, bg.y, 'gameOver', 'MenuButtonHover', 'MenuButton'));
                mainMenuButton.ButtonObject.pivot = new Phaser.Point(mainMenuButton.ButtonObject.width / 2, 12);
                mainMenuButton.MoveTo(this.game.width / 2, bg.y + bg.height / 2 - 95);
                mainMenuButton.ButtonObject.scale = new Phaser.Point(0.45, 0.45);
                mainMenuButton.SetCallback(this.GoToMainMenu, this);
                let playButton = this._gameOverPopUp.AddElement(new UI.UIButton(this.game, bg.x, bg.y, 'mainMenu', 'Play_Button_Click', 'Play_Button'));
                playButton.ButtonObject.pivot = new Phaser.Point(playButton.ButtonObject.width / 2, 12);
                playButton.MoveTo(this.game.width / 2, bg.y + bg.height / 2 - 95 - mainMenuButton.ButtonObject.height - 15);
                playButton.ButtonObject.scale = new Phaser.Point(0.45, 0.45);
                playButton.AddTag('play_button');
                this._gameOverPopUp.SetVisibility(false);
                game.camera.onFadeComplete.addOnce(this.mainMenu, this);
            }
            ToggleGameOverPopup(state) {
                this._gameOverPopUp.SetVisibility(state);
            }
            SetPlayButtonCallback(resetGameFunction, gameStateContext) {
                let playButton = this._gameOverPopUp.FindElement('play_button');
                if (playButton) {
                    playButton.SetCallback(resetGameFunction, gameStateContext);
                }
            }
            SetGameOverPopupContent(score) {
                var scoreText = this._gameOverPopUp.FindElement('current_score');
                if (scoreText) {
                    var highscore = Number(ProeveVanBekwaamheid.Object.SharedObject.GetLocalStorageItem('highscore'));
                    if (score > highscore) {
                        scoreText.TextObject.fill = '#55f';
                        let newHighscoreText = this._gameOverPopUp.AddElement(new UI.UIText(this.game, scoreText.TextObject.x, scoreText.TextObject.y - 20, 'NEW HIGHSCORE!', 14, '#aaf', '#aaa', 0.5));
                        newHighscoreText.TextObject.pivot = new Phaser.Point(newHighscoreText.TextObject.width / 2, 12);
                        var tween = this.game.add.tween(newHighscoreText.TextObject);
                        tween.to({ y: newHighscoreText.TextObject.y - 15 }, 1250, 'Cubic.easeInOut', true, 0, -1, true);
                    }
                    else {
                        scoreText.Translate(0, -12);
                    }
                    scoreText.Text = 'SCORE: ' + score.toString();
                    scoreText.TextObject.pivot = new Phaser.Point(scoreText.TextObject.width / 2, 12);
                }
            }
            GoToMainMenu() {
                var gameState = ProeveVanBekwaamheid.Client.GameEngine.Instance.CurrentState;
                gameState.dissableMusic(800);
                this.game.camera.fade(0x000000, 1000);
            }
            mainMenu() {
                var gameState = ProeveVanBekwaamheid.Client.GameEngine.Instance.CurrentState;
                gameState.ResetGame();
                this.game.state.start('menuState');
            }
        }
        UI.InGameUICanvas = InGameUICanvas;
    })(UI = ProeveVanBekwaamheid.UI || (ProeveVanBekwaamheid.UI = {}));
})(ProeveVanBekwaamheid || (ProeveVanBekwaamheid = {}));
var ProeveVanBekwaamheid;
(function (ProeveVanBekwaamheid) {
    var UI;
    (function (UI) {
        class HighscoresEntry {
            constructor(game, name, score, textColor = '#fff') {
                this._name = name;
                this._score = score;
                this._game = game;
                let style = { fill: textColor, stroke: '#333' };
                this._container = this._game.add.group();
                this._backgroundImage = game.add.image(0, 0, 'shopMenu', 'ChoiceBorder', this._container);
                this._nameText = game.add.text(0, 0, name, style, this._container);
                this._scoreText = game.add.text(0, 0, score.toString(), style, this._container);
            }
            static get ENTRY_Y_DIFFERENCE() {
                return 8;
            }
            static get SCORE_NAME_DISTANCE() {
                return 95;
            }
            static get ENTRY_TEXT_X_PADDING() {
                return 15;
            }
            static get ENTRY_TEXT_Y_PADDING() {
                return 8;
            }
            static get MINIMUM_ENTRY_WIDTH() {
                return 375;
            }
            get Score() {
                return this._score;
            }
            get Container() {
                return this._container;
            }
            get MinimumWidth() {
                let width = HighscoresEntry.ENTRY_TEXT_X_PADDING * 2 + this._nameText.width + HighscoresEntry.SCORE_NAME_DISTANCE + this._scoreText.width;
                if (width < HighscoresEntry.MINIMUM_ENTRY_WIDTH) {
                    width = HighscoresEntry.MINIMUM_ENTRY_WIDTH;
                }
                return width;
            }
            get MinimumHeight() {
                return HighscoresEntry.ENTRY_TEXT_Y_PADDING * 2 + this._nameText.height;
            }
            Transform(x, y, width) {
                this._backgroundImage.width = width;
                this._backgroundImage.height = HighscoresEntry.ENTRY_TEXT_Y_PADDING * 2 + this._nameText.height;
                this._backgroundImage.x = x;
                this._backgroundImage.y = y;
                this._nameText.x = x + HighscoresEntry.ENTRY_TEXT_X_PADDING;
                this._nameText.y = y + HighscoresEntry.ENTRY_TEXT_Y_PADDING;
                this._scoreText.pivot.set(this._scoreText.width, this._scoreText.pivot.y);
                this._scoreText.x = this._backgroundImage.x + this._backgroundImage.pivot.x + this._backgroundImage.width - HighscoresEntry.ENTRY_TEXT_X_PADDING;
                this._scoreText.y = y + HighscoresEntry.ENTRY_TEXT_Y_PADDING;
            }
        }
        UI.HighscoresEntry = HighscoresEntry;
    })(UI = ProeveVanBekwaamheid.UI || (ProeveVanBekwaamheid.UI = {}));
})(ProeveVanBekwaamheid || (ProeveVanBekwaamheid = {}));
var ProeveVanBekwaamheid;
(function (ProeveVanBekwaamheid) {
    var UI;
    (function (UI) {
        class HighscoresScreen {
            constructor(game, screenClosedCallback, closedCallbackContext) {
                this._widestEntry = -1;
                this._inScreenX = 0;
                this._backButtonActive = false;
                this._game = game;
                this._screenClosedCallback = screenClosedCallback;
                this._closedCallbackContext = closedCallbackContext;
                this._entries = new Array();
                this._entries.push(new UI.HighscoresEntry(game, "Daan", 1));
                this._entries.push(new UI.HighscoresEntry(game, "Menno", 2));
                this._entries.push(new UI.HighscoresEntry(game, "Koen", 3));
                this._entries.push(new UI.HighscoresEntry(game, "Lotte", 4));
                this._entries.push(new UI.HighscoresEntry(game, "Cerys", 5));
                this._entries.push(new UI.HighscoresEntry(game, "Luuc", 6));
                this._entries.push(new UI.HighscoresEntry(game, "Bart", 7));
                this._entries.push(new UI.HighscoresEntry(game, "Gerben", 8));
                if (ProeveVanBekwaamheid.Object.SharedObject.HasLocalStorageItem('highscore')) {
                    let score = Number(ProeveVanBekwaamheid.Object.SharedObject.GetLocalStorageItem('highscore'));
                    this._entries.push(new UI.HighscoresEntry(game, "You", score, '#ffdf44'));
                }
                this._entries.sort(function (a, b) { return (a.Score < b.Score) ? 1 : ((b.Score < a.Score) ? -1 : 0); });
                var startY = 350;
                this._highscoresLogo = game.add.image(0, 0, 'highscores-logo');
                this._highscoresLogo.scale.setTo(0.45);
                this._highscoresLogo.x = game.width / 2 - this._highscoresLogo.width / 2;
                this._highscoresLogo.y = startY - this._highscoresLogo.height - UI.HighscoresEntry.ENTRY_Y_DIFFERENCE;
                for (let i = 0; i < this._entries.length; i++) {
                    let entry = this._entries[i];
                    if (entry.MinimumWidth > this._widestEntry) {
                        this._widestEntry = entry.MinimumWidth;
                    }
                }
                var lastY = 0;
                for (let i = 0; i < this._entries.length; i++) {
                    let y = startY + (UI.HighscoresEntry.ENTRY_Y_DIFFERENCE + this._entries[i].MinimumHeight) * i;
                    this._entries[i].Transform(game.width / 2 - this._widestEntry / 2, y, this._widestEntry);
                    if (i + 1 < this._entries.length) {
                        lastY = y + this._entries[i].MinimumHeight;
                    }
                }
                this._backButton = game.add.button(0, 0, 'back-button', this.OnBackButtonPressed, this);
                this._backButton.scale.set(0.35);
                this._backButton.x = this._game.width / 2 - this._backButton.width / 2;
                this._backButton.y = lastY + 100;
            }
            TweenShow() {
                this._backButtonActive = true;
                this._highscoresLogo.alpha = 0;
                var logoTween = this._game.add.tween(this._highscoresLogo);
                logoTween.to({ alpha: 1 }, 650, Phaser.Easing.Sinusoidal.InOut);
                logoTween.start();
                for (let i = 0; i < this._entries.length; i++) {
                    if (this._inScreenX == -1) {
                        this._inScreenX = this._entries[i].Container.x;
                    }
                    this._entries[i].Container.x = this._game.width;
                    let tween = this._game.add.tween(this._entries[i].Container);
                    tween.to({ x: this._inScreenX }, 300 + i * 85, Phaser.Easing.Sinusoidal.InOut);
                    tween.start();
                }
                this._backButton.alpha = 0;
                var backButtonTween = this._game.add.tween(this._backButton);
                backButtonTween.to({ alpha: 1 }, 350, Phaser.Easing.Sinusoidal.InOut, true, 500);
            }
            TweenHide() {
                this._backButtonActive = false;
                var logoTween = this._game.add.tween(this._highscoresLogo);
                logoTween.to({ alpha: 0 }, 350, Phaser.Easing.Sinusoidal.InOut);
                logoTween.start();
                for (let i = 0; i < this._entries.length; i++) {
                    var tween = this._game.add.tween(this._entries[i].Container);
                    tween.to({ x: this._game.width }, 100 + i * 85, Phaser.Easing.Sinusoidal.InOut);
                    tween.start();
                }
                var backButtonTween = this._game.add.tween(this._backButton);
                backButtonTween.to({ alpha: 0 }, 150, Phaser.Easing.Sinusoidal.InOut);
                backButtonTween.start();
            }
            Toggle(newState) {
                this._backButton.visible = newState;
                this._highscoresLogo.visible = newState;
                for (let i = 0; i < this._entries.length; i++) {
                    this._entries[i].Container.visible = newState;
                }
            }
            OnBackButtonPressed() {
                if (!this._backButtonActive) {
                    return;
                }
                this.TweenHide();
                this._screenClosedCallback.call(this._closedCallbackContext);
            }
        }
        UI.HighscoresScreen = HighscoresScreen;
    })(UI = ProeveVanBekwaamheid.UI || (ProeveVanBekwaamheid.UI = {}));
})(ProeveVanBekwaamheid || (ProeveVanBekwaamheid = {}));
var ProeveVanBekwaamheid;
(function (ProeveVanBekwaamheid) {
    var State;
    (function (State) {
        class MainMenu {
            constructor(game, menuState) {
                this._game = game;
                this._menuState = menuState;
                this._game.camera.onFadeComplete.add(this.startGame, this);
                this.createMenu();
                var firstTimePlaying = !ProeveVanBekwaamheid.Object.SharedObject.HasLocalStorageItem('hasPlayedOnce');
                if (firstTimePlaying) {
                    this._game.camera.onFadeComplete.add(this.StartNovel, this);
                }
                else {
                    this._game.camera.onFadeComplete.add(this.startGame, this);
                }
            }
            disableMenu() {
                var i = 0;
                var tween;
                for (var v in this._menuArray) {
                    this._menuArray[v].inputEnabled = false;
                    tween = this._game.add.tween(this._menuArray[v]);
                    tween.to({ x: this._game.width + this._menuArray[v].width }, 750, 'Quart.easeOut', true, i * 50);
                    i++;
                }
            }
            enableMenu() {
                var i = 0;
                var tween;
                for (var v in this._menuArray) {
                    this._menuArray[v].inputEnabled = true;
                    tween = this._game.add.tween(this._menuArray[v]);
                    tween.to({ x: this._menuPositionArray[v] }, 750, 'Quart.easeOut', true, i * 100);
                    i++;
                }
            }
            updateCurrencyDisplay() {
                this._currencyAmountDisplay.text = ProeveVanBekwaamheid.statics.Inventory.CoinsOwned.toString();
            }
            createMenu() {
                this.logo = this._menuState.CreateUi.createImage(this._game.width / 2, this._game.height / 10, 'mainMenu', 'Logo', 0.5, 0);
                this._coinBorder = this._menuState.CreateUi.createImage(this._game.width, 0, 'mainMenu', 'Score_Border', 1, 0);
                this._coinBorder.width *= 1.25;
                this._coinAnimation = this._menuState.CreateUi.createImage(this._game.width - this._coinBorder.width * 10 / 10.5, this._coinBorder.height / 8, 'mainMenu', 'CoinTurn-1', 0, 0);
                this._coinAnimation.animations.add('coinrot', Phaser.Animation.generateFrameNames('CoinTurn-', 1, 12), 10, true, false);
                this._coinAnimation.play('coinrot');
                this._currencyAmountDisplay = this._menuState.CreateUi.createText(this._coinAnimation.width * 1.1 + this._game.width - this._coinBorder.width * 10 / 10.5, this._coinBorder.height / 4, ProeveVanBekwaamheid.statics.Inventory.CoinsOwned.toString(), 48, '#fff', 0, 0);
                this._mainMenuButtonOptions = this._menuState.CreateUi.createButton(0, 0, 'mainMenu', this._menuState.optionsMenu, 'Settings_Button', 'Settings_Button_Click', this._menuState, 0, 0);
                this._mainMenuButtonStart = this._menuState.CreateUi.createButton(this._game.width / 4, this._game.height / 4 * 3, 'mainMenu', this.fadeScreenOut, 'Play_Button', 'Play_Button_Click', this, 0.5, 0.5);
                this._mainMenuButtonShop = this._menuState.CreateUi.createButton(this._game.width / 4 * 3, this._game.height / 4 * 3, 'mainMenu', this._menuState.shopMenu, 'Shop_Button', 'Shop_Button_Click', this._menuState, 0.5, 0.5);
                this._menuPositionArray = new Array();
                this._highscoreScreen = new ProeveVanBekwaamheid.UI.HighscoresScreen(this._game, this.OnHighscoresScreenClosed, this);
                this._highscoreScreen.Toggle(false);
                this._highscoresButton = this._game.add.button(0, 0, 'highscores-button', this.OnHighscoresButtonPressed, this);
                this._highscoresButton.scale.setTo(0.55);
                this._highscoresButton.x = this._game.width / 2 - this._highscoresButton.width / 2;
                this._highscoresButton.y = this._mainMenuButtonStart.y - this._mainMenuButtonStart.height - this._highscoresButton.height * 1.25;
                this._menuArray = new Array();
                this._menuArray.push(this._highscoresButton);
                this._menuArray.push(this._mainMenuButtonOptions);
                this._menuArray.push(this._mainMenuButtonStart);
                this._menuArray.push(this.logo);
                this._menuArray.push(this._mainMenuButtonShop);
                var i = 0;
                var tween;
                for (var v in this._menuArray) {
                    this._menuPositionArray[v] = this._menuArray[v].x;
                    i++;
                }
                this._menuState.MainMenuButtonOptions = this._mainMenuButtonOptions.width;
            }
            OnHighscoresButtonPressed() {
                this.disableMenu();
                this._highscoreScreen.Toggle(true);
                this._highscoreScreen.TweenShow();
                State.MenuState.ButtonAudio.play();
            }
            OnHighscoresScreenClosed() {
                this.enableMenu();
                State.MenuState.BackAudio.play();
            }
            fadeScreenOut() {
                this._menuArray.push(this._coinAnimation);
                this._menuArray.push(this._coinBorder);
                this._menuArray.push(this._currencyAmountDisplay);
                this.disableMenu();
                State.MenuState.ButtonAudio.play();
                this._menuState.dissableMusic(800);
                var firstTimePlaying = !ProeveVanBekwaamheid.Object.SharedObject.HasLocalStorageItem('hasPlayedOnce');
                if (firstTimePlaying) {
                    this._game.camera.fade(0xffffff, 1000);
                }
                else {
                    this._game.camera.fade(0x000000, 1000);
                }
            }
            StartNovel() {
                this._menuState.OnGameStarted();
                this._menuState.dissableMusic(0);
                this._game.stage.backgroundColor = '#fff';
                this._game.state.start('Novel');
            }
            startGame() {
                this._menuState.OnGameStarted();
                this._menuState.dissableMusic(0);
                this._game.stage.backgroundColor = '#000';
                this._game.state.start('Preload');
            }
        }
        State.MainMenu = MainMenu;
    })(State = ProeveVanBekwaamheid.State || (ProeveVanBekwaamheid.State = {}));
})(ProeveVanBekwaamheid || (ProeveVanBekwaamheid = {}));
var ProeveVanBekwaamheid;
(function (ProeveVanBekwaamheid) {
    var State;
    (function (State) {
        class OptionsMenu {
            constructor(game, menuState, optionsButton) {
                this._game = game;
                this._menuState = menuState;
                this._mainMenuButtonOptions = optionsButton;
                this.createOptions();
            }
            static get VOLUME_LOCAL_STORAGE_KEY() {
                return 'volume';
            }
            dissableOptionsMenu() {
                var tween;
                for (var v in this._optionsArray) {
                    tween = this._game.add.tween(this._optionsArray[v]);
                    tween.to({ y: -this._mainMenuButtonOptions }, 500, 'Quart.easeOut', true, 0);
                }
            }
            enableOptionsMenu() {
                var tween;
                tween = this._game.add.tween(this._optionsArray[0]);
                tween.to({ y: this._mainMenuButtonOptions }, 500, 'Quart.easeOut', true, 0);
                tween = this._game.add.tween(this._optionsArray[1]);
                tween.to({ y: this._mainMenuButtonOptions + this._audioSlider.height / 2 }, 500, 'Quart.easeOut', true, 0);
            }
            createOptions() {
                this._audioSlider = this._menuState.CreateUi.createImage(this._mainMenuButtonOptions, -this._mainMenuButtonOptions, 'mainMenu', 'Sound', 0, 0);
                this._inputHandler = this._menuState.CreateUi.createImage(this._audioSlider.x + this._audioSlider.width / 10 * 9, this._audioSlider.y + this._audioSlider.height / 2, 'mainMenu', 'Sound_Button', 0.5, 0.5);
                this._inputHandler.inputEnabled = true;
                this._inputHandler.input.enableDrag(true);
                this._optionsArray = new Array();
                this._optionsArray.push(this._audioSlider);
                this._optionsArray.push(this._inputHandler);
                this._inputHandler.events.onDragUpdate.add(this.dragUpdateAudioSlider, this);
                if (ProeveVanBekwaamheid.Object.SharedObject.HasLocalStorageItem(OptionsMenu.VOLUME_LOCAL_STORAGE_KEY)) {
                    var loadedVolume = Number(ProeveVanBekwaamheid.Object.SharedObject.GetLocalStorageItem(OptionsMenu.VOLUME_LOCAL_STORAGE_KEY));
                    var maxVolume = (this._audioSlider.x + this._audioSlider.width / 10 * 9) - (this._audioSlider.x + this._audioSlider.width / 3);
                    this._game.sound.volume = loadedVolume;
                    this._inputHandler.x = this._audioSlider.x + this._audioSlider.width * loadedVolume;
                    this.dragUpdateAudioSlider();
                }
            }
            dragUpdateAudioSlider() {
                if (this._inputHandler.x < this._audioSlider.x + this._audioSlider.width / 3) {
                    this._inputHandler.x = this._audioSlider.x + this._audioSlider.width / 3;
                }
                else if (this._inputHandler.x > this._audioSlider.x + this._audioSlider.width / 10 * 9) {
                    this._inputHandler.x = this._audioSlider.x + this._audioSlider.width / 10 * 9;
                }
                this._inputHandler.y = this._audioSlider.y + this._audioSlider.height / 2;
                var MaxVol = (this._audioSlider.x + this._audioSlider.width / 10 * 9) - (this._audioSlider.x + this._audioSlider.width / 3);
                var newVolume = this._inputHandler.x - (this._audioSlider.x + this._audioSlider.width / 3);
                this._game.sound.volume = (newVolume / MaxVol);
                ProeveVanBekwaamheid.Object.SharedObject.SetLocalStorageItem(OptionsMenu.VOLUME_LOCAL_STORAGE_KEY, this._game.sound.volume);
            }
        }
        State.OptionsMenu = OptionsMenu;
    })(State = ProeveVanBekwaamheid.State || (ProeveVanBekwaamheid.State = {}));
})(ProeveVanBekwaamheid || (ProeveVanBekwaamheid = {}));
var ProeveVanBekwaamheid;
(function (ProeveVanBekwaamheid) {
    var State;
    (function (State) {
        class ShopMenu extends ProeveVanBekwaamheid.Object.SharedObject {
            constructor(game, menuState) {
                super(game, 0, 0);
                this._selectedShopButton = 0;
                this._isDragging = false;
                this._scrollProgress = 0;
                this._previousScrollProgress = 0;
                this._dragStartMouseY = 0;
                this._game = game;
                this._menuState = menuState;
                this.createShop();
                this._returnSound = this._game.sound.add('returnSound');
            }
            OnStart() { }
            IndexDependencies(graphicsRequests) { }
            OnUpdate() {
                super.OnUpdate();
                var mousePos = this.game.input.activePointer;
                if (this.game.input.activePointer.isDown && this._objectBorder.getBounds().contains(mousePos.x, mousePos.y)) {
                    var yPositions = [];
                    var yDiff = mousePos.y - this._dragStartMouseY;
                    for (let i = 0; i < this._shopItems.length; i++) {
                        var isFirst = i == 0;
                        var isLast = i + 1 >= this._shopItems.length;
                        var yPos = this._shopItems[i].StartY + this._previousScrollProgress + yDiff;
                        if (isFirst && yPos > this._shopItems[i].StartY)
                            return;
                        if (isLast && yPos < (this._objectBorder.y + this._objectBorder.height - this._shopItems[i].height / 2 - 25))
                            return;
                        yPositions.push(yPos);
                    }
                    this._scrollProgress = this._previousScrollProgress + yDiff;
                    for (let i = 0; i < this._shopItems.length; i++) {
                        this._shopItems[i].y = yPositions[i];
                    }
                }
            }
            dissableShopMenu() {
                var i = 0;
                var tween;
                this.RemoveEventListeners();
                this._returnSound.play();
                this._shopArray.reverse();
                for (var v in this._shopArray) {
                    this._shopArray[v].inputEnabled = false;
                    tween = this._game.add.tween(this._shopArray[v]);
                    tween.to({ x: this._game.width + this._shopArray[v].width }, 500, 'Quart.easeOut', true, i * 25);
                    i++;
                }
                this._shopArray.reverse();
                this._mask.inputEnabled = false;
            }
            enableShopMenu() {
                var i = 0;
                var tween;
                this.AddEventListeners();
                this.ChangeActiveButtons();
                for (var v in this._shopArray) {
                    this._shopArray[v].inputEnabled = true;
                    tween = this._game.add.tween(this._shopArray[v]);
                    tween.to({ x: this._shopPositionArray[v] }, 500, 'Quart.easeOut', true, i * 40);
                    i++;
                }
                this._mask.inputEnabled = true;
            }
            OnShopButtonDown(index) {
                this._shopItems[this._selectedShopButton].DeselectButton();
                this._selectedShopButton = index;
                this._shopItems[this._selectedShopButton].SelectButton();
                this.ChangeActiveButtons();
                if (index >= ProeveVanBekwaamheid.statics.Inventory.SKINS.length) {
                    this._shopItems[index].UpdatePrice();
                }
                this.StartDrag();
            }
            OnShopButtonUp(index) {
                this.StopDrag();
            }
            showAdsButton() {
            }
            startAddsButton() {
                this._adsButton.alpha = 0;
                this._adsButton.inputEnabled = false;
                this.dissableShopMenu();
                this._menuState.startAdd();
            }
            createShop() {
                var selectedSkin = ProeveVanBekwaamheid.statics.Inventory.SelectedSkin;
                this._shopLogo = this._menuState.CreateUi.createImage(this._game.width / 2, this._game.height / 12.5, 'shopMenu', 'ShopLogo', 0.5, 0);
                this._shopCoinBorder = this._menuState.CreateUi.createImage(this._game.width / 2, this._game.height / 4, 'shopMenu', 'CoinCountBorder', 0.5, 0.5);
                this._objectBorder = this._menuState.CreateUi.createImage(this._game.width / 2, this._game.height / 10 * 3.25, 'shopMenu', 'ChoiceBorder', 0.5, 0);
                this._objectBorder.height = this._objectBorder.height *= 0.8;
                this._buttonBorder = this._menuState.CreateUi.createImage(this._game.width / 2, this._game.height / 10 * 9, 'shopMenu', 'ButtonHolder', 0.5, 0.5);
                this._shopButtonBackToMenu = this._menuState.CreateUi.createButton(0, 0, 'shopMenu', this._menuState.mainMenu, 'BackButton', 'BackButton', this._menuState, 0, 0);
                this._mask = this._menuState.add.graphics(0, 0);
                this._mask.beginFill(0x000000, 0);
                this._mask.drawRoundedRect(this._objectBorder.x - this._objectBorder.width / 2, this._objectBorder.y, this._objectBorder.width, this._objectBorder.height, 8);
                this._adsButton = this._menuState.CreateUi.createButton(this._shopLogo.width * 1.5, this._shopLogo.y + this._shopLogo.height / 2, 'adsButton', this.startAddsButton, 'adsButton', 'adsButton', this, 0.5, 0.5);
                this._adsButton.scale.x = this._adsButton.scale.y = this._menuState.CreateUi.GeneralScale / 2;
                this._mask.events.onInputDown.add(this.StartDrag, this);
                this._mask.events.onInputUp.add(this.StopDrag, this);
                this._shopItems = [];
                var powerupsAmount = ProeveVanBekwaamheid.statics.Inventory.POWERUPS.length;
                var skinsAmount = ProeveVanBekwaamheid.statics.Inventory.SKINS.length;
                var itemsAmount = skinsAmount + powerupsAmount;
                var columns = Math.ceil(itemsAmount / 3);
                var rows = Math.ceil(itemsAmount / columns);
                var index = 0;
                for (var column = 0; column < columns; column++) {
                    for (var row = 0; row < rows; row++) {
                        if (index >= itemsAmount)
                            break;
                        var startX = this._objectBorder.x + 90 - this._objectBorder.width / 2;
                        var startY = this._objectBorder.y + 90;
                        var xPos = startX + (150 * row);
                        var yPos = startY + (150 * column);
                        var newItemButton = new ProeveVanBekwaamheid.UI.ShopButton(this._game, xPos, yPos, 'shopMenu', null, null, index, 'Choice', 'Choice', 'Choice');
                        newItemButton.events.onInputDown.add(newItemButton.OnClick, newItemButton);
                        newItemButton = this._menuState.add.existing(newItemButton);
                        newItemButton.anchor.x = 0.5;
                        newItemButton.anchor.y = 0.5;
                        newItemButton.scale.setTo(this._menuState.CreateUi.GeneralScale);
                        newItemButton.SavedScale = newItemButton.scale;
                        newItemButton.mask = this._mask;
                        this._shopItems.push(newItemButton);
                        index++;
                    }
                }
                this._buyButton = this._menuState.CreateUi.createButton(this._buttonBorder.x - this._buttonBorder.width / 4, this._buttonBorder.y, 'shopMenu', this.BuySelectedButton, 'BuyClick', 'BuyHover', this, 0.5, 0.5);
                this._applyButton = this._menuState.CreateUi.createButton(this._buttonBorder.x + this._buttonBorder.width / 5, this._buttonBorder.y, 'shopMenu', this.ApplyButton, 'Apply', 'ApplyClick', this, 0.5, 0.5);
                this._shopArray = new Array();
                this._shopArray.push(this._shopLogo);
                this._shopArray.push(this._shopCoinBorder);
                this._shopArray.push(this._objectBorder);
                this._shopArray.push(this._buttonBorder);
                this._shopArray.push(this._buyButton);
                this._shopArray.push(this._applyButton);
                this._shopArray.push(this._shopButtonBackToMenu);
                this._shopArray.push(this._adsButton);
                for (var i = 0; i < itemsAmount; i++) {
                    this._shopArray.push(this._shopItems[i]);
                }
                var i = 0;
                var tween;
                this._shopPositionArray = new Array();
                for (var v in this._shopArray) {
                    this._shopPositionArray[v] = this._shopArray[v].x;
                    this._shopArray[v].x = this._game.width + this._shopArray[v].width;
                    i++;
                }
            }
            ChangeActiveButtons() {
                if (this._selectedShopButton < ProeveVanBekwaamheid.statics.Inventory.SKINS.length) {
                    this.CheckSelectedSkin();
                }
                else {
                    this.CheckSelectedUpgrade();
                }
            }
            AddEventListeners() {
                var length = this._shopItems.length;
                for (var i = 0; i < length; i++) {
                    this._shopItems[i].OnShopButtonPressedEventHandler.AddEventListener(this);
                }
            }
            RemoveEventListeners() {
                var length = this._shopItems.length;
                for (var i = 0; i < length; i++) {
                    this._shopItems[i].OnShopButtonPressedEventHandler.RemoveEventListener(this);
                }
            }
            BuySelectedButton() {
                if (this._selectedShopButton < ProeveVanBekwaamheid.statics.Inventory.SKINS.length) {
                    this.TryBuySkin();
                }
                else {
                    this.TryBuyPowerup();
                }
                State.MenuState.ButtonAudio.play();
            }
            TryBuySkin() {
                var selectedSkin = ProeveVanBekwaamheid.statics.Inventory.SKINS[this._selectedShopButton].SkinKey;
                if (ProeveVanBekwaamheid.statics.Inventory.HasSkin(selectedSkin)) {
                    return;
                }
                if (ProeveVanBekwaamheid.statics.Inventory.AttemptBuySkin(selectedSkin)) {
                    this._menuState.updateCurrencyDisplay();
                    ProeveVanBekwaamheid.statics.Inventory.SelectSkin(selectedSkin);
                    this.ChangeActiveButtons();
                    this._shopItems.forEach(function (item) {
                        item.UpdatePrice();
                    });
                }
                else {
                }
            }
            TryBuyPowerup() {
                var powerupIndex = this._selectedShopButton - ProeveVanBekwaamheid.statics.Inventory.SKINS.length;
                var selectedPowerup = ProeveVanBekwaamheid.statics.Inventory.POWERUPS[powerupIndex].PowerupKey;
                if (ProeveVanBekwaamheid.statics.Inventory.HasMaxUpgrade(selectedPowerup)) {
                    return;
                }
                if (ProeveVanBekwaamheid.statics.Inventory.AttemptBuyUpgrade(selectedPowerup)) {
                    this._menuState.updateCurrencyDisplay();
                    this.ChangeActiveButtons();
                }
                else {
                }
            }
            CheckSelectedSkin() {
                var selectedSkin = ProeveVanBekwaamheid.statics.Inventory.SKINS[this._selectedShopButton].SkinKey;
                if (ProeveVanBekwaamheid.statics.Inventory.HasSkin(selectedSkin)) {
                    this._buyButton.tint = 0x2f4f4f;
                    if (ProeveVanBekwaamheid.statics.Inventory.SelectedSkin.SkinKey === selectedSkin) {
                        this._applyButton.tint = 0x2f4f4f;
                    }
                    else {
                        this._applyButton.tint = 0xffffff;
                    }
                }
                else {
                    this._applyButton.tint = 0x2f4f4f;
                    this._buyButton.tint = 0xffffff;
                }
            }
            CheckSelectedUpgrade() {
                var upgradeIndex = this._selectedShopButton - ProeveVanBekwaamheid.statics.Inventory.SKINS.length;
                var selectedUpgrade = ProeveVanBekwaamheid.statics.Inventory.POWERUPS[upgradeIndex].PowerupKey;
                if (ProeveVanBekwaamheid.statics.Inventory.HasMaxUpgrade(selectedUpgrade)) {
                    this._buyButton.tint = 0x2f4f4f;
                    this._applyButton.tint = 0x2f4f4f;
                }
                else {
                    this._applyButton.tint = 0x2f4f4f;
                    this._buyButton.tint = 0xffffff;
                }
                this._shopItems[this._selectedShopButton].UpdatePrice();
            }
            StartDrag() {
                this._isDragging = true;
                this._dragStartMouseY = this.game.input.activePointer.position.y;
            }
            StopDrag() {
                this._isDragging = false;
                this._previousScrollProgress = this._scrollProgress;
            }
            ApplyButton() {
                var selectedSkin = ProeveVanBekwaamheid.statics.Inventory.SKINS[this._selectedShopButton].SkinKey;
                if (!ProeveVanBekwaamheid.statics.Inventory.HasSkin(selectedSkin)) {
                    return;
                }
                ProeveVanBekwaamheid.statics.Inventory.SelectSkin(selectedSkin);
                this._shopItems.forEach(function (item) {
                    item.UpdatePrice();
                });
                this.ChangeActiveButtons();
                State.MenuState.ButtonAudio.play();
            }
        }
        State.ShopMenu = ShopMenu;
    })(State = ProeveVanBekwaamheid.State || (ProeveVanBekwaamheid.State = {}));
})(ProeveVanBekwaamheid || (ProeveVanBekwaamheid = {}));
var ProeveVanBekwaamheid;
(function (ProeveVanBekwaamheid) {
    var UI;
    (function (UI) {
        class UiCreater {
            constructor(game, menuState) {
                this._game = game;
                this._menuState = menuState;
                this.setScale();
            }
            get GeneralScale() {
                return this._generalScale;
            }
            createImage(xPos, yPos, fileKey, nameKey, anchorX, anchorY) {
                var item;
                item = this._menuState.add.sprite(xPos, yPos, fileKey, nameKey);
                item.anchor.x = anchorX;
                item.anchor.y = anchorY;
                item.scale.setTo(this._generalScale);
                return item;
            }
            createButton(xPos, yPos, fileKey, callbackFunc, nameKey, nameKeyPressed, _this, anchorX, anchorY) {
                var item;
                item = this._menuState.add.button(xPos, yPos, fileKey, callbackFunc, _this, nameKey, nameKey, nameKeyPressed);
                item.anchor.x = anchorX;
                item.anchor.y = anchorY;
                item.scale.setTo(this._generalScale);
                return item;
            }
            createText(x, y, content, fontSize, fillColor, anchorX, anchorY) {
                var text = this._menuState.add.text(x, y, content, {
                    fill: fillColor,
                    fontSize: fontSize
                });
                text.anchor.x = anchorX;
                text.anchor.y = anchorY;
                text.scale.setTo(this._generalScale);
                return text;
            }
            setScale() {
                this._logo = this.createImage(this._game.width / 2, this._game.height / 10, 'mainMenu', 'Logo', 0.5, 0);
                this._logo.width = this._game.width * 0.8;
                this._generalScale = this._logo.scale.x;
                this._logo.scale = this._generalScale;
            }
        }
        UI.UiCreater = UiCreater;
    })(UI = ProeveVanBekwaamheid.UI || (ProeveVanBekwaamheid.UI = {}));
})(ProeveVanBekwaamheid || (ProeveVanBekwaamheid = {}));
var ProeveVanBekwaamheid;
(function (ProeveVanBekwaamheid) {
    var UI;
    (function (UI) {
        class ShopButton extends Phaser.Button {
            constructor(game, x, y, key, callback, callbackContext, inventoryID, overFrame, outFrame, downFrame, upFrame) {
                super(game, x, y, key, callback, callbackContext, overFrame, outFrame, downFrame, upFrame);
                this._savedScale = new Phaser.Point(1, 1);
                this._selectedSprite = this.game.add.sprite(0, 0, 'shopMenu', 'ChoiceHover');
                this._selectedSprite.anchor.set(0.5, 0.5);
                this.addChild(this._selectedSprite);
                this._selectedSprite.visible = false;
                this._startY = y;
                this._inventoryID = inventoryID;
                this._onShopButtonPressedEventHandler = new ProeveVanBekwaamheid.Events.EventHandlerHolder();
                this.DisplayPrice();
            }
            get InventoryID() {
                return this._inventoryID;
            }
            get PriceText() {
                return this._price;
            }
            get StartY() {
                return this._startY;
            }
            get OnShopButtonPressedEventHandler() {
                return this._onShopButtonPressedEventHandler;
            }
            set SavedScale(value) {
                this._savedScale.set(value.x, value.y);
            }
            OnClick() {
                ProeveVanBekwaamheid.State.MenuState.ButtonAudio.play();
                var eventHandlers = this._onShopButtonPressedEventHandler.EventHandlers;
                for (var i = 0; i < eventHandlers.length; i++) {
                    eventHandlers[i].OnShopButtonDown(this._inventoryID);
                }
            }
            SelectButton() {
                this.game.add.tween(this.scale).to({ x: this._savedScale.x * 1.25, y: this._savedScale.y * 1.25 }, 95).start();
                this._selectedSprite.visible = true;
                this._selectedSprite.alpha = 0.15;
                if (this._fadeTween) {
                    this._fadeTween.stop();
                }
                this._fadeTween = this.game.add.tween(this._selectedSprite);
                this._fadeTween.to({ alpha: 1 }, 500, Phaser.Easing.Linear.None, true, 0, -1, true);
            }
            DeselectButton() {
                this.game.add.tween(this.scale).to({ x: this._savedScale.x, y: this._savedScale.y }, 95).start();
                this._selectedSprite.visible = false;
            }
            UpdatePrice() {
                if (this._inventoryID < ProeveVanBekwaamheid.statics.Inventory.SKINS.length) {
                    let skin = ProeveVanBekwaamheid.statics.Inventory.SKINS[this._inventoryID];
                    if (ProeveVanBekwaamheid.statics.Inventory.HasSkin(skin.SkinKey)) {
                        if (ProeveVanBekwaamheid.statics.Inventory.SelectedSkin.SkinKey == skin.SkinKey) {
                            this._price.text = "APPLIED";
                            this._selectedSprite.tint = 0xfcfa94;
                            this.tint = 0xfcfa94;
                        }
                        else {
                            this._price.text = "BOUGHT";
                            this._selectedSprite.tint = 0x83838c;
                            this.tint = 0x83838c;
                        }
                    }
                    else if (skin.Price <= 0) {
                        this._price.text = 'FREE';
                        this._selectedSprite.tint = 0xffffff;
                        this.tint = 0xffffff;
                    }
                    else {
                        this._price.text = skin.Price.toString();
                        this._selectedSprite.tint = 0xffffff;
                        this.tint = 0xffffff;
                    }
                }
                else if (this._inventoryID >= ProeveVanBekwaamheid.statics.Inventory.SKINS.length) {
                    var powerupIndex = this._inventoryID - ProeveVanBekwaamheid.statics.Inventory.SKINS.length;
                    if (ProeveVanBekwaamheid.statics.Inventory.HasMaxUpgrade(ProeveVanBekwaamheid.statics.Inventory.POWERUPS[powerupIndex].PowerupKey)) {
                        this._price.text = "MAX";
                    }
                    else {
                        this._price.text = ProeveVanBekwaamheid.statics.Inventory.POWERUPS[powerupIndex].CurrentPrice.toString();
                    }
                }
            }
            DisplayPrice() {
                var border = this.game.add.sprite(0, 0, 'shopMenu', 'ChoicePriceBorder');
                border.anchor.set(0.5, 0.5);
                var text = this.game.add.text(0, 0, '', {
                    fill: '#fff',
                    fontSize: 26,
                    stroke: 0x000000,
                    strokeThickness: 3
                });
                this._price = this.game.add.text(0, 0, '', {
                    fill: '#fff',
                    fontSize: 32,
                    stroke: 0x000000,
                    strokeThickness: 3
                });
                if (this._inventoryID < ProeveVanBekwaamheid.statics.Inventory.SKINS.length) {
                    text.text = ProeveVanBekwaamheid.statics.Inventory.SKINS[this._inventoryID].SkinKey;
                    var icon = this.game.add.sprite(0, 0, ProeveVanBekwaamheid.statics.Inventory.SKINS[this._inventoryID].SkinKey + '-icon');
                    icon.anchor.set(0.5);
                    icon.scale.set(2.25);
                    this.addChild(icon);
                }
                else {
                    var powerupIndex = this._inventoryID - ProeveVanBekwaamheid.statics.Inventory.SKINS.length;
                    var powerupKey = ProeveVanBekwaamheid.statics.Inventory.POWERUPS[powerupIndex].PowerupKey;
                    text.text = powerupKey;
                    var icon = this.game.add.sprite(0, 0, ProeveVanBekwaamheid.statics.Inventory.POWERUPS[powerupIndex].Icon);
                    icon.anchor.set(0.5, 0.5);
                    this.addChild(icon);
                }
                this.UpdatePrice();
                text.anchor.set(0.5, 0.5);
                this._price.anchor.set(0.5, 0.5);
                text.y -= 55;
                text.x -= 0;
                this._price.y += 58;
                this._price.x -= 0;
                this.addChild(border);
                this.addChild(text);
                this.addChild(this._price);
            }
        }
        UI.ShopButton = ShopButton;
    })(UI = ProeveVanBekwaamheid.UI || (ProeveVanBekwaamheid.UI = {}));
})(ProeveVanBekwaamheid || (ProeveVanBekwaamheid = {}));
var ProeveVanBekwaamheid;
(function (ProeveVanBekwaamheid) {
    var UI;
    (function (UI) {
        class UIButton extends UI.UIElement {
            constructor(game, x, y, key, frame = null, onClickFrame = null) {
                super(game, x, y, null);
                this._button = game.add.button(x, y, key, this.OnButtonPressed, this, frame, onClickFrame, frame, onClickFrame);
            }
            get ButtonObject() {
                return this._button;
            }
            SetCallback(callback, context) {
                this._onPressedCallback = callback;
                this._onPressedContext = context;
            }
            SetVisibility(visible) {
                this._button.visible = visible;
            }
            Translate(x, y) {
                this._button.x += x;
                this._button.y += y;
            }
            MoveTo(x, y) {
                this._button.x = x;
                this._button.y = y;
            }
            GetRenderObject() {
                return this._button;
            }
            OnButtonPressed() {
                ProeveVanBekwaamheid.State.MenuState.ButtonAudio.play();
                if (this._onPressedCallback != null && this._onPressedContext != null) {
                    this._onPressedCallback.call(this._onPressedContext);
                }
            }
        }
        UI.UIButton = UIButton;
    })(UI = ProeveVanBekwaamheid.UI || (ProeveVanBekwaamheid.UI = {}));
})(ProeveVanBekwaamheid || (ProeveVanBekwaamheid = {}));
var ProeveVanBekwaamheid;
(function (ProeveVanBekwaamheid) {
    var UI;
    (function (UI) {
        class UICanvas extends ProeveVanBekwaamheid.Object.SharedObject {
            constructor(game, x, y) {
                super(game, x, y, null);
                this._groups = new Array();
            }
            get Groups() {
                return this._groups;
            }
            AddGroup(group) {
                this._groups.push(group);
                if (group.parent != null) {
                    group.parent.removeChild(group);
                }
                this.addChild(group);
                return group;
            }
            RemoveGroup(group) {
                let index = this._groups.indexOf(group);
                if (index >= 0) {
                    this._groups.splice(index);
                    if (group.parent = this) {
                        this.removeChild(group);
                    }
                }
                return group;
            }
            SetAlpha(alpha) {
                for (var i = 0; i < this._groups.length; i++) {
                    this._groups[i].SetAlpha(alpha);
                }
            }
            SetVisibility(visible) {
                for (var i = 0; i < this._groups.length; i++) {
                    this._groups[i].SetVisibility(visible);
                }
            }
            SetFullScreen() {
                this.width = this.game.width;
                this.height = this.game.height;
            }
            FindElement(tag) {
                for (var i = 0; i < this._groups.length; i++) {
                    var elements = this._groups[i].Elements;
                    for (var j = 0; j < elements.length; j++) {
                        if (elements[j].HasTag(tag)) {
                            return elements[j];
                        }
                    }
                }
                return null;
            }
        }
        UI.UICanvas = UICanvas;
    })(UI = ProeveVanBekwaamheid.UI || (ProeveVanBekwaamheid.UI = {}));
})(ProeveVanBekwaamheid || (ProeveVanBekwaamheid = {}));
var ProeveVanBekwaamheid;
(function (ProeveVanBekwaamheid) {
    var UI;
    (function (UI) {
        class UIElement extends ProeveVanBekwaamheid.Object.SharedObject {
            constructor(game, x, y, key = null, frame = null) {
                super(game, x, y, key, frame);
                this.AddTag('ui');
                game.add.existing(this);
            }
        }
        UI.UIElement = UIElement;
    })(UI = ProeveVanBekwaamheid.UI || (ProeveVanBekwaamheid.UI = {}));
})(ProeveVanBekwaamheid || (ProeveVanBekwaamheid = {}));
var ProeveVanBekwaamheid;
(function (ProeveVanBekwaamheid) {
    var UI;
    (function (UI) {
        class UIGroup extends ProeveVanBekwaamheid.Object.SharedObject {
            constructor(game, x, y) {
                super(game, x, y, null);
                this._uiElements = new Array();
                this._renderGroup = this.game.add.group();
            }
            get Elements() {
                return this._uiElements;
            }
            OnUpdate() {
                this.game.world.bringToTop(this._renderGroup);
            }
            AddElement(element) {
                this._uiElements.push(element);
                if (element.parent != null) {
                    element.parent.removeChild(element.GetRenderObject());
                }
                this.addChild(element.GetRenderObject());
                this._renderGroup.add(element.GetRenderObject());
                return element;
            }
            RemoveElement(element) {
                let index = this._uiElements.indexOf(element);
                if (index >= 0) {
                    this._uiElements.splice(index);
                    if (element.parent = this) {
                        this.removeChild(element);
                        this._renderGroup.remove(element);
                    }
                }
                return element;
            }
            SetAlpha(alpha) {
                for (var i = 0; i < this._uiElements.length; i++) {
                    this._uiElements[i].alpha = alpha;
                }
            }
            SetVisibility(visible) {
                for (var i = 0; i < this._uiElements.length; i++) {
                    this._uiElements[i].SetVisibility(visible);
                }
            }
            Translate(x, y) {
                for (var i = 0; i < this._uiElements.length; i++) {
                    this._uiElements[i].Translate(x, y);
                }
            }
            MoveTo(x, y) {
                for (var i = 0; i < this._uiElements.length; i++) {
                    this._uiElements[i].MoveTo(x, y);
                }
            }
            FindElement(tag) {
                for (var i = 0; i < this._uiElements.length; i++) {
                    if (this._uiElements[i].HasTag(tag)) {
                        return this._uiElements[i];
                    }
                }
                return null;
            }
        }
        UI.UIGroup = UIGroup;
    })(UI = ProeveVanBekwaamheid.UI || (ProeveVanBekwaamheid.UI = {}));
})(ProeveVanBekwaamheid || (ProeveVanBekwaamheid = {}));
var ProeveVanBekwaamheid;
(function (ProeveVanBekwaamheid) {
    var UI;
    (function (UI) {
        class UIImage extends UI.UIElement {
            constructor(game, x, y, key, frame = null) {
                super(game, x, y, key, frame);
                game.add.existing(this);
            }
            SetVisibility(visible) {
                this.visible = visible;
            }
            Translate(x, y) {
                this.x += x;
                this.y += y;
            }
            MoveTo(x, y) {
                this.x = x;
                this.y = y;
            }
            GetRenderObject() {
                return this;
            }
        }
        UI.UIImage = UIImage;
    })(UI = ProeveVanBekwaamheid.UI || (ProeveVanBekwaamheid.UI = {}));
})(ProeveVanBekwaamheid || (ProeveVanBekwaamheid = {}));
var ProeveVanBekwaamheid;
(function (ProeveVanBekwaamheid) {
    var UI;
    (function (UI) {
        class UIText extends UI.UIElement {
            constructor(game, x, y, text, fontSize = 32, fillColor = '#fff', outlineColor = '#000', outlineThickness = 6, horizontalAlignment = 'left') {
                super(game, x, y, null);
                this._text = game.add.text(x, y, text, {
                    fill: fillColor,
                    fontSize: fontSize,
                    stroke: outlineColor,
                    strokeThickness: outlineThickness,
                    boundsAlignH: horizontalAlignment
                });
            }
            set Text(value) {
                this._text.text = value;
            }
            get Text() {
                return this._text.text;
            }
            get TextObject() {
                return this._text;
            }
            OnUpdate() {
                this._text.bringToTop();
            }
            SetVisibility(visible) {
                this._text.visible = visible;
            }
            Translate(x, y) {
                this._text.x += x;
                this._text.y += y;
            }
            MoveTo(x, y) {
                this._text.x = x;
                this._text.y = y;
            }
            GetRenderObject() {
                return this._text;
            }
        }
        UI.UIText = UIText;
    })(UI = ProeveVanBekwaamheid.UI || (ProeveVanBekwaamheid.UI = {}));
})(ProeveVanBekwaamheid || (ProeveVanBekwaamheid = {}));
//# sourceMappingURL=game.js.map