# Tony vs the World#

## Beschrijving ##

 Dit is het project voor de proeve van bekwaamheid voor de klassen game-development & game-artist van 2017 aan het Mediacollege Amsterdam.
 De game draait in HTML5 en vereist geen plugin-ins of downloads om gespeeld te worden.
 We gebruiken Phaser en een game met Typescript te maken wat naar Javascript wordt vertaald.

## Links ##

* **Phaser:** [Phaser Homepage](https://phaser.io)
* **Google drive folder:** [Google Drive, Proeve van bekwaamheid](https://drive.google.com/drive/folders/0B22VQ6sxaPlCeDZCWFBEeEdDaXc)
* **Technisch ontwerp:** [Google Doc, Proeve van bekwaamheid Technisch design](https://docs.google.com/document/d/1y90GJ692HV_q60eQqUv40U_fktfuYkuF4VV8BrCsI8Q)
* **IDE document:** [Google Doc, IDE document](https://docs.google.com/document/d/1YijW9Lww6SbGN6eORqfqsYxYZSzXpsuFpCPaVZnmoC8/edit)

## Framework ##
 
 ![class-diagram](http://daanruiter.net/games/tonyvstheworld/docs/media/ClassDiagram.png)
 
 Het spel draait op een eigen framework dat om Phaser is heen gebouwd.
 Alle objecten die zich in de game view plaats vinden nemen de [SharedObject](./classes/_sharedobjects_sharedobject_.proevevanbekwaamheid.object.sharedobject.html) class over.
 Alle SharedObjects worden opgeslagen in een [SharedObjectCache](./classes/_sharedobjects_sharedobjectcache_.proevevanbekwaamheid.object.sharedobjectcache.html) opgeslagen.
 SharedObjects hebben tags waarmee er via de SharedObjectCache gezocht kan worden naar SharedObjects met bepaalde tags.
 Dingen die zich in de game wereld verplaatsen nemen de [Entity](./classes/_entities_entity_.proevevanbekwaamheid.object.entity.html) class over.
 Als het object een physics body nodig heeft, kan je de [PhysicsEntity](./classes/_entities_physicsentity_.proevevanbekwaamheid.object.physicsentity.html) klasse overnemen.

## Software & IDE ##

* **Game Engine:** Phaser
* **Code Editor:** Visual Studio
* **Versioning:** GIT, Bitbucket, SourceTree
* **2D Art:** Photoshop, Flash
* **2D Animatie:** Spine, Flash

## Team ##
#### Art ####
* Cerys Hancock, **Lead**
* Bart Willemsen
* Gerben van de Bosch
* Lotte Hageman
* Luuc Veenker

#### Programming ####
* Menno Jongejan, **Lead**
* Koen van der Velden
* Daan Ruiter