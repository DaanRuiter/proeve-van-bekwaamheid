﻿module ProeveVanBekwaamheid.Object
{
    /**
     * List of all SharedObjects with current game.
     * Can be used to get references to objets with certain tags or types.
     */
    export class SharedObjectCache
    {
       /**
        * All currently cached SharedObjects.
        */
        public get CachedObjects(): SharedObject[]
        {
            return this._cache;
        }

        private _cache: SharedObject[];

        constructor()
        {
            this._cache = new Array<SharedObject>();
        }

        /**
         * Add an object to the cache.
         * @param object Object to add.
         */
        public CacheObject(object: SharedObject)
        {
            if (this._cache.indexOf(object) == -1)
            {
                this._cache.push(object);
            }
        }

        /**
         * Remove an object from the cache.
         * @param object Object to remove.
         */
        public UnCacheObject(object: SharedObject)
        {
            var index = this._cache.indexOf(object)
            if (index > -1)
            {
                this._cache.splice(index, 1);
            }
        }

        /**
         * Removes an object with given id.
         * @param id Id to be used to identify object to remove.
         */
        public UnCacheObjectWithId(id: number)
        {
            var foundObject = this.FindObjectWithId(id);

            if (foundObject != null)
            {
                this.UnCacheObject(foundObject);
            }
        }

        /**
         * Returns the cached object with given id.
         * Returns null if no object with given id is cached.
         * @param id Id to be used to identify object to return.
         */
        public FindObjectWithId(id: number): SharedObject
        {
            for (var i = 0; i < this._cache.length; i++)
            {
                if (this._cache[i].Id == id)
                {
                    return this._cache[i];
                }
            }

            return null;
        }

        /**
         * Returns the first cached object that has the given tag.
         * Returns null if no object with given tag is cached.
         * @param tag Tag used to identify object to return.
         */
        public FindObjectWithTag(tag: string): SharedObject
        {
            for (var i = 0; i < this._cache.length; i++)
            {
                if (this._cache[i].HasTag(tag))
                {
                    return this._cache[i];
                }
            }

            return null;
        }

        /**
         * Returns all cached obhects that have the given tag.
         * @param tag Tag used to identify objects to return.
         */
        public FindObjectsWithTag(tag: string): SharedObject[]
        {
            let result = new Array<SharedObject>();

            for (var i = 0; i < this._cache.length; i++)
            {
                if (this._cache[i].HasTag(tag))
                {
                    result.push(this._cache[i]);
                }
            }

            return result;
        }
    }
}