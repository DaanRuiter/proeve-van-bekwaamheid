﻿module ProeveVanBekwaamheid.Object
{
   /**
    * Eventhandler used for a sharedObjects destory event.
    */
    export interface IEntityDestroyedEventHandler
    {
        OnDestroyed();
    }

    /**
     * A SharedObject is a an object whos reference can be found at any time by any other SharedObjects.
     */
    export abstract class SharedObject extends Phaser.Sprite
    {
        /**
         * Returns if the used browsers supports HTML5 LocalStorage.
         */
        public static get IsLocalStorageSupported(): boolean
        {
            return typeof (Storage) !== "undefined";
        }

       /**
        * The unique ID of this shared object.
        */
        public get Id(): number
        {
            return this._id;
        }

       /**
        * The EventHandlerHolder for the event when this sharedobject is destroyed using the Destory() method.
        */
        public get OnDestroyedEvent(): Events.EventHandlerHolder<IEntityDestroyedEventHandler>
        {
            return this._onDestroyedEventHandler;
        }

       /**
        * Alias for Client.GameEngine.SharedObjectCache.
        */
        protected get objectCache(): SharedObjectCache
        {
            return Client.GameEngine.SharedObjectCache;
        }

       /**
        * Gets all tags of this SharedObject.
        */
        public get Tags(): string[]
        {
            return this._tags;
        }

        private static _lastUsedId = -1;
        private _tags: string[];
        private _id: number;
        private _onDestroyedEventHandler: Events.EventHandlerHolder<IEntityDestroyedEventHandler>;

        /**
         * @param game Reference to Phaser game used internally.
         * @param x X position of the object on the screen.
         * @param y Y position of the object on the screen.
         * @param key Key of the image in the phaser cache. Optional.
         * @param frame Frame of image in spritesheet. Optional.
         */
        constructor(game: Phaser.Game, x: number, y: number, key?: string | Phaser.RenderTexture | Phaser.BitmapData | PIXI.Texture, frame?: string | number)
        {
            super(game, x, y, key, frame);

            this._id = SharedObject.GeneratedId();
            this._tags = new Array<string>();
            this._onDestroyedEventHandler = new Events.EventHandlerHolder<IEntityDestroyedEventHandler>();

            Client.GameEngine.SharedObjectCache.CacheObject(this);
        }

        /**
         * Add or set a value in the local storage.
         * Will silently fail if LocalStorage is not supported by the browser.
         * @param key Key of the item in the LocalStorage.
         * @param value Value to store, will be serialized as a string.
         */
        public static SetLocalStorageItem(key: string, value: any): void
        {
            if (!this.IsLocalStorageSupported) { return; }

            localStorage.setItem(key, value);
        }

        /**
         * Add a value in the list with the given key in the local storage.
         * @param key Key of the list in the localStorage.
         * @param value Value to add to the list, will be serialized as a string.
         */
        public static AddLocalStorageListedItem(key: string, value: any): void
        {
            if (!this.IsLocalStorageSupported) { return; }
            if (!this.HasLocalStorageItem(key)) { this.AddLocalStorageListedItemEntry(key); }

            var list: string[] = this.GetLocalStorageListedItem(key);
            list.push(value);

            localStorage.setItem(key, JSON.stringify(list));
        }

        /**
         * Creates a emtpy string array and stores it into the LocalStorage.
         * @param key Key of the list in the localStorage.
         */
        public static AddLocalStorageListedItemEntry(key: string): void
        {
            if (!this.IsLocalStorageSupported) { return; }
            if (this.HasLocalStorageItem(key)) { return; }

            localStorage.setItem(key, JSON.stringify(new Array<string>()));
        }

        /**
         * Returns the value of a localy stored item.
         * @param key Key of the item in the LocalStorage.
         */
        public static GetLocalStorageItem(key: string): any
        {
            if (!this.IsLocalStorageSupported) { return null; }

            return localStorage.getItem(key);
        }

        /**
         * Returns an item from the LocalStorage as an array of strings.
         * @param key Key of the list in the localStorage.
         */
        public static GetLocalStorageListedItem(key: string): string[]
        {
            var result: string[] = [];

            if (!this.IsLocalStorageSupported) { return result; }
            if (!this.HasLocalStorageItem(key)) { return result; }

            result = JSON.parse(localStorage.getItem(key));

            return result;
        }

        /**
         * Removes an item from the LocalStorage.
         * @param key Key of the item in the LocalStorage.
         */
        public static RemoveLocalStorageItem(key: string): void
        {
            if (!this.IsLocalStorageSupported) { return; }

            localStorage.removeItem(key);
        }

        /**
         * Removes an item from an array in the LocalStorage.
         * @param key Key of the list in the LocalStorage.
         * @param value Value to remove from the list.
         * @param stopAtFirstEntry When true, will stop after finding a matching value. Will enumerate through the entire array and delete any matching value otherwise.
         */
        public static RemoveLocalStorageListedItem(key: string, value: string, stopAtFirstEntry: boolean = false): void
        {
            var list = this.GetLocalStorageListedItem(key);

            for (let i = list.length - 1; i >= 0; i--)
            {
                if (list[i] == value)
                {
                    list.splice(i, 1);

                    if (stopAtFirstEntry) { break; }
                }
            }

            localStorage.setItem(key, JSON.stringify(list));
        }

        /**
         * Returns whether an item with the given key is present in the LocalStorage.
         * @param key Key of the item in the LocalStorage.
         */
        public static HasLocalStorageItem(key: string): boolean
        {
            if (!this.IsLocalStorageSupported) { return false; }

            return localStorage.getItem(key) != null;
        }

        /**
         * Returns if the id of the given SharedObjects matches that of this SharedObject.
         * @param other SharedObject of whom's id to compare to.
         */
        public Equals(other: SharedObject): boolean
        {
            return this._id == other.Id;
        }

        /**
         * Executed each frame.
         * Called internally.
         */
        public OnUpdate(): void { }

        /**
         * Destroys this object and removes it from the SharedObjectCache in the GameEngine
         * Also calls all OnDestroyEventHandlers on this SharedObject.
         */
        public Destroy(): void
        {
            var eventHandlers = this._onDestroyedEventHandler.EventHandlers;
            for (var i = 0; i < eventHandlers.length; i++)
            {
                eventHandlers[i].OnDestroyed();
            }

            Client.GameEngine.SharedObjectCache.UnCacheObject(this);

            super.destroy();
        }

        /**
         * Add a tag to this SharedObject which can be used to find other SharedObjects with certain tags.
         * Will silently fail if tag is already present on this SharedObject.
         * @param tag Tag to add.
         */
        public AddTag(tag: string): void
        {
            if (!this.HasTag(tag))
            {
                this._tags.push(tag);
            }
        }

        /**
         * Remove a tag from this SharedObject.
         * Will silently fail if tag is not present on this SharedObject.
         * @param tag Tagg to remove.
         */
        public RemoveTag(tag: string): void
        {
            let index = this._tags.indexOf(tag);

            if (index >= 0)
            {
                this._tags.splice(index);
            }
        }

        /**
         * Returns if the given tag is present on this SharedObject.
         * @param tag Tag to search for.
         */
        public HasTag(tag: string): boolean
        {
            return this._tags.indexOf(tag) >= 0;
        }

        private static GeneratedId(): number
        {
            return ++this._lastUsedId;
        }
    }
}