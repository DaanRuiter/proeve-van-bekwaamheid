﻿module ProeveVanBekwaamheid.Factories
{
    /**
     * Factory class for keypoints.
     */
    export class EnemyFactory
    {
        private static get STATIC_ENEMIES(): string[]
        {
            return ["Enemy-Static-1"];
        }

        private static get MOVING_ENEMIES(): string[]
        {
            return ["Enemy-Moving-1"];
        }

        /**
         * Creates a static enemy on given position.
         * @param game client.
         * @param player player to interact with.
         * @param position position of enemy.
         */
        public static CreateStaticEnemy(game: Client.GameEngine, x: number, y: number): Actors.Enemy
        {
            var enemy: Actors.Enemy;
            var randomEnemy: string;
            randomEnemy = EnemyFactory.STATIC_ENEMIES[game.rnd.integerInRange(0, EnemyFactory.STATIC_ENEMIES.length - 1)];

            enemy = game.CurrentState.InstantiateEntity(Actors.Enemy, x, y, randomEnemy);
            enemy.position = new Phaser.Point(x, y);
            enemy.isHovering = true;
            enemy.scale.set(1.5, 1.5);

            (game.CurrentState as State.GameState).Enemies.push(enemy);

            (game.CurrentState as State.GameState).ObjectScroller.ScrollingObjects.push(enemy);

            return enemy;
        }

        /**
         * Creates a moving enemy on given position.
         * @param game client.
         * @param player player to interact with.
         * @param position position of enemy.
         */
        public static CreateMoveEnemy(game: Client.GameEngine, x: number, y: number): Actors.Enemy
        {
            var enemy: Actors.Enemy;
            var randomEnemy: string;
            randomEnemy = EnemyFactory.MOVING_ENEMIES[game.rnd.integerInRange(0, EnemyFactory.MOVING_ENEMIES.length - 1)];

            enemy = game.CurrentState.InstantiateEntity(Actors.Enemy, x, y, randomEnemy);
            enemy.position = new Phaser.Point(x, y);
            enemy.isMoving = true;

            (game.CurrentState as State.GameState).Enemies.push(enemy);

            (game.CurrentState as State.GameState).ObjectScroller.ScrollingObjects.push(enemy);

            return enemy;
        }

        /**
         * Creates a laser beam on given position.
         * @param game client.
         * @param x x position.
         * @param y y position.
         */
        public static CreateLaserBeam(game: Client.GameEngine, x: number, y: number): Actors.LaserBeam
        {
            var laserBeam: Actors.LaserBeam;

            laserBeam = game.CurrentState.InstantiateEntity(Actors.LaserBeam, x, y, 'laser-beam');
            laserBeam.position = new Phaser.Point(0, y);
            laserBeam.StartPosition.set(0, y + game.rnd.integerInRange(-100, 100));
            laserBeam.EndPosition.set(game.width, y + game.rnd.integerInRange(-100, 100));
            laserBeam.CalculateScaleAndAngle();

            (game.CurrentState as State.GameState).LaserBeams.push(laserBeam);

            (game.CurrentState as State.GameState).ObjectScroller.ScrollingObjects.push(laserBeam);

            return laserBeam;
        }
    }
}