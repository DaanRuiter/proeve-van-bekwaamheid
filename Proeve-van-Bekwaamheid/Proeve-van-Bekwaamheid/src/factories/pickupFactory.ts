﻿module ProeveVanBekwaamheid.Factories
{
    export class PickupFactory
    {
        private static get GUN_POWERUP(): string
        {
            return "Gun-Powerup";
        }
        private static get JETPACK_POWERUP(): string
        {
            return "Jetpack-Powerup";
        }
        private static get PLATFORM_POWERUP(): string
        {
            return "Platform-Powerup";
        }
        private static get SHIELD_POWERUP(): string
        {
            return "Shield-Powerup";
        }
        private static get MAGNET_POWERUP(): string
        {
            return "Magnet-Powerup";
        }

        private static get PICKUPS(): string[]
        {
            return [PickupFactory.GUN_POWERUP, PickupFactory.JETPACK_POWERUP, PickupFactory.PLATFORM_POWERUP, PickupFactory.SHIELD_POWERUP, PickupFactory.MAGNET_POWERUP];
        }

        /**
         * Creates a random pickup on given position.
         * @param game client.
         * @param player player to interact with.
         * @param position position of pickup.
         */
        public static CreateRandomPickup(game: Client.GameEngine, x: number, y: number): PickUp.PickUpObject
        {
            var pickup: PickUp.PickUpObject;
            var randomPickup: string;
            randomPickup = PickupFactory.PICKUPS[game.rnd.integerInRange(0, PickupFactory.PICKUPS.length - 1)];

            switch (randomPickup)
            {
                case PickupFactory.GUN_POWERUP:
                    pickup = game.CurrentState.InstantiateEntity(PickUp.GunPowerUp, x, y, randomPickup);
                    break;
                case PickupFactory.JETPACK_POWERUP:
                    pickup = game.CurrentState.InstantiateEntity(PickUp.JetpackPowerUp, x, y, randomPickup);
                    break;
                case PickupFactory.SHIELD_POWERUP:
                    pickup = game.CurrentState.InstantiateEntity(PickUp.ShieldPowerUp, x, y, randomPickup);
                    break;
                case PickupFactory.PLATFORM_POWERUP:
                    pickup = game.CurrentState.InstantiateEntity(PickUp.PlatformPowerUp, x, y, randomPickup);
                    break;
                case PickupFactory.MAGNET_POWERUP:
                    pickup = game.CurrentState.InstantiateEntity(PickUp.MagnetPowerUp, x, y, randomPickup);
                    break;
                default:
                    pickup = game.CurrentState.InstantiateEntity(PickUp.GunPowerUp, x, y, randomPickup);
            }

            
            pickup.position = new Phaser.Point(x, y);
            pickup.scale.x = 0.75;
            pickup.scale.y = 0.75;
            pickup.anchor.set(0.5, 0.5);

            (game.CurrentState as State.GameState).Pickups.push(pickup);

            // The keypoints should always scroll with the game.
            (game.CurrentState as State.GameState).ObjectScroller.ScrollingObjects.push(pickup);

            return pickup;
        }


        /**
         * Creates a coin on given position.
         * @param game client.
         * @param player player to interact with.
         * @param position position of coin.
         */
        public static CreateCoin(game: Client.GameEngine, x: number, y: number): PickUp.Coin
        {
            var coin = (game.CurrentState as State.GameState).InstantiateEntity(PickUp.Coin, x, y, "mainMenu", "CoinTurn-1");

            if ((game.CurrentState as State.GameState).Player.MagnetEnabled)
            {
                coin.SetCircleRadius(PickUp.MagnetPowerUp.MAGNET_PULL_STRENGTH);
            }

            (game.CurrentState as State.GameState).Coins.push(coin);
            (game.CurrentState as State.GameState).ObjectScroller.ScrollingObjects.push(coin);

            return coin;
        }
    }
        
}