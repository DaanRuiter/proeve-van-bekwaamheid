﻿module ProeveVanBekwaamheid.statics
{
    export abstract class BuyableItem 
    {
        public get UpgradeText(): string
        {
            return this.upgradeText;
        }

        protected upgradeText: string;

        constructor(upgradeText: string)
        {
            this.upgradeText = upgradeText;
        }

        public abstract GetPrice(): number;
    }

    export class PlayerSkin extends BuyableItem
    {
        public static get OWNED_PLAYER_SKINS_LOCAL_STORAGE_KEY(): string
        {
            return 'owned-player-skins';
        }

        public static get SELECTED_PLAYER_SKIN_LOCAL_STORAGE_KEY(): string
        {
            return 'selected-player-skin';
        }

        public static get PLAYER_SKIN_DEFAULT_KEY(): string
        {
            return 'Tony';
        }

        public SkinKey: string;
        public Price: number;
        public SpriteFolderName: string;

        constructor(skinKey: string, price: number, spriteFolderName: string, upgradeText: string)
        {
            super(upgradeText);
            this.SkinKey = skinKey;
            this.Price = price;
            this.SpriteFolderName = spriteFolderName;
        }

        public GetPrice(): number
        {
            return this.Price;
        }
    }

    export class PowerupUpgrade extends BuyableItem
    {
        public static get OWNED_PLAYER_SKINS_LOCAL_STORAGE_KEY(): string
        {
            return 'owned-powerups';
        }

        public static get POWERUP_MAGNET_KEY(): string
        {
            return 'Magnet';
        }

        public static get POWERUP_JETPACK_KEY(): string
        {
            return 'Jetpack';
        }
        public static get POWERUP_PLATFORM_KEY(): string
        {
            return 'Platform';
        }
        public static get POWERUP_SHIELD_KEY(): string
        {
            return 'Shield';
        }
        public static get POWERUP_GUN_KEY(): string
        {
            return 'Gun';
        }

        public get CurrentModifier(): number
        {
            if (Object.SharedObject.GetLocalStorageItem(this.PowerupKey) != 0)
                return 1 + this.Modifier * Object.SharedObject.GetLocalStorageItem(this.PowerupKey);

            return 1;
        }
        
        public PowerupKey: string;
        public Price: number;
        public Modifier: number;
        public MaxAmountModifiers: number;
        public Icon: string;

        constructor(powerupKey: string, price: number, modifier: number, maxModifiers: number, icon: string, upgradeText: string)
        {
            super(upgradeText);
            this.PowerupKey = powerupKey;
            this.Price = price;
            this.Modifier = modifier;
            this.MaxAmountModifiers = maxModifiers;
            this.Icon = icon;
        }

        public GetPrice(): number
        {
            if (Object.SharedObject.GetLocalStorageItem(this.PowerupKey) != 0)
                return this.Price * (Number(Object.SharedObject.GetLocalStorageItem(this.PowerupKey)) + 1);

            return this.Price;
        }
    }

    export class Inventory
    {
        public static get SKINS(): PlayerSkin[]
        {
            return [
                new PlayerSkin(PlayerSkin.PLAYER_SKIN_DEFAULT_KEY, 0, 'default', "The default skin for Tony."),
                new PlayerSkin('Stony', 1000, 'stony', "Unlock this rock-solid stone skin for Tony."),
                new PlayerSkin('Barrel', 2000, 'barrel', "Unlock this well rounded barrel skin for Tony."),
                new PlayerSkin('Catbug', 2000, 'catbug', "Unlock this adorable catbug skin for Tony."),
                new PlayerSkin('Caveman', 2500, 'caveman', "Unlock this ancient caveman skin for Tony."),
                new PlayerSkin('Pumpkin', 3000, 'pumpkin', "Unlock this scary pumpkin skin for Tony."),
                new PlayerSkin('Chewbacca', 3000, 'chewbacca', "Unlock this kick-ass Chewbacca skin for Tony."),
                new PlayerSkin('Punk', 3500, 'punk', "Unlock this rocking punker skin for Tony."),
                new PlayerSkin('Bomb King', 4000, 'bomb-king', "Unlock this explosive bomb king skin for Tony."),
                new PlayerSkin('Skeleton', 4500, 'skeleton', "Unlock this spooky skeletal skin for Tony."),
                new PlayerSkin('President', 5000, 'president', "Unlock this presidental skin for Tony.")
            ];
        }

        public static get POWERUPS(): PowerupUpgrade[]
        {
            return [
                new PowerupUpgrade(PowerupUpgrade.POWERUP_JETPACK_KEY, 500, 0.25, 3, 'Jetpack-Icon', "Increase the duration of the jetpack powerup."),
                new PowerupUpgrade(PowerupUpgrade.POWERUP_GUN_KEY, 500, 0.25, 3, 'Gun-Icon', "Increase the duration of the gun powerup."),
                new PowerupUpgrade(PowerupUpgrade.POWERUP_MAGNET_KEY, 500, 0.25, 3, 'Magnet-Icon', "Increase the duration of the magnet powerup."),
                new PowerupUpgrade(PowerupUpgrade.POWERUP_PLATFORM_KEY, 500, 0.25, 3, 'Platform-Icon', "Increase the duration of the platform powerup."),
                new PowerupUpgrade(PowerupUpgrade.POWERUP_SHIELD_KEY, 500, 0.25, 3, 'Shield-Icon', "Increase the duration of this shield powerup.")
            ];
        }

       /**
        * The key of the player's coin amount item in the LocalStorage.
        */
        public static get COINS_STORAGE_KEY(): string
        {
            return 'coins';
        }

       /**
        * The amount of coins the player has earned in the current or previous session(s).
        */
        public static get CoinsOwned(): number
        {
            if (!Inventory._coinsLoadedThisSessions)
            {
                if (!Object.SharedObject.HasLocalStorageItem(Inventory.COINS_STORAGE_KEY))
                {
                    Object.SharedObject.SetLocalStorageItem(Inventory.COINS_STORAGE_KEY, '0');

                    Inventory._coinsOwned = 0;
                } else
                {
                    Inventory._coinsOwned = Number(Object.SharedObject.GetLocalStorageItem(Inventory.COINS_STORAGE_KEY));
                }
            }

            return Inventory._coinsOwned;
        }
        public static set CoinsOwned(value: number)
        {
            Inventory._coinsOwned = value;
            Object.SharedObject.SetLocalStorageItem(Inventory.COINS_STORAGE_KEY, Inventory._coinsOwned);
        }

        public static get SelectedSkin(): PlayerSkin
        {
            if (!Object.SharedObject.HasLocalStorageItem(PlayerSkin.SELECTED_PLAYER_SKIN_LOCAL_STORAGE_KEY))
            {
                Object.SharedObject.AddLocalStorageListedItem(PlayerSkin.OWNED_PLAYER_SKINS_LOCAL_STORAGE_KEY, PlayerSkin.PLAYER_SKIN_DEFAULT_KEY);
                Object.SharedObject.SetLocalStorageItem(PlayerSkin.SELECTED_PLAYER_SKIN_LOCAL_STORAGE_KEY, PlayerSkin.PLAYER_SKIN_DEFAULT_KEY);
            }

            var selectedSkinKey: string = Object.SharedObject.GetLocalStorageItem(PlayerSkin.SELECTED_PLAYER_SKIN_LOCAL_STORAGE_KEY);

            return Inventory.FindSkin(selectedSkinKey);
        }

        public static InitializeUpgrades(): void
        {
            for (var i: number = 0; i < Inventory.POWERUPS.length; i++)
            {
                if (!Object.SharedObject.HasLocalStorageItem(Inventory.POWERUPS[i].PowerupKey))
                {
                    Object.SharedObject.SetLocalStorageItem(Inventory.POWERUPS[i].PowerupKey, 0);
                }
            }
        }

        /**
         * Selects ingame skin.
         * @param skin skin as skinkey or as index of list SKINS.
         */
        public static SelectSkin(skin: string | number): void
        {
            var skinKey = "";
            if (typeof skin === 'string')
            {
                skinKey = skin;
            }
            else if (typeof skin === 'number')
            {
                skinKey = Inventory.SKINS[skin].SkinKey;
            }

            Object.SharedObject.SetLocalStorageItem(PlayerSkin.SELECTED_PLAYER_SKIN_LOCAL_STORAGE_KEY, skinKey);
        }

        public static HasSkin(skinKey: string): boolean
        {
            var ownedSkins: string[] = Object.SharedObject.GetLocalStorageListedItem(PlayerSkin.OWNED_PLAYER_SKINS_LOCAL_STORAGE_KEY);

            return ownedSkins.indexOf(skinKey) >= 0;
        }

        public static HasMaxUpgrade(upgradeKey: string): boolean
        {
            var amountUpgrades: number = Object.SharedObject.GetLocalStorageItem(upgradeKey);
            var upgrade: PowerupUpgrade = Inventory.FindUpgrade(upgradeKey);

            return (amountUpgrades == upgrade.MaxAmountModifiers);
        }

        public static AttemptBuySkin(skinKey: string): boolean
        {
            if (Inventory.HasSkin(skinKey)) { return true; }

            var skin: PlayerSkin = Inventory.FindSkin(skinKey);

            if (skin && skin.Price <= Inventory.CoinsOwned)
            {
                Object.SharedObject.AddLocalStorageListedItem(PlayerSkin.OWNED_PLAYER_SKINS_LOCAL_STORAGE_KEY, skinKey);
                Inventory.CoinsOwned -= skin.Price;
                return true;
            }

            return false;
        }

        public static AttemptBuyUpgrade(upgradeKey: string): boolean
        {
            if (Inventory.HasMaxUpgrade(upgradeKey)) { return true; }

            var upgrade: PowerupUpgrade = Inventory.FindUpgrade(upgradeKey);

            if (upgrade && upgrade.Price <= Inventory.CoinsOwned)
            {
                Inventory.CoinsOwned -= upgrade.GetPrice();

                var amountUpgrades: number = Object.SharedObject.GetLocalStorageItem(upgradeKey);
                amountUpgrades++;

                Object.SharedObject.SetLocalStorageItem(upgradeKey, amountUpgrades);

                return true;
            }

            return false;
        }

        public static FindSkin(skinKey: string): PlayerSkin
        {
            for (let i = 0; i < Inventory.SKINS.length; i++)
            {
                if (Inventory.SKINS[i].SkinKey == skinKey)
                {
                    return Inventory.SKINS[i];
                }
            }
            return Inventory.SKINS[0]; // returning default skin.
        }

        public static FindUpgrade(upgradeKey: string): PowerupUpgrade
        {
            for (let i = 0; i < Inventory.POWERUPS.length; i++)
            {
                if (Inventory.POWERUPS[i].PowerupKey == upgradeKey)
                {
                    return Inventory.POWERUPS[i];
                }
            }
            return null;
        }

        private static _coinsOwned: number = 0;
        private static _coinsLoadedThisSessions: boolean = false;
    }
}