﻿module ProeveVanBekwaamheid.State
{
    class NovelFrame
    {
        public get Frame(): Phaser.Image
        {
            return this._allImages[0];
        }

        public get Images(): Phaser.Image[]
        {
            return this._allImages;
        }

        public get IsCompleted(): boolean
        {
            return this._progress + 1 >= this._allImages.length;
        }

        private _progress: number = -1;
        private _allImages: Phaser.Image[];

        constructor(frame: Phaser.Image, texts: Phaser.Image[], speechBubbles: Phaser.Image[])
        {
            this._allImages = [frame];

            let allImages = this._allImages;
            texts.forEach(function (image) { allImages.push(image); });
            speechBubbles.forEach(function (image) { allImages.push(image); });
        }

        public GetNextImage(): Phaser.Image
        {
            if (this.IsCompleted) { return this._allImages[this._allImages.length - 1]; }

            return this._allImages[++this._progress];
        }
    }

    export class NovelState extends PhaserStateBase
    {
        public game: Client.GameEngine;

        private _frames: NovelFrame[];
        private _frameIndex: number;
        private _initialDelay: number;
        private _fadeDuration: number;
        private _fadetween: Phaser.Tween;
        private _fadingOut: boolean;
        private _tapInstructions: Phaser.Text;

        public preload(): void
        {            
            this.load.image('novel-frame-0', './assets/novel/novel_frame_0.png');
            this.load.image('novel-frame-1', './assets/novel/novel_frame_1.png');
            this.load.image('novel-frame-2', './assets/novel/novel_frame_2.png');
            this.load.image('novel-text-0', './assets/novel/novel_text_0.png');
            this.load.image('novel-text-1', './assets/novel/novel_text_1.png');
            this.load.image('novel-speech-0', './assets/novel/novel_speech_0.png');
            this.load.image('novel-speech-1', './assets/novel/novel_speech_1.png');
            this.load.image('novel-speech-2', './assets/novel/novel_speech_2.png');
            this.load.image('novel-speech-3', './assets/novel/novel_speech_3.png');
        }

        public create(): void
        {
            this.game.stage.backgroundColor = '#fff';

            var scale = 0.55;
            var xOffset = this.game.width / 2;
            var yOffset = this.game.height / 100 * 1.5;

            // Add comic images
            this._frames =
                [
                new NovelFrame(
                    this.game.add.image(0, yOffset, 'novel-frame-0'),
                    [this.game.add.image(0, yOffset, 'novel-text-0')],
                    [this.game.add.image(0, yOffset, 'novel-speech-0'), this.game.add.image(0, yOffset, 'novel-speech-1')]),

                new NovelFrame(
                    this.game.add.image(0, yOffset, 'novel-frame-1'),
                    [this.game.add.image(0, yOffset, 'novel-text-1')],
                    []),

                new NovelFrame(
                    this.game.add.image(0, yOffset, 'novel-frame-2'),
                    [],
                    [this.game.add.image(0, yOffset, 'novel-speech-2'), this.game.add.image(0, yOffset, 'novel-speech-3')])
                ];

            let x = this.game.width / 2;
            let combinedPreviousHeight = 0;
            this._frames.forEach(function (frame)
            {
                frame.Images.forEach(function (image)
                {
                    image.pivot.set(image.width / 2, 0);
                    image.scale.setTo(scale, scale);
                    image.alpha = 0;
                });

                frame.Frame.x = x;
                frame.Frame.y = yOffset + combinedPreviousHeight;

                combinedPreviousHeight += frame.Frame.height + yOffset;
            });


            let frameWidth = this._frames[0].Frame.width;
            let frameOffset = (this.game.width - frameWidth) / 2;

            // First frame
            let celebrated = this._frames[0].Images[1];
            celebrated.pivot.set(0, 0);
            celebrated.x = frameOffset;

            let happy = this._frames[0].Images[2];
            happy.pivot.set(happy.width / 2, 0);
            happy.position.set(this._frames[0].Frame.x + frameWidth / 2 - happy.width, this._frames[0].Frame.y);
            happy.y += 20;

            let thank = this._frames[0].Images[3];
            thank.pivot.set(0, thank.height);
            thank.x = thank.width;
            thank.y = yOffset + this._frames[0].Frame.height / 2;
            
            // Second frame
            let darkness = this._frames[1].Images[1];
            darkness.pivot.set(0, 0);
            darkness.x = frameWidth + frameOffset - darkness.width;
            darkness.y = this._frames[1].Frame.y;
            
            // Third frame
            let save = this._frames[2].Images[1];
            save.pivot.set(0, save.height / 2);
            save.x = save.width;
            save.y = this._frames[2].Frame.y + this._frames[2].Frame.height * 0.325;

            let kid = this._frames[2].Images[2];
            kid.pivot.set(0, kid.height / 2);
            kid.x = this.game.width / 2 + kid.width / 2;
            kid.y = this._frames[2].Frame.y + this._frames[2].Frame.height - kid.height;


            //  Add instructions text
            let instructionText: string;
            if (this.game.device.desktop)
            {
                instructionText = 'Click the screen to continue..';
            }
            else
            {
                instructionText = 'Tap the screen to continue..';
            }

            this._tapInstructions = this.game.add.text(0, 0, instructionText, { fill: '#000', stroke: '#000' });
            this._tapInstructions.alpha = 0;
            this._tapInstructions.x = this.game.width / 2 - this._tapInstructions.width / 2;
            this._tapInstructions.y = this.game.height - this._tapInstructions.height;

            this._frameIndex = 0;

            this._fadeDuration = Phaser.Timer.SECOND * 1.35;
            this._initialDelay = Phaser.Timer.SECOND * 1.25;

            this.FadeNextImage(this._initialDelay, this._initialDelay * 3);

            this.game.input.onDown.add(this.OnClick, this);
            this.game.camera.onFadeComplete.add(this.StartGameState, this);
        }

        private OnClick(): void
        {
            if (!this._fadingOut)
            {
                if (this._fadetween && this._fadetween.isRunning)
                {
                    (this._fadetween.target as Phaser.Image).alpha = 1;
                    this._fadetween.stop();
                }

                this.FadeNextImage(0, this._initialDelay * 4);
                var textTween = this.game.add.tween(this._tapInstructions);
                textTween.to({ alpha: 0 }, 150);
                textTween.start();
            }
        }

        private FadeNextImage(delay:number = 0, instructionsDelay:number = 0): void
        {
            if (this._frames[this._frameIndex].IsCompleted)
            {
                if (this._frameIndex + 1 >= this._frames.length)
                {
                    this._fadingOut = true;
                    this.game.camera.fade(0x000000, 1000);
                    return;
                }
                this._frameIndex++;
            }

            var textTween = this.game.add.tween(this._tapInstructions);
            textTween.to({ alpha: 1 }, 650, Phaser.Easing.Linear.None, true, instructionsDelay);
            textTween.start();

            var image: Phaser.Image = this._frames[this._frameIndex].GetNextImage();
            this._fadetween = this.game.add.tween(image);
            this._fadetween.to({ alpha: 1 }, this._fadeDuration, Phaser.Easing.Linear.None);
            this._fadetween.delay(delay);
            this._fadetween.start();
        }

        private StartGameState(): void
        {
            this.game.stage.backgroundColor = '#000';
            this.game.state.start("Preload");
        }
    }
}