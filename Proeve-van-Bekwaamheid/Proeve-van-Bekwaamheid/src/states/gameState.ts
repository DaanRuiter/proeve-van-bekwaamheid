﻿module ProeveVanBekwaamheid.State
{
    /**
     * Base state for the game.
     * Extends PhaserStateBase.
     */
    export class GameState extends PhaserStateBase implements Actors.IOnDeathEventHandler
    {
        // Override game variable so it's casted to Client.GameEngine.
        public game: Client.GameEngine;


        // Consts
        /**
        * The target length of the score and currency displays.
        * If the amount has les characters than this number, 0's are added before the text to make it this length.
        */
        public static get NUMERIC_CHAR_AMOUNT(): number 
        {
            return 6;
        }

        /**
        * Amount of points are awarded for each pixel the player travels upwards.
        */
        public static get POINTS_PER_PIXEL(): number
        {
            return 0.5;
        }

        /**
        * The margin in pixels that the player is allowed to be under the kill height before dying.
        */
        public static get PLAYER_HEIGHT_LOSE_MARGIN(): number
        {
            return 5;
        }

        /**
        * Is the game currently in a tutorial state.
        */
        public get IsTutorial(): boolean
        {
            return this._isTutorial;
        }
        public set IsTutorial(value: boolean)
        {
            this._isTutorial = value;
        }

        /**
         * Returns the current object scroller used in this instance.
         */
        public get ObjectScroller(): Handlers.ScrollRect
        {
            return this._objectScroller;
        }

        /**
        * Returns the current background scroller used in this instance.
        */
        public get BackgroundScroller(): Handlers.ScrollRect
        {
            return this._backgroundScroller;
        }


        /**
         * Returns a list of all keypoints used in this instance.
         */
        public get KeyPoints(): Actors.Keypoint[]
        {
            return this._keyPoints;
        }

        /**
         * Returns a list of all hands used in this instance.
         */
        public get Hands(): Actors.Hand[]
        {
            return this._hands;
        }

         /**
         * Returns a list of all backgrounds used in this instance.
         */
        public get Backgrounds(): Actors.Background[]
        {
            return this._backgrounds;
        }

        /**
        * Returns a list of all coins used in this instance.
        */
        public get Coins(): PickUp.Coin[]
        {
            return this._coins;
        }

        /**
       * Returns a list of all texts used in this instance.
       */
        public get WorldTexts(): Actors.WorldText[]
        {
            return this._worldTexts;
        }

        /**
        * Returns a list of all texts used in this instance.
        */
        public get TutorialTexts(): Actors.WorldText[]
        {
            return this._tutorialTexts;
        }

        /**
         * Returns a list of all enemies used in this instance.
         */
        public get Enemies(): Actors.Enemy[]
        {
            return this._enemies;
        }

        /**
         * Returns a list of all pickups used in this instance.
         */
        public get Pickups(): PickUp.PickUpObject[]
        {
            return this._pickups;
        }

        /**
         * Returns a list of all laserbeams used in this instance.
         */
        public get LaserBeams(): Actors.LaserBeam[]
        {
            return this._laserBeams;
        }

       /**
        * Returns the reference to the player.
        */
        public get Player(): Actors.Player
        {
            return this._player;
        }        

        /**
        * The player's current score.
        */
        public get Score(): number
        {
            return Math.round(this._playerMaxHeightReached * GameState.POINTS_PER_PIXEL);
        }

        // Utils
        private _backgroundGenerator: Generators.BackgroundGenerator;
        private _presetGenerator: Generators.JsonObjectGenerator;
        private _laserBeamGenerator: Generators.EnemiesGenerator;
        private _tutorial: Tutorial;
        private _objectScroller: Handlers.ScrollRect;
        private _backgroundScroller: Handlers.ScrollRect;

        //UI
        private _loaderText: Phaser.Text;
        private _inGameUI: UI.InGameUICanvas;

        // GameObjects
        private _player: Actors.Player;
        private _keyPoints: Actors.Keypoint[];
        private _backgrounds: Actors.Background[];
        private _enemies: Actors.Enemy[];
        private _laserBeams: Actors.LaserBeam[];
        private _coins: PickUp.Coin[];
        private _worldTexts: Actors.WorldText[];
        private _tutorialTexts: Actors.WorldText[];
        private _pickups: PickUp.PickUpObject[];
        private _hands: Actors.Hand[];

        // Score
        private _playerNewHeight: number;
        private _playerMaxHeightReached: number;

        private _isGameOver: boolean = false;
        private _isTutorial: boolean = true;

        //audio
        private _gameOverAudio: Phaser.Sound;
        private _themeMusic: Phaser.Sound;
        private _launchArm: Phaser.Sound;
        private _hitEnemyAudio: Phaser.Sound;
        private _coinPickupAudio: Phaser.Sound;
        

        constructor()
        {
            super();
            this._backgrounds = [];
            this._keyPoints = [];
            this._enemies = [];
            this._coins = [];
            this._worldTexts = [];
            this._hands = [];
            this._tutorialTexts = [];
            this._pickups = [];
            this._laserBeams = [];

        }

        public preload() :void
        {
            this._player = this.InstantiateEntity(Actors.Player, this.game.width / 2, this.game.height / 2, "", 1);
            //Loading ui
            var bar = this.game.add.graphics(0, 100);
            bar.beginFill(0x000000, 0.2);
            bar.drawRect(0, 100, 800, 100);

            var style = { font: "bold 32px Arial", fill: "#fff", boundsAlignH: "center", boundsAlignV: "middle" };
            this._loaderText = this.game.add.text(0, 0, "Loading...", style);
            this._loaderText.setShadow(3, 3, 'rgba(0,0,0,0.5)', 2);
            this._loaderText.setTextBounds(0, this.game.height / 2 - 100, this.game.width, 100);

            this.game.load.audio('themeMusic', './assets/sounds/ThemeMusic.mp3');
            this.game.load.audio('gameOverSpace', './assets/sounds/inGame/GameOver.mp3');
            this.game.load.audio('armLaunch', './assets/sounds/tony/ArmLaunch.wav');
            this.game.load.audio('shieldPickUp', './assets/sounds/powerUps/Shield/shieldPickUp.wav');
            this.game.load.audio('gunPickUp', './assets/sounds/powerUps/GunPowerUp/GunPickUp.wav');
            this.game.load.audio('GunShoot', './assets/sounds/powerUps/GunPowerUp/GunSound.wav');
            this.game.load.audio('jetpackSound', './assets/sounds/powerUps/JetPack/Jetpack.wav');
            this.game.load.audio('coinPickupSound', './assets/sounds/inGame/CoinSound.mp3');
            this.game.load.audio('TonyHitsEnemy','./assets/sounds/enemy/MonsterDeathSound.mp3');
        }

        public create(): void
        {
            super.create();

            this._loaderText.destroy();


            if (this._themeMusic == undefined)
            {
                this._themeMusic = this.add.audio('themeMusic', this.game.sound.volume / 2, true);
                this._themeMusic.play();

                this._gameOverAudio = this.add.audio('gameOverSpace', 10);
                this._launchArm = this.add.audio('armLaunch');
                this._coinPickupAudio = this.add.audio('coinPickupSound');
                this._hitEnemyAudio = this.add.audio('TonyHitsEnemy',10);
            }

            Physics.Collision.AddRadiusCollisionOnGroups(Physics.CollisionGroups.PLAYER, Physics.CollisionGroups.ENEMY);
            Physics.Collision.AddRadiusCollisionOnGroups(Physics.CollisionGroups.PICKUP, Physics.CollisionGroups.PLAYER);

            this.fadeIn();          

            this._objectScroller = new Handlers.ScrollRect(this.game, null, 50, 50, 500, 60);
            this._backgroundScroller = new Handlers.ScrollRect(this.game, null, 10, 10, 100, 60); // Background scroller has the velocity of objectscroller devided by 5

            this._backgroundGenerator = new Generators.BackgroundGenerator(this.game, true);
            this._presetGenerator = new Generators.JsonObjectGenerator(this.game, this._player);
            this._laserBeamGenerator = new Generators.EnemiesGenerator(this.game);
            this._tutorial = new Tutorial(this.game, this._presetGenerator);

            this._inGameUI = new UI.InGameUICanvas(this.game, 0, 0);

            this.Start();
            this.game.load.bitmapFont('heroes legend', './assets/fonts/heroes-legend.png', './assets/fonts/heroes-legend.xml');
            this.UpdateNumericDisplay('display_currency', statics.Inventory.CoinsOwned);

            Object.SharedObject.SetLocalStorageItem('hasPlayedOnce', 'true');
        }

        private fadeIn()
        {
            this.game.camera.flash(0x000000, 500, false);
        }

        /**
         * Called earch frame.
         * Updates the game logic.
         */
        public update(): void
        {
            super.update();

            if (this._isGameOver) { return; }

            // Updating collision system.
            Physics.Collision.Update();
            
            // Object updates.
            if ((this._player.body) && this._player.body.dynamic)
            {
                this._presetGenerator.Update(this._playerMaxHeightReached);
                this._laserBeamGenerator.Update();

                if (this._player.IsOnBorder)
                {
                    this._objectScroller.ScrollObjects(this._player.body.velocity.y);
                    this._backgroundScroller.ScrollObjects(this._player.body.velocity.y / 5);

                    if (this._player.body.velocity.y < 0)
                    {
                        this._playerNewHeight -= this._player.body.velocity.y * this.game.physics.p2.frameRate;
                    }
                }

                this.CheckClean();
                this.CheckLoseCondition();
                this.CheckPlayerHeightForScore();
            }
        }

        /**
         * Triggers the lose on the game.
         * Resets game.
         */
        public Lose(): void
        {            
            this._gameOverAudio.play();
            
            this._isGameOver = true;

            this._player.KillRagdoll();
            this._player.ToggleRope(false);

            this._inGameUI.SetGameOverPopupContent(this.Score);
            this._inGameUI.ToggleGameOverPopup(true);
            this._inGameUI.SetPlayButtonCallback(function ()
            {
                this.game.state.start('Game');
                this.ResetGame();
            }, this);

            var highscore = Number(Object.SharedObject.GetLocalStorageItem('highscore'));

            if (this.Score > highscore)
            {
                Object.SharedObject.SetLocalStorageItem('highscore', this.Score);
            }
        }

        /**
         * OnDeath condition.
         */
        public OnDeath(): void
        {
            this.Lose();
        }

        /**
         * Resets the game's variables and destroys all living entities.
         */
        public ResetGame(): void
        {
            //this._themeMusic.destroy();

            this._isTutorial = true;
            this._player.OnDeathEvent.RemoveEventListener(this);
            this._player.Destroy();

            for (let i: number = this._backgrounds.length - 1; i >= 0; i--)
            {
                this._backgrounds[i].Destroy();
            }

            for (let i: number = this._keyPoints.length - 1; i >= 0; i--)
            {
                this._keyPoints[i].Destroy();
            }

            for (let i: number = this._enemies.length - 1; i >= 0; i--)
            {
                this._enemies[i].Destroy();
            }

            for (let i: number = this._coins.length - 1; i >= 0; i--)
            {
                this._coins[i].Destroy();
            }

            for (let i: number = this._laserBeams.length - 1; i >= 0; i--)
            {
                this._laserBeams[i].Destroy();
            }

            for (let i: number = this._worldTexts.length - 1; i >= 0; i--)
            {
                this._worldTexts[i].Destroy();
            }

            var pickUps: Object.SharedObject[] = Client.GameEngine.SharedObjectCache.FindObjectsWithTag('pick-up');
            for (let i = 0; i < pickUps.length; i++)
            {
                pickUps[i].Destroy();
            }

            this._keyPoints = [];
            this._backgrounds = [];
            this._enemies = [];
            this._coins = [];
            this._laserBeams = [];
            this._tutorialTexts = [];
            this._worldTexts = [];

            this._objectScroller.Reset();
            this._backgroundScroller.Reset();

            Physics.Collision.Reset();
        }

        /**
         * dissables the theme music
         * @param fadeTime time to fade out the music
         */
        public dissableMusic(fadeTime: number = 0) {
            this._themeMusic.fadeOut(fadeTime);
        }

        /**
         * Plays the audio for launching the arm
         */
        public ArmLaunchAudio(): void
        {
            this._launchArm.play();
        }

        /**
         * Audio for the coin collection
         */

        public collectCoinAudio(): void
        {
            this._coinPickupAudio.play();
        }

        /**
         * Audio for the player hitting the enemy
         */
        public playerHitEnemyAudio(): void
        {
            this._hitEnemyAudio.play();
        }

        /**
         * Starts the game logic and timers.
         */
        private Start(): void
        {
            this._tutorial.CreateTutorial();

            this._player.initialize(new Phaser.Point(this.game.width / 2, this.game.height - 200));

            this._playerMaxHeightReached = this._player.y;
            this._playerNewHeight = this._playerMaxHeightReached;

            this._player.OnDeathEvent.AddEventListener(this);

            this._laserBeamGenerator.Start();

            this._isGameOver = false;
        }
                
        /**
         * Checks if the player is on the bottom of the screen and if the last keypoint is out of reach.
         * If true the game will trigger its lose condition.
         */
        private CheckLoseCondition() : void
        {
            if (this._player.body.y >= this.game.height - this._player.height - GameState.PLAYER_HEIGHT_LOSE_MARGIN && this._keyPoints[0].y < -100)
            {
                this.Lose();
            }
        }

        /**
         * Checks to see if the player has reached a new height and rewards the player points if it is so.
         */
        private CheckPlayerHeightForScore(): void
        {
            if (this._playerNewHeight > this._playerMaxHeightReached)
            {                
                this.UpdateNumericDisplay('display_score', Math.round(this._playerMaxHeightReached * GameState.POINTS_PER_PIXEL));
                this._playerMaxHeightReached = this._playerNewHeight;
            } 
        }

        /**
         * Checkup to clean out any object below the bottom of the screen.
         */
        private CheckClean(): void
        {
            var fxObjects: Object.Entity[] = Client.GameEngine.SharedObjectCache.FindObjectsWithTag('fx') as Object.Entity[];
            this.CheckCleanList(fxObjects, true);
            this.CheckCleanList(this._keyPoints, true);
            this.CheckCleanList(this._enemies, true);
            this.CheckCleanList(this._coins, true);
            this.CheckCleanList(this._worldTexts, true);
            this.CheckCleanList(this._laserBeams, true);
        }

        /**
         * Updates a UIText element with the given tag to show the given number that is a child of the _inGameUI canvas.
         * The length of the string will be filled with 0's  before the score if the length is smaller than GameState.NUMERIC_CHAR_AMOUNT.
         * @param tag Required tag that the UIElement should have.
         * @param score Number for the given element to display.
         */
        public UpdateNumericDisplay(tag: string, score: number): void
        {
            var display = this._inGameUI.FindElement(tag) as UI.UIText;

            if (display != null)
            {
                var scoreAsString: string = score.toString();
                let missingChars = GameState.NUMERIC_CHAR_AMOUNT - scoreAsString.length;

                for (var i = 0; i < missingChars; i++)
                {
                    scoreAsString = "0" + scoreAsString;
                }

                display.Text = scoreAsString;
            }
        }

        private CheckCleanList(array: any[], inObjectScroller: boolean): void
        {
            // Cleans a generic list
            for (let i: number = array.length - 1; i >= 0; i--)
            {
                if ((array[i]) && array[i].y > this.game.height + 200)
                {
                    if (inObjectScroller)
                    {
                        // Remove out of object scroller.
                        var scrollObjectIndex = this._objectScroller.ScrollingObjects.indexOf(array[i]);
                        //console.log(this._objectScroller.ScrollingObjects[scrollObjectIndex].name);
                        this._objectScroller.ScrollingObjects.splice(scrollObjectIndex, 1);
                        
                    }

                    array[i].Destroy();
                    array.splice(i, 1);
                }
            }
        }
    }
}