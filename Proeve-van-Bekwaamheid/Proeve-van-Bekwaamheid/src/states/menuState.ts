﻿module ProeveVanBekwaamheid.State
{
	export class MenuState extends PhaserStateBase
    {
        public static ButtonAudio: Phaser.Sound;
        public static BackAudio: Phaser.Sound;

		game: Client.GameEngine;

		preload()
        {
            this.game.load.image('ad1', './assets/advertenties/advertentie1.png');
            this.game.load.image('ad2', './assets/advertenties/advertentie2.png');
            this.game.load.image('ad3', './assets/advertenties/advertentie3.png');
            this.game.load.image('ad4', './assets/advertenties/advertentie4.png');
            this.game.load.image('ad5', './assets/advertenties/advertentie5.png');
            this.game.load.image('ad6', './assets/advertenties/advertentie6.png');
            this.game.load.image('ad7', './assets/advertenties/advertentie7.png');

            this.game.load.atlasJSONHash('mainMenu', './assets/ui/MenuSpriteSheetJsonHash.png', './assets/ui/MenuSpriteSheetJsonHash.json');
            this.game.load.atlasJSONHash('shopMenu', './assets/ui/ShopSpriteSheet.png', './assets/ui/ShopSpriteSheet.json');
            this.game.load.atlasJSONHash('gameOver', './assets/ui/SpriteSheetGameOver.png', './assets/ui/SpriteSheetGameOver.json');
            
            this.game.load.image('Gun-Icon', './assets/pickups/Gun.png');
            this.game.load.image('Shield-Icon', './assets/pickups/Shield.png');
            this.game.load.image('Jetpack-Icon', './assets/pickups/SpeedBoost.png');
            this.game.load.image('Platform-Icon', './assets/pickups/Platform.png');
            this.game.load.image('Magnet-Icon', './assets/pickups/magnet.png');

            this.game.load.audio('buttonClick', './assets/sounds/Ui/SelectSound.wav');
            this.game.load.audio('mainMenuMusic', './assets/sounds/MenuMusic.mp3');
            this.game.load.audio('returnSound', './assets/sounds/Ui/ReturnSound.wav');

            this.game.load.image('highscores-logo', './assets/ui/HighScores.png');
            this.game.load.image('back-button', './assets/ui/BackButton.png');
            this.game.load.image('highscores-button', './assets/ui/HighScoreButton.png');
            this.game.load.image('highscores-button-hover', './assets/ui/HighScoreButtonHover.png');
            this.game.load.image('fullscreen-button', './assets/ui/Fullscreen.png');
            this.game.load.image('fullscreen-button-hover', './assets/ui/FullscreenHover.png');

            this.game.load.image('adsButton', './assets/ui/AdButton.png');

            for (let i = 0; i < statics.Inventory.SKINS.length; i++)
            {
                this.game.load.image(statics.Inventory.SKINS[i].SkinKey + '-icon', './assets/sprites/player/' + statics.Inventory.SKINS[i].SpriteFolderName + '/Icon.png');
            }
        }

        public CreateUi: ProeveVanBekwaamheid.UI.UiCreater;
        private _mainMenu: MainMenu;
        private _optionsMenu: OptionsMenu;
        private _shopMenu: ShopMenu;

        public MainMenuButtonOptions: any;

        private _inOptionScreen: boolean = false;

        public ButtonAudio: Phaser.Sound;
        private _mainMenuMusic: Phaser.Sound;
        private _backButton: Phaser.Sound;

        private _ads: ProeveVanBekwaamheid.Ads.adManager;


		create()
        {
            this.game.stage.backgroundColor = '#17222b';

            this.CreateUi = new ProeveVanBekwaamheid.UI.UiCreater(this.game, this);

            this._ads = new Ads.adManager(this.game);

            this._mainMenu = new MainMenu(this.game, this);
            this._optionsMenu = new OptionsMenu(this.game, this, this.MainMenuButtonOptions);
            this._shopMenu = new ShopMenu(this.game, this, this._ads.checkIfAdsAllowed());

            

            MenuState.ButtonAudio = this.game.add.audio('buttonClick');
            MenuState.BackAudio = this.game.add.audio('returnSound');
            this._mainMenuMusic = this.game.add.audio('mainMenuMusic', this.game.sound.volume, true);
            this._mainMenuMusic.play();
            this.fadeIn();
        }

        /**
         * fades in the screen
         */
        private fadeIn(): void
        {
            this.game.camera.flash(0x000000, 500, false);
        }

        /**
         * Goes back to the main menu
         */
        public mainMenu(): void
        {
            this._mainMenu.enableMenu();
            this._shopMenu.dissableShopMenu();

            //this._buttonaudio.play();

        }

        public startAdd(): void
        {
            this._ads.startAdd();
            //create ad
        }

        /**
         * Updates the currency display to amount of coinsowned inside Inventory
         */
        public updateCurrencyDisplay(): void
        {
            this._mainMenu.updateCurrencyDisplay();
        }

        /**
         * Toggles the options menu
         */
        public optionsMenu(): void
        {
            if (this._inOptionScreen)
            {
                this._optionsMenu.dissableOptionsMenu();
                this._inOptionScreen = false;
                MenuState.BackAudio.play();
            }
            else
            {
                this._optionsMenu.enableOptionsMenu();
                this._inOptionScreen = true;
                MenuState.ButtonAudio.play();
            }
        }

        /**
         * Opens the shop menu
         */
        public shopMenu(): void
        {
            this._mainMenu.disableMenu();
            this._optionsMenu.dissableOptionsMenu();
            this._shopMenu.enableShopMenu();
            this._inOptionScreen = false;

            MenuState.ButtonAudio.play();
        }

        public OnGameStarted(): void
        {
            this._shopMenu.Destroy();
        }

        public dissableMusic(fadeTime: number = 0): void
        {
            this._mainMenuMusic.fadeOut(fadeTime);
        }

    }
}