﻿module ProeveVanBekwaamheid.State
{
    export class Preloader extends PhaserStateBase
    {
        public game: Client.GameEngine;

        public preload() :void
        {
            statics.Inventory.InitializeUpgrades();

            // Adding spine plugin.
            this.game.add.plugin(new PhaserSpine.SpinePlugin(this.game, this.game.plugins));

            // Load required images.
            this.LoadImages();

            // Loading presets.
            this.load.tilemap('presets', './assets/json/Presets.json', null, Phaser.Tilemap.TILED_JSON);
            this.load.tilemap('tutorial', './assets/json/Tutorial.json', null, Phaser.Tilemap.TILED_JSON);

            // Initialize physics system.
            this.game.physics.startSystem(Phaser.Physics.P2JS);
           
            this.game.physics.p2.gravity.y = 90;
            this.game.physics.p2.applyGravity = true;
            this.game.physics.p2.restitution = 0.8;
            this.game.physics.p2.frameRate = 1 / 30;
            this.game.physics.p2.solveConstraints = false;
            
            // After physics system is initialized we can initialize our own collision system.
            Physics.Collision.Initialize(this.game);
        }

        public create(): void
        {
            this.game.state.start("Game");
        }
        
        /**
         * Loads all required assets into the cache.
         */
        private LoadImages(): void
        {
            // Loading sprites
            this.game.load.image('SpacePart-01', './assets/backgrounds/Space01.png');
            this.game.load.image('SpacePart-02', './assets/backgrounds/Space02.png');
            this.game.load.image('SpacePart-03', './assets/backgrounds/Space03.png');
            this.game.load.image('Keypoint', './assets/props/Keypoint01.png');
            this.game.load.image('Hand', './assets/ui/hand.png');
            this.game.load.image('Glow', './assets/ui/GlowKeypoint.png');

            this.game.load.image('Gun-Powerup', './assets/pickups/GunPickUp.png');
            this.game.load.image('Shield-Powerup', './assets/pickups/ShieldPickUp.png');
            this.game.load.image('Jetpack-Powerup', './assets/pickups/SpeedBoostPickUp.png');
            this.game.load.image('Platform-Powerup', './assets/pickups/PlatformPickUp.png');
            this.game.load.image('Magnet-Powerup', './assets/pickups/MagnetPickUp.png');

            this.game.load.image('Shield', './assets/pickups/ShieldPowerup.png');
            this.game.load.image('Platform', './assets/pickups/PlatformPowerup.png');
            this.game.load.image('particles-magnet', './assets/pickups/Pulse.png');
            this.game.load.image('laser-beam', './assets/enemy/laserbeam.png');
            this.game.load.image('laser-coil', './assets/enemy/lasercoil.png');

            this.game.load.image('enemy-death', './assets/enemy/Death ghost.png');

            this.game.load.spritesheet('particles-jetack', './assets/particles/jetpack_particles_placeholder.png', 32, 32);
            this.game.load.image('muzzle-flare', './assets/sprites/player/Muzzle-flare.png');
            
            //this.game.load.atlasJSONHash('Keypoints', './assets/props/SpriteSheetKeypoints.png', './assets/props/SpriteSheetKeypoints.json');

            Spine.SpineFactory.loadSpineAsset(this.game, "Player-Stuf-Effect", "./assets/sprites/player/Stun.json");
            Spine.SpineFactory.loadSpineAsset(this.game, "Enemy-Moving-1", "./assets/enemy/Character_Moving_Space.json");
            Spine.SpineFactory.loadSpineAsset(this.game, "Enemy-Static-1", "./assets/enemy/Character_Static_Space.json");
        }
    }
}