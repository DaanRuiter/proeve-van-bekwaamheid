﻿module ProeveVanBekwaamheid
{
    export class PhaserStateBase extends Phaser.State
    {
        public get EntityGroup(): Phaser.Group
        {
            return this.entityGroup;
        }

        protected entityGroup: Phaser.Group;

        private static _entityGraphicLoadCache: EntityGraphicsLoadRequestCache;

        private _loadQueue: Object.Entity[];
        private _started: boolean;

        constructor()
        {
            super();

            this._loadQueue = new Array<Object.Entity>();
        }

        /**
         * Adds all sprites and audio from the entity's dependencies to the load queue and returns the instance of the entity
         * The entity should not be used untill create() is called since not all assets will be loaded right away after calling this method.
         * @param entityType type of desired entity
         * @return The instance of the entity, dependencies are not loaded untill create
         */
        public InstantiateEntity<EntityType extends Object.Entity>(entityType: { new (game: Phaser.Game, x: number, y: number, key?: string | Phaser.RenderTexture | Phaser.BitmapData | PIXI.Texture, frame?: string | number): EntityType; }, x?: number, y?: number, key?: string | Phaser.RenderTexture | Phaser.BitmapData | PIXI.Texture, frame?: string | number): EntityType
        {
            if (PhaserStateBase._entityGraphicLoadCache == null)
            {
                PhaserStateBase._entityGraphicLoadCache = new EntityGraphicsLoadRequestCache();
            }

            if (!x)
                x = 0;
            if (!y)
                y = 0;

            var instance = new entityType(this.game as Client.GameEngine, x, y, key, frame);
            instance.IndexDependencies(PhaserStateBase._entityGraphicLoadCache);

            this.LoadSprites(PhaserStateBase._entityGraphicLoadCache.GetSpriteRequests());
            this.LoadSpriteSheets(PhaserStateBase._entityGraphicLoadCache.GetSpritesheetRequests());


            if (this.entityGroup == null)
            {
                this.entityGroup = this.game.add.group();
            }

            this.entityGroup.add(instance);

            this.game.add.existing(instance);

            if (!this._started)
            {
                this._loadQueue.push(instance);
            }
            else
            {
                instance.OnStart();
            }

            return instance;
        }

        //Called by phaser
        public create()
        {
            this.StartLoadedEntities();
        }

        private LoadSprites(requests: PhaserSpriteLoadRequest[])
        {
            for (var i = 0; i < requests.length; i++)
            {
                let request = requests[i];
                
                this.game.load.image(request.Key, request.Path);
            }
        }

        private LoadSpriteSheets(requests: PhaserSpriteSheetLoadRequest[])
        {
            for (var i = 0; i < requests.length; i++)
            {
                let request = requests[i];

                this.game.load.spritesheet(request.Key, request.Path, request.FrameWidth, request.FrameHeight, request.FrameCount);
            }
        }

        private StartLoadedEntities()
        {
            for (var i = this._loadQueue.length - 1; i >= 0; i--)
            {
                this._loadQueue[i].OnStart();
            }
            this._loadQueue.splice(0, this._loadQueue.length);

            this._started = true;
        }
    }
}