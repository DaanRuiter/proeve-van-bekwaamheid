
module ProeveVanBekwaamheid.Client
{
    export class GameEngine extends Phaser.Game 
    {
        public static get Instance(): GameEngine
        {
            return GameEngine._instance;
        }
        private static _instance; GameEngine;

       /**
        * Object cache shared across the entire game.
        */
        public static get SharedObjectCache(): Object.SharedObjectCache
        {
            if (GameEngine._sharedObjectCache == null)
            {
                GameEngine._sharedObjectCache = new Object.SharedObjectCache();
            }

            return GameEngine._sharedObjectCache;
        }

        public add: PhaserSpine.SpineObjectFactory;
        public load: PhaserSpine.SpineLoader;
        public cache: PhaserSpine.SpineCache;

       /**
        * Returns the current state casted to PhaserStateBase.
        */
        public get CurrentState(): PhaserStateBase
        {
            return this.state.getCurrentState() as PhaserStateBase;
        }

        private static get WINDOW_MIN_WIDTH(): number { return 720; }
        private static get WINDOW_MIN_HEIGHT(): number { return 1080; }
        private static get WINDOW_MAX_WIDTH(): number { return 720; }
        private static get WINDOW_MAX_HEIGHT(): number { return 1080; }

        private static _sharedObjectCache: Object.SharedObjectCache;

        private _hasWindowSettingsBeenSet: boolean = false;

        constructor() 
        {
            super(GameEngine.WINDOW_MIN_WIDTH, GameEngine.WINDOW_MIN_HEIGHT, Phaser.AUTO, 'content', null);

            GameEngine._instance = this;

            this.state.add('menuState', State.MenuState, false);
            this.state.add('Novel', State.NovelState, false);
            this.state.add('Preload', State.Preloader, false);
            this.state.add("Game", State.GameState, false);

            this.state.start('menuState');
            
            //this.SetWindowSettings();
        }

        /**
         * Sets all window settings.
         */
        private SetWindowSettings(): void
        {
            if (this.device.android || this.device.iOS || this.device.windowsPhone) //this.device.desktop doesn't work so we use this instead.
            {
                this.scale.setGameSize(GameEngine.WINDOW_MIN_WIDTH, GameEngine.WINDOW_MIN_HEIGHT);
                this.scale.minHeight = GameEngine.WINDOW_MIN_HEIGHT;
                this.scale.minWidth = GameEngine.WINDOW_MIN_WIDTH;
                this.scale.maxHeight = GameEngine.WINDOW_MAX_HEIGHT;
                this.scale.maxWidth = GameEngine.WINDOW_MAX_WIDTH;
                this.scale.forcePortrait = true;
                this.scale.pageAlignVertically = true;
                this.scale.forceOrientation(true, false);
            }
            else
            {
                this.scale.scaleMode = Phaser.ScaleManager.EXACT_FIT;
                this.scale.pageAlignHorizontally = true;
            }
            this.scale.refresh();

            //Scale game window to 80% of browser height while keeping the correct aspect ratio
            var g = document.getElementById("content");
            var height = window.innerHeight * 0.8;

            g.style.width = height * (720 / 1080) + "px";
            g.style.height = height + "px";

            this._hasWindowSettingsBeenSet = true;
        }

        /**
         * Overriden Phaser update method.
         * @param time Time between last update and this one.
         */
        public update(time: number): void
        {
            super.update(time);

            this.UpdateSharedObjects();

            //We have to wait a frame before setting the window settings because the scaling manager null on the first frame.
            if (!this._hasWindowSettingsBeenSet && this.scale) 
            {
                this.SetWindowSettings();
            }
        }

        /**
         * Call the OnUpdate method on all cached SharedObjects.
         */
        private UpdateSharedObjects(): void
        {
            GameEngine.SharedObjectCache.CachedObjects.forEach(function (sharedObject)
            {
                sharedObject.OnUpdate();
            });
        }
    }
}

window.onload = () => 
{
    new ProeveVanBekwaamheid.Client.GameEngine();
};