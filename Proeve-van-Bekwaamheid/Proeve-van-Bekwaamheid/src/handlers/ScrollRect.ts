﻿module ProeveVanBekwaamheid.Handlers
{
    /**
     * Scroll rect handles the scrolling of objects registered to it.
     */
    export class ScrollRect
    {
        /**
         * Returns all scrolling objects of this instance.
         */
        public get ScrollingObjects(): Object.Entity[]
        {
            return this._scrollingObjects;
        }

        private _game: Client.GameEngine;
        private _scrollingObjects: Object.Entity[];

        // Scrolling properties.
        private _startScrollSpeed: number;
        private _scrollSpeed: number;
        private _scrollSpeedAcceleration: number;
        private _maxScrollSpeed: number;
        private _scrollAccelerationTime: number;
        private _timerEvent: Phaser.TimerEvent;

        constructor(game: Client.GameEngine, objects?: Object.Entity[], startScrollSpeed?: number, scrollSpeedAcceleration?: number, maxScrollSpeed?: number, scrollAccelerationTime?: number)
        {
            this._scrollingObjects = [];
            this._game = game;

            if(objects)
                this._scrollingObjects = objects;

            this._startScrollSpeed = startScrollSpeed;
            this._scrollSpeedAcceleration = scrollSpeedAcceleration;
            this._maxScrollSpeed = maxScrollSpeed;
            this._scrollAccelerationTime = scrollAccelerationTime;

            this._scrollSpeed = this._startScrollSpeed;
        }

        /**
         * Removes all objects out of this scroll rect.
         * Also resets the scrollAcceleration.
         */
        public Reset(startScrollAcceleration?: boolean): void
        {
            this._scrollSpeed = this._startScrollSpeed;

            for (let i: number = this._scrollingObjects.length - 1; i >= 0; i--)
            {
                this._scrollingObjects[i].Destroy();
                this._scrollingObjects.pop();
            }

            this._game.time.events.remove(this._timerEvent);

            if (startScrollAcceleration)
            {
                this.StartScrollAcceleration();
            }
        }

        /**
         * Starts the accelaration of the scroll.
         */
        public StartScrollAcceleration(): void
        {
            var timesToScroll = this._maxScrollSpeed / this._scrollAccelerationTime - this._scrollAccelerationTime / this._scrollSpeed;
            this._timerEvent = this._game.time.events.repeat(Phaser.Timer.SECOND * this._scrollAccelerationTime, timesToScroll, this.AccelerateScroll, this);
        }

        /**
         * Update to move all objects inside _scrollinObjects with the velocity of _scrollSpeed.
         * @param otherVelocity given velocity if you don't want to use the velocity of this instance.
         */
        public ScrollObjects(otherVelocity?: number): void
        {
            let velocity = this._scrollSpeed * this._game.physics.p2.frameRate;

            if ((otherVelocity))
            {
                velocity = otherVelocity * this._game.physics.p2.frameRate;
            }

            for (var i: number = 0; i < this._scrollingObjects.length; i++)
            {
                if (this._scrollingObjects[i].body)
                {
                    this._scrollingObjects[i].body.y -= velocity;
                }
                else
                {
                    this._scrollingObjects[i].position.y -= velocity;
                }
            }
        }

        public Update(): void
        {
            this.ScrollObjects();
        }

        /**
        * Accelerates the scroll speed.
        */
        private AccelerateScroll(): void
        {
            this._scrollSpeed += this._scrollSpeedAcceleration;
        }
    }
}