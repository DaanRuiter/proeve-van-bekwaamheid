﻿module ProeveVanBekwaamheid
{
    export class Tutorial
    {
        private _game: Client.GameEngine;
        private _presetGenerator: Generators.JsonObjectGenerator;
        private _tutorial: Phaser.Tilemap;

        constructor(game: Client.GameEngine, presetGenerator: Generators.JsonObjectGenerator)
        {
            this._game = game;
            this._presetGenerator = presetGenerator;

            this._tutorial = this._game.add.tilemap('tutorial');
        }

        public CreateTutorial(): void
        {
            var objects = this._presetGenerator.CreatePresetFromList(this._tutorial.objects['Tutorial'], false);
            for (var i: number = 0; i < objects.length; i++)
            {
                if ((objects[i] as Actors.WorldText).Text)
                {
                    (objects[i] as Actors.WorldText).Text.text = "Hold to pull!";
                    (this._game.CurrentState as State.GameState).TutorialTexts.push(objects[i] as Actors.WorldText);
                    (objects[i] as Actors.WorldText).StartHover();
                }
            }

            
            var text = this._game.CurrentState.InstantiateEntity(Actors.WorldText, this._game.width / 2 - 180, this._game.height / 2, "");
            text.SetProperties("The bigger the distance" + '\n' + "the harder you will pull!", '#fff', 36, 3);
            text.StartHover();
            (this._game.CurrentState as State.GameState).WorldTexts.push(text);
            (this._game.CurrentState as State.GameState).ObjectScroller.ScrollingObjects.push(text); 
        }
    }
}