﻿module ProeveVanBekwaamheid.UI
{
    /**
     * Class that contaings UI groups and allows easy management for multplie groups through a single call.
     */
    export class UICanvas extends Object.SharedObject
    {
       /**
        * The groups contained in this canvas.
        */
        public get Groups(): UIGroup[]
        {
            return this._groups;
        }

        private _groups: UIGroup[];

        /**
         * @param game Reference to Phaser game used internally.
         * @param x X position of the canvas.
         * @param y Y position of the canvas.
         */
        public constructor(game: Phaser.Game, x: number, y: number)
        {
            super(game, x, y, null);

            this._groups = new Array<UIGroup>();
        }

        /**
         * Add a group to this canvas.
         * @param group Group to add.
         */
        public AddGroup(group: UIGroup): UIGroup
        {
            this._groups.push(group);

            if (group.parent != null)
            {
                group.parent.removeChild(group);
            }

            this.addChild(group);

            return group;
        }

        /**
         * Remove a group from this canvas.
         * @param group Group to remove.
         */
        public RemoveGroup(group: UIGroup): UIGroup
        {
            let index = this._groups.indexOf(group);

            if (index >= 0)
            {
                this._groups.splice(index);

                if (group.parent = this)
                {
                    this.removeChild(group);
                }
            }

            return group;
        }

        /**
         * Set the alpha value for all groups in this canvas.
         * Will set the same alpha value to all elements in those groups.
         * @param alpha new alpha value of groups.
         */
        public SetAlpha(alpha: number): void
        {
            for (var i = 0; i < this._groups.length; i++)
            {
                this._groups[i].SetAlpha(alpha);
            }
        }

        /**
         * Sets the visibilty of all elements within this canvas.
         * @param visible The new Visibility state of all the elements within this canvas.
         */
        public SetVisibility(visible: boolean): void
        {
            for (var i = 0; i < this._groups.length; i++)
            {
                this._groups[i].SetVisibility(visible);
            }
        }

        /**
         * Sets the width and height of this canvas to those of the game window.
         */
        public SetFullScreen(): void
        {
            this.width = this.game.width;
            this.height = this.game.height;
        }

        /**
         * Searches through all groups and their elements and returns the first element that has the given tag.
         * @param tag Tag to search for.
         */
        public FindElement(tag: string): UIElement
        {
            for (var i = 0; i < this._groups.length; i++)
            {
                var elements = this._groups[i].Elements;

                for (var j = 0; j < elements.length; j++)
                {
                    if (elements[j].HasTag(tag))
                    {
                        return elements[j];
                    }
                }
            }

            return null;
        }
    }
}