﻿module ProeveVanBekwaamheid.UI
{
    /**
     * Base class for UI elements like images and text.
     */
    export abstract class UIElement extends Object.SharedObject
    {
        /**
         * @param game Reference to Phaser game used internally.
         * @param x X position of the image.
         * @param y Y position of the image.
         * @param key used internally, use UIImage class instead when rendering images as UI.
         */
        public constructor(game: Phaser.Game, x: number, y: number, key:string = null, frame:string = null)
        {
            super(game, x, y, key, frame);

            this.AddTag('ui');

            game.add.existing(this);
        }

        /**
         * Set the visibilty of the element.
         * @param visible
         */
        public abstract SetVisibility(visible: boolean): void

        /**
         * Translates the image's position by given amount.
         * @param x Pixels to move on the X axis.
         * @param y Pixels to move on the Y axis.
         */
        public abstract Translate(x: number, y: number): void;

        /**
         * Sets the element's position to the given position.
         * @param x New X axis position of the element.
         * @param y New Y axis position of the element.
         */
        public abstract MoveTo(x: number, y: number): void;

        /**
         * The main visible object used by this UI element.
         * e.g.: The UIText will return it's internal Phaser.Text object.
         */
        public abstract GetRenderObject(): any
    }
}