﻿module ProeveVanBekwaamheid.UI
{
     /**
     * A class that implements both UIElement and Phaser.Sprite and follows the internal UI system.
     * Used to render text.
     * Will be rendered on top of entities.
     */
    export class UIText extends UIElement
    {
       /**
        * The Phaser.Text object used internally.
        * Can be used to change appearance or the text to display.
        */
        public set Text(value: string)
        {
            this._text.text = value;
        }
        public get Text(): string
        {
            return this._text.text;
        }

        /**
        * The internally used Phaser.Text object.
        */
        public get TextObject():Phaser.Text
        {
            return this._text;
        }

        private _text: Phaser.Text;

        /**
         * @param game Reference to Phaser game used internally.
         * @param x X position of the image.
         * @param y Y position of the image.
         * @param text Starting text for this element.
         * @param fontSize Fontsize of the text.
         */
        public constructor(game: Phaser.Game, x: number, y: number, text: string, fontSize: number = 32, fillColor: string = '#fff', outlineColor:string = '#000', outlineThickness: number = 6, horizontalAlignment:string = 'left')
        {
            super(game, x, y, null);

            this._text = game.add.text(x, y, text,
                {
                    fill: fillColor,
                    fontSize: fontSize,
                    stroke: outlineColor,
                    strokeThickness: outlineThickness,
                    boundsAlignH: horizontalAlignment
                });
        }

        /**
         * Makes sure UI is always rendered on top by bringing it to the front of it's group.
         */
        public OnUpdate(): void
        {
            this._text.bringToTop();
        }

        /**
         * Set the visibility of the button.
         * @param visible The new state of the image.
         */
        public SetVisibility(visible: boolean): void
        {
            this._text.visible = visible;
        }

        /**
         * Translates the image's position by given amount.
         * @param x Pixels to move on the X axis.
         * @param y Pixels to move on the Y axis.
         */
        public Translate(x: number, y: number)
        {
            this._text.x += x;
            this._text.y += y;
        }

        /**
         * Sets the position of the text to the given position.
         * @param x New X axis position of the text.
         * @param y New Y axis position of the text.
         */
        public MoveTo(x: number, y: number): void
        {
            this._text.x = x;
            this._text.y = y;
        }

        /**
         * The main visible object used by this UI element.
         * This will return the internally used Phaser.Text object.
         */
        public GetRenderObject(): any
        {
            return this._text;
        }
    }
}