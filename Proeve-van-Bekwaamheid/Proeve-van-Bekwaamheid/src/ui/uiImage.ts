﻿module ProeveVanBekwaamheid.UI
{
    /**
     * A class that implements both UIElement and Phaser.Sprite and follows the internal UI system.
     * Used for UI images or animated sprites.
     * Will be rendered on top of entities.
     */
    export class UIImage extends UIElement
    {
        /**
         * @param game Reference to Phaser game used internally.
         * @param x X position of the image.
         * @param y Y position of the image.
         * @param key Key of the image in the phaser cache.
         * @param frame Frame of image in spritesheet.
         */
        public constructor(game: Phaser.Game, x: number, y: number, key:string, frame:string = null)
        {
            super(game, x, y, key, frame);

            game.add.existing(this);
        }

        /**
         * Set the visibility of the button.
         * @param visible The new state of the image.
         */
        public SetVisibility(visible: boolean): void
        {
            this.visible = visible;
        }

        /**
         * Translates the image's position by given amount.
         * @param x Pixels to move on the X axis.
         * @param y Pixels to move on the Y axis.
         */
        public Translate(x: number, y: number): void
        {
            this.x += x;
            this.y += y;
        }

        /**
         * Sets the image's position to the given position.
         * @param x New X axis position of the image.
         * @param y New Y axis position of the image.
         */
        public MoveTo(x: number, y: number): void
        {
            this.x = x;
            this.y = y;
        }

        /**
         * The main visible object used by this UI element.
         * This will return this object.
         */
        public GetRenderObject(): any
        {
            return this;
        }
    }
}