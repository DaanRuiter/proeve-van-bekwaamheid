﻿module ProeveVanBekwaamheid.State {
    export class MainMenu {

        private _game: Client.GameEngine;

        public logo: Phaser.Image;

        //main menu
        private _menuPositionArray: any[];
        private _menuArray: any[];
        private _mainMenuButtonStart: Phaser.Button;
        private _mainMenuButtonOptions: Phaser.Button;
        private _mainMenuButtonShop: Phaser.Button;
        private _highscoresButton: Phaser.Button;
        private _fullscreenButton: Phaser.Button;
        private _coinAnimation: Phaser.Image;
        private _coinBorder: Phaser.Image;
        private _currencyAmountDisplay: Phaser.Text;

        private _menuState: MenuState;
        private _highscoreScreen: UI.HighscoresScreen;

        /**
         * 
         * @param game  A reference to the game
         * @param menuState A reference to the menu state
         */
        constructor(game: Client.GameEngine, menuState: MenuState) {
            this._game = game;
            this._menuState = menuState;
            this._game.camera.onFadeComplete.add(this.startGame, this);
            this.createMenu();

            var firstTimePlaying: boolean = !Object.SharedObject.HasLocalStorageItem('hasPlayedOnce');
            if (firstTimePlaying)
            {
                this._game.camera.onFadeComplete.add(this.StartNovel, this);
            } else
            {
                this._game.camera.onFadeComplete.add(this.startGame, this);
            }
        }

        /**
         * Dissables the main menu, tweens out and dissables the menu buttons
         */
        public disableMenu(): void
        {
            var i = 0;

            var tween;

            for (var v in this._menuArray) {
                this._menuArray[v].inputEnabled = false;
                tween = this._game.add.tween(this._menuArray[v]);
                tween.to({ x: this._game.width + this._menuArray[v].width }, 750, 'Quart.easeOut', true, i * 50)
                i++;
            }
        }

        /**
         * Enables the main menu, tweens in and enables the menu buttons
         */
        public enableMenu(): void
        {
            var i = 0;

            var tween

            for (var v in this._menuArray) {
                this._menuArray[v].inputEnabled = true;
                tween = this._game.add.tween(this._menuArray[v]);
                tween.to({ x: this._menuPositionArray[v] }, 750, 'Quart.easeOut', true, i * 100);
                i++;
            }
        }

        /**
         * Updates the currency display to amount of coinsowned inside Inventory
         */
        public updateCurrencyDisplay(): void
        {
            this._currencyAmountDisplay.text = statics.Inventory.CoinsOwned.toString();
        }

        //Creates the main menu
        private createMenu() {
            this.logo = this._menuState.CreateUi.createImage(this._game.width / 2, this._game.height / 10, 'mainMenu', 'Logo', 0.5, 0);

            this._coinBorder = this._menuState.CreateUi.createImage(this._game.width, 0, 'mainMenu', 'Score_Border', 1, 0);
            this._coinBorder.width *= 1.25;
            this._coinAnimation = this._menuState.CreateUi.createImage(this._game.width - this._coinBorder.width * 10 / 10.5, this._coinBorder.height / 8, 'mainMenu', 'CoinTurn-1', 0, 0);
            this._coinAnimation.animations.add('coinrot', Phaser.Animation.generateFrameNames('CoinTurn-', 1, 12), 10, true, false);
            this._coinAnimation.play('coinrot');
            this._currencyAmountDisplay = this._menuState.CreateUi.createText(this._coinAnimation.width * 1.1 + this._game.width - this._coinBorder.width * 10 / 10.5, this._coinBorder.height / 4, statics.Inventory.CoinsOwned.toString(), 48, '#fff', 0, 0);

            this._mainMenuButtonOptions = this._menuState.CreateUi.createButton(0, 0, 'mainMenu', this._menuState.optionsMenu, 'Settings_Button', 'Settings_Button_Click', this._menuState, 0, 0);
            this._mainMenuButtonStart = this._menuState.CreateUi.createButton(this._game.width / 4, this._game.height / 4 * 3, 'mainMenu', this.fadeScreenOut, 'Play_Button', 'Play_Button_Click', this, 0.5, 0.5)
            this._mainMenuButtonShop = this._menuState.CreateUi.createButton(this._game.width / 4 * 3, this._game.height / 4 * 3, 'mainMenu', this._menuState.shopMenu, 'Shop_Button', 'Shop_Button_Click', this._menuState, 0.5, 0.5);
            
            this._menuPositionArray = new Array<any>();

            this._highscoreScreen = new UI.HighscoresScreen(this._game, this.OnHighscoresScreenClosed, this);
            this._highscoreScreen.Toggle(false);

            this._highscoresButton = this._game.add.button(0, 0, 'highscores-button', this.OnHighscoresButtonPressed, this, 'highscores-button-hover');
            this._highscoresButton.scale.setTo(0.55);
            this._highscoresButton.x = this._game.width / 2 - this._highscoresButton.width / 2;
            this._highscoresButton.y = this._mainMenuButtonStart.y - this._mainMenuButtonStart.height - this._highscoresButton.height * 1.25;

            this._fullscreenButton = this._game.add.button(0, 0, 'fullscreen-button', this.ToggleFullscreen, this, 'fullscreen-button-hover');
            this._fullscreenButton.scale.setTo(0.45);
            this._fullscreenButton.x = this._game.width / 2 - this._fullscreenButton.width / 2;
            this._fullscreenButton.y = this._game.height - this._highscoresButton.height - this._game.height * 0.025;

            this._menuArray = new Array<any>();
            this._menuArray.push(this._highscoresButton);
            this._menuArray.push(this._mainMenuButtonOptions);
            this._menuArray.push(this._mainMenuButtonStart);
            this._menuArray.push(this._fullscreenButton);
            this._menuArray.push(this.logo);
            this._menuArray.push(this._mainMenuButtonShop);

            var i = 0;
            var tween

            for (var v in this._menuArray) {
                this._menuPositionArray[v] = this._menuArray[v].x;
                i++;
            }

            this._menuState.MainMenuButtonOptions = this._mainMenuButtonOptions.width;
        }

        private OnHighscoresButtonPressed(): void
        {
            this.disableMenu();
            this._highscoreScreen.Toggle(true);
            this._highscoreScreen.TweenShow();

            State.MenuState.ButtonAudio.play();
        }

        private ToggleFullscreen(): void
        {
            if (this._game.scale.isFullScreen)
            {
                this._game.scale.stopFullScreen();
            } else
            {
                this._game.scale.startFullScreen();
            }
        }

        private OnHighscoresScreenClosed(): void
        {
            this.enableMenu();

            State.MenuState.BackAudio.play();
        }

        //fades out the screen to black
        private fadeScreenOut()
        {
            this._menuArray.push(this._coinAnimation);
            this._menuArray.push(this._coinBorder);
            this._menuArray.push(this._currencyAmountDisplay);

            this.disableMenu();
            MenuState.ButtonAudio.play();
            this._menuState.dissableMusic(800);


            var firstTimePlaying: boolean = !Object.SharedObject.HasLocalStorageItem('hasPlayedOnce');

            if (firstTimePlaying)
            {
                this._game.camera.fade(0xffffff, 1000);
            } else
            {
                this._game.camera.fade(0x000000, 1000);
            }
        }

        private StartNovel(): void
        {
            this._menuState.OnGameStarted();
            this._menuState.dissableMusic(0);
            this._game.stage.backgroundColor = '#fff';
            this._game.state.start('Novel');
        }

        //Starts when the start game button is pressed
        private startGame(): void
        {
            this._menuState.OnGameStarted();
            this._menuState.dissableMusic(0);
            this._game.stage.backgroundColor = '#000';
            this._game.state.start('Preload');
        }
    }
}