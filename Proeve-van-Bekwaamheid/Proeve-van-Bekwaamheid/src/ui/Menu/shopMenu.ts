﻿module ProeveVanBekwaamheid.State
{
    export class ShopMenu extends Object.SharedObject implements UI.IShopButtonPressedEventHandler
    {
        private _game: Client.GameEngine;

        private _menuState: MenuState;

        //shop menu
        private _shopPositionArray: any[];
        private _shopArray: any[];
        private _shopLogo: Phaser.Sprite;
        private _shopCoinBorder: Phaser.Sprite;
        private _objectBorder: Phaser.Sprite;
        private _buttonBorder: Phaser.Sprite;
        private _buyButton: Phaser.Button;
        private _applyButton: Phaser.Button;
        private _shopButtonBackToMenu: Phaser.Button;
        private _shopSlider: Phaser.Image;
        private _shopSliderHandler: Phaser.Sprite;
        private _upgradeText: Phaser.Text;

        private _adsButton: Phaser.Button;

        private _mask: Phaser.Graphics;
        private _shopItems: UI.ShopButton[];
        private _selectedShopButton: number = 0;

        private _isDragging: boolean = false;
        private _scrollProgress: number = 0;
        private _previousScrollProgress: number = 0;
        private _dragStartMouseY: number = 0;

        private _adsAllowed: boolean;

        private _returnSound: Phaser.Sound;
        /**
         * 
         * @param game A reference to the game
         * @param menuState menuState A reference to the menu state
         */
        constructor(game: Client.GameEngine, menuState: MenuState, adsAllowed: boolean) 
        {
            super(game, 0, 0);

            this._adsAllowed = adsAllowed;

            this._game = game;
            this._menuState = menuState;
            this.createShop();

            this._returnSound = this._game.sound.add('returnSound');

            
        }

        public OnStart(): void { }
        public IndexDependencies(graphicsRequests: EntityGraphicsLoadRequestCache): void { }

        public OnUpdate(): void
        {
            super.OnUpdate();

            var mousePos = this.game.input.activePointer;

            if (this.game.input.activePointer.isDown && this._objectBorder.getBounds().contains(mousePos.x, mousePos.y))
            {
                var yPositions: number[] = [];

                var yDiff = mousePos.y - this._dragStartMouseY;

                for (let i = 0; i < this._shopItems.length; i++)
                {
                    var isFirst: boolean = i == 0;
                    var isLast: boolean = i + 1 >= this._shopItems.length;

                    var yPos = this._shopItems[i].StartY + this._previousScrollProgress + yDiff;

                    // Keep items in the view
                    if (isFirst && yPos > this._shopItems[i].StartY)
                        return;                    
                    if (isLast && yPos < (this._objectBorder.y + this._objectBorder.height - this._shopItems[i].height / 2 - 25)) // 25 = bottom padding
                        return;

                    yPositions.push(yPos);
                }

                this._scrollProgress = this._previousScrollProgress + yDiff;

                for (let i = 0; i < this._shopItems.length; i++)
                {
                    this._shopItems[i].y = yPositions[i];
                }
            }
        }

        /**
         * Dissables the shop menu, tweens out and dissables the shop buttons
         */
        public dissableShopMenu(): void
        {
            var i = 0;

            var tween

            this.RemoveEventListeners();

            this._returnSound.play()

            this._shopArray.reverse();
            for (var v in this._shopArray) {
                this._shopArray[v].inputEnabled = false;
                tween = this._game.add.tween(this._shopArray[v]);
                tween.to({ x: this._game.width + this._shopArray[v].width }, 500, 'Quart.easeOut', true, i * 25);
                i++;
            }
            this._shopArray.reverse();

            this._upgradeText.text = "";
            this._mask.inputEnabled = false;
        }

        /**
         * Enables the shop menu, tweens in and enables the shop buttons
         */
        public enableShopMenu(): void
        {
            var i = 0;

            var tween

            this.AddEventListeners();

            this.ChangeActiveButtons();

            for (var v in this._shopArray) {
                this._shopArray[v].inputEnabled = true;
                tween = this._game.add.tween(this._shopArray[v]);
                tween.to({ x: this._shopPositionArray[v] }, 500, 'Quart.easeOut', true, i * 40);
                i++;
            }


            this._mask.inputEnabled = true;
        }

        /**
         * When a shop button is down this will be called as callback.
         */
        public OnShopButtonDown(index: number): void
        {
            this._shopItems[this._selectedShopButton].DeselectButton();
            this._selectedShopButton = index;
            this._shopItems[this._selectedShopButton].SelectButton();
            this.ChangeActiveButtons();

            // Update upgradeables
            if (index >= statics.Inventory.SKINS.length)
            {
                this._shopItems[index].UpdatePrice();
            }

            this._upgradeText.text = this._shopItems[index].ShopItem.UpgradeText;
            this._upgradeText.pivot.x = this._upgradeText.width / 2;

            this.StartDrag();
        }

        public OnShopButtonUp(index: number): void
        {
            this.StopDrag();
        }

        public startAddsButton(): void
        {
            this._adsButton.alpha = 0;
            this._adsButton.inputEnabled = false;

            this.dissableShopMenu();
            this._menuState.startAdd();

            for (var v in this._shopArray) {

                if (this._shopArray[v] == this._adsButton)
                {

                    this._shopArray.splice(parseInt(v), 1);
                    this._shopPositionArray.splice(parseInt(v), 1);
                }
            }

            this._adsButton.destroy();
        }

        //Creates the shop menu
        private createShop()
        {
            var selectedSkin = statics.Inventory.SelectedSkin;

            this._shopLogo = this._menuState.CreateUi.createImage(this._game.width / 2, this._game.height / 12.5, 'shopMenu', 'ShopLogo', 0.5, 0);
            this._shopCoinBorder = this._menuState.CreateUi.createImage(this._game.width / 2, this._game.height / 4, 'shopMenu', 'CoinCountBorder', 0.5, 0.5);
            this._objectBorder = this._menuState.CreateUi.createImage(this._game.width / 2, this._game.height / 10 * 3.25, 'shopMenu', 'ChoiceBorder', 0.5, 0);
            this._objectBorder.height = this._objectBorder.height *= 0.8;
            this._buttonBorder = this._menuState.CreateUi.createImage(this._game.width / 2, this._game.height / 10 * 9, 'shopMenu', 'ButtonHolder', 0.5, 0.5);
            this._shopButtonBackToMenu = this._menuState.CreateUi.createButton(0, 0, 'shopMenu', this._menuState.mainMenu, 'BackButton', 'BackButton', this._menuState, 0, 0);
            this._mask = this._menuState.add.graphics(0, 0);
            this._mask.beginFill(0x000000, 0);
            this._mask.drawRoundedRect(this._objectBorder.x - this._objectBorder.width / 2, this._objectBorder.y, this._objectBorder.width, this._objectBorder.height, 8);
            
            this._adsButton = this._menuState.CreateUi.createButton(this._shopLogo.width * 1.5, this._shopLogo.y + this._shopLogo.height / 2, 'adsButton', this.startAddsButton, 'adsButton', 'adsButton', this, 0.5, 0.5);
            this._adsButton.scale.x = this._adsButton.scale.y = this._menuState.CreateUi.GeneralScale / 2;

            this._mask.events.onInputDown.add(this.StartDrag, this);
            this._mask.events.onInputUp.add(this.StopDrag, this);    

            this._upgradeText = this.game.add.text(this._shopCoinBorder.x, this._shopCoinBorder.y, '', { fill: '#fff',  fontSize: 22, stroke: 3, strokeThinkness: 3, strokeColor: "222" });
            this._upgradeText.pivot.set(this._upgradeText.width / 2, this._upgradeText.height / 2);

            this._shopItems = [];
            var powerupsAmount = statics.Inventory.POWERUPS.length;
            var skinsAmount = statics.Inventory.SKINS.length;
            var itemsAmount = skinsAmount + powerupsAmount;
            var columns = Math.ceil(itemsAmount / 3);
            var rows = Math.ceil(itemsAmount / columns);
            var index = 0;
            for (var column: number = 0; column < columns; column++)
            {
                for (var row: number = 0; row < rows; row++)
                {
                    if (index >= itemsAmount)
                        break;

                    var startX = this._objectBorder.x + 90 - this._objectBorder.width/2;
                    var startY = this._objectBorder.y + 90;
                    var xPos = startX + (150 * row);
                    var yPos = startY + (150 * column);

                    var newItemButton = new UI.ShopButton(this._game, xPos, yPos, 'shopMenu', null, null, index, 'Choice', 'Choice', 'Choice');
                    newItemButton.events.onInputDown.add(newItemButton.OnClick, newItemButton);

                    newItemButton = this._menuState.add.existing(newItemButton);
                    newItemButton.anchor.x = 0.5;
                    newItemButton.anchor.y = 0.5;
                    newItemButton.scale.setTo(this._menuState.CreateUi.GeneralScale);
                    newItemButton.SavedScale = newItemButton.scale;
                    newItemButton.mask = this._mask;

                    this._shopItems.push(newItemButton);
                    index++;
                }
            }

            this._buyButton = this._menuState.CreateUi.createButton(this._buttonBorder.x - this._buttonBorder.width / 4, this._buttonBorder.y, 'shopMenu', this.BuySelectedButton, 'BuyClick', 'BuyHover', this, 0.5, 0.5);
            this._applyButton = this._menuState.CreateUi.createButton(this._buttonBorder.x + this._buttonBorder.width / 5, this._buttonBorder.y, 'shopMenu', this.ApplyButton, 'Apply', 'ApplyClick', this, 0.5, 0.5);


            this._shopArray = new Array<any>();
            this._shopArray.push(this._shopLogo);
            this._shopArray.push(this._shopCoinBorder);
            this._shopArray.push(this._objectBorder);
            this._shopArray.push(this._buttonBorder);
            this._shopArray.push(this._buyButton);
            this._shopArray.push(this._applyButton);
            this._shopArray.push(this._shopButtonBackToMenu);
            this._shopArray.push(this._upgradeText);
            this._shopArray.push(this._adsButton);

            if (this._adsAllowed)
            {
                this._adsButton = this._menuState.CreateUi.createButton(this._shopLogo.width * 1.5, this._shopLogo.y + this._shopLogo.height / 2, 'adsButton', this.startAddsButton, 'adsButton', 'adsButton', this, 0.5, 0.5);
                this._adsButton.scale.x = this._adsButton.scale.y = this._menuState.CreateUi.GeneralScale / 2;

                this._shopArray.push(this._adsButton);
            }

           

            //this._shopArray.push(this._shopSlider);
            //this._shopArray.push(this._shopSliderHandler);
            
            for (var i: number = 0; i < itemsAmount; i++)
            {
                this._shopArray.push(this._shopItems[i]);
            }

            var i = 0;
            var tween

            this._shopPositionArray = new Array<any>();

            for (var v in this._shopArray) {
                this._shopPositionArray[v] = this._shopArray[v].x;
                this._shopArray[v].x = this._game.width + this._shopArray[v].width;
                i++;
            }

            this.game.input.onUp.add(this.StopDrag, this);
        }

        private ChangeActiveButtons(): void
        {
            if (this._selectedShopButton < statics.Inventory.SKINS.length)
            {
                this.CheckSelectedSkin();
            }
            else
            {
                this.CheckSelectedUpgrade();
            }
        }

        private AddEventListeners(): void
        {
            var length = this._shopItems.length;
            for (var i: number = 0; i < length; i++)
            {
                this._shopItems[i].OnShopButtonPressedEventHandler.AddEventListener(this);
            }
        }

        private RemoveEventListeners(): void
        {
            var length = this._shopItems.length;
            for (var i: number = 0; i < length; i++)
            {
                this._shopItems[i].OnShopButtonPressedEventHandler.RemoveEventListener(this);
            }
        }

        //Activated when the buy button is pressed
        private BuySelectedButton(): void
        {
            if (this._selectedShopButton < statics.Inventory.SKINS.length)
            {
                this.TryBuySkin();
            }
            else
            {
                this.TryBuyPowerup();
            }
            State.MenuState.ButtonAudio.play();
        }

        private TryBuySkin(): void
        {
            var selectedSkin = statics.Inventory.SKINS[this._selectedShopButton].SkinKey;
            if (statics.Inventory.HasSkin(selectedSkin))
            {
                // Already has
                return;
            }
            if (statics.Inventory.AttemptBuySkin(selectedSkin))
            {
                this._menuState.updateCurrencyDisplay();
                statics.Inventory.SelectSkin(selectedSkin);
                this.ChangeActiveButtons();

                this._shopItems.forEach(function (item)
                {
                    item.UpdatePrice();
                });


                // Succes
            }
            else
            {
                // Failed
            }
        }

        private TryBuyPowerup(): void
        {
            var powerupIndex: number = this._selectedShopButton - statics.Inventory.SKINS.length;
            var selectedPowerup = statics.Inventory.POWERUPS[powerupIndex].PowerupKey;
            if (statics.Inventory.HasMaxUpgrade(selectedPowerup))
            {
                // Already has
                return;
            }
            if (statics.Inventory.AttemptBuyUpgrade(selectedPowerup))
            {
                this._menuState.updateCurrencyDisplay();
                this.ChangeActiveButtons();
                // Succes
            }
            else
            {
                // Failed
            }
        }

        private CheckSelectedSkin(): void
        {
            var selectedSkin = statics.Inventory.SKINS[this._selectedShopButton].SkinKey;
            if (statics.Inventory.HasSkin(selectedSkin))
            {
                this._buyButton.tint = 0x2f4f4f;
                if (statics.Inventory.SelectedSkin.SkinKey === selectedSkin)
                {
                    this._applyButton.tint = 0x2f4f4f;
                }
                else
                {
                    this._applyButton.tint = 0xffffff;
                }
            }
            else
            {
                this._applyButton.tint = 0x2f4f4f;
                this._buyButton.tint = 0xffffff;
            }
        }

        private CheckSelectedUpgrade(): void
        {
            var upgradeIndex = this._selectedShopButton - statics.Inventory.SKINS.length;
            var selectedUpgrade = statics.Inventory.POWERUPS[upgradeIndex].PowerupKey;
            if (statics.Inventory.HasMaxUpgrade(selectedUpgrade))
            {
                this._buyButton.tint = 0x2f4f4f;
                this._applyButton.tint = 0x2f4f4f;
            }
            else
            {
                this._applyButton.tint = 0x2f4f4f;
                this._buyButton.tint = 0xffffff;
            }

            this._shopItems[this._selectedShopButton].UpdatePrice();
        }

        private StartDrag(): void
        {
            this._isDragging = true;
            this._dragStartMouseY = this.game.input.activePointer.position.y;
        }

        private StopDrag(): void
        {
            this._isDragging = false;
            this._previousScrollProgress = this._scrollProgress;
        }

        //activated when the apply button is pressed
        private ApplyButton(): void
        {
            var selectedSkin = statics.Inventory.SKINS[this._selectedShopButton].SkinKey;
            if (!statics.Inventory.HasSkin(selectedSkin))
            {
                // Don't have the skin.
                return;
            }

            statics.Inventory.SelectSkin(selectedSkin);

            this._shopItems.forEach(function (item)
            {
                item.UpdatePrice();
            });

            this.ChangeActiveButtons();

            State.MenuState.ButtonAudio.play();
        }
    }
}