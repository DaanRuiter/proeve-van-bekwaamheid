﻿module ProeveVanBekwaamheid.UI {
    export class UiCreater
    {
        public get GeneralScale(): number
        {
            return this._generalScale;
        }

        private _game: Client.GameEngine;

        public _logo: Phaser.Image;
        private _generalScale: any;
        private _menuState: ProeveVanBekwaamheid.State.MenuState;

        /**
         * 
         * @param game A reference to the game
         * @param menuState A reference to the menu state
         */
        constructor(game: Client.GameEngine, menuState: ProeveVanBekwaamheid.State.MenuState) {
            this._game = game;
            this._menuState = menuState;
            this.setScale();
        }


        /**
         * Creates an Image, scales and sets the anchors for the image
         * @param xPos X position where the image will be placed
         * @param yPos Y position where the image will be placed
         * @param fileKey The key attatched to the file you want to reach
         * @param nameKey The name of the object you want to reach
         * @param anchorX X anchor position of the object
         * @param anchorY Y anchor position of the project
         */
        public createImage(xPos, yPos, fileKey, nameKey, anchorX, anchorY): Phaser.Sprite
        {
            var item;

            item = this._menuState.add.sprite(xPos, yPos, fileKey, nameKey);
            item.anchor.x = anchorX;
            item.anchor.y = anchorY;
            item.scale.setTo(this._generalScale);

            return item;
        }

        //Create buttons
        //Scale buttons
        //Set button anchors
        /**
         * Creates a button, scales and sets the anchors for the button
         * @param xPos X position where the image will be placed
         * @param yPos Y position where the image will be placed
         * @param fileKey The key attatched to the file you want to reach
         * @param callbackFunc Function that is being called when the button is pressed
         * @param nameKey The name of the object you want to reach
         * @param nameKeyPressed The name of the object you want to reach when the button is pressed
         * @param _this callback function reference script
         * @param anchorX X anchor position of the object
         * @param anchorY Y anchor position of the project
         */
        public createButton(xPos, yPos, fileKey, callbackFunc, nameKey, nameKeyPressed, _this, anchorX, anchorY): Phaser.Button
        {
            var item;

            item = this._menuState.add.button(xPos, yPos, fileKey, callbackFunc, _this, nameKey, nameKey, nameKeyPressed);
            item.anchor.x = anchorX;
            item.anchor.y = anchorY;
            item.scale.setTo(this._generalScale);

            return item;
        }

        public createText(x: number, y: number, content: string, fontSize: number, fillColor: string, anchorX: number, anchorY: number): Phaser.Text
        {
            var text = this._menuState.add.text(x, y, content,
                {
                    fill: fillColor,
                    fontSize: fontSize
                });

            text.anchor.x = anchorX;
            text.anchor.y = anchorY;
            text.scale.setTo(this._generalScale);

            return text;
        }

        //Sets the general scale of objects
        private setScale() {
            this._logo = this.createImage(this._game.width / 2, this._game.height / 10, 'mainMenu', 'Logo', 0.5, 0);

            this._logo.width = this._game.width * 0.8;
            this._generalScale = this._logo.scale.x;
            this._logo.scale = this._generalScale;
        }
    }
}