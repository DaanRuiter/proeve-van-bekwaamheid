﻿module ProeveVanBekwaamheid.State
{
    export class OptionsMenu
    {
        public static get VOLUME_LOCAL_STORAGE_KEY(): string
        {
            return 'volume';
        }

        private _game: Client.GameEngine;

        //options menu
        private _optionsArray: any[];
        private _audioSlider: Phaser.Image;
        private _inputHandler: Phaser.Image;

        private _mainMenuButtonOptions: any;
        private _menuState: MenuState;

        /**
         * 
         * @param game A reference to the game
         * @param menuState menuState A reference to the menu state
         * @param optionsButton A reference to the options button width
         */
        constructor(game: Client.GameEngine, menuState: MenuState, optionsButton)
        {
            this._game = game;
            this._menuState = menuState;
            this._mainMenuButtonOptions = optionsButton;
            this.createOptions();
        }

        //Dissables the option menu
        /**
         * Dissables the options menu, tweens out and dissables the options slider
         */
        public dissableOptionsMenu(): void
        {
            var tween;

            for (var v in this._optionsArray) {
                tween = this._game.add.tween(this._optionsArray[v]);
                tween.to({ y: -this._mainMenuButtonOptions }, 500, 'Quart.easeOut', true, 0);

            }
        }

        /**
         * Enables the options menu, tweens in and enables the options slider
         */
        public enableOptionsMenu(): void
        {
            var tween;
            tween = this._game.add.tween(this._optionsArray[0]);
            tween.to({ y: this._mainMenuButtonOptions }, 500, 'Quart.easeOut', true, 0);
            tween = this._game.add.tween(this._optionsArray[1]);
            tween.to({ y: this._mainMenuButtonOptions + this._audioSlider.height / 2 }, 500, 'Quart.easeOut', true, 0);
        }

        //creates the option menu
        private createOptions(): void
        {
            this._audioSlider = this._menuState.CreateUi.createImage(this._mainMenuButtonOptions, -this._mainMenuButtonOptions, 'mainMenu', 'Sound', 0, 0);

            this._inputHandler = this._menuState.CreateUi.createImage(this._audioSlider.x + this._audioSlider.width / 10 * 9, this._audioSlider.y + this._audioSlider.height / 2, 'mainMenu', 'Sound_Button', 0.5, 0.5);

            this._inputHandler.inputEnabled = true;
            this._inputHandler.input.enableDrag(true);

            this._optionsArray = new Array<any>();
            this._optionsArray.push(this._audioSlider);
            this._optionsArray.push(this._inputHandler);

            this._inputHandler.events.onDragUpdate.add(this.dragUpdateAudioSlider, this);

            //load saved audio level
            if (Object.SharedObject.HasLocalStorageItem(OptionsMenu.VOLUME_LOCAL_STORAGE_KEY))
            {
                var loadedVolume = Number(Object.SharedObject.GetLocalStorageItem(OptionsMenu.VOLUME_LOCAL_STORAGE_KEY));
                var maxVolume = (this._audioSlider.x + this._audioSlider.width / 10 * 9) - (this._audioSlider.x + this._audioSlider.width / 3);
                

                this._game.sound.volume = loadedVolume;
                this._inputHandler.x = this._audioSlider.x + this._audioSlider.width * loadedVolume;
                this.dragUpdateAudioSlider();
            }
        }

        //Holds the audio slider inside of its border
        private dragUpdateAudioSlider() {
            if (this._inputHandler.x < this._audioSlider.x + this._audioSlider.width / 3) {
                this._inputHandler.x = this._audioSlider.x + this._audioSlider.width / 3
            }
            else if (this._inputHandler.x > this._audioSlider.x + this._audioSlider.width / 10 * 9) {
                this._inputHandler.x = this._audioSlider.x + this._audioSlider.width / 10 * 9;
            }

            this._inputHandler.y = this._audioSlider.y + this._audioSlider.height / 2;

            var MaxVol = (this._audioSlider.x + this._audioSlider.width / 10 * 9) - (this._audioSlider.x + this._audioSlider.width / 3);
            var newVolume = this._inputHandler.x - (this._audioSlider.x + this._audioSlider.width / 3);
            //change volume

            this._game.sound.volume = (newVolume / MaxVol);

            Object.SharedObject.SetLocalStorageItem(OptionsMenu.VOLUME_LOCAL_STORAGE_KEY, this._game.sound.volume);
        }
    }
}