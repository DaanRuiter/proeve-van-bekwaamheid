﻿module ProeveVanBekwaamheid.UI
{
    /**
     * Displays the player's highscore and compares is to that of the developers
     */
    export class HighscoresScreen
    {
        private _game: Client.GameEngine;

        private _widestEntry: number = -1;
        private _inScreenX: number = 0;
        private _backButtonActive: boolean = false;
        
        private _highscoresLogo: Phaser.Image;
        private _entries: HighscoresEntry[];
        private _backButton: Phaser.Button;

        private _screenClosedCallback: Function;
        private _closedCallbackContext: any;

        constructor(game: Client.GameEngine, screenClosedCallback: Function, closedCallbackContext: any)
        {
            this._game = game;
            this._screenClosedCallback = screenClosedCallback;
            this._closedCallbackContext = closedCallbackContext;

            this._entries = new Array<HighscoresEntry>();
            this._entries.push(new HighscoresEntry(game, "Daan", 1));
            this._entries.push(new HighscoresEntry(game, "Menno", 2));
            this._entries.push(new HighscoresEntry(game, "Koen", 3));
            this._entries.push(new HighscoresEntry(game, "Lotte", 4));
            this._entries.push(new HighscoresEntry(game, "Cerys", 5));
            this._entries.push(new HighscoresEntry(game, "Luuc", 6));
            this._entries.push(new HighscoresEntry(game, "Bart", 7));
            this._entries.push(new HighscoresEntry(game, "Gerben", 8));

            if (Object.SharedObject.HasLocalStorageItem('highscore'))
            {
                let score: number = Number(Object.SharedObject.GetLocalStorageItem('highscore'));

                this._entries.push(new HighscoresEntry(game, "You", score, '#ffdf44')); 
            }

            this._entries.sort(function (a, b) { return (a.Score < b.Score) ? 1 : ((b.Score < a.Score) ? -1 : 0); }); 

            var startY = 350;

            this._highscoresLogo = game.add.image(0, 0, 'highscores-logo');
            this._highscoresLogo.scale.setTo(0.45);
            this._highscoresLogo.x = game.width / 2 - this._highscoresLogo.width / 2;
            this._highscoresLogo.y = startY - this._highscoresLogo.height - HighscoresEntry.ENTRY_Y_DIFFERENCE;

            for (let i = 0; i < this._entries.length; i++)
            {
                let entry: HighscoresEntry = this._entries[i];

                if (entry.MinimumWidth > this._widestEntry)
                {
                    this._widestEntry = entry.MinimumWidth;
                }
            }

            var bottomElementY = 0;
            for (let i = 0; i < this._entries.length; i++)
            {
                let y = startY + (HighscoresEntry.ENTRY_Y_DIFFERENCE + this._entries[i].MinimumHeight) * i;

                this._entries[i].Transform(
                    game.width / 2 - this._widestEntry / 2,
                    y, 
                    this._widestEntry);

                if (i + 1 < this._entries.length)
                {
                    bottomElementY = y + this._entries[i].MinimumHeight;
                }
            }

            this._backButton = game.add.button(0, 0, 'back-button', this.OnBackButtonPressed, this);
            this._backButton.scale.set(0.35);
            this._backButton.x = this._game.width / 2 - this._backButton.width / 2;
            this._backButton.y = bottomElementY + 100;
        }

        /**
         * Enables the screen and animates the elements into view.
         */
        public TweenShow(): void
        {
            this._backButtonActive = true;

            this._highscoresLogo.alpha = 0;

            var logoTween = this._game.add.tween(this._highscoresLogo);
            logoTween.to({ alpha: 1 }, 650, Phaser.Easing.Sinusoidal.InOut);
            logoTween.start();
            
            for (let i = 0; i < this._entries.length; i++)
            {
                if (this._inScreenX == -1)
                {
                    this._inScreenX = this._entries[i].Container.x;
                }
                
                this._entries[i].Container.x = this._game.width;

                let tween = this._game.add.tween(this._entries[i].Container);
                tween.to({ x: this._inScreenX }, 300 + i * 85, Phaser.Easing.Sinusoidal.InOut);
                tween.start();
            }

            this._backButton.alpha = 0;

            var backButtonTween = this._game.add.tween(this._backButton);
            backButtonTween.to({ alpha: 1 }, 350, Phaser.Easing.Sinusoidal.InOut, true, 500);
        }

        /**
         * Disables the screen and animates the elements out of view.
         */
        public TweenHide(): void
        {
            this._backButtonActive = false;

            var logoTween = this._game.add.tween(this._highscoresLogo);
            logoTween.to({ alpha: 0 }, 350, Phaser.Easing.Sinusoidal.InOut);
            logoTween.start();

            for (let i = 0; i < this._entries.length; i++)
            {
                var tween = this._game.add.tween(this._entries[i].Container);
                tween.to({ x: this._game.width }, 100 + i * 85, Phaser.Easing.Sinusoidal.InOut);
                tween.start();
            }

            var backButtonTween = this._game.add.tween(this._backButton);
            backButtonTween.to({ alpha: 0 }, 150, Phaser.Easing.Sinusoidal.InOut);
            backButtonTween.start();
        }

        /**
         * Disables the visibilty of all elements.
         * @param newState The new visibilty state of each element.
         */
        public Toggle(newState:boolean): void
        {
            this._backButton.visible = newState;
            this._highscoresLogo.visible = newState;

            for (let i = 0; i < this._entries.length; i++)
            {
                this._entries[i].Container.visible = newState;
            }
        }

        private OnBackButtonPressed(): void
        {
            if (!this._backButtonActive) { return; }

            this.TweenHide();

            this._screenClosedCallback.call(this._closedCallbackContext);
        }
    }
}