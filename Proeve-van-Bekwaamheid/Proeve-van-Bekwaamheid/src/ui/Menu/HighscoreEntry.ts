﻿module ProeveVanBekwaamheid.UI
{
    /**
     * Used for the leaderboards to display a single entry in the list.
     */
    export class HighscoresEntry
    {
        /**
        * Difference in pixels between each element on the vertical axis.
        */
        public static get ENTRY_Y_DIFFERENCE(): number
        {
            return 8;
        }

        /**
        * Distance between the name and the score amount.
        */
        public static get SCORE_NAME_DISTANCE(): number
        {
            return 95;
        }

        /**
        * The horizontal padding between the edges of the entry and the score and name displays
        */
        public static get ENTRY_TEXT_X_PADDING(): number
        {
            return 15;
        } 

        /**
        * The vertical padding between the edges of the entry and the score and name displays
        */
        public static get ENTRY_TEXT_Y_PADDING(): number
        {
            return 8;
        }

        /**
        * Minimum width of an entry.
        */
        public static get MINIMUM_ENTRY_WIDTH(): number
        {
            return 375;
        }

        /**
        * The score that this entry displays.
        */
        public get Score(): number
        {
            return this._score;
        }

        /**
        * The group of that contains all the elements of this entry.
        */
        public get Container(): Phaser.Group
        {
            return this._container;
        }

        /**
        * Calculates the minimum width of this entry to comply with the MINIMUM_ENTRY_WIDTH and any padding between the texts.
        */
        public get MinimumWidth(): number
        {
            let width = HighscoresEntry.ENTRY_TEXT_X_PADDING * 2 + this._nameText.width + HighscoresEntry.SCORE_NAME_DISTANCE + this._scoreText.width;

            if (width < HighscoresEntry.MINIMUM_ENTRY_WIDTH)
            {
                width = HighscoresEntry.MINIMUM_ENTRY_WIDTH;
            }

            return width;
        }

        /**
        * Calculates the minimum width of this entry to comply with any padding between the texts.
        */
        public get MinimumHeight(): number
        {
            return HighscoresEntry.ENTRY_TEXT_Y_PADDING * 2 + this._nameText.height;
        }

        private _game: Client.GameEngine;
        private _name: string;
        private _score: number;

        private _nameText: Phaser.Text;
        private _scoreText: Phaser.Text;
        private _backgroundImage: Phaser.Image;
        private _container: Phaser.Group

        constructor(game: Client.GameEngine, name: string, score: number, textColor:string = '#fff')
        {
            this._name = name;
            this._score = score;
            this._game = game;

            let style = { fill: textColor, stroke: '#333' };

            this._container = this._game.add.group();

            this._backgroundImage = game.add.image(0, 0, 'shopMenu', 'ChoiceBorder', this._container);
            this._nameText = game.add.text(0, 0, name, style, this._container);
            this._scoreText = game.add.text(0, 0, score.toString(), style, this._container);
        }

        /**
         * Moves the position and changes the width of the conainer.
         * @param x New x position of the container in pixels.
         * @param y New y position of the container in pixels.
         * @param width New width position of the container in pixels.
         */
        public Transform(x: number, y: number, width: number): void
        {
            this._backgroundImage.width = width;
            this._backgroundImage.height = HighscoresEntry.ENTRY_TEXT_Y_PADDING * 2 + this._nameText.height;
            this._backgroundImage.x = x;
            this._backgroundImage.y = y;

            this._nameText.x = x + HighscoresEntry.ENTRY_TEXT_X_PADDING;
            this._nameText.y = y + HighscoresEntry.ENTRY_TEXT_Y_PADDING;

            this._scoreText.pivot.set(this._scoreText.width, this._scoreText.pivot.y);
            this._scoreText.x = this._backgroundImage.x + this._backgroundImage.pivot.x + this._backgroundImage.width - HighscoresEntry.ENTRY_TEXT_X_PADDING;
            this._scoreText.y = y + HighscoresEntry.ENTRY_TEXT_Y_PADDING;
        }
    }
}