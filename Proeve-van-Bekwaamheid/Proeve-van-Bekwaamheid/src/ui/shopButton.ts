﻿module ProeveVanBekwaamheid.UI
{
    export interface IShopButtonPressedEventHandler
    {
        OnShopButtonDown(index: number);
        OnShopButtonUp(index: number);
    }

    export class ShopButton extends Phaser.Button
    {
        public get ShopItem(): statics.BuyableItem
        {
            if (this._inventoryID < statics.Inventory.SKINS.length)
            {
                return statics.Inventory.SKINS[this.InventoryID];
            }
            var powerupIndex = this._inventoryID - statics.Inventory.SKINS.length;
            return statics.Inventory.POWERUPS[powerupIndex];
        }

        public get InventoryID(): number
        {
            return this._inventoryID;
        }

        public get PriceText(): Phaser.Text
        {
            return this._price;
        }

        public get StartY(): number
        {
            return this._startY;
        }

        /**
       * Event for when this button is clicked.
       */
        public get OnShopButtonPressedEventHandler(): Events.EventHandlerHolder<IShopButtonPressedEventHandler>
        {
            return this._onShopButtonPressedEventHandler;
        }

        public set SavedScale(value: Phaser.Point)
        {
            this._savedScale.set(value.x, value.y);
        }

        private _onShopButtonPressedEventHandler: Events.EventHandlerHolder<IShopButtonPressedEventHandler>;
        private _inventoryID: number;
        private _price: Phaser.Text;
        private _selectedSprite: Phaser.Sprite;
        private _savedScale: Phaser.Point;
        private _fadeTween: Phaser.Tween;
        private _startY: number;

        constructor(game: Phaser.Game, x: number, y: number, key: string, callback: Function, callbackContext: any, inventoryID: number, overFrame?: string | number, outFrame?: string | number, downFrame?: string | number, upFrame?: string | number)
        {
            super(game, x, y, key, callback, callbackContext, overFrame, outFrame, downFrame, upFrame);

            this._savedScale = new Phaser.Point(1, 1);
            this._selectedSprite = this.game.add.sprite(0, 0, 'shopMenu', 'ChoiceHover');
            this._selectedSprite.anchor.set(0.5, 0.5);
            this.addChild(this._selectedSprite);
            this._selectedSprite.visible = false;

            this._startY = y;
            this._inventoryID = inventoryID;
            this._onShopButtonPressedEventHandler = new Events.EventHandlerHolder<IShopButtonPressedEventHandler>();

            this.DisplayPrice();
        }

        /**
         * Onclick handler.
         */
        public OnClick(): void
        {
            State.MenuState.ButtonAudio.play();
            var eventHandlers = this._onShopButtonPressedEventHandler.EventHandlers;
            for (var i = 0; i < eventHandlers.length; i++)
            {
                eventHandlers[i].OnShopButtonDown(this._inventoryID);
            }
        }

        /**
         * Selects this button.
         */
        public SelectButton(): void
        {
            this.game.add.tween(this.scale).to({ x: this._savedScale.x * 1.25, y: this._savedScale.y * 1.25 }, 95).start();
            this._selectedSprite.visible = true;
            this._selectedSprite.alpha = 0.15;

            if (this._fadeTween)
            {
                this._fadeTween.stop();
            }

            this._fadeTween = this.game.add.tween(this._selectedSprite);
            this._fadeTween.to({ alpha: 1 }, 500, Phaser.Easing.Linear.None, true, 0, -1, true);
        }

        /**
         * Deselects this button.
         */
        public DeselectButton(): void
        {
            this.game.add.tween(this.scale).to({ x: this._savedScale.x, y: this._savedScale.y }, 95).start();
            this._selectedSprite.visible = false;
        }

        /**
         * Updates the price if this button can be bought multiple times.
         */
        public UpdatePrice(): void
        {
            if (this._inventoryID < statics.Inventory.SKINS.length)
            {
                let skin: statics.PlayerSkin = statics.Inventory.SKINS[this._inventoryID];

                if (statics.Inventory.HasSkin(skin.SkinKey))
                {
                    if (statics.Inventory.SelectedSkin.SkinKey == skin.SkinKey)
                    {
                        this._price.text = "APPLIED";
                        this._selectedSprite.tint = 0xfcfa94;
                        this.tint = 0xfcfa94;
                    }
                    else
                    {
                        this._price.text = "BOUGHT";
                        this._selectedSprite.tint = 0x83838c;
                        this.tint = 0x83838c;
                    }
                }
                else if (skin.Price <= 0)
                {
                    this._price.text = 'FREE';
                    this._selectedSprite.tint = 0xffffff;
                    this.tint = 0xffffff;
                }
                else
                {
                    this._price.text = skin.Price.toString();
                    this._selectedSprite.tint = 0xffffff;
                    this.tint = 0xffffff;
                }
            }
            else if (this._inventoryID >= statics.Inventory.SKINS.length)
            {
                var powerupIndex = this._inventoryID - statics.Inventory.SKINS.length;
                if (statics.Inventory.HasMaxUpgrade(statics.Inventory.POWERUPS[powerupIndex].PowerupKey))
                {
                    this._price.text = "MAX";
                }
                else
                {
                    this._price.text = statics.Inventory.POWERUPS[powerupIndex].GetPrice().toString();
                }
            }
        }

        private DisplayPrice(): void
        {
            var border = this.game.add.sprite(0, 0, 'shopMenu', 'ChoicePriceBorder');
            border.anchor.set(0.5, 0.5);

            var text = this.game.add.text(0, 0, '', {
                fill: '#fff',
                fontSize: 26,
                stroke: 0x000000,
                strokeThickness: 3
            });

            this._price = this.game.add.text(0, 0, '', {
                fill: '#fff',
                fontSize: 32,
                stroke: 0x000000,
                strokeThickness: 3
            });

            if (this._inventoryID < statics.Inventory.SKINS.length)
            {
                text.text = statics.Inventory.SKINS[this._inventoryID].SkinKey;

                var icon = this.game.add.sprite(0, 0, statics.Inventory.SKINS[this._inventoryID].SkinKey + '-icon');
                icon.anchor.set(0.5);
                icon.scale.set(2.25);
                this.addChild(icon);
            }
            else
            {
                var powerupIndex = this._inventoryID - statics.Inventory.SKINS.length;
                var powerupKey = statics.Inventory.POWERUPS[powerupIndex].PowerupKey;
                text.text = powerupKey;
                var icon = this.game.add.sprite(0, 0, statics.Inventory.POWERUPS[powerupIndex].Icon);
                icon.anchor.set(0.5, 0.5);
                this.addChild(icon);

            }
            this.UpdatePrice();

            text.anchor.set(0.5, 0.5);
            this._price.anchor.set(0.5, 0.5);

            text.y -= 55;
            text.x -= 0;
            this._price.y += 58;
            this._price.x -= 0;

            this.addChild(border);
            this.addChild(text);
            this.addChild(this._price);
        }
    }
}