﻿module ProeveVanBekwaamheid.UI
{
    /**
     * The UI Canvas containing all UI elements and logic of the GameState.
     */
    export class InGameUICanvas extends UICanvas
    {
        private _gameOverPopUp: UIGroup;

        /**
         * @param game Internally used reference to the game.
         * @param x X position of the canvas.
         * @param y Y position of the canvas.
         */
        constructor(game: Phaser.Game, x: number, y: number)
        {
            super(game, x, y);

            // Currency display
            let currenyDisplay: UI.UIGroup = this.AddGroup(new UI.UIGroup(this.game, 0, 0));
            currenyDisplay.AddElement(new UI.UIText(this.game, 60, 10, '000000', 24)).AddTag('display_currency');

            let currencyImage: UI.UIImage = currenyDisplay.AddElement(new UI.UIImage(this.game, 8, 4, 'mainMenu', 'CoinTurn-1')) as UI.UIImage;
            currencyImage.scale.setTo(0.5);
            currencyImage.animations.add('coinrot', Phaser.Animation.generateFrameNames('CoinTurn-', 1, 12), 10, true, false);
            currencyImage.play('coinrot');

            let scoreDisplay: UI.UIGroup = this.AddGroup(new UI.UIGroup(this.game, this.game.width - 150, 0));
            scoreDisplay.AddElement(new UI.UIText(this.game, this.game.width - 100, 10, '000000', 24)).AddTag('display_score');


            // Game over pop-up
            this._gameOverPopUp = this.AddGroup(new UIGroup(this.game, this.game.width / 2, this.game.height / 2));
            let bg = this._gameOverPopUp.AddElement(new UIImage(this.game, 0, 300, 'shopMenu', 'ChoiceBorder'));
            bg.pivot = new Phaser.Point(bg.width / 2, bg.height / 2);
            bg.scale.x = 0.35;
            bg.scale.y = 0.45;
            bg.x = this.game.width / 2;
            bg.y = this.game.height / 2 + bg.height * 0.15;

            let gameOverTitle: UIImage = this._gameOverPopUp.AddElement(new UIImage(this.game, bg.x, bg.y, 'gameOver', 'GameOver')) as UIImage;
            gameOverTitle.scale.set(0.25);
            gameOverTitle.anchor.set(0.5);
            gameOverTitle.MoveTo(this.game.width / 2, bg.y - bg.height / 2 + 74);

            let currentScoreText: UIText = this._gameOverPopUp.AddElement(new UIText(this.game, bg.x, bg.y, 'SCORE: 000000', 24, '#fff', '#000', 3, 'middle')) as UIText;
            currentScoreText.TextObject.pivot = new Phaser.Point(currentScoreText.TextObject.width / 2, 12);
            currentScoreText.MoveTo(this.game.width / 2, bg.y - bg.height / 2 + 214);
            currentScoreText.AddTag('current_score');

            var highscore = Number(Object.SharedObject.GetLocalStorageItem('highscore'));

            let highscoreText: UIText = this._gameOverPopUp.AddElement(new UIText(this.game, bg.x, bg.y, 'Highscore: ' + highscore.toString(), 24, '#fff', '#000', 3, 'middle')) as UIText;
            highscoreText.TextObject.pivot = new Phaser.Point(highscoreText.TextObject.width / 2, 12);
            highscoreText.MoveTo(this.game.width / 2, bg.y - bg.height / 2 + 162);
            highscoreText.AddTag('high_score');

            let mainMenuButton: UIButton = this._gameOverPopUp.AddElement(new UIButton(this.game, bg.x, bg.y, 'gameOver', 'MenuButtonHover', 'MenuButton')) as UIButton;
            mainMenuButton.ButtonObject.pivot = new Phaser.Point(mainMenuButton.ButtonObject.width / 2, 12);
            mainMenuButton.MoveTo(this.game.width / 2, bg.y + bg.height / 2 - 95);
            mainMenuButton.ButtonObject.scale = new Phaser.Point(0.45, 0.45);
            mainMenuButton.SetCallback(this.GoToMainMenu, this);

            let playButton: UIButton = this._gameOverPopUp.AddElement(new UIButton(this.game, bg.x, bg.y, 'mainMenu', 'Play_Button_Click', 'Play_Button')) as UIButton;
            playButton.ButtonObject.pivot = new Phaser.Point(playButton.ButtonObject.width / 2, 12);
            playButton.MoveTo(this.game.width / 2, bg.y + bg.height / 2 - 95 - mainMenuButton.ButtonObject.height - 15); 
            playButton.ButtonObject.scale = new Phaser.Point(0.45, 0.45);
            playButton.AddTag('play_button');

            // Hide the game over pop-up
            this._gameOverPopUp.SetVisibility(false);

            game.camera.onFadeComplete.addOnce(this.mainMenu, this);
        }

        /**
         * Toggle the visibilty of the canvas and all its elements.
         * @param state The new visibility state of the canvas and all its elements.
         */
        public ToggleGameOverPopup(state: boolean): void
        {
            this._gameOverPopUp.SetVisibility(state);
        }

        /**
         * Set the callback function for when the play button on the game over pop-up is pressed.
         * @param resetGameFunction Function to execute.
         * @param gameStateContext Object on which the function will be called.
         */
        public SetPlayButtonCallback(resetGameFunction: Function, gameStateContext: any): void
        {
            let playButton: UIButton = this._gameOverPopUp.FindElement('play_button') as UIButton;

            if (playButton)
            {
                playButton.SetCallback(resetGameFunction, gameStateContext);
            }
        }

        /**
         * Set the content of the game over pop-up.
         * @param score The player's final score before death.
         */
        public SetGameOverPopupContent(score: number): void
        {
            var scoreText: UIText = this._gameOverPopUp.FindElement('current_score') as UIText;

            if (scoreText)
            {
                var highscore = Number(Object.SharedObject.GetLocalStorageItem('highscore'));

                if (score > highscore)
                {
                    scoreText.TextObject.fill = '#55f';

                    let newHighscoreText: UIText = this._gameOverPopUp.AddElement(new UIText(this.game, scoreText.TextObject.x, scoreText.TextObject.y - 20, 'NEW HIGHSCORE!', 14, '#aaf', '#aaa', 0.5)) as UIText;
                    newHighscoreText.TextObject.pivot = new Phaser.Point(newHighscoreText.TextObject.width / 2, 12);
                    var tween: Phaser.Tween = this.game.add.tween(newHighscoreText.TextObject);
                    tween.to({ y: newHighscoreText.TextObject.y - 15 }, 1250, 'Cubic.easeInOut', true, 0, -1, true);
                } else
                {
                    scoreText.Translate(0, -12);
                }

                scoreText.Text = 'SCORE: ' + score.toString();
                scoreText.TextObject.pivot = new Phaser.Point(scoreText.TextObject.width / 2, 12);
            }
        }

        private GoToMainMenu(): void
        {
            var gameState = (Client.GameEngine.Instance.CurrentState as State.GameState);
            gameState.dissableMusic(800);

            this.game.camera.fade(0x000000, 1000);

        }

        private mainMenu()
        {
            var gameState = (Client.GameEngine.Instance.CurrentState as State.GameState);
            gameState.ResetGame();

            this.game.state.start('menuState');
        }
    }
}