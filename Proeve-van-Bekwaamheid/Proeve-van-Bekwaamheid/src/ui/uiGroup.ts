﻿module ProeveVanBekwaamheid.UI
{
    /**
     * Class that contains UI elements and allows moving, chaning alpha values and finding multiple ui elements with a single call.
     */
    export class UIGroup extends Object.SharedObject
    {
       /**
        * Elements contained in this group.
        */
        public get Elements(): UIElement[]
        {
            return this._uiElements;
        }

        private _uiElements: UIElement[];
        private _renderGroup: Phaser.Group;

        /**
         * @param game Reference to Phaser game used internally.
         * @param x X position of the group.
         * @param y Y position of the group.
         */
        public constructor(game: Phaser.Game, x: number, y: number)
        {
            super(game, x, y, null);

            this._uiElements = new Array<UIElement>();
            this._renderGroup = this.game.add.group();
        }

        /**
         * Executed every frame.
         * Used internally.
         */
        public OnUpdate(): void
        {
            //Keep UI on top
            this.game.world.bringToTop(this._renderGroup);
        }

        /**
         * Add an element to this ui group.
         * @param element Element to add.
         */
        public AddElement(element: UIElement): UIElement
        {
            this._uiElements.push(element);

            if (element.parent != null)
            {
                element.parent.removeChild(element.GetRenderObject());
            }

            this.addChild(element.GetRenderObject());
            this._renderGroup.add(element.GetRenderObject());

            return element;
        }

        /**
         * Remove an element from this ui group.
         * @param element Element to remove.
         */
        public RemoveElement(element: UIElement): UIElement
        {
            let index = this._uiElements.indexOf(element);

            if (index >= 0)
            {
                this._uiElements.splice(index);

                if (element.parent = this)
                {
                    this.removeChild(element);
                    this._renderGroup.remove(element);
                }
            }

            return element;
        }

        /**
         * Set the alpha value for all elements in this group.
         * @param alpha New alpha value for all elements.
         */
        public SetAlpha(alpha: number): void
        {
            for (var i = 0; i < this._uiElements.length; i++)
            {
                this._uiElements[i].alpha = alpha;
            }
        }

        /**
         * Sets the visibilty of all elements within this group.
         * @param visible The new Visibility state of all the elements within this group.
         */
        public SetVisibility(visible: boolean): void
        {
            for (var i = 0; i < this._uiElements.length; i++)
            {
                this._uiElements[i].SetVisibility(visible);
            }
        }

        /**
         * Moves all elements by given amount.
         * @param x Pixels to move on the X axis.
         * @param y Pixels to move on the Y axis.
         */
        public Translate(x: number, y: number): void
        {
            for (var i = 0; i < this._uiElements.length; i++)
            {
                this._uiElements[i].Translate(x, y);
            }
        }

        /**
         * Sets the position of all elements to the given position.
         * @param x New X axis position of all elements.
         * @param y New Y axis position of all elements.
         */
        public MoveTo(x: number, y: number): void
        {
            for (var i = 0; i < this._uiElements.length; i++)
            {
                this._uiElements[i].MoveTo(x, y);
            }
        }

        /**
         * Returns the first element containing the given tag.
         * @param tag Tag to search for.
         */
        public FindElement(tag: string): UIElement
        {
            for (var i = 0; i < this._uiElements.length; i++)
            {
                if (this._uiElements[i].HasTag(tag))
                {
                    return this._uiElements[i];
                }
            }

            return null;
        }
    }
}