﻿module ProeveVanBekwaamheid.UI
{
    /**
     * A class that implements both UIElement and Phaser.Sprite and follows the internal UI system.
     * Used for UI images or animated sprites.
     * Will be rendered on top of entities.
     */
    export class UIButton extends UI.UIElement
    {
        /**
        * The internally used Phaser.Button object.
        */
        public get ButtonObject(): Phaser.Button
        {
            return this._button;
        }

        private _button: Phaser.Button;
        private _onPressedCallback: Function;
        private _onPressedContext: any;

        /**
         *
         * @param game Reference to Phaser game used internally.
         * @param callback Function executed when the button is pressed.
         * @param context Context in which the callback is called.
         * @param x X position of the button.
         * @param y Y position of the button.
         * @param key Key of the image in the phaser cache.
         */
        public constructor(game: Phaser.Game, x: number, y: number, key: string, frame: string = null, onClickFrame: string = null)
        {
            super(game, x, y, null);

            this._button = game.add.button(x, y, key, this.OnButtonPressed, this, frame, onClickFrame, frame, onClickFrame);
        }

        /**
         * Set the callback function for when this button is pressed.
         * @param callback Function to execute.
         * @param context Object to call the function on.
         */
        public SetCallback(callback: Function, context: any): void
        {
            this._onPressedCallback = callback;
            this._onPressedContext = context;
        }

        /**
         * Set the visibility of the button.
         * @param visible The new state of the visility.
         */
        public SetVisibility(visible: boolean): void
        {
            this._button.visible = visible;
        }

        /**
         * Translates the button's position by given amount.
         * @param x Pixels to move on the X axis.
         * @param y Pixels to move on the Y axis.
         */
        public Translate(x: number, y: number): void
        {
            this._button.x += x;
            this._button.y += y;
        }

        /**
         * Sets the button's position to the given position.
         * @param x New X axis position of the image.
         * @param y New Y axis position of the image.
         */
        public MoveTo(x: number, y: number): void
        {
            this._button.x = x;
            this._button.y = y;
        }

        /**
         * The main visible object used by this UI element.
         * This will return the iternally used Phaser.Button object.
         */
        public GetRenderObject(): any
        {
            return this._button;
        }

        /**
         * The internal callback for the internally used Phaser.Button object.
         */
        private OnButtonPressed(): void
        {
            State.MenuState.ButtonAudio.play();
            if (this._onPressedCallback != null && this._onPressedContext != null)
            {
                this._onPressedCallback.call(this._onPressedContext);
            }
        }
    }
}