﻿namespace ProeveVanBekwaamheid
{
    /**
     * Entry for a sprite load request used in EntityGraphicsLoadRequestCache
     */
    export class PhaserSpriteLoadRequest
    {
        public Key;
        public Path;

        constructor(key: string, path: string)
        {
            this.Key = key;
            this.Path = path;
        }
    }

    /**
     * Entry for a spritesheet load request used in EntityGraphicsLoadRequestCache
     */
    export class PhaserSpriteSheetLoadRequest extends PhaserSpriteLoadRequest
    {
        public FrameWidth;
        public FrameHeight;
        public FrameCount;

        constructor(key:string, path: string, frameWidth: number, frameHeight: number, frameCount: number)
        {
            super(key, path);

            this.FrameWidth = frameWidth;
            this.FrameHeight = frameHeight;
            this.FrameCount = frameCount;
        }
    }

    /**
     * List of sprite load requests
     * Contains both unloaded sprite requests and archived requests that have already been loaded into Phaser cache
     */
    export class EntityGraphicsLoadRequestCache
    {
        private _activeSpriteRequests: PhaserSpriteLoadRequest[];
        private _activeSpriteSheetRequests: PhaserSpriteSheetLoadRequest[];

        private _archivedSpriteRequests: PhaserSpriteLoadRequest[];
        private _archivedSpriteSheetRequests: PhaserSpriteSheetLoadRequest[];

        constructor()
        {
            this._activeSpriteRequests = new Array<PhaserSpriteLoadRequest>();
            this._activeSpriteSheetRequests = new Array<PhaserSpriteSheetLoadRequest>();
            this._archivedSpriteRequests = new Array<PhaserSpriteLoadRequest>();
            this._archivedSpriteSheetRequests = new Array<PhaserSpriteSheetLoadRequest>();
        }

        /**
         * Add a sprite load request to be loaded on the onload function.
         * @param key Key of the sprite to be used in the Phaser cache.
         * @param path Path to the source file, example: ./assets/sprites/image.png.
         * @param ignoreArchived If set to true, the sprite will be loaded even if it has already been loaded previously.
         */
        public AddSpriteRequest(key: string, path: string, ignoreArchived: boolean = false)
        {
            if (ignoreArchived || !this.HasKeyInArchive(key))
            {
                this._activeSpriteRequests.push(new PhaserSpriteLoadRequest(key, path));
            }
        }

        /**
         * Add a spritesheet load request to be loaded on the onload function
         * @param key Key of the spritesheet to be used in the Phaser cache
         * @param path Path to the source file, example: ./assets/sprites/image.png
         * @param frameWidth Width of a single frame in the spritesheet
         * @param frameHeight Hieght of a single frame in the spritesheet
         * @param frameCount Amount of frames present in the spritesheet
         */
        public AddSpriteSheetRequest(key: string, path: string, frameWidth: number, frameHeight: number, frameCount: number)
        {
            if (!this.HasKeyInArchive(key))
            {
                this._activeSpriteSheetRequests.push(new PhaserSpriteSheetLoadRequest(key, path, frameWidth, frameHeight, frameCount));
            }
        }

        /**
         * Gets a sprite load request from the list of active requests
         * @param archiveActive If overriden to false, the requests are archived before the requests are returned
         */
        public GetSpriteRequests(archiveActive: boolean = true): PhaserSpriteLoadRequest[]
        {
            if (!archiveActive)
            {
                return this._activeSpriteRequests;
            }

            var returnList = new Array<PhaserSpriteLoadRequest>();
                       
            for (var i = this._activeSpriteRequests.length - 1; i >= 0; i--)
            {
                returnList.push(this._activeSpriteRequests[i]);
                this._archivedSpriteRequests.push(this._activeSpriteRequests[i]);
            }
            this._activeSpriteRequests.splice(0, this._activeSpriteRequests.length);

            return returnList;
        }

        /**
         * Gets a spritesheet load request from the list of active requests
         * @param archiveActive If overriden to false, the requests are archived before the requests are returned
         */
        public GetSpritesheetRequests(archiveActive:boolean = true) : PhaserSpriteSheetLoadRequest[]
        {
            if (!archiveActive)
            {
                return this._activeSpriteSheetRequests;
            }

            var returnList = new Array<PhaserSpriteSheetLoadRequest>();

            for (var i = this._activeSpriteSheetRequests.length - 1; i >= 0; i--)
            {
                returnList.push(this._activeSpriteSheetRequests[i]);
                this._archivedSpriteSheetRequests.push(this._activeSpriteSheetRequests[i]);
                this._activeSpriteSheetRequests.splice(0, this._activeSpriteSheetRequests.length);
            }

            return returnList;
        }

        /**
         * Checks if a load request with the given key is resent in the archived load requests
         * Archived load requests are all requests that were active when GetSprite(sheet)Requests was called
         * @param key
         */
        public HasKeyInArchive(key: string): boolean
        {
            for (var i = 0; i < this._archivedSpriteRequests.length; i++)
            {
                if (this._archivedSpriteRequests[i].Key == key)
                {
                    return true;
                }
            }

            for (var i = 0; i < this._archivedSpriteSheetRequests.length; i++)
            {
                if (this._archivedSpriteSheetRequests[i].Key == key)
                {
                    return true;
                }
            }

            return false;
        }
    }
}