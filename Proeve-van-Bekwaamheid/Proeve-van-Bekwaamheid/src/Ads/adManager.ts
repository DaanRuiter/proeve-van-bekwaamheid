﻿module ProeveVanBekwaamheid.Ads {
    export class adManager {

        private _game: Client.GameEngine;

        private _date: Date;

        private _adsAllowed: Boolean;

        private _currentYear: Number;
        private _currentMonth: Number;
        private _currentDay: Number;

        private _date2: Date;

        private counter: number;

        private _adsArray: string[];

        private openAdd: Phaser.Image;
        /**
         * 
         * @param game  A reference to the game
         * @param menuState A reference to the menu state
         */
        constructor(game: Client.GameEngine)
        {
            this._game = game;

            this.checkDate();

            if (ProeveVanBekwaamheid.Object.SharedObject.GetLocalStorageItem('AdDate') == null)
            {
                ProeveVanBekwaamheid.Object.SharedObject.SetLocalStorageItem('AdDate', this._date);
            }

            this._date2 = new Date(ProeveVanBekwaamheid.Object.SharedObject.GetLocalStorageItem('AdDate'));

            this.counter = 0;

            this._adsArray = new Array<string>();

            this._adsArray.push('ad1','ad2','ad3','ad4','ad5','ad6','ad7');
        }


        public checkIfAdsAllowed(): boolean
        {
            if (this._date2.getFullYear() >= this._currentYear) {
                if (this._date2.getDay() >= this._currentMonth) {
                    if (this._date2.getDate() == this._currentDay)
                    {
                        console.log('true');
                        return true;
                    }
                }
            }
            console.log('false');
            return false;
        }

        private checkDate()
        {
            this._date = new Date();

            this._currentYear = this._date.getFullYear();
            this._currentMonth = this._date.getDay();
            this._currentDay = this._date.getDate();
        }

        private renewAddDate()
        {
            var tomorrow = new Date(new Date().getTime() + 24 * 60 * 60 * 1000);
            ProeveVanBekwaamheid.Object.SharedObject.SetLocalStorageItem('AdDate', tomorrow);
        }

        private giveReward()
        {
            this.renewAddDate();
            this.renewAddDate();

            statics.Inventory.CoinsOwned += 50;

            var menuState = this._game.CurrentState as State.MenuState;
            menuState.shopMenu();
            menuState.updateCurrencyDisplay();

            this.openAdd.destroy();
        }

        public startAdd()
        {
            this.createAdd(this._adsArray[Math.floor(Math.random() * this._adsArray.length)]);
            this.counter = 0;
            this._game.time.events.loop(Phaser.Timer.SECOND, this.countDown, this);
        }

        private countDown()
        {
            this.counter++;
            
            if (this.counter >= 5)
            {
                this._game.time.events.stop();
                this.giveReward();
            }
        }

        private createAdd(adName: string)
        {

            this.openAdd = this._game.CurrentState.add.sprite(0,0, adName);
            this.openAdd.anchor.x = 0;
            this.openAdd.anchor.y = 0;

            this.openAdd.width = this._game.width;

            this.openAdd.scale.y = this.openAdd.scale.x;
            
        }
    }
}