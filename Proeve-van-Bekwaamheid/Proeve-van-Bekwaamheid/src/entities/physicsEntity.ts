﻿module ProeveVanBekwaamheid.Object
{
   /**
    * Used to define the mode of a PhysicsEntity
    */
    export enum PhysicsMode
    {
        Static,
        Dynamic,
        Kinematic
    }

    /**
     * An Entity that has a body and allows easy management of the childrens bodies.
     */
    export abstract class PhysicsEntity extends Entity
    {
        public body: Phaser.Physics.P2.Body;
        public MaxVelocity: number = -1;

        public get Radius(): number
        {
            return this._circleRadius;
        }

        /**
        * Is the Phaser physics debug mode enabled on new physics bodies?
        */
        private static get PHYSICS_DEBUG_ENABLED(): boolean
        {
            return false;
        }

        private _childConstraints: Phaser.Physics.P2.LockConstraint[];
        protected _circleRadius: number;

        /**
         * @param game Reference to Phaser game used internally.
         * @param x X position of the object on the screen.
         * @param y Y position of the object on the screen.
         * @param key Key of the image in the phaser cache. Optional.
         * @param frame Frame of image in spritesheet. Optional.
         */
        constructor(game: Phaser.Game, x: number, y: number, key?: string | Phaser.RenderTexture | Phaser.BitmapData | PIXI.Texture, frame?: string | number)
        {
            super(game,x,y,key,frame);
            
            this._childConstraints = [];
        }
        
        public OnUpdate(): void
        {
            this.ConstrainVelocity();
        }

        /**
         * A on collision handler.
         * Currently only works with Circle Collision!
         * @param object Object that this instance collides with.
         * @param group Group that this instance collides with.
         */
        public abstract OnCollision(object: PhysicsEntity, group: string): void;

        /**
         * Enables circle collision on this instance.
         * @param enable If true circle collision is active.
         * @param radius Radius of circle collision.
         * @param collisionGroup Collision group to stay in.
         */
        public EnableCircleCollision(radius: number, collisionGroup: string): void
        {
            Physics.Collision.AddRadiusCollisionObject(this, collisionGroup);
            this._circleRadius = radius;
        }

        /**
         * Disable circle collision on this instance.
         * @param collisionGroup Collision group to stay in.
         */
        public DisableCircleCollision(collisionGroup: string): void
        {
            Physics.Collision.RemoveRadiusCollisionObject(this, collisionGroup);
        }

        /**
         * Creates a P2JS body for this entity.
         * @param mode The mode for this entity's body.
         */
        public EnableBody(mode: PhysicsMode): void
        {
            this.game.physics.p2.enableBody(this, PhysicsEntity.PHYSICS_DEBUG_ENABLED);
            this.SetPhysicsMode(this.body, mode);
        }

        /**
         * Disables the body of this physics entity and removes all constraints to this entity.
         */
        public DisableBody(): void
        {
            if (this.body != null)
            {
                this.game.physics.p2.removeBody(this.body);
                if (this._childConstraints.length > 0)
                {
                    for (var i: number = this._childConstraints.length - 1; i >= 0; i--)
                    {
                        this.game.physics.p2.removeConstraint(this._childConstraints[i]);
                        this._childConstraints.pop();
                    }
                }
            }
        }
        
        /**
         * Creates a P2JS body for all the children of this entity.
         * @param mode Physics mode of the child.
         * @param lockConstraint Creates a constraint with the childs body and that of this entity if set to true.
         */
        public EnableChildren(mode: PhysicsMode, lockConstraint: boolean): void
        {
            for (var i = 0; i < this._children.length; i++)
            {
                var child: Phaser.Sprite = this._children[i] as Phaser.Sprite;
                this.game.physics.p2.enable(child, PhysicsEntity.PHYSICS_DEBUG_ENABLED);

                this.SetPhysicsMode(child.body, mode);

                if (this.body && lockConstraint)
                {
                    this._childConstraints.push(this.game.physics.p2.createLockConstraint(this.body, child.body));
                }
            }
        }

        /**
         * Limit the velocity of the physics entity.
         */
        private ConstrainVelocity(): void
        {
            if (this.MaxVelocity == -1) { return; }


            var angle, currVelocitySqr, vx, vy;
            vx = this.body.data.velocity[0]; vy = this.body.data.velocity[1];

            currVelocitySqr = vx * vx + vy * vy;

            if (currVelocitySqr > this.MaxVelocity * this.MaxVelocity)
            {
                angle = Math.atan2(vy, vx);
                vx = Math.cos(angle) * this.MaxVelocity; vy = Math.sin(angle) * this.MaxVelocity;
                this.body.data.velocity[0] = vx;
                this.body.data.velocity[1] = vy;
            }
        };

        /**
         * Switches the phyics mode of the phycis entity.
         * @param body The body to switch the physics mode on.
         * @param mode The new physics mode of the body.
         */
        private SetPhysicsMode(body: Phaser.Physics.P2.Body, mode: PhysicsMode): void
        {
            switch (mode)
            {
                case PhysicsMode.Static:
                    body.static = true;
                    break;
                case PhysicsMode.Dynamic:
                    body.dynamic = true;
                    break;
                case PhysicsMode.Kinematic:
                    body.kinematic = true;
                    break;
            }
        }
    }
}