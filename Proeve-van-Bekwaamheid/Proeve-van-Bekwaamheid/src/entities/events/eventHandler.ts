﻿module ProeveVanBekwaamheid.Events
{
    /**
     * Object that holds references to eventhanlder of the given generic type.
     */
    export class EventHandlerHolder <EventInterface>
    {
       /**
        * Returns event handlers in the internal list.
        */
        public get EventHandlers(): EventInterface[]
        {
            return this._eventHandlers;
        }

        private _eventHandlers: EventInterface[];
        
        public constructor()
        {
            this._eventHandlers = new Array<EventInterface>();
        }

        /**
         * Add a reference to the given event handler to the internal list.
         * Will not check if the eventhandler is already present in the internal list.
         * @param eventHandler Eventhandler to add.
         */
        public AddEventListener(eventHandler: EventInterface): void
        {
            this._eventHandlers.push(eventHandler);
        }

        /**
         * Remove the given event handler from the internal list.
         * Will silently fail if the eventhandler is not present in the internal list.
         * @param eventHandler Eventhandler to remove.
         */
        public RemoveEventListener(eventHandler: EventInterface): void
        {
            var index = this._eventHandlers.indexOf(eventHandler);

            if (index > -1)
            {
                this._eventHandlers.splice(index);
            }
        }
    }
}