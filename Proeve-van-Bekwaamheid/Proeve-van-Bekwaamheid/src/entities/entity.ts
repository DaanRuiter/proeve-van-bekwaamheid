﻿module ProeveVanBekwaamheid.Object
{
    /**
     * An Entity is an object that is present in the game view.
     * This can be anything like the player, an enemy, a platform or a background.
     * This should not be used if the sprite should have physics behaviour.
     * In that case, use PhysicsEntity instead.
     */
    export abstract class Entity extends SharedObject
    {
        public game: Client.GameEngine;

        /**
         * Set the scale of this entity and all it's children.
         */
        public set scale(scale: Phaser.Point)
        {
            super.scale = scale;

            if (this._children != null)
            {
                this._children.forEach(function (child)
                {
                    child.scale = scale;
                });
            }
        }

       /**
        * Returns all the children of this entity.
        */
        public get children(): Phaser.Sprite[]
        {
            return this._children;
        }

        protected _children: Phaser.Sprite[];

        /**
         * @param game Reference to Phaser game used internally.
         * @param x X position of the object on the screen.
         * @param y Y position of the object on the screen.
         * @param key Key of the image in the phaser cache. Optional.
         * @param frame Frame of image in spritesheet. Optional.
         */
        constructor(game: Phaser.Game, x: number, y: number, key?: string | Phaser.RenderTexture | Phaser.BitmapData | PIXI.Texture, frame?: string | number)
        {
            super(game, x, y, key, frame);

            this.game = game as Client.GameEngine;
            this._children = new Array<Phaser.Sprite>();
        }

        /**
         * Add requests to load sprite or spritesheets used for this entity on startup.
         * @param graphicsRequests shared list for all entity load requests on startup.
         */
        public abstract IndexDependencies(graphicsRequests: EntityGraphicsLoadRequestCache): void;
        /**
         * Called by the game state in the create() method.
         * Assets requested in the IndexDependencies method are loaded at this point.
         */
        public abstract OnStart(): void;
        /**
         * Called each frame.
         * Called internally.
         */
        public abstract OnUpdate(): void;

        /**
         * Attaches the given child to this object.
         * This is not the same as using addChild(child).
         * This entities scale will be applied to the child when calling this method.
         * Returns the added child.
         * @param child Sprite to child.
         */
        public AttachChild(child: Phaser.Sprite): Phaser.Sprite
        {
            let index = this._children.indexOf(child);

            if (index == -1)
            {
                this._children.push(child);
            }

            child.scale = this.scale;

            return child;
        }

        /**
         * Removes the given child from this object's children.
         * Will silently fail if the object is not a child of this entity.
         * @param child Child to remove.
         */
        public DetachChild(child: Phaser.Sprite): Phaser.Sprite
        {
            let index = this._children.indexOf(child);

            if (index >= 0)
            {
                this._children.splice(index);
            }

            return child;
        }
    }
}