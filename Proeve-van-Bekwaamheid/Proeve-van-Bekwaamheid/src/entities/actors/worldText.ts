﻿module ProeveVanBekwaamheid.Actors
{
    /**
     * Text that follows the other entities in the scrolling objects field.
     */
    export class WorldText extends Object.Entity
    {
        public get Text(): Phaser.Text
        {
            return this._text;
        }

        /*
        public set y(value: number)
        {
            this.position.y = value;
            this._text.y = value;
            this._startY = value;
        }*/

        private _text: Phaser.Text;
        private _moveSpeed: number = 0.2;
        private _direction: number = 1;
        private _maxY: number = 3;
        private _startY: number;

        private _isHovering: boolean = false;

        constructor(game: Client.GameEngine, x: number, y: number, text?: string, fill?: Phaser.Color, fontSize?: number, stroke?: number, strokeThickness?: number)
        {
            super(game, x, y);

            this._startY = y;

            this._text = game.add.text(x, y, text,
                {
                    fill: fill,
                    fontSize: fontSize,
                    stroke: stroke,
                    strokeThickness: strokeThickness
                });
        }

        /**
         * Sets the properties of the text
         * @param text the text containing inside this textfield.
         * @param fill the color of the text.
         * @param fontSize the size.
         * @param strokeThickness the stroke thickness.
         */
        public SetProperties(text: string, fill: Phaser.Color, fontSize: number, strokeThickness: number): void
        {
            this._text.text = text;
            this._text.fill = fill;
            this._text.fontSize = fontSize;
            this._text.strokeThickness = strokeThickness;
        }

        public StartHover(): void
        {
            this._isHovering = true;
        }

        public OnStart(): void
        {

        }

        public IndexDependencies(graphicsRequests: EntityGraphicsLoadRequestCache): void
        {

        }

        public Destroy(): void
        {
            this._text.destroy();
            super.Destroy();
        }

        public OnUpdate(): void
        {
            this._text.x = this.x;
            this._text.y = this.y;

            if (this._isHovering)
            {
                // Movement
                this.position.y += this._moveSpeed * this._direction;

                // Bopping
                if (this.position.y >= this._startY + this._maxY && this._direction == 1
                    || this.position.y <= this._startY - this._maxY && this._direction == -1)
                {
                    this._direction *= -1;
                }
            }
        }
    }

}