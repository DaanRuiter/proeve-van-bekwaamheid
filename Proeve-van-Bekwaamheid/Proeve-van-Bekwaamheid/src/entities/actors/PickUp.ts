﻿module ProeveVanBekwaamheid.State
{
    export class pickUp extends Object.PhysicsEntity
    {
        player: Phaser.Sprite;
        group: Phaser.Group;

        public OnStart():void
        {
            this.game.physics.arcade.enable(true);
            this.game.physics.startSystem(Phaser.Physics.ARCADE);

            this.group = this.game.add.group();
        }

        public OnDestroy(): void { }

        public IndexDependencies(requests: EntityGraphicsLoadRequestCache): void { }

        public OnUpdate(): void
        {
            this.game.physics.arcade.overlap(this.player, this.group, this.collisionHandler, null, this);
        }

        public CreateObject(): void { }

        private collisionHandler(player, pickupObject): void
        {
            pickupObject.kill();
        }

        /**
         * A on collision handler.
         * Currently only works with Circle Collision!
         * @param object Object that this instance collides with.
         */
        public OnCollision(other: Object.PhysicsEntity, group: string): void
        {

        }
    }
}