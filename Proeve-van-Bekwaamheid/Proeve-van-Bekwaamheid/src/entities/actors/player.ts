﻿module ProeveVanBekwaamheid.Actors
{
    export interface IOnDeathEventHandler
    {
        OnDeath();
    }

    /**
     * The powerup protecting the player from enemies.
     */
    export class PlayerShield extends Object.PhysicsEntity
    {
        /**
        * The vertical offset in pixels between the shield sprite and the player.
        */
        public static get VERTICAL_OFFSET(): number
        {
            return -100;
        }

        /**
        * The horizontal offset in pixels between the shield sprite and the player.
        */
        public static get HORIZONTAL_OFFSET(): number
        {
            return -100;
        }

        private _uses: number = 0;

        constructor(game: Client.GameEngine, x: number, y: number, texture: string)
        {
            super(game, x, y, texture);
            this.visible = false;
        }

        public OnDestroy(): void { }
        public OnStart(): void { }
        public IndexDependencies(graphicsRequests: EntityGraphicsLoadRequestCache): void { }

        /**
         * Enables this shield.
         */
        public Enable(uses: number): void
        {
            this._uses = uses;
            this.visible = true;
            this.EnableCircleCollision(50, Physics.CollisionGroups.PLAYER);
        }

        /**
         * Disables this shield.
         */
        public Disable(): void
        {
            this.visible = false;
            this.DisableCircleCollision(Physics.CollisionGroups.PLAYER);
        }

        /**
         * A on collision handler.
         * Currently only works with Circle Collision!
         * @param object Object that this instance collides with.
         */
        public OnCollision(other: Object.PhysicsEntity, group: string): void
        {
            if (group == Physics.CollisionGroups.ENEMY)
            {
                var index = (this.game.CurrentState as State.GameState).Enemies.indexOf(other as Actors.Enemy);
                (this.game.CurrentState as State.GameState).Enemies.splice(index, 1);

                index = (this.game.CurrentState as State.GameState).ObjectScroller.ScrollingObjects.indexOf(other);
                (this.game.CurrentState as State.GameState).ObjectScroller.ScrollingObjects.splice(index, 1);

                other.Destroy();

                this._uses--;
                if (this._uses <= 0)
                {
                    this.Disable();
                }
            }
        }
    }

    /**
     * Data model for the a players gun.
     */
    export class PlayerGun
    {
        /**
        * The physics body parent of the gun.
        */
        public Parent: Phaser.Physics.P2.Body;
        /**
        * The sprite of the gun.
        */
        public Sprite: Phaser.Sprite;
        /**
        * The muzzle flare sprite for this gun.
        */
        public MuzzleFlare: Phaser.Sprite;
        /**
        * Offset in pixels on the X axis from the center of the player.
        */
        public XOffset: number;

        constructor(parent: Phaser.Physics.P2.Body, sprite: Phaser.Sprite, muzzleFlare: Phaser.Sprite, xOffset:number = 0)
        {
            this.Parent = parent;
            this.Sprite = sprite;
            this.MuzzleFlare = muzzleFlare;
            this.XOffset = xOffset;
        }
    }

    /**
     * Base class for player.
     * Shoots ropes and has his own Physics body.
     * Extends Object.PhysicsEntity.
     */
    export class Player extends Object.PhysicsEntity implements IOnKeypointClickedEventHandler
    {
        /**
        * The radius of the player's hitbox used for enemy & pickup collision.
        */
        public static get HITBOX_RADIUS(): number
        {
            return 45;
        }

       /**
        * Amount of time in seconds it takes for the rope to be enabled when the player hits an enemy.
        */
        public static get ON_ENEMY_HIT_ROPE_COOLDOWN(): number
        {
            return Phaser.Timer.SECOND * 1.25;
        }

        /**
        * Event for when this player dies.
        */
        public get OnDeathEvent(): Events.EventHandlerHolder<IOnDeathEventHandler>
        {
            return this._onDeathEventHandler;
        }

        /**
         * The vertical offset used on borders.
         */
        public get VerticalClampOffset(): number
        {
            return this._verticalClampOffset;
        }

        /**
         * Returns true of player is currently on the vertical border.
         */
        public get IsOnBorder(): boolean
        {
            return this._isOnBorder;
        }

        /**
        * Returns true of player has a magnet.
        */
        public get MagnetEnabled(): boolean
        {
            return this._magnetEnabled;
        }

        /**
        * Returns true of player has a magnet.
        */
        public set MagnetEnabled(value: boolean)
        {
            this._magnetEnabled = value;
        }

        /**
         * Returns the bodies of each body part of the players ragdoll.
         * The bodies of each body part of the players ragdoll.
         */
        public get RagdollBodies(): Phaser.Physics.P2.Body[]
        {
            return this._ragdollBodies;
        }

       /**
        * The GunPowerUp guns on the player's shoulders.
        */
        public get Guns(): PlayerGun[]
        {
            return this._guns;
        }

       /**
        * The physics body of the torso body part.
        */
        public get Torso(): Phaser.Physics.P2.Body
        {
            return this._torso;
        }

        /**
         * The physics body of the keypoint that the player is currently roped to.
         */
        public get CurrentKeyPoint(): Keypoint
        {
            if (this._currentKeypoint)
            {
                return this._currentKeypoint;
            }
            return null;
        }

       /**
        * The player's rendergroup.
        */
        public get RenderGroup(): Phaser.Group
        {
            return this._renderGroup;
        }

        /**
        * Can the player die.
        */
        public Invulnerable: boolean = false;

        private static _currentLoadedSkinKey: string = 'none';

        private _onDeathEventHandler: Events.EventHandlerHolder<IOnDeathEventHandler>;

        private _line: any;
        private _ropeBitmapData: Phaser.BitmapData;
        private _currentKeypoint: Keypoint;
        private _currentRopeBody: Phaser.Physics.P2.Body;
        private _ropeSpring: Phaser.Physics.P2.Spring;
        private _rightHandBody: Phaser.Physics.P2.Body;
        private _torso: Phaser.Physics.P2.Body;
        private _ragdollBodies: Phaser.Physics.P2.Body[];
        private _guns: PlayerGun[];
        private _renderGroup: Phaser.Group;
        private _shield: PlayerShield;
        private _stunAnimation: PhaserSpine.Spine;

        private _initialized: boolean;
        private _ropeAnchored: boolean;
        private _isOnBorder: boolean;
        private _isRopeEnabled: boolean;
        private _magnetEnabled: boolean;

        private _ropeAnchorX: number;
        private _ropeAnchorY: number;
        private _ropeLength: number;
        private _ropeStrength: number;
        private _verticalClampOffset: number;
        private _scale: number = 1.5;

        private _savedHorizontalVelocity: number;
        private _savedVerticalVelocity: number;

        private _armLaunchAudio: any;
        private _beam: Phaser.Sprite;
        private _beamDirectionX: number;
        private _addedBeamScaleX: number;
        private _maxBeamScaleX: number;
        private _minBeamScaleX: number;

        constructor(game: Client.GameEngine, x: number, y: number, key?: string | Phaser.RenderTexture | Phaser.BitmapData | PIXI.Texture, frame?: string | number)
        {
            super(game, x, y, key, frame);

            this.name = "Player";
            this.AddTag("Player");
        }

        public OnDestroy(): void
        {
            this.DisableCircleCollision(Physics.CollisionGroups.ENEMY);
        }

        /**
         * Initializes this instance.
         * @param position Start position of this instance.
         */
        public initialize(position: Phaser.Point)
        {
            this._onDeathEventHandler = new Events.EventHandlerHolder<IOnDeathEventHandler>();

            this.position = position;
            this.scale = new Phaser.Point(1, 1);
            this._renderGroup = this.game.add.group(this);
            this.MaxVelocity = 70;

            // The higher the rope strenght the stronger it will pull.
            this._ropeStrength = 4.25;
            this._ropeLength = 1;
            this._verticalClampOffset = this.game.height * 0.5;
            this._isRopeEnabled = true;

            this.SetPhysics();

            this._beam = this.game.add.sprite(-10000, -10000, 'beam');
            this._beam.visible = false;
            this._beamDirectionX = 1;
            this._addedBeamScaleX = 0;
            this._maxBeamScaleX = 0.5;
            this._minBeamScaleX = -0.5;

            this.CreateRagdoll();

            var fakeSprite = this.game.add.sprite(0, 0);
            this.game.physics.p2.enableBody(fakeSprite, false);
            this._currentRopeBody = fakeSprite.body;
            this._currentRopeBody.kinematic = true;

            this.EnableCircleCollision(Player.HITBOX_RADIUS, Physics.CollisionGroups.PLAYER);            

            // Create a new sprite using the bitmap data
            this._line = this.game.add.sprite(0, 0, this._ropeBitmapData);
            this._shield = (this.game.CurrentState as State.GameState).InstantiateEntity(PlayerShield, this.x + PlayerShield.HORIZONTAL_OFFSET, this.y + PlayerShield.VERTICAL_OFFSET, "Shield");
            this._stunAnimation = this.game.add.spine(this.x, this.y, 'Player-Stuf-Effect');
            this._stunAnimation.setAnimationByName(0, "Stun", true);
            this._stunAnimation.scale.set(1.25);
            this._stunAnimation.visible = false;

            this._initialized = true;
        }

        /**
         * Triggers when keypoint is pressed.
         * @param keypoint keyPoint that has been pressed.
         * @param position position of press.
         */
        public OnKeypointDown(keypoint: Keypoint, position: Phaser.Point): void
        {
            (this.game.CurrentState as State.GameState).ArmLaunchAudio()

            if (!this._isRopeEnabled) { return; }

            if (!this.body.dynamic)
            {
                this.body.dynamic = true;
            }

            if ((this.game.CurrentState as State.GameState).IsTutorial)
            {
                var texts = (this.game.CurrentState as State.GameState).TutorialTexts;
                for (var i: number = texts.length - 1; i >= 0; i--)
                {
                    texts[i].Text.text = "Release to fling!";
                }

                var hands = (this.game.CurrentState as State.GameState).Hands;
                for (var i: number = hands.length - 1; i >= 0; i--)
                {
                    hands[i].Destroy();
                    hands.splice(i, 1);
                }
            } 

            
            if (!this._ropeAnchored)
            {
                this.CreateRope(keypoint, position);
                return;
            }


            //this._currentKeypoint = keypoint;
            //Remove last spring
            this.game.physics.p2.removeSpring(this._ropeSpring);

            //Create new spring at pointer x and y
            this._ropeSpring = this.game.physics.p2.createSpring(this._currentRopeBody, this, this._ropeLength, this._ropeStrength, 10, [-position.x, -position.y]);
            this._ropeAnchorX = position.x - this._currentRopeBody.x;
            this._ropeAnchorY = position.y - this._currentRopeBody.y;
            this._ropeAnchored = true;
        }

        /**
         * Triggers after keypoint has been pressed.
         * @param keypoint keyPoint that has been pressed.
         * @param position position of press.
         */
        public OnKeypointUp(keypoint: Keypoint, position: Phaser.Point): void
        {
            if ((this.game.CurrentState as State.GameState).IsTutorial)
            {
                (this.game.CurrentState as State.GameState).IsTutorial = false;
            }
            this._beam.visible = false;

            this.DestroyRope();
        }

        /**
         * Registers keypoint to this instance for press handlers.
         * @param keypoint keypoint to register.
         */
        public RegisterKeypoint(keypoint: Keypoint): void
        {
            keypoint.OnClickedEvent.AddEventListener(this);
        }

        /**
         * Removes given keypoint out of the click handlers.
         * @param keypoint keypoint to remove.
         */
        public RemoveKeypoint(keypoint: Keypoint): void
        {
            keypoint.OnClickedEvent.RemoveEventListener(this);
        }

        public OnStart(): void { }

        public IndexDependencies(graphicsRequests: EntityGraphicsLoadRequestCache): void
        {
            let selectedSkinFolder: string = statics.Inventory.SelectedSkin.SpriteFolderName;

            // Unload previous skin if the player has selected a new skin
            if (Player._currentLoadedSkinKey != 'none' && Player._currentLoadedSkinKey != statics.Inventory.SelectedSkin.SkinKey)
            {
                this.game.cache.removeImage('tony-torso');
                this.game.cache.removeImage('tony-belly-1');
                this.game.cache.removeImage('tony-belly-2');
                this.game.cache.removeImage('tony-belly-3');
                this.game.cache.removeImage('tony-foot');
                this.game.cache.removeImage('tony-left-leg-piece');
                this.game.cache.removeImage('tony-right-leg-piece');
                this.game.cache.removeImage('tony-left-arm-piece');
                this.game.cache.removeImage('tony-right-arm-piece');
                this.game.cache.removeImage('tony-left-hand');
                this.game.cache.removeImage('tony-right-hand');
            }

            graphicsRequests.AddSpriteRequest('tony-gun', './assets/sprites/player/guns.png');
            graphicsRequests.AddSpriteRequest('tony-torso', './assets/sprites/player/' + selectedSkinFolder + '/Body-1.png', true);
            graphicsRequests.AddSpriteRequest('tony-belly-1', './assets/sprites/player/' + selectedSkinFolder + '/Body-2.png', true);
            graphicsRequests.AddSpriteRequest('tony-belly-2', './assets/sprites/player/' + selectedSkinFolder + '/Body-3.png', true);
            graphicsRequests.AddSpriteRequest('tony-belly-3', './assets/sprites/player/' + selectedSkinFolder + '/Body-4.png', true);
            graphicsRequests.AddSpriteRequest('tony-foot', './assets/sprites/player/' + selectedSkinFolder + '/Foot.png', true);
            graphicsRequests.AddSpriteRequest('beam', './assets/sprites/player/Beam.png');

            graphicsRequests.AddSpriteRequest('tony-left-leg-piece', './assets/sprites/player/' + selectedSkinFolder + '/Leg-joint-left.png', true);
            graphicsRequests.AddSpriteRequest('tony-right-leg-piece', './assets/sprites/player/' + selectedSkinFolder + '/Leg-joint-right.png', true);
            graphicsRequests.AddSpriteRequest('tony-left-arm-piece', './assets/sprites/player/' + selectedSkinFolder + '/Arm-joint-left.png', true);
            graphicsRequests.AddSpriteRequest('tony-right-arm-piece', './assets/sprites/player/' + selectedSkinFolder + '/Arm-joint-right.png', true);
            graphicsRequests.AddSpriteRequest('tony-left-hand', './assets/sprites/player/' + selectedSkinFolder + '/Hand-Left.png', true);
            graphicsRequests.AddSpriteRequest('tony-right-hand', './assets/sprites/player/' + selectedSkinFolder + '/Hand-Right.png', true);

            Player._currentLoadedSkinKey = statics.Inventory.SelectedSkin.SkinKey;
        }

        public OnUpdate(): void
        {
            super.OnUpdate();

            if (!this._initialized || !this.body)
                return;

            //this.DrawLine(this._ropeBitmapData, this._rightHandBody, this._currentRopeBody, '#ffffff');
            this.DrawBeam(this._rightHandBody, this._currentRopeBody);
            this.ClampPlayer();
            this.UpdateGunTransforms();
            this.UpdateShieldTransform();

            this._stunAnimation.position.set(this._torso.x, this._torso.y + this._stunAnimation.height / 2);
        }

        /**
         * Enables shield on this player.
         */
        public EnableShield(uses: number): void
        {
            this._shield.Enable(uses);
        }

        /**
         * Disables shield on this player.
         */
        public DisableShield(): void
        {
            this._shield.Disable();
        }

        /**
         * A on collision handler.
         * Currently only works with Circle Collision!
         * @param object Object that this instance collides with.
         */
        public OnCollision(other: Object.PhysicsEntity, group: string): void
        {
            if (this.Invulnerable) { return; }
            if (group == Physics.CollisionGroups.ENEMY)
            {
                (this.game.CurrentState as State.GameState).playerHitEnemyAudio();

                (other as Actors.Enemy).Destroy();
                (this.game.CurrentState as State.GameState).Enemies.splice((this.game.CurrentState as State.GameState).Enemies.indexOf(other as Actors.Enemy), 1);
                this.ToggleRope(false);
                this._stunAnimation.visible = true;

                this.game.time.events.add(Player.ON_ENEMY_HIT_ROPE_COOLDOWN, function ()
                {
                    this.ToggleRope(true);
                    this._stunAnimation.visible = false;
                }, this);

                return;
            } 

            if (this.y >= other.y - other.height * 0.25)
            {
                this.body.velocity.x *= -0.5;
                this.body.velocity.y *= -0.5;
            }
            else
            {
                this.body.velocity.y = -(this.body.velocity.y / 2);
                this.body.velocity.x *= -0.5;
                if (this.body.velocity.y > -100)
                {
                    this.body.velocity.y = -100;
                }
            }
        }

        private OnBodyCollisonHandler(body1: Phaser.Physics.P2.Body, body2: Phaser.Physics.P2.Body): void
        {
            (this.game.CurrentState as State.GameState).OnDeath();
        }

        /**
         * Toggles the rope on or off.
         * When toggled off, the current rope is destroyed.
         * While toggles off, no new rope can be created untill toggled on again.
         * @param newState The new state of the rope.
         */
        public ToggleRope(newState: boolean): void
        {
            if (newState)
            {
                this._isRopeEnabled = true;
            }
            else
            {
                this._beam.visible = false;
                this.DestroyRope();
                this._isRopeEnabled = false;
            }
        }

        /**
         * Toggles the visibilty of the player's guns.
         * @param newState The new visibilty state of the guns.
         */
        public ToggleGuns(newState: boolean): void
        {
            for (let i = 0; i < this._guns.length; i++)
            {
                this._guns[i].Sprite.visible = newState;
                this._guns[i].MuzzleFlare.visible = false;
            }
        }

        /**
         * Creates a rope from te player to te given point.
         * @param block The keypoint to create a rope to.
         * @param position Position where to create the rope to.
         */
        private CreateRope(block: Keypoint, position: Phaser.Point): void
        {
            this._currentKeypoint = block;
            this._currentRopeBody.x = position.x;
            this._currentRopeBody.y = position.y;

            // Keep track of where the rope is anchored
            this._ropeAnchorX = (position.x - this._currentKeypoint.x);
            this._ropeAnchorY = (position.y - this._currentKeypoint.y);

            if (this._ropeSpring)
            {
                this.game.physics.p2.removeSpring(this._ropeSpring);
            }

            // Create a spring between the player and block to act as the rope
            this._ropeSpring = this.game.physics.p2.createSpring(
                this._currentRopeBody,      // sprite 1
                this._rightHandBody,                       // sprite 2
                this._ropeLength,           // length of the rope
                this._ropeStrength,         // stiffness
                3,                          // damping
                [-(position.x), -(position.y)]
            );

            /*
            // Draw a line from the player to the block to visually represent the spring
            this._line = new Phaser.Line(this._rightHandBody.x, this._rightHandBody.y,
                (position.x), (position.y)); */
            this._ropeAnchored = true;
            this.DrawBeam(this._rightHandBody, this._currentRopeBody);
            this._beam.visible = true;
        }

        /**
         * Draws a beam effect between two bodies.
         * @param bodyA Body to draw the beam from.
         * @param bodyB Body to draw the beam to.
         */
        private DrawBeam(bodyA: Phaser.Physics.P2.Body, bodyB: Phaser.Physics.P2.Body): void
        {
            if (!this._ropeAnchored)
                return;

            this._currentRopeBody.x = this._currentKeypoint.x + this._ropeAnchorX;
            this._currentRopeBody.y = this._currentKeypoint.y + this._ropeAnchorY;

            this._addedBeamScaleX += 0.025 * this._beamDirectionX;
            if (this._addedBeamScaleX >= this._maxBeamScaleX && this._beamDirectionX == 1 ||
                this._addedBeamScaleX <= this._minBeamScaleX && this._beamDirectionX == -1)
            {
                this._beamDirectionX *= -1;
            }

            var beamX = bodyB.x;
            var beamY = bodyB.y;
            var distance = Phaser.Point.distance(bodyA, bodyB);
            var angle: number = Phaser.Math.radToDeg(Phaser.Math.angleBetweenPoints(new Phaser.Point(bodyA.x, bodyA.y), new Phaser.Point(bodyB.x, bodyB.y))) + 90;

            this._beam.x = beamX;
            this._beam.y = beamY;

            this._beam.scale.set(1 + this._addedBeamScaleX, distance / 20);
            
            this._beam.angle = angle;
        }

        /**
         * Makes sure the player can't leave the game bounds.
         */
        private ClampPlayer(): void
        {
            if (this.body.y < this._verticalClampOffset)
            {
                this.body.y = this._verticalClampOffset;
                this._isOnBorder = true;
            }
            else if (this.body.y > this.game.height - this.height)
            {
                this.body.y = this.game.height - this.height;
                this._isOnBorder = true;
            }
            else
            {
                this._isOnBorder = false;
            }

            if (this.body.x < 0 || this.body.x > this.game.width)
            {
                this.body.x += -(this.body.velocity.x / 60);
                this.body.velocity.x = -(this.body.velocity.x * 2);
            }
        }

        /**
         * Updates the position and rotation of the guns to follow their physics parents defined in the PlayerGun object.
         */
        private UpdateGunTransforms(): void
        {
            for (let i = 0; i < this._guns.length; i++)
            {
                var gun: PlayerGun = this._guns[i];

                gun.Sprite.rotation = gun.Parent.rotation;
                gun.Sprite.x = gun.Parent.sprite.x + gun.XOffset;
                gun.Sprite.y = gun.Parent.sprite.y;
            }
        }

        /**
         * Update the shield's position to that of the player plus the offset.
         */
        private UpdateShieldTransform(): void
        {
            this._shield.x = this.x + PlayerShield.HORIZONTAL_OFFSET; 
            this._shield.y = this.y + PlayerShield.VERTICAL_OFFSET;
        }

        /**
         * Sets the physics properties of the player's body.
         */
        private SetPhysics(): void
        {
            this.EnableBody(Object.PhysicsMode.Static);
            this.body.mass = 100;
            this.body.clearShapes();
            this.body.setRectangleFromSprite(this);

            this.body.setCollisionGroup(Physics.Collision.GetCollisionGroup(Physics.CollisionGroups.PLAYER));
        }

        /**
         * Destroy the rope and remove the spring between the player and the keypoint.
         */
        private DestroyRope(): void
        {
            this.game.physics.p2.removeSpring(this._ropeSpring);
            this._ropeAnchored = false;
            //this._ropeBitmapData.clear();
            this._currentKeypoint = null;
        }

        /**
         * Removes the constraints on the ragdoll causing it to fall apart.
         */
        public KillRagdoll(): void
        {
            var allConstraints = this.game.physics.p2.world.constraints.splice(0, this.game.physics.p2.world.constraints.length);

            for (let i = 0; i <= allConstraints.length; i++)
            {
                this.game.physics.p2.removeConstraint(allConstraints[i]);
            }

            this._ragdollBodies.forEach(function (body)
            {
                body.thrust(450);
            });
        }

        /**
         * Creates physics bodies for each body part of the Tony character and joins them together to form a humanoid skeleton.
         */
        private CreateRagdoll(): void
        {
            //guns
            var leftGun = this.game.add.sprite(this.x, this.y, 'tony-gun');
            leftGun.pivot = new Phaser.Point(leftGun.width / 2, leftGun.height / 2 + 25 * this._scale);
            leftGun.scale = new Phaser.Point(0.2 * this._scale, 0.2 * this._scale);
            var leftFlare = this.game.add.sprite(this.x, this.y, 'muzzle-flare');
            leftGun.addChild(leftFlare);
            leftFlare.pivot.set(leftFlare.width / 2, 0);
            leftFlare.position.set(leftFlare.width / 3, -leftFlare.height);

            var rightGun = this.game.add.sprite(this.x, this.y + -25, 'tony-gun');
            rightGun.pivot = new Phaser.Point(rightGun.width / 2, rightGun.height / 2 + 25 * this._scale);
            rightGun.scale = new Phaser.Point(0.2 * this._scale, 0.2 * this._scale);
            var rightFlare = this.game.add.sprite(this.x, this.y, 'muzzle-flare');
            rightGun.addChild(rightFlare);
            rightFlare.pivot.set(0, 0);
            rightFlare.position.set(0, -rightFlare.height);
            rightFlare.tint = 0xff0000;

            var legOffset = 15;
            var armXOffset = 7;

            //left arm
            var tonyLeftHand = this.CreateBodyPiece('tony-left-hand', -80 - armXOffset, -3);
            var tonyLeftArmPiece1 = this.CreateBodyPiece('tony-left-arm-piece', -35 - armXOffset, -3);
            var tonyLeftArmPiece2 = this.CreateBodyPiece('tony-left-arm-piece', -50 - armXOffset, -3);
            var tonyLeftArmPiece3 = this.CreateBodyPiece('tony-left-arm-piece', -65 - armXOffset, -3);

            //Right arm
            var tonyRightHand = this.CreateBodyPiece('tony-right-hand', 80 + armXOffset, -3);
            var tonyRightArmPiece1 = this.CreateBodyPiece('tony-right-arm-piece', 35 + armXOffset, -3);
            var tonyRightArmPiece2 = this.CreateBodyPiece('tony-right-arm-piece', 50 + armXOffset, -3);
            var tonyRightArmPiece3 = this.CreateBodyPiece('tony-right-arm-piece', 65 + armXOffset, -3);

            //Legs
            var tonyLeftFoot = this.CreateBodyPiece('tony-foot', -22, 64 + legOffset);
            var tonyLeftLegPiece2 = this.CreateBodyPiece('tony-left-leg-piece', -18, 54 + legOffset);
            var tonyLeftLegPiece1 = this.CreateBodyPiece('tony-left-leg-piece', -13, 46 + legOffset);

            var tonyRightFoot = this.CreateBodyPiece('tony-foot', 22, 64 + legOffset);
            var tonyRightLegPiece2 = this.CreateBodyPiece('tony-right-leg-piece', 18, 54 + legOffset);
            var tonyRightLegPiece1 = this.CreateBodyPiece('tony-right-leg-piece', 13, 46 + legOffset);

            //body
            var tonyBelly2 = this.CreateBodyPiece('tony-belly-2', 0, 27 + legOffset);
            var tonyBelly1 = this.CreateBodyPiece('tony-belly-1', 0, 18 + legOffset);
            var tonyBelly3 = this.CreateBodyPiece('tony-belly-3', 0, 40 + legOffset);
            var tonyTorso = this.CreateBodyPiece('tony-torso', 0, 0);


            this.CreateLockConstraint(tonyTorso, this.body);
            this.CreateRevoluteConstraint(tonyBelly1, tonyTorso, 0.025)
            this.CreateRevoluteConstraint(tonyBelly2, tonyBelly1, 0.025);
            this.CreateRevoluteConstraint(tonyBelly3, tonyBelly2, 0.025);

            this.CreateLockConstraint(tonyLeftArmPiece1, tonyTorso);
            this.CreateLockConstraint(tonyLeftArmPiece2, tonyLeftArmPiece1);
            this.CreateLockConstraint(tonyLeftArmPiece3, tonyLeftArmPiece2);
            this.CreateLockConstraint(tonyLeftHand, tonyLeftArmPiece3, 0.1);

            this.CreateLockConstraint(tonyRightArmPiece1, tonyTorso);
            this.CreateLockConstraint(tonyRightArmPiece2, tonyRightArmPiece1);
            this.CreateLockConstraint(tonyRightArmPiece3, tonyRightArmPiece2);
            this.CreateLockConstraint(tonyRightHand, tonyRightArmPiece3, 0.1); 

            this.CreateLockConstraint(tonyLeftLegPiece1, tonyBelly3, 0.035);
            this.CreateLockConstraint(tonyLeftLegPiece2, tonyLeftLegPiece1, 0.035);
            this.CreateLockConstraint(tonyLeftFoot, tonyLeftLegPiece2, 0.035);

            this.CreateLockConstraint(tonyRightLegPiece1, tonyBelly3, 0.035);
            this.CreateLockConstraint(tonyRightLegPiece2, tonyRightLegPiece1, 0.035);
            this.CreateLockConstraint(tonyRightFoot, tonyRightLegPiece2, 0.035);

            this._rightHandBody = tonyRightHand;
            this._ragdollBodies =
            [
                tonyLeftArmPiece1, tonyLeftArmPiece2, tonyLeftArmPiece3, tonyLeftHand,
                tonyRightArmPiece1, tonyRightArmPiece2, tonyRightArmPiece3, tonyRightHand,
                tonyBelly2, tonyBelly3, tonyBelly1, tonyTorso, tonyLeftLegPiece1, tonyLeftLegPiece2, tonyLeftFoot, tonyRightLegPiece1, tonyRightLegPiece2, tonyRightHand
            ];
            this._torso = tonyTorso;
            this._torso.mass = 0.25;
            
            this._guns =
                [
                new PlayerGun(tonyTorso, leftGun, leftFlare, -leftGun.width / 3),
                new PlayerGun(tonyTorso, rightGun, rightFlare, rightGun.width / 3)
                ];

            this.ToggleGuns(false);
        }

        /**
         * Creates a sprite with the given key and enables a P2JS body on it.
         * @param spriteKey Key of the sprite for the body part.
         * @param relativeX X position relative to the center of the player.
         * @param relativeY Y position relative to the center of the player.
         */
        private CreateBodyPiece(spriteKey: string, relativeX: number, relativeY: number): Phaser.Physics.P2.Body
        {
            var position = new Phaser.Point(this.position.x, this.position.y);
            position.x += relativeX * this._scale;
            position.y += relativeY * this._scale;
            var bodyPiece: Phaser.Sprite = this.game.add.sprite(position.x, position.y, spriteKey);

            this.game.physics.p2.enable(bodyPiece);
            this.AttachChild(bodyPiece);

            bodyPiece.body.setCollisionGroup(Physics.Collision.GetCollisionGroup(Physics.CollisionGroups.PLAYER));
            bodyPiece.body.collides([Physics.Collision.GetCollisionGroup(Physics.CollisionGroups.PLATFORM), Physics.Collision.GetCollisionGroup(Physics.CollisionGroups.ENEMY)]);
            bodyPiece.scale.x = this._scale;
            bodyPiece.scale.y = this._scale;

            return bodyPiece.body;
        }

        /**
         * Creates a P2JS RevoluteConstraint between the 2 given bodies.
         * This will disable any collision currently on the body.
         * @param bodyPiece Body to create the constraint on.
         * @param parentBody Body to attach the constraint to.
         * @param rotationLimit Rotational limit between the two bodies.
         * @param mass Custom mass to assign to the bodyPiece.
         */
        private CreateRevoluteConstraint(bodyPiece: Phaser.Physics.P2.Body, parentBody: Phaser.Physics.P2.Body, rotationLimit: number, mass:number = 0.15)
        {
            bodyPiece.mass = mass;

            var constraint: Phaser.Physics.P2.RevoluteConstraint = this.game.physics.p2.createRevoluteConstraint(
                parentBody,
                [bodyPiece.sprite.x, bodyPiece.sprite.height / 2],
                bodyPiece,
                [parentBody.sprite.x, -parentBody.sprite.y], 1000000, [this.body.sprite.position.x, this.body.sprite.position.y]);
            constraint.setLimits(-rotationLimit, rotationLimit);
            constraint.setStiffness(1000000);
            constraint.setRelaxation(0.5);
            constraint.collideConnected = false;
        }

        /**
         * Creates a P2JS LockConstraint between the 2 given bodies.
         * This will disable any collision currently on the body.
         * @param bodyPiece Body to create the constraint on.
         * @param parentBody Body to attach the constraint to.
         */
        private CreateLockConstraint(bodyPiece: Phaser.Physics.P2.Body, parentBody: Phaser.Physics.P2.Body, mass:number = 0.02)
        {
            bodyPiece.mass = mass;

            var constraint: Phaser.Physics.P2.LockConstraint = this.game.physics.p2.createLockConstraint(
                bodyPiece,
                parentBody,
                [bodyPiece.sprite.x - parentBody.sprite.x, bodyPiece.sprite.y - parentBody.sprite.y]);
            constraint.setStiffness(100);
            constraint.setRelaxation(0.85);
        }
    }
}