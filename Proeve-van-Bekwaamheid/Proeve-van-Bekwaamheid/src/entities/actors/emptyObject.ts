﻿module ProeveVanBekwaamheid.Actors
{
    /**
     * An simple entity that has no behaviour.
     */
    export class EmptyObject extends Object.Entity
    {
        constructor(game: Client.GameEngine, x: number, y: number, textureKey: string)
        {
            super(game, x, y, textureKey);
        }

        public OnStart(): void
        {

        }

        public IndexDependencies(graphicsRequests: EntityGraphicsLoadRequestCache): void
        {

        }

        public Destroy(): void
        {
            super.Destroy();
        }

        public OnUpdate(): void
        {

        }
    }
}