﻿module ProeveVanBekwaamheid.PickUp
{
    /**
     * Powerup that protects the player from getting stunned by enemies.
     */
    export class ShieldPowerUp extends PickUpObject
    {
        /**
        * Duration in seconds of the shield powerup.
        */
        public static get SHIELD_DURATION(): number
        {
            return 4.5;
        }

        /**
        * Amount of hits the shield can take before deactivating.
        */
        public static get SHIELD_USES(): number
        {
            return 5;
        }

        protected GetDuration(): number
        {
            var powerup = statics.Inventory.FindUpgrade(statics.PowerupUpgrade.POWERUP_SHIELD_KEY);
            var modifier = powerup.CurrentModifier;
            var shieldDuration = ShieldPowerUp.SHIELD_DURATION;
            if (modifier != 0)
            {
                shieldDuration *= modifier;
            }

            return shieldDuration;
        }

        private _gameState: State.GameState;
        private _shieldPickUp: Phaser.Sound;

        public OnStart(): void
        {
            super.OnStart();
            this._shieldPickUp = this.game.sound.add('shieldPickUp');
        }

        public OnUpdate(): void
        {
            super.OnUpdate();
        }

        public customFunction(): void
        {
            this._shieldPickUp.play();
            this._gameState = this.game.CurrentState as State.GameState;

            if (this._gameState)
            {
                this._pickedUp = true;
                this.visible = false;
                this._gameState.Player.EnableShield(ShieldPowerUp.SHIELD_USES);

                var powerup = statics.Inventory.FindUpgrade(statics.PowerupUpgrade.POWERUP_SHIELD_KEY);
                var modifier = powerup.CurrentModifier;
            }
        }

        protected OnPowerupEnded(): void
        {
            this._gameState.Player.DisableShield();
            this.Destroy();
        }
    }
}