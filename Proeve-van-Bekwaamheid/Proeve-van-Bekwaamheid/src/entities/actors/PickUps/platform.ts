﻿module ProeveVanBekwaamheid.Actors {

    /**
     * A platform that uses the collision of P2 to collide with the ragdoll of the player.
     */
    export class Platform extends Object.PhysicsEntity
    {
        constructor(game: Client.GameEngine, x: number, y: number, texture: string)
        {
            super(game, x, y, texture);

            this.scale.setTo(2, 2);

            this.EnableBody(Object.PhysicsMode.Static);
            this.body.setRectangle(this.width, this.height - 20);
            this.body.setCollisionGroup(Physics.Collision.GetCollisionGroup(Physics.CollisionGroups.PLATFORM));
            this.body.collides([Physics.Collision.GetCollisionGroup(Physics.CollisionGroups.PLAYER)]);
        }

        public OnDestroy(): void { }
        public OnStart(): void { }
        public IndexDependencies(graphicsRequests: EntityGraphicsLoadRequestCache): void { }

        /**
         * Enables this shield.
         */
        public Enable(): void
        {
            this.visible = true;
            //this.EnableCircleCollision(50, Physics.CollisionGroups.PLAYER);
        }

        /**
         * Disables this shield.
         */
        public Disable(): void
        {
            this.visible = false;
            //this.DisableCircleCollision(Physics.CollisionGroups.PLAYER);
        }

        /**
         * A on collision handler.
         * Currently only works with Circle Collision!
         * @param object Object that this instance collides with.
         */
        public OnCollision(other: Object.PhysicsEntity, group: string): void
        {
            /*
            if (group == Physics.CollisionGroups.PLAYER)
            {
                if (other.body.y > this.y)
                {
                    other.body.velocity.y = 0;
                    other.body.y += 1;
                }
            }*/
        }
    }
}