﻿module ProeveVanBekwaamheid.PickUp 
{
    /**
     * Collectable coin which can be used to buy new skins and upgrade powerups.
     */
    export class Coin extends PickUpObject
    {
        /**
        * The radius of the pickup collider.
        */
        public static get CIRCLE_RADIUS(): number
        {
            return 50;
        }

        protected GetDuration(): number
        {
            return -1;
        }

        private _player: Actors.Player;
        private _pullSpeed: number = 200;

        constructor(game: Client.GameEngine, x: number, y: number, key: string, frame: string) {
            super(game, x, y, key, frame);
            this.name = "coin";
        }

        protected CreateObject()
        {
            this.scale.setTo(0.5);
        }

        public customFunction()
        {
            if (!this.game) { return; }

            this._pickedUp = true;
            var gameState: ProeveVanBekwaamheid.State.GameState = this.game.CurrentState as ProeveVanBekwaamheid.State.GameState;
            this._player = gameState.Player;
        }

        protected OnPowerupEnded():void { }

        /**
         * Set the radius of the circle collider.
         * @param radius New radius of the collider.
         */
        public SetCircleRadius(radius: number)
        {
            this._circleRadius = radius;
        }

        public OnUpdate(): void
        {
            super.OnUpdate();

            if (this._pickedUp && this._player)
            {
                var distanceX = (this._player.x - this.x);
                var distanceY = (this._player.y - this.y);
                this.x += distanceX / this.game.width * this._pullSpeed;
                this.y += distanceY / this.game.height * this._pullSpeed;

                if (Math.abs(distanceX) < 50 && Math.abs(distanceY) < 50)
                {
                    ProeveVanBekwaamheid.statics.Inventory.CoinsOwned++;
                    var coins = ProeveVanBekwaamheid.statics.Inventory.CoinsOwned;

                    var gameState: ProeveVanBekwaamheid.State.GameState = this.game.CurrentState as ProeveVanBekwaamheid.State.GameState;
                    gameState.UpdateNumericDisplay('display_currency', coins);

                    gameState.collectCoinAudio();

                    this.Destroy();
                }
            }
        }
    }
}