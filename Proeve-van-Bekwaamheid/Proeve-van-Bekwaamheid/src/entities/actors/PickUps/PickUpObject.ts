﻿module ProeveVanBekwaamheid.PickUp 
{
    /**
     * Base class for powerup effects.
     */
    export abstract class PickUpObject extends Object.PhysicsEntity 
    {
        /**
         * The duration in seconds of the powerup.
         */
        protected abstract GetDuration(): number;

        protected _pickedUp: boolean;

        private _destroyTimestamp: number = -1;

        constructor(game: Client.GameEngine, x: number, y: number, key: string, frame: string)
        {
            super(game, x, y, key, frame);

            this.AddTag('pick-up');

            this.EnableCircleCollision(50, Physics.CollisionGroups.PICKUP);
            this.CreateObject();
        }

        public IndexDependencies(graphicsRequests: EntityGraphicsLoadRequestCache): void { }
        public OnStart(): void { }
        
        public OnUpdate(): void
        {
            super.OnUpdate();

            if (this._destroyTimestamp > 0 && this.game.time.totalElapsedSeconds() >= this._destroyTimestamp)
            {
                this.OnPowerupEnded();
                this._destroyTimestamp = -1;
            }
        }

        /**
         * Callback for when the powerup collides with the player.
         * @param other
         */
        public OnCollision(other: Object.PhysicsEntity): void
        {
            this.DisableCircleCollision(Physics.CollisionGroups.PICKUP);
            if (!this._pickedUp)
            {
                var duration = this.GetDuration();

                if (duration > 0)
                    this._destroyTimestamp = this.game.time.totalElapsedSeconds() + this.GetDuration();

                this._pickedUp = true;
                this.visible = true;
                this.customFunction();

            }
        }

        /**
         * The custom function of the powerup.
         */
        public customFunction(): void
        {
            this.kill();

            if (this._pickedUp) 
            {
                this.Destroy();
            }
        }

        /**
         * Callback for when the powerup duration has elapsed.
         */
        protected abstract OnPowerupEnded(): void

        protected CreateObject(): void { }
    }
}