﻿module ProeveVanBekwaamheid.PickUp
{
    /**
     * A powerup that pulls coins towards the player.
     */
    export class MagnetPowerUp extends PickUpObject
    {
        /**
        * The duration in seconds of the magnet powerup.
        */
        public static get MAGNET_DURATION(): number
        {
            return 15;
        }

        /**
        * The pull strength of the magnet powerup.
        */
        public static get MAGNET_PULL_STRENGTH(): number
        {
            return 350;
        }

        public GetDuration(): number
        {
            var powerup = statics.Inventory.FindUpgrade(statics.PowerupUpgrade.POWERUP_MAGNET_KEY);
            var modifier = powerup.CurrentModifier;
            var magnetDuration = MagnetPowerUp.MAGNET_DURATION;
            if (modifier != 0)
            {
                magnetDuration *= modifier;
            }

            return magnetDuration;
        }

        private _gameState: State.GameState;
        private _magnetParticles: Phaser.Particles.Arcade.Emitter;

        public OnStart(): void
        {
            super.OnStart();
        }

        public OnUpdate(): void
        {
            if (!this._pickedUp) { return; }
            super.OnUpdate();
            
            this._magnetParticles.emitX = this._gameState.Player.x;
            this._magnetParticles.emitY = this._gameState.Player.y;

            for (var i: number = 0; i < this._magnetParticles.children.length; i++)
            {
                this._magnetParticles.children[i].x = this._gameState.Player.x;
                this._magnetParticles.children[i].y = this._gameState.Player.y;
            }
        }

        public customFunction(): void
        {
            this.visible = false;
            this._pickedUp = true;
            this._gameState = this.game.CurrentState as State.GameState;

            if (this._gameState)
            {
                this._gameState.Player.MagnetEnabled = true;
                for (var i: number = 0; i < this._gameState.Coins.length; i++)
                {
                    this._gameState.Coins[i].SetCircleRadius(MagnetPowerUp.MAGNET_PULL_STRENGTH);
                }

                this._magnetParticles = this.game.add.emitter(0, 0, 10);
                this._magnetParticles.makeParticles('particles-magnet');
                this._magnetParticles.minParticleScale = 0;
                this._magnetParticles.maxParticleScale = 4;
                this._magnetParticles.gravity = 0;
                this._magnetParticles.minParticleSpeed.set(0, 0);
                this._magnetParticles.maxParticleSpeed.set(0, 0);
                this._magnetParticles.setAlpha(1, 0, 2500);
                this._magnetParticles.setScale(3, 0, 3, 0, 3000);
                this._magnetParticles.flow(3000, 500, 1, this.GetDuration() / 500);
            }
        }

        protected OnPowerupEnded(): void
        {
            this._gameState.Player.MagnetEnabled = false;
            for (var i: number = 0; i < this._gameState.Coins.length; i++)
            {
                this._gameState.Coins[i].SetCircleRadius(PickUp.Coin.CIRCLE_RADIUS);
            }

            this.Destroy();
        }
    }
}