﻿module ProeveVanBekwaamheid.PickUp
{
    /**
     * Powerup that, for a short duration, kills enemies within a certain range of the player.
     */
    export class GunPowerUp extends PickUpObject
    {
        /**
         * Duration that the player has the gun powerup after activation.
         */
        public static get GUN_DURATION(): number
        {
            return 22;
        }

       /**
        * Cooldown between each shot at an enemy.
        */
        public static get GUN_SHOOT_COOLDOWN(): number
        {
            return Phaser.Timer.SECOND * 1.35;
        }

       /**
        * Time it takes for the gun to aim on an enemy before it can take a shot.
        */
        public static get GUN_AIM_DURATION(): number
        {
            return Phaser.Timer.SECOND * 0.65;
        }

       /**
        * Maximium distance between the player and an enemy when it can be shot.
        */
        public static get GUN_RANGE(): number
        {
            return 450;
        }

        public GetDuration(): number
        {
            var powerup = statics.Inventory.FindUpgrade(statics.PowerupUpgrade.POWERUP_GUN_KEY);
            var modifier = powerup.CurrentModifier;
            var gunDuration = GunPowerUp.GUN_DURATION;
            if (modifier != 0)
            {
                gunDuration *= modifier;
            }

            return gunDuration;
        }

        private _gameState: State.GameState;
        private _isGunOnCooldown: boolean;
        private _currentTarget: Actors.Enemy;
        private _aimTimer: Phaser.TimerEvent;

        private _gunPickUpAudio: Phaser.Sound;
        private _gunShootAudio: Phaser.Sound;

        public OnStart(): void
        {
            super.OnStart();
            
            this.scale = new Phaser.Point(0.2, 0.2);

            this._gunPickUpAudio = this.game.add.audio('gunPickUp');
            this._gunShootAudio = this.game.add.audio('GunShoot');
        }

        public OnUpdate(): void
        {
            super.OnUpdate();

            if (this._currentTarget)
            {
                this.AimAtTarget();
            }
            else
            {
                if (this._isGunOnCooldown || !this._pickedUp) { return; }

                var enemies: Actors.Enemy[] = this._gameState.Enemies;
                for (let i = 0; i < enemies.length; i++)
                {
                    let distance = this._gameState.Player.position.distance(enemies[i].position);

                    if (distance <= GunPowerUp.GUN_RANGE)
                    {
                        this._currentTarget = enemies[i];

                        this._aimTimer = this.game.time.events.add(GunPowerUp.GUN_AIM_DURATION, function ()
                        {
                            this.ShootAtTarget();
                        }, this);

                        this._aimTimer.timer.start();

                        return;
                    }
                }
            }
        }

        public customFunction(): void
        {
            this._gameState = this.game.CurrentState as State.GameState;
            this._gunPickUpAudio.play();

            if (this._gameState)
            {
                this.visible = false;
                this._gameState.Player.ToggleGuns(true);
            }
        }

        /**
         * Kill the current target and start the cooldown timer.
         */
        private ShootAtTarget()
        {
            this._gunShootAudio.play();

            this._isGunOnCooldown = true;

            if (this._currentTarget)
            {
                this._gameState.Enemies.splice(this._gameState.Enemies.indexOf(this._currentTarget), 1);

                this._currentTarget.Destroy();
                this._currentTarget = null;

                for (let i = 0; i < this._gameState.Player.Guns.length; i++)
                {
                    this._gameState.Player.Guns[i].MuzzleFlare.visible = true;
                }
                let muzzleFlareTimer = this.game.time.create();
                muzzleFlareTimer.add(85, this.DisableMuzzleFlares, this);
                muzzleFlareTimer.autoDestroy = true;
                muzzleFlareTimer.start();
            }

            this.game.time.events.add(GunPowerUp.GUN_SHOOT_COOLDOWN, function ()
            {
                this._isGunOnCooldown = false;

            }, this);
        }

        /**
         * Rotate the player's guns to the current target.
         */
        private AimAtTarget(): void
        {
            var guns: Actors.PlayerGun[] = this._gameState.Player.Guns;

            let distance = this._gameState.Player.position.distance(this._currentTarget.position);

            if (distance > GunPowerUp.GUN_RANGE)
            {
                if (this._aimTimer)
                {
                    this._aimTimer.timer.stop();
                    this._aimTimer.timer.destroy();
                }

                this._currentTarget = null;
                
                return;
            }

            for (let i = 0; i < guns.length; i++)
            {
                var angle: number = Phaser.Math.radToDeg(Phaser.Math.angleBetweenPoints(guns[i].Sprite.position, this._currentTarget.position)) + 90;

                guns[i].Sprite.angle = angle;
            }
        }

        private DisableMuzzleFlares(timer: Phaser.Timer): void
        {
            for (let i = 0; i < this._gameState.Player.Guns.length; i++)
            {
                this._gameState.Player.Guns[i].MuzzleFlare.visible = false;
            }
        }

        /**
         * Disable the guns on the player and destroy this powerup.
         */
        protected OnPowerupEnded(): void
        {
            if (this._gameState)
            {
                this._gameState.Player.ToggleGuns(false);
            }

            if (this._aimTimer)
                this._aimTimer.timer.stop();

            this.Destroy();
        }
    }
}