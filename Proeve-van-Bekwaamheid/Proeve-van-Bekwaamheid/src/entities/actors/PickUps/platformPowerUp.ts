﻿module ProeveVanBekwaamheid.PickUp
{
    /**
     * Powerup that protects the player from falling below the camera.
     */
    export class PlatformPowerUp extends PickUpObject
    {
        /**
        * Duration in seconds of the platform powerup.
        */
        public static get PLATFORM_DURATION(): number
        {
            return 5;
        }

        private _gameState: State.GameState;
        private _platform: Actors.Platform;

        public GetDuration(): number
        {
            var powerup = statics.Inventory.FindUpgrade(statics.PowerupUpgrade.POWERUP_PLATFORM_KEY);
            var modifier = powerup.CurrentModifier;
            var platformDuration = PlatformPowerUp.PLATFORM_DURATION;
            if (modifier != 0)
            {
                platformDuration *= modifier;
            }

            return platformDuration;
        }

        public OnStart(): void
        {
            super.OnStart();
        }

        public customFunction(): void
        {
            this._gameState = this.game.CurrentState as State.GameState;
            this.visible = false;
            this._pickedUp = true;

            this.EnablePlatform();
        }

        protected OnPowerupEnded()
        {
            this.Destroy();
            this._platform.Destroy();
        }

        /**
         * Spawn the platform at the bottom of the screen.
         */
        private EnablePlatform()
        {
            this._platform = this._gameState.InstantiateEntity(Actors.Platform, this.game.width / 2, this.game.height - 20, "Platform");
        }

    }
}