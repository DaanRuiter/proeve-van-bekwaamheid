﻿module ProeveVanBekwaamheid.PickUp
{
    /**
     * Powerup that boosts the player upwards while disabling player death and rope shooting for a duration.
     */
    export class JetpackPowerUp extends PickUpObject
    {
       /**
        * Force being applied to the player each frame while the boost is active.
        */
        public static get BOOST_STRENGTH(): number
        {
            return 550;
        }

       /**
        * Duration in seconds of the jetpack boost in seconds.
        */
        public static get BOOST_DURATION(): number
        {
            return 7;
        }

        /**
        * Amount of seconds to wait after the powerup has finished before re-enabling the player's collision.
        */
        public static get COLLISION_ENABLE_DELAY(): number
        {
            return Phaser.Timer.SECOND * 1.5;
        }

       /**
        * The offset between the particle system and the player on the y axis.
        */
        public static get EMIT_Y_OFFSET(): number
        {
            return -5;
        }

        public GetDuration(): number
        {
            var powerup = statics.Inventory.FindUpgrade(statics.PowerupUpgrade.POWERUP_JETPACK_KEY);
            var modifier = powerup.CurrentModifier;
            var jetpackDuration = JetpackPowerUp.BOOST_DURATION;
            if (modifier != 0)
            {
                jetpackDuration *= modifier;
            }

            return jetpackDuration;
        }

        private _gameState: State.GameState;
        private _isMouseDown: boolean;
        private _jetpackParticles: Phaser.Particles.Arcade.Emitter;
        private _jetpackAudio: Phaser.Sound = this.game.add.sound('jetpackSound', 1, true);

        public customFunction():void
        {
            this._gameState = this.game.CurrentState as State.GameState;

            this._jetpackAudio.play();

            if (this._gameState)
            {
                this.visible = false;

                this._jetpackParticles = this.game.add.emitter(0, 0, 150);
                this._jetpackParticles.makeParticles('particles-jetack');
                this._jetpackParticles.minParticleScale = 2;
                this._jetpackParticles.maxParticleScale = 6;
                this._jetpackParticles.gravity = this.game.physics.p2.gravity.y;
                this._gameState.Player.bringToTop();
                
                this._gameState.Player.Invulnerable = true;
                this._gameState.Player.ToggleRope(false);

                this._gameState.Player.inputEnabled = true;
                this._gameState.Player.input.enableDrag(true);

                this._gameState.Player.events.onInputDown.add(this.OnMouseDown, this);
                this._gameState.Player.events.onInputUp.add(this.OnMouseUp, this);
            } else
            {
                this.Destroy();
            }
        }

        public OnUpdate(): void
        {
            super.OnUpdate();

            if (!this._pickedUp || !this.alive) { return; }

            this._gameState.Player.body.thrust(JetpackPowerUp.BOOST_STRENGTH);

            var px = this._gameState.Player.body.velocity.x * -1;
            var py = this._gameState.Player.body.velocity.y * -1;
            
            this._jetpackParticles.minParticleSpeed.set(px, py);
            this._jetpackParticles.maxParticleSpeed.set(px, py);
            this._jetpackParticles.emitX = this._gameState.Player.Torso.x;
            this._jetpackParticles.emitY = this._gameState.Player.Torso.y + JetpackPowerUp.EMIT_Y_OFFSET;
            this._jetpackParticles.alpha = 0.5;
            this._jetpackParticles.flow(350, 750, 2, JetpackPowerUp.BOOST_DURATION / 750 * 2);

            if (!this._isMouseDown) { return; }

            this._gameState.Player.body.moveLeft(this._gameState.Player.body.x - this.game.input.activePointer.x);
        }

        /**
         * Disable the jetpack power up logic and enables the players circle collision and rope.
         */
        protected OnPowerupEnded()
        {
            this._jetpackAudio.fadeOut(500);

            //Phaser has no way of stopping a particle system and contains a bug where after a flow duration will still emit a particle every half a second
            //So we move the emit position to somewhere out of screen to hide any extra bugged particles
            this._jetpackParticles.emitX = 99999;
            this._jetpackParticles.destroy();

            this.events.onInputDown.remove(this.OnMouseDown, this);
            this.events.onInputUp.remove(this.OnMouseUp, this);

            this._gameState.Player.ToggleRope(true);
            this.kill();

            this.game.time.events.add(JetpackPowerUp.COLLISION_ENABLE_DELAY, function ()
            {
                this._gameState.Player.Invulnerable = false;
                this.Destroy();
            }, this);
        }

        /**
         * Callback for when the mouse click / tap is started.
         */
        private OnMouseDown()
        {
            this._isMouseDown = true;
        }

        /**
         * Callback for when the mouse click / tap is released.
         */
        private OnMouseUp()
        {
            this._isMouseDown = false;
        }
    }
}