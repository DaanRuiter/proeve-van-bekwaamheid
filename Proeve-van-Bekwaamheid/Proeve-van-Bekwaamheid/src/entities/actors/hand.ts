﻿module ProeveVanBekwaamheid.Actors
{
    /**
     * Hand that instructs the player to tap on the keypoints.
     */
    export class Hand extends Object.Entity
    {
        private _moveSpeed: number = 0.5;
        private _direction: number = 1;
        private _maxX: number = 20;
        private _startX: number;

        constructor(game: Client.GameEngine, x: number, y: number, textureKey: string)
        {
            super(game, x, y, textureKey);
            
            this.scale.x = 0.5;
            this.scale.y = 0.5;
            this.rotation = 90;

            this._startX = x;
        }

        public OnStart(): void { }
        public IndexDependencies(graphicsRequests: EntityGraphicsLoadRequestCache): void { }

        public OnUpdate(): void
        {
            // Movement
            this.position.x += this._moveSpeed * this._direction;

            // Bopping
            if (this.position.x >= this._startX + this._maxX && this._direction == 1
                || this.position.x <= this._startX - this._maxX && this._direction == -1)
            {
                this._direction *= -1;
            }
        }
    }
}