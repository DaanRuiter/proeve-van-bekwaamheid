﻿module ProeveVanBekwaamheid.Actors
{
    /**
     * Entities that stun the player on collision.
     */
    export class Enemy extends Object.PhysicsEntity
    {
        /**
         * Sets the enemy movement to hover.
         * Disables moving behaviour.
         */
        public set isHovering(hover: boolean)
        {
            if (this._isMoving)
            {
                this._moveTween.stop();
            }

            this._isMoving = false;
            this._isHovering = true;
        }

        /**
         * Sets the enemy movement to moving.
         * Disables hover behaviour.
         */
        public set isMoving(moving: boolean)
        {
            if (this._isHovering)
            {
                this._moveTween.stop();
            }
            this._isHovering = false;
            this._isMoving = true;
        }

        /**
         * Set the scale of this enemy and its spine object.
         */
        public set scale(scale: Phaser.Point)
        {
            super.scale = scale;

            if (this._spineObject)
            {
                this._spineObject.scale = scale;
            }
        }

        /**
         * Gets the position of this enemy or its spine object.
         */
        public get position(): Phaser.Point
        {
            if (this._spineObject)
            {
                return this._spineObject.position;
            }

            return super.position;
        }

        /**
        * Sets the position of this enemy or its spine object.
        */
        public set position(point: Phaser.Point)
        {
            if (this._spineObject)
            {
                this._spineObject.position.x = point.x;
                this._spineObject.position.y = point.y;
                return;
            }

            super.position = new Phaser.Point(point.x, point.y);
        }

        private _spineKey: string;
        private _spineObject: PhaserSpine.Spine;
        private _isHovering: boolean;
        private _isMoving: boolean;
        private _isInitialized: boolean = false;
        private _startedMoving: boolean;

        // Hover properties
        private _maxHoverY: number;
        private _yDirection: number;

        // Movement properties
        private _moveTarget: Phaser.Point;
        private _movePosition: Phaser.Point;
        private _moveTween: Phaser.Tween;
        private _moveDelay: number;
        private _currentMoveDelay: number;
        private _isSlowing: boolean;

        private _deathSprite: Phaser.Sprite;
        private _deathMoveTween: Phaser.Tween;
        private _isDeath: boolean;

        constructor(game: Client.GameEngine, x: number, y: number, spineKey: string)
        {
            super(game, x, y);

            this.Initialize(game, x, y, spineKey);
        }

        public OnStart(): void
        {
            
        }

        public IndexDependencies(graphicsRequests: EntityGraphicsLoadRequestCache): void
        {
        }

        public Destroy(): void
        {
            if (this._isDeath)
                return;

            this._moveTween.stop(false);
            this.DisableCircleCollision(Physics.CollisionGroups.ENEMY);
            this._spineObject.destroy();
            this._isDeath = true;

            this._deathSprite = this.game.add.sprite(0, 0, 'enemy-death');
            this._deathSprite.scale.set(0.75, 0.75);
            this.addChild(this._deathSprite);
            this._deathMoveTween = this.game.add.tween(this._deathSprite);
            var yPos = this._deathSprite.y - 400;
            var xPos = this._deathSprite.x + 100;
            this._deathMoveTween.to({ y: yPos, x: xPos, alpha: 0 }, 2000, Phaser.Easing.Linear.None);
            this._deathMoveTween.start();
            this._deathMoveTween.onComplete.add(this.DestroySuper, this);
        }



        public OnUpdate(): void
        {
            super.OnUpdate();

            if (!this._isInitialized && !this._isDeath)
                return;

            if (this.y > 0 && !this._startedMoving)
            {
                if (this._isHovering)
                {
                    this.Hover();
                }
                else if (this._isMoving)
                {
                    this.Move();
                }
            }
            else if (this.y > 0)
            {
                this.x += this._movePosition.x;
                this.y += this._movePosition.y;
            }

            if (this._spineObject)
            {
                this._spineObject.x = this.x;
                this._spineObject.y = this.y;
            }
        }

        /**
         * A on collision handler.
         * Currently only works with Circle Collision!
         * @param object Object that this instance collides with.
         */
        public OnCollision(other: Object.PhysicsEntity, group: string): void
        {

        }

        /**
         * Initializes this instance.
         */
        private Initialize(game: Client.GameEngine, x: number, y: number, spineKey: string): void
        {
            this.game = game;
            this.name = "Enemy";
            this.SetSpineObject(game, x, y, spineKey);
            this.SetPhysics();

            // Setting properties
            this._maxHoverY = 5;
            this._moveDelay = 3;
            this._currentMoveDelay = 0;
            this._yDirection = 1;
            this._startedMoving = false;
            this._isDeath = false;

            var x = this.game.rnd.integerInRange(-100, 100);
            var y = this.game.rnd.integerInRange(-100, 100);
            this._moveTarget = new Phaser.Point(x, y);
            this._movePosition = new Phaser.Point(0, 0);

            this._moveTween = this.game.add.tween(this._movePosition);

            this._isInitialized = true;
        }

        private DestroySuper(): void
        {
            super.Destroy();
        }

        /**
         * Hover behaviour.
         */
        private Hover(): void
        {
            if (!this._isInitialized)
                return;

            this._startedMoving = true;
            var duration = 2000;
            var yMove = this._maxHoverY * this._yDirection * (duration/1000 * this.game.physics.p2.frameRate);

            this._moveTween.to({ y: yMove }, duration, 'Back.easeInOut', true, 0, -1, true);
        }

        /**
         * Moving behaviour.
         */
        private Move(): void
        {
            if (!this._isInitialized || this.game == null)
                return;

            this._movePosition.x = 0;
            this._movePosition.y = 0;

            this._startedMoving = true;

            var duration = 2000; // 2 secondes
            var maxDistance = 50 * (duration / 1000 * this.game.physics.p2.frameRate);

            var x = this.game.rnd.integerInRange(-maxDistance, maxDistance);
            if (this.x + x > this.game.width)
            {
                x -= 100;
            } else if (this.x + x < 0)
            {
                x += 100;
            }
            console.log(this.x + x);
            
            this._moveTarget = new Phaser.Point(x, 0);

            this._moveTween.to({ x: this._moveTarget.x/*, y: this._moveTarget.y*/ }, duration, 'Back.easeInOut', true);
            this._moveTween.onComplete.addOnce(this.Move, this);
        }
        
        /**
         * Hover movement as followup for moving movement.
         */
        private MoveHover(): void
        {
            if (!this._isInitialized)
                return;

            this._movePosition.x = 0;
            this._movePosition.y = 0;

            var duration = 2000;
            var yMove = this.y + this._maxHoverY * this._yDirection * (duration / 1000 * this.game.physics.p2.frameRate);

            this._moveTween.to({ y: yMove }, duration, 'Back.easeInOut', true);
            this._moveTween.onComplete.addOnce(this.Move, this);
        }

        /**
         * Initializes the spine object for this enemy.
         * @param game Internally used game reference.
         * @param x x position of the spine object.
         * @param y y position of the spine object.
         * @param spineKey key of the spine data.
         */
        private SetSpineObject(game: Client.GameEngine, x: number, y: number, spineKey: string): void
        {
            this._spineObject = Spine.SpineFactory.createSpineObject(game, x, y, spineKey);

            this._spineObject.setAnimationByName(0, "animation", true);

            this._spineObject.scale = this.scale;
        }

        /**
         * Enable the circle collision on this enenmy.
         */
        private SetPhysics(): void
        {
            this.EnableCircleCollision(60, Physics.CollisionGroups.ENEMY);
        }
    }
}