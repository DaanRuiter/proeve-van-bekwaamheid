﻿module ProeveVanBekwaamheid.PickUp {

    export class PlatformObject extends Object.PhysicsEntity
    {

        constructor(game: Client.GameEngine, x: number, y: number, key: string, frame: string)
        {
            super(game, x, y, key, frame);

            this.CreateObject();
        }

        protected CreateObject()
        {
            this.scale.setTo(0.5);
        }

        public IndexDependencies(graphicsRequests: EntityGraphicsLoadRequestCache): void {

        }

        public OnStart() {

        }

        public OnUpdate() {
            super.OnUpdate();
        }

        public OnCollision(other: Object.PhysicsEntity) {

        }

        public customFunction()
        {
            this.kill();
        }


    }
}