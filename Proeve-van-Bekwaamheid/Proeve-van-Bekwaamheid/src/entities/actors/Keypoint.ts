﻿module ProeveVanBekwaamheid.Actors
{
    export interface IOnKeypointClickedEventHandler
    {
        OnKeypointDown(keypoint: Keypoint, position: Phaser.Point);
        OnKeypointUp(keypoint: Keypoint, position: Phaser.Point);
    }

    /**
     * base class for all Keypoints.
     * Has his own physics body and onclick event.
     * Extends Object.PhysicsEntity.
     */
    export class Keypoint extends Object.PhysicsEntity
    {
       /**
        * Event for when this keypoint is clicked on.
        */
        public get OnClickedEvent(): Events.EventHandlerHolder<IOnKeypointClickedEventHandler>
        {
            return this._onClickedEventHandler;
        }

        private _onClickedEventHandler: Events.EventHandlerHolder<IOnKeypointClickedEventHandler>;

        private _collisionRadius: number;

        constructor(game: Client.GameEngine, x: number, y: number, key?: string | Phaser.RenderTexture | Phaser.BitmapData | PIXI.Texture, frame?: string | number)
        {
            super(game, x, y, key, frame);

            this.name = "Keypoint";

            this.SetOnClickHandlers();
            this.SetEventHandlers();
            this.CalculateRadius();

            this.pivot.x = this.width / 2;
            this.pivot.y = this.height / 2;
        }

        public OnStart():void { }
        public OnDestroy(): void { }
        public OnCollision(other: Object.PhysicsEntity, group: string) { }
        public IndexDependencies(requests: EntityGraphicsLoadRequestCache): void { }

        /**
         * Sets all physics information for this instance.
         */
        public SetOnClickHandlers(): void
        {
            // Enable P2 Physics and set the block not to move
            //this.EnableBody(Object.PhysicsMode.Kinematic);

            // Enable clicking on the block and trigger a function when it is clicked
            this.inputEnabled = true;
            this.events.onInputDown.add(this.OnFingerDown, this);
            this.events.onInputUp.add(this.OnFingerUp, this);
        }

        /**
         * Sets the collision radius of this keypoint to that of the sprites width / 2.
         */
        private CalculateRadius(): void
        {
            this._collisionRadius = this.width / 2;
        }

        /**
         * Instantiates the event hanlder holder.
         */
        private SetEventHandlers(): void
        {
            this._onClickedEventHandler = new Events.EventHandlerHolder<IOnKeypointClickedEventHandler>();
        }

        /**
         * Callback method for when the keypoint is clicked / tapped on.
         * @param sprite Sprite that was clicked on.
         * @param position Position where whas clicked.
         */
        private OnFingerDown(sprite: Phaser.Sprite, position: Phaser.Point): void
        {
            var eventHandlers = this._onClickedEventHandler.EventHandlers;
            for (var i = 0; i < eventHandlers.length; i++)
            {
                eventHandlers[i].OnKeypointDown(this, position);
            }
        }

        /**
         * Callback method for when a tap / click on this keypoint is released.
         * @param sprite Sprite that was clicked on.
         * @param position Position where whas clicked.
         */
        private OnFingerUp(sprite: Phaser.Sprite, position: Phaser.Point): void
        {
            var eventHandlers = this._onClickedEventHandler.EventHandlers;
            for (var i = 0; i < eventHandlers.length; i++)
            {
                eventHandlers[i].OnKeypointUp(this, position);
            }
        }
    }
}