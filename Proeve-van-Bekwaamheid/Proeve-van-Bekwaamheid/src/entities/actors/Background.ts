﻿module ProeveVanBekwaamheid.Actors
{
    /**
     * Purpose of being background, can loop and can change textures on the go.
     * Extends Object.Entity.
     */
    export class Background extends Object.Entity
    {
        /**
         * Basic height of all backgrounds.
         */
        public static get BACKGROUND_HEIGHT(): number
        {
            return 1920;
        }

        /**
         * Basic width of all backgrounds.
         */
        public static get BACKGROUND_WIDTH(): number
        {
            return 1080;
        }

        /**
         * If true loops background on bottom reached otherwise nothing.
         */
        public set IsLooping(value: boolean)
        {
            this._isLooping = value;
        }
        public get IsLooping(): boolean
        {
            return this._isLooping;
        }

        /**
         * If true randomises image on bottom reached otherwise nothing.
         */
        public set RandomizeImage(value: boolean)
        {
            this._randomiseImage = value;
        }
        public get RandomizeImage(): boolean
        {
            return this._randomiseImage;
        }

        private _isLooping: boolean;
        private _randomiseImage: boolean;
        private _textureKeys: string[];

        constructor(game: Client.GameEngine, x: number, y: number, key?: string | Phaser.RenderTexture | Phaser.BitmapData | PIXI.Texture, frame?: string | number)
        {
            super(game, x, y, key, frame);

            this._textureKeys = [];
            this._isLooping = false;
            this._randomiseImage = false;
        }

        /**
         * Add a texture key that has already been loaded inside Phaser.Game.
         * @param key key of texture that has already been loaded.
         */
        public AddTextureKey(key: string): void
        {
            this._textureKeys.push(key);
        }

        /**
         * Removes texture key out of texturekeys list.
         * @param key key of texture that has been added to this list.
         */
        public RemoveTextureKey(key: string): void
        {
            for (var i: number = 0; i > this._textureKeys.length; i++)
            {
                if (this._textureKeys[i] == key)
                {
                    this._textureKeys.splice(i, 1);
                    break;
                }
            }
        }

        /**
         * Randomises texture of this background.
         */
        public ChangeTexture(): void
        {
            if (this._randomiseImage && this._textureKeys.length > 0)
            {
                var texture = this._textureKeys[this.game.rnd.integerInRange(0, this._textureKeys.length-1)];
                this.loadTexture(texture);
            }
        }

        public OnStart(): void { }
        public IndexDependencies(graphicsRequests: EntityGraphicsLoadRequestCache): void { }

        public OnUpdate(): void
        {
            if (this.position.y >= this.game.height)
            {
                this.OnBottomReached(this.position.y - this.height);
            }
        }

        /**
         * Callback method for when the background reaches the bottom of the screen.
         * @param yOffset Offset to set the backgrounds y position to.
         */
        private OnBottomReached(yOffset: number): void
        {
            if (this._isLooping)
            {
                this.position.y = -1920 + yOffset;
            }

            this.ChangeTexture();
        }
    }
}