﻿module ProeveVanBekwaamheid.Actors
{
    /**
     * Laserbeam that kills the player on collision.
     */
    export class LaserBeam extends Object.PhysicsEntity
    {
        /**
        * The start position of the laserbeam.
        */
        public set StartPosition(value: Phaser.Point)
        {
            this._startPosition = value;
        }
        public get StartPosition(): Phaser.Point
        {
            return this._startPosition;
        }

        /**
        * The end position of the laserbeam.
        */
        public set EndPosition(value: Phaser.Point)
        {
            this._endPosition = value;
        }
        public get EndPosition(): Phaser.Point
        {
            return this._endPosition;
        }

        /**
        * Time in miliseconds that the laser is active and will kill the player on collision.
        */
        private static get LASERBEAM_ACTIVE_TIME(): number
        {
            return 3000;
        }

        /**
        * Time in miliseconds that the laster is inactive and will not kill the player on collision.
        */
        private static get LASERBEAM_DISABLED_TIME(): number
        {
            return 5000;
        }

        private _alphaTween: Phaser.Tween;
        private _activeTimer: Phaser.Timer;
        private _disabledTimer: Phaser.Timer;
        private _firstTimeInScreen: boolean;
        private _startPosition: Phaser.Point;
        private _endPosition: Phaser.Point;
        private _beamDirectionX: number;
        private _addedBeamScaleX: number;
        private _maxBeamScaleX: number;
        private _minBeamScaleX: number;
        private _coils: Object.Entity[];

        constructor(game: Client.GameEngine, x: number, y: number, texture: string)
        {
            super(game, x, y, texture);

            this._activeTimer = game.time.create(false);
            this._disabledTimer = game.time.create(false);
            this._startPosition = new Phaser.Point(0, 0);
            this._endPosition = new Phaser.Point(0, 0);
            this._firstTimeInScreen = false;
            this._addedBeamScaleX = 0;
            this._beamDirectionX = 1;
            this._maxBeamScaleX = 1.5;
            this._minBeamScaleX = -1.5;
            this.alpha = 0;
            this.SetPhysics();
            this.name = "LaserBeam";
        }
        
        public Destroy(): void
        {
            if (this._alphaTween)
                this._alphaTween.stop(false);

            if (this._disabledTimer)
            {
                this._disabledTimer.stop();
                this._disabledTimer.destroy();
            }

            if (this._activeTimer)
            {
                this._activeTimer.stop();
                this._activeTimer.destroy();
            }

            this._coils.forEach(function (coil) { coil.Destroy(); });

            super.Destroy();
        }

        public OnStart(): void { }
        public IndexDependencies(graphicsRequests: EntityGraphicsLoadRequestCache): void { }
        public OnCollision(other: Object.PhysicsEntity, group: string): void { }

        /**
         * Scales and angles the laserbeam according to the start and end position.
         */
        public CalculateScaleAndAngle(): void
        {
            var angle: number = Phaser.Math.radToDeg(Phaser.Math.angleBetweenPoints(this._startPosition, this._endPosition)) + 90;
            
            this._coils = [new EmptyObject(this.game, this._startPosition.x, this._startPosition.y, 'laser-coil'),
                           new EmptyObject(this.game, this._endPosition.x, this._endPosition.y, 'laser-coil')];

            for (let i = 0; i < this._coils.length; i++)
            {
                (this.game.CurrentState as State.GameState).ObjectScroller.ScrollingObjects.push(this._coils[i]);
                this.game.CurrentState.EntityGroup.add(this._coils[i]);
                this.game.add.existing(this._coils[i]);
            }

            this.bringToTop();

            let leftCoil = this._coils[0];
            leftCoil.anchor.set(0.5);
            leftCoil.angle = angle;
            leftCoil.x += leftCoil.width / 2;
            this._startPosition.x += leftCoil.width;

            let rightCoil = this._coils[1];
            rightCoil.anchor.set(0.5);
            rightCoil.angle = angle + 180;
            rightCoil.x -= rightCoil.width / 2;
            this._endPosition.x -= rightCoil.width;

            var beamX = (this._endPosition.x + this._startPosition.x) / 2;
            var beamY = (this._endPosition.y + this._startPosition.y) / 2;
            var distance = Phaser.Point.distance(this._startPosition, this._endPosition);
            this.body.angle = angle;
            this.body.x = beamX;
            this.body.y = beamY;
            this.scale.set(3, distance / 20);
            this.CalculateRectangle();

        }

        public OnUpdate(): void
        {
            if (!this._firstTimeInScreen && this.y > 0)
            {
                this.ActivateBeam();
                this._firstTimeInScreen = true;
            }
            else
            {
                this.DrawBeam();
            }
        }

        /**
         * Scales the laserbeam to make it look animated.
         */
        private DrawBeam(): void
        {
            this._addedBeamScaleX += 0.025 * this._beamDirectionX;
            if (this._addedBeamScaleX >= this._maxBeamScaleX && this._beamDirectionX == 1 ||
                this._addedBeamScaleX <= this._minBeamScaleX && this._beamDirectionX == -1)
            {
                this._beamDirectionX *= -1;
            }
            this.scale.set(3 + this._addedBeamScaleX, this.scale.y);
        }

        /**
         * Disable the collision and stop the timers.
         */
        private DisableBeam(): void
        {
            this.DisableCollision();

            this._disabledTimer.stop();
            this._disabledTimer.removeAll();
            this._disabledTimer.add(LaserBeam.LASERBEAM_DISABLED_TIME, this.ActivateBeam, this);
            this._disabledTimer.start();
        }

        /**
         * Start the tween for the alpha of the laserbeam sprite.
         */
        private ActivateBeam(): void
        {
            this._alphaTween = this.game.add.tween(this);
            this._alphaTween.to({ alpha: 1 }, 2500, Phaser.Easing.Linear.None, true);
            this._alphaTween.onComplete.addOnce(this.BeamActivated, this);
        }

        /**
         * Stop the tween for the alpha of the laserbeam sprite.
         */
        private DeactivateBeam(): void
        {
            this._alphaTween = this.game.add.tween(this);
            this._alphaTween.to({ alpha: 0 }, 1000, Phaser.Easing.Linear.None, true);
            this._alphaTween.onComplete.addOnce(this.DisableBeam, this);
        }

        /**
         * Activate the collision and start the timers.
         */
        private BeamActivated(): void
        {
            this.EnableCollision();

            this._activeTimer.stop();
            this._activeTimer.removeAll();
            this._activeTimer.add(LaserBeam.LASERBEAM_ACTIVE_TIME, this.DeactivateBeam, this);
            this._activeTimer.start();
        }

        private SetPhysics(): void
        {
            this.EnableBody(Object.PhysicsMode.Kinematic);
        }

        /**
         * Calculate the rectangular collider according to the rotation and size of the laserbeam sprite.
         */
        private CalculateRectangle()
        {
            this.body.setRectangle(this.width, this.height, 0, 0, this.rotation);
            this.body.setCollisionGroup(Physics.Collision.GetCollisionGroup(Physics.CollisionGroups.ENEMY));
            //this.body.collides([Physics.Collision.GetCollisionGroup(Physics.CollisionGroups.PLAYER)]);
            this.body.onBeginContact.addOnce(this.OnBodyCollisonHandler, this);
        }

        private EnableCollision(): void
        {
            this.body.collides(Physics.Collision.GetCollisionGroup(Physics.CollisionGroups.PLAYER));
        }

        private DisableCollision(): void
        {
            this.body.removeCollisionGroup(Physics.Collision.GetCollisionGroup(Physics.CollisionGroups.PLAYER), true);
        }

        private OnBodyCollisonHandler(body1: Phaser.Physics.P2.Body, body2: Phaser.Physics.P2.Body, shape01: any, shape02: any, equation: any): void
        {
            (this.game.CurrentState as State.GameState).OnDeath();
        }
    }
}
