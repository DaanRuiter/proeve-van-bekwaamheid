﻿
module ProeveVanBekwaamheid.Spine {

    export class SpineFactory {
        /**
         * Creates a spine object on the Client.GameEngine.
         * Finds object by given key which should already have been loaded.
         * @param game Game client.
         * @param x horizontal position inside game client.
         * @param y vertical position inside game client.
         * @param key the given key of the spine animation.
         */
        public static createSpineObject(game: Client.GameEngine, x: number, y: number, key: string): PhaserSpine.Spine
        {
            var spineObject = game.add.spine(
                x,        //X positon
                y,        //Y position
                key     //the key of the object in cache
            );

            return spineObject;
        }

        /**
         * Loading spine assets by key and path of json file.
         * NOTE: atlas should be in the same folder as json file!
         * @param game Game client.
         * @param key The key which will be used on creating object.
         * @param jsonPath Path of json file.
         */
        public static loadSpineAsset(game: Client.GameEngine, key: string, jsonPath: string) : void
        {
            // Loading spine animation.
            game.load.spine(
                key,                                    //The key used for Phaser's cache
                jsonPath                                //The location of the spine's json file
            );
        }
    }

}