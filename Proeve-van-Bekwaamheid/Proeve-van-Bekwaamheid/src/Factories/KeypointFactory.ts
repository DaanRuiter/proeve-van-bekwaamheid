﻿module ProeveVanBekwaamheid.Factories
{
    /**
     * Factory class for keypoints.
     */
    export class KeypointFactory
    {
        private static get KEYPOINT_ART(): string[]
        {
            return ["Keypoint"];
        }

        private static get MAX_SCALE(): number
        {
            return 0.75;
        }

        private static get MIN_SCALE(): number
        {
            return 0.75;
        }

        /**
         * Creates a static keypoint on given position.
         * @param game client.
         * @param player player to interact with.
         * @param position position of keypoint.
         */
        public static CreateStaticKeypoint(game: Client.GameEngine, player: Actors.Player, x: number, y: number): Actors.Keypoint
        {
            var keypoint: Actors.Keypoint;
            var randomArt: string;
            randomArt = KeypointFactory.KEYPOINT_ART[game.rnd.integerInRange(0, KeypointFactory.KEYPOINT_ART.length - 1)];

            keypoint = game.CurrentState.InstantiateEntity(Actors.Keypoint, x, y, randomArt);
            keypoint.position = new Phaser.Point(x, y);

            var random = game.rnd.realInRange(KeypointFactory.MIN_SCALE, KeypointFactory.MAX_SCALE);
            keypoint.scale.set(random);

            player.RegisterKeypoint(keypoint);

            (game.CurrentState as State.GameState).KeyPoints.push(keypoint);

            // The keypoints should always scroll with the game.
            (game.CurrentState as State.GameState).ObjectScroller.ScrollingObjects.push(keypoint);

            return keypoint;
        }
    }
}