﻿namespace ProeveVanBekwaamheid {

    export class Boot extends Phaser.State {
        public game: GameEngine;

        create() {
            this.stage.setBackgroundColor(0xDDDDDD);

            this.input.maxPointers = 1;
            this.stage.disableVisibilityChange = true;

            if (this.game.device.desktop) {
                this.scale.pageAlignHorizontally = true;
                //this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
            } else {
                // mobile
                //this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
                this.scale.minWidth = 480;
                this.scale.minHeight = 260;
                this.scale.maxWidth = 1024;
                this.scale.maxHeight = 768;
                this.scale.forceLandscape = true;
                this.scale.pageAlignHorizontally = true;
                this.scale.refresh();
            }

            this.game.physics.startSystem(Phaser.Physics.P2JS);
            this.game.physics.p2.gravity.y = 1200;

            this.createRope(25, this.scale.width / 2, 100);  // (length, xAnchor, yAnchor)

            // Creating spine buddy
            var spineboy = Spine.SpineFactory.createSpineObject(this.game, 400, 300, 'spineboy');

            // Setting spine animation
            spineboy.setAnimationByName(0, "idle", true);
        }



        

        createRope(length, xAnchor, yAnchor)
        {
            var lastRect;
            var height = 20;        //  Height for the physics body - your image height is 8px
            var width = 16;         //  This is the width for the physics body. If too small the rectangles will get scrambled together.
            var maxForce = 20000;   //  The force that holds the rectangles together.
            

            for (var i = 0; i <= length; i++) {
                var newRect;
                var x = xAnchor;                    //  All rects are on the same x position
                var y = yAnchor + (i * height);     //  Every new rect is positioned below the last

                if (i % 2 === 0) {
                    //  Add sprite (and switch frame every 2nd time)
                    newRect = this.game.add.sprite(x, y, 'chain', 1);
                }
                else {
                    newRect = this.game.add.sprite(x, y, 'chain', 0);
                    lastRect.bringToTop();
                }

                //newRect.events.onInputDown.add(onDown, this)

                //  Enable physicsbody
                this.game.physics.p2.enable(newRect, false);

                //  Set custom rectangle
                newRect.body.setRectangle(width, height);

                if (i === 0) {
                    newRect.body.static = true;
                }
                else {
                    //  Anchor the first one created
                    newRect.body.velocity.x = 400;      //  Give it a push :) just for fun
                    newRect.body.mass = length / i;     //  Reduce mass for evey rope element
                }

                //  After the first rectangle is created we can add the constraint
                if (lastRect) {
                    this.game.physics.p2.createRevoluteConstraint(newRect, [0, -10], lastRect, [0, 10], maxForce);
                }

                lastRect = newRect;
            }
        }
    }
}