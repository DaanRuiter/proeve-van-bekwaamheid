﻿namespace ProeveVanBekwaamheid {

    export class Preloader extends Phaser.State {
        loaderText: Phaser.Text;
        public game: GameEngine;

        preload() {
            // Enabling spine plugin.
            this.game.add.plugin(new PhaserSpine.SpinePlugin(this.game, this.game.plugins));

            this.loaderText = this.game.add.text(this.world.centerX, 200, "Loading...",
                { font: "18px Arial", fill: "#A9A91111", align: "center" });
            this.loaderText.anchor.setTo(0.5);

            this.game.load.spritesheet('chain', './assets/sprites/chain.png', 16, 26); 

            this.loadSpineAssets();
        }

        loadSpineAssets() {
            //Loading spine assets.
            Spine.SpineFactory.loadSpineAsset(this.game, 'spineboy', 'assets/spine/spineboy.json');
        }

        create() {
            var tween = this.add.tween(this.loaderText).to({ alpha: 0 }, 2000,
                Phaser.Easing.Linear.None, true);
            tween.onComplete.add(this.startMainMenu, this);
        }

        startMainMenu() {
            this.game.state.start('Boot', true, false);
        }

    }

}