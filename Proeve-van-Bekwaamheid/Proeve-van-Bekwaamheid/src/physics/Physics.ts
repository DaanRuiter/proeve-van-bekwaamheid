﻿module ProeveVanBekwaamheid.Physics
{
    export class CollisionGroups
    {
        public static KEY_POINTS:string = "KeyPoints";
        public static ROPE:string = "Rope";
        public static PLAYER:string = "Player";
        public static ENEMY:string = "Enemy";
        public static PICKUP:string = "Pickup";
        public static PLATFORM:string = "Platform"
    }

    export class Collision
    {
        private static game: Client.GameEngine;
        private static initialized: boolean;
        private static collisionGroups: Map<string, Phaser.Physics.P2.CollisionGroup>;
        private static radiusCollisionObjects: Map<string, Object.PhysicsEntity[]>;
        private static radiusCollisionGroups: Map<string, string[]>;

        public static Update(): void
        {
            Collision.radiusCollisionGroups.forEach(Collision.CheckCollision);
        }

        /**
         * Checks circle collision in a for each kind of way.
         * @param value values of map.
         * @param key key of map.
         * @param map map.
         */
        private static CheckCollision(value: string[], key: string, map: any): void
        {
            if (Collision.radiusCollisionObjects.has(key))
            {
                for (var i: number = value.length - 1; i >= 0; i--)
                {
                    // Getting current group key.
                    var groupKey = value[i];
                    for (var object of Collision.radiusCollisionObjects.get(key)) // Getting objects from the key given in the forEach.
                    {
                        if (Collision.radiusCollisionObjects.has(groupKey))
                        {
                            for (var objectToCollideWith of Collision.radiusCollisionObjects.get(groupKey)) // Getting objects from current group key inside iteration.
                            {
                                var dx = object.x - objectToCollideWith.x;
                                var dy = object.y - objectToCollideWith.y;
                                var distance = Math.sqrt(dx * dx + dy * dy);

                                if (distance < object.Radius + objectToCollideWith.Radius)
                                {
                                    object.OnCollision(objectToCollideWith, groupKey);
                                }
                            }
                        }
                    }
                }
            }
        }

        /**
         * Removes all collision groups and objects.
         */
        public static Reset(): void
        {
            Collision.radiusCollisionGroups.clear();
            Collision.radiusCollisionObjects.clear();
        }

        /**
         * Registers collision between group01 and group02.
         * Events will only be send to group01 objects.
         * @param group01 group one
         * @param group02 group two
         */
        public static AddRadiusCollisionOnGroups(group01: string, group02: string): void
        {
            if (!Collision.radiusCollisionGroups.has(group01))
            {
                Collision.radiusCollisionGroups.set(group01, []);
            }

            Collision.radiusCollisionGroups.get(group01).push(group02);
        }

        /**
        * Registers collision between group01 and group02.
        * Events will only be send to group01 objects.
        * @param group01 group one
        * @param group02 group two
        */
        public static RemoveRadiusCollisionOnGroups(group01: string, group02: string): void
        {
            if (Collision.radiusCollisionGroups.has(group01))
            {
                var index = Collision.radiusCollisionGroups.get(group01).indexOf(group02);
                Collision.radiusCollisionGroups.get(group01).splice(index, 1);

                // Destroy the whole entry if list is 0
                if (Collision.radiusCollisionGroups.get(group01).length == 0)
                {
                    Collision.radiusCollisionGroups.delete(group01);
                }
            }
        }

        /**
         * Adds a object to radius collision group.
         * @param object object to set radius collision.
         * @param objectRadius radius of object.
         * @param key key of group.
         */
        public static AddRadiusCollisionObject(object: Object.PhysicsEntity, key: string): void
        {
            if (!Collision.radiusCollisionObjects.has(key))
            {
                Collision.radiusCollisionObjects.set(key, []);
            }

            Collision.radiusCollisionObjects.get(key).push(object);
        } 

        /**
         * Adds a object to radius collision group.
         * @param object object to set radius collision.
         * @param objectRadius radius of object.
         * @param key key of group.
         */
        public static RemoveRadiusCollisionObject(object: Object.PhysicsEntity, key: string): void
        {
            if (Collision.radiusCollisionObjects.has(key))
            {
                var index = Collision.radiusCollisionObjects.get(key).indexOf(object);
                Collision.radiusCollisionObjects.get(key).splice(index, 1);
                 // Destroy the whole entry if list is 0
                if (Collision.radiusCollisionObjects.get(key).length == 0)
                {
                    Collision.radiusCollisionObjects.delete(key);
                }
                return;
            }
        }


        /**
         * Creates a new collision group with the given key.
         * @param key Key of collision group used for getting the group.
         */
        public static CreateCollisionGroup(key: string) : void
        {
            if (!Collision.initialized)
            {
                console.log("Physics class not initialized can not create new collision group!");
                return;
            }

            var newCollisionGroup: Phaser.Physics.P2.CollisionGroup;
            newCollisionGroup = Collision.game.physics.p2.createCollisionGroup();

            Collision.collisionGroups.set(key, newCollisionGroup);
        }

        /**
         * Gets a P2 physics collision group created with the given key.
         * @param key Key of collision group.
         */
        public static GetCollisionGroup(key: string) : Phaser.Physics.P2.CollisionGroup
        {
            if (Collision.collisionGroups.has(key))
            {
                return Collision.collisionGroups.get(key);
            }

            console.log("Collision group not found, make sure that the key is correct!");
            return null;
        }

        /**
         * Initializes Collision class.
         * @param game The current game client.
         */
        public static Initialize(game: Client.GameEngine) : void
        {
            Collision.game = game;

            Collision.collisionGroups = new Map<string, Phaser.Physics.P2.CollisionGroup>();
            Collision.radiusCollisionObjects = new Map<string, Object.PhysicsEntity[]>();
            Collision.radiusCollisionGroups = new Map<string, string[]>();

            Collision.initialized = true;

            Collision.CreateCollisionGroup(CollisionGroups.PLAYER);
            Collision.CreateCollisionGroup(CollisionGroups.PLATFORM);
            Collision.CreateCollisionGroup(CollisionGroups.ENEMY);        
        }
    }
}