﻿module ProeveVanBekwaamheid.Generators
{
    /**
     * Creates entities from json files.
     * Is used to generate the levels from Tiled presets.
     */
    export class JsonObjectGenerator
    {
        /**
         * Width of one tile.
         * 60 * 18 = 1080.
         */
        private static get TILE_WIDTH(): number
        {
            return 60;
        }

        /**
         * Height of one tile.
         * 60 * 32 = 1920.
         */
        private static get TILE_HEIGHT(): number
        {
            return 60;
        }

        private static get PRESETS(): string[]
        {
            return ["Preset 1","Preset 2","Preset 3"];
        }

        private _map: Phaser.Tilemap;
        private _game: Client.GameEngine;
        private _player: Actors.Player;
        private _currentHeight: number;
        private _pickupChance: number;

        constructor(game: Client.GameEngine, player: Actors.Player)
        {
            this._game = game;
            this._player = player;
            this._map = this._game.add.tilemap('presets');
            this._currentHeight = this._game.height / 2;
            this._pickupChance = 1;

            this.CreateRandomPreset();
        }

        /**
         * Creates random presets on the go.
         * @param playerHeight height of player inside game.
         */
        public Update(playerHeight: number): void
        {
            if (playerHeight - this._currentHeight >= 1920)
            {
                this._currentHeight = playerHeight;
                this.CreateRandomPreset();
            }
        }

        /**
         * Creates one random preset and places it in the game.
         */
        public CreateRandomPreset(): void
        {
            var random = this._game.rnd.integerInRange(0, JsonObjectGenerator.PRESETS.length - 1);
            var randomPreset = JsonObjectGenerator.PRESETS[random];
            this.CreatePresetFromList(this._map.objects[randomPreset], true);
        }

        /**
         * Creates coins from object list.
         * @param objects objects from list.
         */
        public CreatePresetFromList(objects: any[], inverseY?: boolean): Object.Entity[]
        {
            var generatedObjects = new Array<Object.Entity>();
            var game = this._game;
            var player = this._player;
            var pickupChance = this._pickupChance;

            objects.forEach(function (value)
            {
                var x = value.x;
                var y = value.y;
                if (inverseY)
                    y = -value.y;

                if (value.type === "Coin")
                {
                    var random = game.rnd.integerInRange(0, 10000); // 100% = 100.00
                    random *= 0.01;
                    
                    if (random <= pickupChance)
                    {
                        /*
                        var glow = (game.CurrentState as State.GameState).InstantiateEntity(Actors.EmptyObject, x, y, "Glow");
                        (game.CurrentState as State.GameState).ObjectScroller.ScrollingObjects.push(glow);*/
                        
                        var pickup = Factories.PickupFactory.CreateRandomPickup(game, x, y);
                        //pickup.GlowEffect = glow;
                        generatedObjects.push(pickup);
                        /*
                        glow.AddTag('fx');
                        glow.pivot.set(glow.width / 2, glow.height / 2);
                        glow.position.set(glow.x + pickup.width / 2, glow.y + pickup.height / 2);
                        glow.scale.set(0.55);
                        glow.alpha = 0.75;

                        var glowTween = game.add.tween(glow);
                        glowTween.to({ alpha: 0.35 }, 1000, Phaser.Easing.Linear.None, true, 0, -1, true);*/
                    }
                    else
                    {
                        var coin = Factories.PickupFactory.CreateCoin(game, x, y);
                        generatedObjects.push(coin);
                    }
                }
                else if (value.type === "HoverEnemy")
                {
                    var staticEnemy = Factories.EnemyFactory.CreateStaticEnemy(game, x, y);
                    generatedObjects.push(staticEnemy);
                }
                else if (value.type === "Keypoint")
                {
                    var keypoint = Factories.KeypointFactory.CreateStaticKeypoint(game, player, x, y);
                }
                else if (value.type === "KeypointGlow")
                {
                    var glow = (game.CurrentState as State.GameState).InstantiateEntity(Actors.EmptyObject, x, y, "Glow");
                    var keypoint = Factories.KeypointFactory.CreateStaticKeypoint(game, player, x, y);
                    glow.pivot.set(glow.width / 2, glow.height/2);
                    glow.x = x;
                    glow.y = y;
                    glow.AddTag('fx');
                    (game.CurrentState as State.GameState).ObjectScroller.ScrollingObjects.push(glow);
                    generatedObjects.push(keypoint);
                }
                else if (value.type === "Text")
                {
                    var textField = (game.CurrentState as State.GameState).InstantiateEntity(Actors.WorldText, x - 150, y, "");
                    (game.CurrentState as State.GameState).ObjectScroller.ScrollingObjects.push(textField);
                    var text: string = "";
                    if (value.Input)
                    {
                        text = value.Input;
                    }
                    textField.SetProperties(text, '#fff', 32, 3);
                    (game.CurrentState as State.GameState).WorldTexts.push(textField);

                    generatedObjects.push(textField);
                }
                else if (value.type === "Hand")
                {
                    var hand = (game.CurrentState as State.GameState).InstantiateEntity(Actors.Hand, x, y, "Hand");
                    (game.CurrentState as State.GameState).ObjectScroller.ScrollingObjects.push(hand);
                    (game.CurrentState as State.GameState).Hands.push(hand);

                    generatedObjects.push(hand);
                }
            });
            
            (game.CurrentState as State.GameState).KeyPoints.sort(function (a, b) { return (a.y < b.y) ? 1 : ((b.y < a.y) ? -1 : 0); }); 

            return generatedObjects;
        }

        //find objects in a Tiled layer that containt a property called "type" equal to a certain value
        private FindObjectsByType(type: string, map: Phaser.Tilemap, layer: string): any[]
        {
            var result = new Array();
            map.objects[layer].forEach(function (element)
            {
                if (element.type === type)
                {
                    result.push(element);
                }
            });
            return result;
        }
    }
}