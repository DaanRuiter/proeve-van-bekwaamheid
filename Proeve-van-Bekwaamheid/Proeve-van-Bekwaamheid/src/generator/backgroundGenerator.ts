﻿module ProeveVanBekwaamheid.Generators
{
    /**
     * Creates backgrounds that randomize their textures.
     */
    export class BackgroundGenerator
    {
        private _game: Client.GameEngine;

        constructor(game: Client.GameEngine, createBackgrounds?: boolean)
        {
            this._game = game;

            if (createBackgrounds)
                this.CreateBackgrounds();
        }
        
        /**
         * Creates the backgrounds for this game.
         */
        public CreateBackgrounds(): void
        {
            var currentState = this._game.CurrentState as State.GameState;
            currentState.Backgrounds.push(currentState.InstantiateEntity(Actors.Background, 0, 0));
            currentState.Backgrounds.push(currentState.InstantiateEntity(Actors.Background, 0, -Actors.Background.BACKGROUND_HEIGHT));
            currentState.Backgrounds.push(currentState.InstantiateEntity(Actors.Background, 0, -Actors.Background.BACKGROUND_HEIGHT * 2));

            for (let i = 0; i < currentState.Backgrounds.length; i++)
            {
                currentState.Backgrounds[i].AddTextureKey("SpacePart-01");
                currentState.Backgrounds[i].AddTextureKey("SpacePart-02");
                currentState.Backgrounds[i].AddTextureKey("SpacePart-03");

                currentState.Backgrounds[i].RandomizeImage = true;
                currentState.Backgrounds[i].IsLooping = true;
                currentState.Backgrounds[i].ChangeTexture();

                currentState.BackgroundScroller.ScrollingObjects.push(currentState.Backgrounds[i]);
            }
        }
    }
}